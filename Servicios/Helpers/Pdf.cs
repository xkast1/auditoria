﻿namespace Browne.Intranet.Servicios.Helpers
{
    public class Pdf
    {
        public static byte[] Firmar(byte[] pdf, byte[] certificate)
        {
            try
            {
                var certificado = new System.Security.Cryptography.X509Certificates.X509Certificate2(certificate);

                var cp = new Org.BouncyCastle.X509.X509CertificateParser();
                var chain = new Org.BouncyCastle.X509.X509Certificate[] {
                cp.ReadCertificate(certificado.RawData)};

                var externalSignature = new iTextSharp.text.pdf.security.X509Certificate2Signature(certificado, "SHA-1");

                return ProcesoFirmarDocumentos(externalSignature, chain, pdf, true);
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                throw ex;
            }
        }

        private static byte[] ProcesoFirmarDocumentos(iTextSharp.text.pdf.security.IExternalSignature externalSignature, Org.BouncyCastle.X509.X509Certificate[] chain, byte[] datosArchivo, bool AgregarFirmaDocumento)
        {
            //iTextSharp.text.pdf.PdfReader pdfLector = null;
            //DotNetIO.FileStream PdfFirmado = null;
            //System.Collections.Generic.List<System.IO.FileInfo> listaPDF = null;
            var archivoDestino = string.Empty;
            //iTextSharp.text.pdf.PdfStamper pdfStamper = null;
            //iTextSharp.text.pdf.PdfSignatureAppearance signatureAppearance = null;
            //System.IO.MemoryStream PdfFirmado = null;

            try
            {
                //foreach (DotNetIO.FileInfo archivo in listaPDF)
                //{
                var pdfLector = new iTextSharp.text.pdf.PdfReader(datosArchivo);
                var pdfFirmado = new System.IO.MemoryStream();
                var pdfStamper = iTextSharp.text.pdf.PdfStamper.CreateSignature(pdfLector, pdfFirmado, '\0');
                var signatureAppearance = pdfStamper.SignatureAppearance;

                if (AgregarFirmaDocumento)
                {
                    //Por si queremos agregar una imagen (Timbre, etc.)
                    //signatureAppearance.SignatureGraphic = Image.GetInstance(pathToSignatureImage);

                    signatureAppearance.SetVisibleSignature(new iTextSharp.text.Rectangle(100, 100, 250, 150), pdfLector.NumberOfPages, "Signature");
                    signatureAppearance.SignatureRenderingMode = iTextSharp.text.pdf.PdfSignatureAppearance.RenderingMode.NAME_AND_DESCRIPTION;
                }

                iTextSharp.text.pdf.security.MakeSignature.SignDetached(signatureAppearance, externalSignature, chain, null, null, null, 0, iTextSharp.text.pdf.security.CryptoStandard.CMS);

                pdfStamper.Writer.CloseStream = false;
                pdfStamper.Close();

                //}

                return pdfFirmado.ToArray();
            }
            catch (System.Exception ex)
            {
                throw new System.IO.DirectoryNotFoundException(ex.Message);
            }
            //return PdfFirmado.ToArray();
        }
    }
}
