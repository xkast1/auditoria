﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Auth;
using Browne.Core.Servicio;

namespace Browne.Intranet.Servicios.Sesion
{
    public class ServicioLog : Core.Servicio.Sesion.IServicioLog
    {
        private readonly AccesoDatos.Contextos.Auth.LogAccesoContext contexto;

        public ServicioLog(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Auth.LogAccesoContext(connectionString);
        }
        public IEnumerable<LogsAccesos> Buscar(Expression<Func<LogsAccesos, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(LogsAccesos item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LogsAccesos> Obtener(DateTime? desde = null, DateTime? hasta = null, int? id = null)
        {
            try
            {
                return contexto.Obtener(desde, hasta, id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<LogsAccesos> ObtenerTodo()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LogsAccesos> ObtenerTodo(int? usuarioID = null, DateTime ? desde = null, DateTime ? hasta = null)
        {
            try
            {
                return contexto.ObtenerTodos(usuarioID, desde, hasta);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        LogsAccesos IServicioCrud<LogsAccesos>.Obtener(int id)
        {
            throw new NotImplementedException();
        }
    }
}
