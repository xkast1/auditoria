﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;
using Browne.Core.Servicio.Contabilidad;

namespace Browne.Intranet.Servicios.Contabilidad
{
    public class ServicioCentroCosto : Core.Servicio.Contabilidad.IServicioCentroCosto
    {
        private readonly AccesoDatos.Contextos.Contabilidad.Mantenedores.CentrosCostosContext contexto;

        public ServicioCentroCosto(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Contabilidad.Mantenedores.CentrosCostosContext(connectionString);
        }
        public IEnumerable<CentroCosto> Buscar(Expression<Func<CentroCosto, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(CentroCosto item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(IEnumerable<CentroCosto> centroCosto)
        {
            try
            {
                contexto.Guardar(centroCosto);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public CentroCosto Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<CentroCosto> ObtenerTodo()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CentroCosto> ObtenerTodo(int? ID = null, bool? bloqueado = null, int? padreID = null, int? nivel = null)
        {
            try
            {
                return contexto.ObtenerTodos(ID, bloqueado, padreID, nivel);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
