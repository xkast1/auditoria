﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace Browne.Intranet.Servicios.Contabilidad
{
    public class ServicioTipoOrdenCompra : Core.Servicio.Contabilidad.IServicioTipoOrdenCompra
    {
        private readonly AccesoDatos.Contextos.Contabilidad.Mantenedores.TiposOrdenesComprasContext contexto;

        public ServicioTipoOrdenCompra(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Contabilidad.Mantenedores.TiposOrdenesComprasContext(connectionString);
        }
        public IEnumerable<TipoOrdenCompra> Buscar(Expression<Func<TipoOrdenCompra, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(TipoOrdenCompra item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(TipoOrdenCompra tipoOrdenCompra)
        {
            try
            {
                contexto.Guardar(tipoOrdenCompra);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public TipoOrdenCompra Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<TipoOrdenCompra> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
