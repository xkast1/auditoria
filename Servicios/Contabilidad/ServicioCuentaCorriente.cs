﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace Browne.Intranet.Servicios.Contabilidad
{
    public class ServicioCuentaCorriente : Core.Servicio.Contabilidad.IServicioCuentaCorriente
    {
        private readonly AccesoDatos.Contextos.Contabilidad.Mantenedores.CuentasCorrientesContext contexto;

        public ServicioCuentaCorriente(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Contabilidad.Mantenedores.CuentasCorrientesContext(connectionString);
        }

        public IEnumerable<CuentaCorriente> Buscar(Expression<Func<CuentaCorriente, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(CuentaCorriente item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(CuentaCorriente cuenta)
        {
            try
            {
                contexto.Guardar(cuenta);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public CuentaCorriente Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<CuentaCorriente> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
