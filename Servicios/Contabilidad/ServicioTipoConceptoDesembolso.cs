﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace Browne.Intranet.Servicios.Contabilidad
{
    public class ServicioTipoConceptoDesembolso : Core.Servicio.Contabilidad.IServicioTipoConceptoDesembolso
    {
        private readonly AccesoDatos.Contextos.Contabilidad.Mantenedores.TipoConceptoDesembolsoContext contexto;

        public ServicioTipoConceptoDesembolso(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Contabilidad.Mantenedores.TipoConceptoDesembolsoContext(connectionString);
        }
        public IEnumerable<TipoConceptoDesembolso> Buscar(Expression<Func<TipoConceptoDesembolso, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(TipoConceptoDesembolso item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }


        public void Guardar(TipoConceptoDesembolso concepto)
        {
            try
            {
                contexto.Guardar(concepto);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public TipoConceptoDesembolso Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<TipoConceptoDesembolso> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
