﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace Browne.Intranet.Servicios.Contabilidad
{
    public class ServicioCheque : Core.Servicio.Contabilidad.IServicioCheque
    {
        private readonly AccesoDatos.Contextos.Contabilidad.Mantenedores.ChequesContext contexto;

        public ServicioCheque(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Contabilidad.Mantenedores.ChequesContext(connectionString);
        }
        public IEnumerable<Cheque> Buscar(Expression<Func<Cheque, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Cheque item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Cheque cheque)
        {
            try
            {
                contexto.Guardar(cheque);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Cheque Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Cheque> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
