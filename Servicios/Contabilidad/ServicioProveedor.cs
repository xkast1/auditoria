﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace Browne.Intranet.Servicios.Contabilidad
{
    public class ServicioProveedor : Core.Servicio.Contabilidad.IServicioProveedor
    {
        private readonly AccesoDatos.Contextos.Contabilidad.Mantenedores.ProveedoresContext contexto;

        public ServicioProveedor(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Contabilidad.Mantenedores.ProveedoresContext(connectionString);
        }

        public IEnumerable<Proveedor> Buscar(Expression<Func<Proveedor, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Proveedor item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Proveedor proveedor)
        {
            try
            {
                contexto.Guardar(proveedor);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Proveedor Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Proveedor> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
