﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace Browne.Intranet.Servicios.Contabilidad
{
    public class ServicioCuentaCorrienteTalonario : Core.Servicio.Contabilidad.IServicioCuentaCorrienteTalonario
    {
        private readonly AccesoDatos.Contextos.Contabilidad.Mantenedores.CuentasCorrientesTalonariosContext contexto;

        public ServicioCuentaCorrienteTalonario(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Contabilidad.Mantenedores.CuentasCorrientesTalonariosContext(connectionString);
        }

        public IEnumerable<CuentaCorrienteTalonario> Buscar(Expression<Func<CuentaCorrienteTalonario, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(CuentaCorrienteTalonario item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(CuentaCorrienteTalonario TalonarioCheques)
        {
            try
            {
                contexto.Guardar(TalonarioCheques);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public CuentaCorrienteTalonario Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<CuentaCorrienteTalonario> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
