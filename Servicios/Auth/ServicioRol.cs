﻿using Browne.Core.Servicio;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Browne.Core.Modelo.Auth;
using Browne.Core.Servicio.Auth;

namespace Browne.Intranet.Servicios.Auth
{
    public class ServicioRol : IServicioCrud<Rol>, IServicioRol
    {
        private readonly AccesoDatos.Contextos.Auth.RolesContext contexto;

        public ServicioRol(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Auth.RolesContext(cadenaConexion);
        }
        public IEnumerable<Rol> Buscar(Expression<Func<Rol, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Rol rol)
        {
            contexto.Guardar(rol);

            return rol.Id;
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public Rol Guardar(Rol rol)
        {
            try
            {
                return contexto.Guardar(rol);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Rol Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Rol> ObtenerTodo()
        {
            return contexto.ObtenerTodos();
        }

        public Rol ObtenerPorUsuario(Core.Modelo.Auth.Usuario usuario)
        {
            return contexto.ObteneTodos(usuario.Id, false)[0];
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        Rol IServicioRol.Guardar(Rol rol)
        {
            try
            {
                return contexto.Guardar(rol);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
