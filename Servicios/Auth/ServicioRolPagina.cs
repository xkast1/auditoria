﻿using Browne.Core.Modelo.Auth;
using Browne.Core.Modelo.Navegacion;

namespace Browne.Intranet.Servicios.Auth
{
    public class ServicioRolPagina : Core.Servicio.Auth.IServicioRolPagina
    {
        private readonly AccesoDatos.Contextos.Auth.RolesPaginasContext context;

        public ServicioRolPagina(string connectionString)
        {
            context = new AccesoDatos.Contextos.Auth.RolesPaginasContext(connectionString);
        }

        public bool ExisteAutorizacion(Rol rol, Pagina pagina)
        {
            return context.ExisteAutorizacion(rol, pagina);
        }
    }
}
