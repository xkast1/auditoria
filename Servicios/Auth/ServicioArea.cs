﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Auth;

namespace Browne.Intranet.Servicios.Auth
{
    public class ServicioArea : Core.Servicio.Auth.IServicioArea
    {
        private readonly AccesoDatos.Contextos.Auth.AreasContext contexto;

        public ServicioArea(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Auth.AreasContext(cadenaConexion);
        }
        public IEnumerable<Area> Buscar(Expression<Func<Area, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Area item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public Area Guardar(Area area)
        {
            try
            {
                contexto.Guardar(area);
                return area;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Area Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Area> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
