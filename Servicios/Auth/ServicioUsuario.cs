﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Auth;
using Browne.Core.Servicio.Auth;

namespace Browne.Intranet.Servicios.Auth
{
    public class ServicioUsuario : Core.Servicio.Auth.IServicioUsuario
    {
        private readonly AccesoDatos.Contextos.Auth.UsuariosContext context;

        public ServicioUsuario(string connectionString)
        {
            context = new AccesoDatos.Contextos.Auth.UsuariosContext(connectionString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuarios"></param>
        /// <param name="contrasena"></param>
        /// <param name="ip"></param>
        /// <returns></returns>
        public Browne.Core.Modelo.Auth.Usuario Obtener(string cadenaConexion, string usuarios, string contrasena, string ip)
        {
            try
            {
                return new AccesoDatos.Contextos.Auth.UsuariosContext(cadenaConexion)
                    .VerificarAcceso(usuarios, contrasena, ip);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void ActualizarTokenCookie(string identificador, string token)
        {
        }

        public void ActualizarTokenCookie(string identificador)
        {
            //repo.Obtener(identificador);
        }

        public string CrearTokenCookie(string identificador)
        {
            var context = new AccesoDatos.Contextos.Auth.UsuariosContext();
            var usuario = context.Obtener(identificador);
            var token = Guid.NewGuid().ToString("N");
            usuario.TokenCookie = token;
            context.ActualizarTokenCookie(usuario);
            return token;
        }

        public int Crear(Core.Modelo.Auth.Usuario item)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Usuario usuario)
        {
            try
            {
                context.Guardar(usuario);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public Core.Modelo.Auth.Usuario Obtener(string identificador)
        {
            try
            {
                return context.Obtener(identificador);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Core.Modelo.Auth.Usuario> ObtenerTodo()
        {
            try
            {
                return context.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Core.Modelo.Auth.Usuario> Buscar(Expression<Func<Core.Modelo.Auth.Usuario, bool>> func)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public Core.Modelo.Auth.Usuario ObtenerPorToken(string token)
        {
            return context.ObtenerPorToken(token);
        }

        public Usuario Obtener(int id)
        {
            try
            {
                return context.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        Usuario IServicioUsuario.Obtener(string identificador)
        {
            try
            {
                return context.Obtener(identificador);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void ActualizarContraseña(Usuario usuario)
        {
            try
            {
                context.ActualizarContraseña(usuario);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void SubirAvatar(Usuario usuario)
        {
            try
            {
                context.SubirAvatar(usuario);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
