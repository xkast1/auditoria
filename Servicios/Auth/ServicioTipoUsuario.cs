﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Auth;

namespace Browne.Intranet.Servicios.Auth
{
    public class ServicioTipoUsuario : Core.Servicio.Auth.IServicioTipoUsuario
    {
        private readonly AccesoDatos.Contextos.Auth.TiposUsuariosContext contexto;

        public ServicioTipoUsuario(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Auth.TiposUsuariosContext(cadenaConexion);
        }
        public IEnumerable<Core.Modelo.Auth.TipoUsuario> Buscar(Expression<Func<Core.Modelo.Auth.TipoUsuario, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Core.Modelo.Auth.TipoUsuario item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public TipoUsuario Guardar(Core.Modelo.Auth.TipoUsuario tipoUsuario)
        {
            try
            {
                contexto.Guardar(tipoUsuario);
                return tipoUsuario;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Core.Modelo.Auth.TipoUsuario Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Core.Modelo.Auth.TipoUsuario> ObtenerTodo()
        {
            try
            {
                return contexto.obtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
