﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Auth;
using Browne.Core.Servicio.Auth;

namespace Browne.Intranet.Servicios.Auth
{
    public class ServicioGrupoTrabajo : IServicioGrupoTrabajo
    {
        private readonly AccesoDatos.Contextos.Auth.GrupoTrabajoContext contexto;

        public ServicioGrupoTrabajo(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Auth.GrupoTrabajoContext(cadenaConexion);
        }
        public IEnumerable<GrupoTrabajo> Buscar(Expression<Func<GrupoTrabajo, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(GrupoTrabajo item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public GrupoTrabajo Guardar(GrupoTrabajo grupoTrabajo)
        {
            try
            {
                contexto.Guardar(grupoTrabajo);
                return grupoTrabajo;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public GrupoTrabajo Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<GrupoTrabajo> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
