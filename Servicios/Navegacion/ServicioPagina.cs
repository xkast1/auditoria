﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Browne.Core.Modelo.Navegacion;

namespace Browne.Intranet.Servicios.Navegacion
{
    public class ServicioPagina : Browne.Core.Servicio.Navegacion.IServicioPagina
    {
        private readonly AccesoDatos.Contextos.Navegacion.PaginasContext context;

        public ServicioPagina(string connectionString)
        {
            this.context = new AccesoDatos.Contextos.Navegacion.PaginasContext(connectionString);
        }

        public IEnumerable<Pagina> Buscar(Expression<Func<Pagina, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Pagina p)
        {
            context.Guardar(p);
            return p.Id;
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Pagina pagina)
        {
            try
            {
                context.Guardar(pagina);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Pagina Obtener(int id)
        {
            try
            {
                return context.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Pagina ObtenerPorRuta(string ruta)
        {
            return context.Obtener(new Pagina
            {
                Ruta = ruta
            });
        }

        public IEnumerable<Pagina> ObtenerTodo()
        {
            try
            {
                return context.ObteneTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
