﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Navegacion;

namespace Browne.Intranet.Servicios.Navegacion
{
    public class ServicioPlataforma : Core.Servicio.Navegacion.IServicioPlataforma
    {
        private readonly AccesoDatos.Contextos.Navegacion.PlataformasContext context;

        public ServicioPlataforma(string connectionString)
        {
            this.context = new AccesoDatos.Contextos.Navegacion.PlataformasContext(connectionString);
        }

        public IEnumerable<Plataforma> Buscar(Expression<Func<Plataforma, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Plataforma item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public Plataforma Guardar(Plataforma plataforma)
        {
            try
            {
                context.Guardar(plataforma);
                return plataforma;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Plataforma Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public Plataforma Obtener(string codigo)
        {
            try
            {
                return context.Obtener(codigo);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Plataforma> ObtenerTodo()
        {
            try
            {
                return context.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
