﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Navegacion;

namespace Browne.Intranet.Servicios.Navegacion
{
    public class ServicioMenuItem : Core.Servicio.Navegacion.IServicioMenuItem
    {
        private readonly AccesoDatos.Contextos.Navegacion.MenusItemsContext contexto;

        public ServicioMenuItem(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Navegacion.MenusItemsContext(connectionString);
        }
        public IEnumerable<MenuItem> Buscar(Expression<Func<MenuItem, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(MenuItem item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(MenuItem menuItem)
        {
            try
            {
                contexto.Guardar(menuItem);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message); ;
            }
        }

        public MenuItem Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message); ;
            }
        }

        public IEnumerable<MenuItem> ObtenerTodo()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MenuItem> ObtenerTodos(int? menuId = null, int? rolId = null)
        {
            try
            {
                return contexto.ObtenerTodos(menuId, rolId);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message); ;
            }
        }
    }
}
