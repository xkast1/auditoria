﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Browne.Core.Modelo.Auth;
using Browne.Core.Modelo.Navegacion;
using Browne.Core.Servicio.Navegacion;

namespace Browne.Intranet.Servicios.Navegacion
{
    public class ServicioMenu : Core.Servicio.Navegacion.IServicioMenu
    {
        private readonly AccesoDatos.Contextos.Navegacion.MenusContext contexto;

        public ServicioMenu(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Navegacion.MenusContext(connectionString);
        }

        public IEnumerable<Menu> Buscar(Expression<Func<Menu, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Menu item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Core.Modelo.Navegacion.Menu menu)
        {
            try
            {
                contexto.Guardar(menu);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Menu Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Menu ObtenerPorRol(Rol rol)
        {
            return ObtenerPorRolId(rol.Id);
        }

        public Menu ObtenerPorRolId(int rolId)
        {
            return ObtenerPorRolId(rolId);
        }

        public IEnumerable<Menu> ObtenerTodo()
        {
            throw new NotImplementedException();
        }

        IEnumerable<Menu> IServicioMenu.ObtenerTodos()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        IEnumerable<Menu> IServicioMenu.ObtenerTodosPorRol(Rol rol)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Menu> IServicioMenu.ObtenerTodosPorRolId(int rolId)
        {
            return contexto.ObtenerTodos(rolId);
        }
    }
}
