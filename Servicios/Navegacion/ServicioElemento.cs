﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Navegacion;

namespace Browne.Intranet.Servicios.Navegacion
{
    public class ServicioElemento : Core.Servicio.Navegacion.IServicioElemento
    {
        private readonly AccesoDatos.Contextos.Navegacion.ElementosContext context;

        public ServicioElemento(string connectionString)
        {
            this.context = new AccesoDatos.Contextos.Navegacion.ElementosContext(connectionString);
        }
        public IEnumerable<Elemento> Buscar(Expression<Func<Elemento, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Elemento item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public Elemento Guardar(Elemento elemento)
        {
            throw new NotImplementedException();
            //try
            //{
            //    context.Guardar(elemento);
            //    return elemento;
            //}
            //catch (Exception e)
            //{
            //    throw new Exception(e.Message);
            //}
        }

        public Elemento Obtener(int id)
        {
            try
            {
                return context.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Elemento> ObtenerElementosPagina(int? paginaID = null, int? rolId = null, int? elementoID = null, bool? bloqueado = null, bool? invisible = null)
        {

            try
            {
                return context.ObtenerElementosPagina(paginaID, rolId, elementoID, bloqueado, invisible);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Elemento> ObtenerTodo()
        {
            try
            {
                return context.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Elemento> ObtenerTodo(int paginaID)
        {
            try
            {
                return context.ObtenerTodos(paginaID);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
