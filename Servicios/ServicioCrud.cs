﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Intranet.Servicios
{
    public class ServicioCrud<T> : Core.Servicio.IServicioCrud<T> where T : new()
    {
        public IEnumerable<T> Buscar(Expression<Func<T, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(T item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public T Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> ObtenerTodo()
        {
            throw new NotImplementedException();
        }
    }
}
