﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Digitalizacion;

namespace Browne.Intranet.Servicios.Digitalizacion
{
    public class ServicioCarpetaSeguimiento : Browne.Core.Servicio.Digitalizacion.IServicioCarpetaSeguimiento
    {
        private readonly AccesoDatos.Contextos.Digitalizacion.CarpetasSeguimientosContext context;

        public ServicioCarpetaSeguimiento(string conexion)
        {
            this.context = new AccesoDatos.Contextos.Digitalizacion.CarpetasSeguimientosContext(conexion);
        }

        public IEnumerable<CarpetaSeguimiento> Buscar(Expression<Func<CarpetaSeguimiento, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(CarpetaSeguimiento item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar( )
        {
            throw new NotImplementedException();
        }

        public CarpetaSeguimiento Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CarpetaSeguimiento> ObtenerTodo()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Browne.Core.Modelo.Digitalizacion.CarpetaSeguimiento seguimiento, int id)
        {
            context.Guardar(seguimiento, id);
        }
    }
}
