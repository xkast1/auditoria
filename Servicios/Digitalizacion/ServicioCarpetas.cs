﻿using Browne.Core.Modelo.Digitalizacion;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Security.Cryptography;

namespace Browne.Intranet.Servicios.Digitalizacion
{
    public class ServicioCarpetas : Core.Servicio.Digitalizacion.IServicioCarpeta
    {
        private readonly string cadenaConexion;
        private readonly AccesoDatos.Contextos.Digitalizacion.CarpetasContext context;

        public ServicioCarpetas(string conexion)
        {
            this.cadenaConexion = conexion;
            this.context = new AccesoDatos.Contextos.Digitalizacion.CarpetasContext(conexion);
        }

        public Carpeta Obtener(int id)
        {
            return context.Obtener(new Carpeta { Id = id }, true);
        }

        public IEnumerable<Carpeta> ObtenerTodasPorRangoDeFechas(DateTime desde, DateTime hasta)
        {
            return new AccesoDatos.Contextos.Digitalizacion.CarpetasContext()
                .ObtenerTodas(desde, hasta);
        }

        public IEnumerable<Carpeta> ObtenerTodas(DateTime? desde = null, DateTime? hasta = null)
        {
            desde = desde ?? DateTime.Now.AddMonths(-1);
            hasta = hasta ?? DateTime.Now;

            return new AccesoDatos.Contextos.Digitalizacion.CarpetasContext().ObtenerTodas(desde, hasta);
        }

        public IEnumerable<Carpeta> ObtenerUltimoMes()
        {
            var desde = DateTime.Now.AddMonths(-1);
            var hasta = DateTime.Now;

            return new AccesoDatos.Contextos.Digitalizacion.CarpetasContext()
                .ObtenerTodas(desde, hasta);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carpeta"></param>
        public void Generar(Carpeta carpeta)
        {
            // Se obtiene la hora de inicio del proceso.
            var now = DateTime.Now;

            // Se obtiene el estado inicial para la generación
            // de esta carpeta.
            var estado = new Servicios.Digitalizacion.ServicioCarpetaEstado(this.cadenaConexion).Obtener(1);

            carpeta.Fecha = now;
            carpeta.CreacionFecha = now;

            // Se crea un nuevo seguimiento para la carpeta.
            carpeta.SeguimientoActual = new Core.Modelo.Digitalizacion.CarpetaSeguimiento()
            {
                Fecha = now,
                // TODO: Utilizar enumeración de estados.
                Estado = estado,
                CreacionUsuario = carpeta.CreacionUsuario,
                CreacionIp = carpeta.CreacionIp
            };

            context.Guardar(carpeta);
        }

        public int Crear(Carpeta item)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Carpeta> ObtenerTodo()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Carpeta> Buscar(Expression<Func<Carpeta, bool>> func)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Rechazar(Carpeta carpeta)
        {
            throw new NotImplementedException();
        }

        public void Aceptar(Carpeta carpeta)
        {
            try
            {
                var contextSeguimientoCarpetas = new AccesoDatos.Contextos.Digitalizacion.CarpetasSeguimientosContext();
                // Se obtiene el seguimiento actual de la carpeta.
                var seguimientoActual = carpeta.SeguimientoActual;

                // Se obtiene el estado en el que quedará esta carpeta
                // una vez haya concluído el proceso de aceptación.
                //
                // TODO Cargar estados siguientes al cargar CDE,
                //      para evitar abrir nuevas conexiones.
                //var siguienteEstado = seguimientoActual.Estado.SiguienteEstadoAprobado;
                var siguienteEstado = new AccesoDatos.Contextos.Digitalizacion.CarpetasEstadosContext(this.cadenaConexion).Obtener(seguimientoActual.Estado.SiguienteEstadoAprobado);

                // Se crea un nuevo seguimiento
                // por la aceptación de esta carpeta.
                carpeta.SeguimientoActual = new Core.Modelo.Digitalizacion.CarpetaSeguimiento
                {
                    CreacionIp = carpeta.ActualizacionIp,
                    CreacionUsuario = carpeta.ActualizacionUsuario,
                    Estado = siguienteEstado,
                    Fecha = DateTime.Now,
                    Observacion = string.Empty
                };

                // Se persiste la carpeta
                // para reflejar el nuevo estado.
                //context.Guardar(carpeta);
            }
            catch (Exception e)
            {
                // TODO Manejar excepción y lanzar una personalizada.
                throw new Exception(e.Message);
            }
        }

        // TODO Refactorizar: Extraer en clase de ayuda de formatos.
        private byte[] Tiff2Pdf(byte[] tiff)
        {
            var imgStream = new System.IO.MemoryStream();

            using (var ms = new System.IO.MemoryStream(tiff))
            {
                ms.Position = 0;
                ms.Write(tiff, 0, tiff.Length);

                var doc = new PdfSharp.Pdf.PdfDocument();

                var page = new PdfSharp.Pdf.PdfPage();
                page.Size = PdfSharp.PageSize.Legal;
                doc.Pages.Add(page);

                var xgr = PdfSharp.Drawing.XGraphics.FromPdfPage(page);

                var ximg = PdfSharp.Drawing.XImage.FromStream(ms);
                xgr.DrawImage(ximg, 0, 0);

                doc.Save(imgStream);
            }

            return imgStream.ToArray();
        }

        // TODO Refactorizar: Extraer en clase de ayuda de formatos.
        private string CambiarExtension(string filename, string extension)
        {
            return string.Format("{0}.{1}", filename.Remove(filename.LastIndexOf(".")), extension);
        }

        public byte[] GenerarZip(Carpeta carpeta)
        {

            try
            {
                var despacho = carpeta.NumeroDespacho;
                // TODO Máscara en web.config
                var nombreCarpeta = string.Format("Carpeta_Electronica_{0}_{1}", despacho, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                var zipFileName = nombreCarpeta + ".zip";
                var archivos = carpeta.LstArchivos;
                var webClient = new System.Net.WebClient();

                using (var zipFile = new ZipFile())
                {
                    var zipGenerado = new MemoryStream();

                    // Se crea una carpeta para almacenar los documentos generados.
                    // TODO Trabajar todo en memoria!
                    foreach (var archivo in archivos)
                    {
                        var docUrl = archivo.Documento.Url;
                        var ext = System.IO.Path.GetExtension(docUrl);

                        if (ext != ".pdf" && ext != ".tif" && ext != ".tiff")
                        {
                            throw new Exception("Archivo no soportado [" + docUrl + "].");
                        }

                        var documentData = webClient.DownloadData(docUrl);
                        //var data = ".pdf" == ext ? Pdf2Pdf(documentData) : Tiff2Pdf(documentData);
                        var data = ".pdf" == ext ? documentData : Tiff2Pdf(documentData);

                        // TODO manejar nombre sin extensión para agregar extensiones a discreción.
                        zipFile.AddEntry(CambiarExtension(archivo.Documento.Nombre, "pdf"), data);

                        // Se guarda el documento PDF en disco.
                        //string path = @"C:\Carpetas_Electronicas\" + nombreCarpeta + @"\" + archivo.Documento.Nombre + ".pdf";

                        //System.IO.File.WriteAllBytes(path, data);
                    }

                    zipFile.Save(zipGenerado);

                    return zipGenerado.ToArray();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message);
                throw e;
            }

            throw new Exception("Carpeta no encontrada [" + carpeta.Id + "]");
        }

        /// <summary>
        /// Genera archivo Zip firmado electronicamente.
        /// </summary>
        /// <param name="carpeta"></param>
        /// <returns></returns>
        public byte[] GenerarZipConFirma(Carpeta carpeta)
        {
            try
            {
                var despacho = carpeta.NumeroDespacho;
                var nombreCarpeta = string.Format("Carpeta_Electronica_{0}_{1}", despacho, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                var zipFileName = nombreCarpeta + ".zip";

                return Convert.FromBase64String(carpeta.ZipFirmado);
            }
            catch (Exception)
            {
                throw new Exception("Error generando zip con firma");
            }
        }

        public void Firmar(Carpeta carpeta, Certificado certificado)
        {
            try
            {
                var carpetaFirmable = GenerarZip(carpeta);
                var tempFileName = System.IO.Path.GetTempFileName();
                System.IO.File.WriteAllBytes(tempFileName + "_1.zip", carpetaFirmable);

                // Se busca la carpeta en el repositorio, para determinar
                // si ya existe una CDE firmada para el despacho de la
                // carpeta recibida.
                //var firmada = context.Obtener(0, carpeta.NumeroDespacho);
                var firmada = context.Obtener(new Carpeta { NumeroDespacho = carpeta.NumeroDespacho }, true);

                // Si se encuentra una carpeta firmada para el despacho buscado,
                // se asocia la fecha de la firma de la carpeta encontrada con
                // la carpeta que se está firmando, para que el proceso de
                // firma la actualice en lugar de guardar una nueva;
                // esta discriminación se realiza comprobando la existencia
                // de un valor en la propiedad FechaFirma de la carpeta.
                // En este punto, si no se encontró una carpeta firmada para
                // el número de despacho de la carpeta recibida, su fecha de firma
                // estará con valor null, indicando que no está firmada.
                carpeta.FechaFirma = firmada.FechaFirma;

                // Se asigna un token para acceso temporal a esta carpeta.
                carpeta.TokenAccesoTemporal = Guid.NewGuid().ToString("N");

                // TODO: Revisar
                //       A futuro, se debería inicializar este campo al momento
                //       de generar la carpeta, evitando esta conversión.
                carpeta.Zip = Convert.ToBase64String(carpetaFirmable);

                // TODO ¿Por qué se guarda en disco?
                File.WriteAllBytes(tempFileName + "_2.zip", carpetaFirmable);

                if (!carpeta.FechaFirma.HasValue)
                {
                    GuardarCarpeta(carpeta, certificado);
                }
                else
                {
                    //ActualizarCarpeta(carpeta);
                    throw new NotImplementedException("AdministradorArchivo.cs: No está implementada la funcionalidad para actualizar carpetas.");
                }

                //carpeta.CantidadDocumentos = carpetaFirmada.CantidadDocumentos;
                //carpeta.ZipFirmado = carpetaFirmada.ZipFirmado;
                //carpeta.Tamanio = carpetaFirmada.Tamanio;
                //carpeta.HashMD5 = carpetaFirmada.HashMD5;
                carpeta.FechaFirma = DateTime.Now;

                // Se obtiene el seguimiento actual de la carpeta.
                var seguimientoActual = carpeta.SeguimientoActual;

                // Se obtiene el estado en el que quedará esta carpeta
                // una vez haya concluído el proceso de aceptación.
                //
                // TODO Cargar estados siguientes al cargar CDE,
                //      para evitar abrir nuevas conexiones.
                //var siguienteEstado = seguimientoActual.Estado.SiguienteEstadoAprobado;
                var siguienteEstado = new AccesoDatos.Contextos.Digitalizacion.CarpetasEstadosContext(this.cadenaConexion).Obtener(seguimientoActual.Estado.SiguienteEstadoAprobado);

                // Se crea un nuevo seguimiento
                // por la firma de esta carpeta.
                carpeta.SeguimientoActual = new Core.Modelo.Digitalizacion.CarpetaSeguimiento
                {
                    CreacionIp = carpeta.ActualizacionIp, // TODO Obtener IP actual!
                    CreacionUsuario = carpeta.ActualizacionUsuario, // TODO Obtener usuario actual!
                    Estado = siguienteEstado,
                    Fecha = DateTime.Now,
                    Observacion = string.Empty
                };

                context.Guardar(carpeta);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private Carpeta GuardarCarpeta(Carpeta carpeta, Certificado certificado)
        {
            try
            {
                if (certificado.Data != null)
                {
                    //Convertimos los Bytes en Stream para leer el ZIP
                    var stream = new MemoryStream(Convert.FromBase64String(carpeta.Zip));

                    //Creamos el nuevo ZIP donde quedarán los documentos Firmados.
                    using (var zipCE = new ZipFile())
                    {
                        var msArchivoZip = new MemoryStream();
                        using (var zip = ZipFile.Read(stream))
                        {
                            var listaArchivosCarpeta = new List<Archivo>();

                            foreach (var entry in zip.Entries)
                            {
                                var msArchivo = new MemoryStream();
                                var archivoCarpeta = new Archivo();

                                entry.Extract(msArchivo);

                                archivoCarpeta.TamanioSinFirmar = msArchivo.ToArray().Length;
                                //TODO archivoCarpeta.HashMD5SinFirmar = ManejadorArchivos.CalcularHashMD5(msArchivo.ToArray());

                                var pdfFirmado = Helpers.Pdf.Firmar(msArchivo.ToArray(), certificado.Data);

                                // TODO archivoCarpeta.CodigoDocumento = entry.Comment;
                                // TODO archivoCarpeta.DetalleID = -1;
                                archivoCarpeta.Nombre = Path.GetFileName(entry.FileName);
                                archivoCarpeta.Version = 1;

                                var hash = MD5CryptoServiceProvider.Create().ComputeHash(stream);
                                archivoCarpeta.HashMD5 = BitConverter.ToString(hash);

                                archivoCarpeta.Tamanio = pdfFirmado.Length;
                                archivoCarpeta.Datos = Convert.ToBase64String(pdfFirmado, 0, pdfFirmado.Length);

                                //Separamos el nombre del archivo para determinra el codigo de documento
                                //Antes leemos el nombre en un array
                                var delimitadores = new char[] { '_' };
                                var nombreArchivo = Path.GetFileNameWithoutExtension(entry.FileName).Split(delimitadores);

                                //TODO archivoCarpeta.CodigoDocumento = Path.GetFileNameWithoutExtension(entry.FileName);

                                foreach (var parte in nombreArchivo)
                                {
                                    if (!System.Text.RegularExpressions.Regex.IsMatch(parte, @"^\d+$"))
                                    {
                                        //TODO archivoCarpeta.CodigoDocumento = parte;
                                        break;
                                    }
                                }

                                listaArchivosCarpeta.Add(archivoCarpeta);

                                ZipEntry e = zipCE.AddEntry(entry.FileName, pdfFirmado);
                                e.Comment = archivoCarpeta.HashMD5;
                            }
                        }

                        //Creamos el ZIP                        
                        zipCE.Save(msArchivoZip);

                        carpeta.ZipFirmado = Convert.ToBase64String(msArchivoZip.ToArray(), 0, msArchivoZip.ToArray().Length);
                        
                        // TODO: Crear tamaño y tamaño firmado;
                        //       el siguiente sería el tamaño firmado.
                        carpeta.Tamanio = msArchivoZip.ToArray().Length;

                        carpeta.HashMD5 = BitConverter.ToString(MD5CryptoServiceProvider.Create().ComputeHash(msArchivoZip.ToArray()));
                        carpeta.Tamanio = msArchivoZip.ToArray().Length;
                        
                        // TODO: carpeta.FechaFirma = DateTime.Now;
                    }
                }
                else
                {
                    //no hay certificado
                    throw new ArgumentException("No existe certificado para el proceso o Agente especificado");
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception("[NOK] Ocurrio un error no especificado: " + ex.Message.ToString());
            }

            return carpeta;
        }

        public IEnumerable<Certificado> ObtenerCertificados(bool activo = true)
        {
            return new ServicioCertificado(cadenaConexion).ObtenerTodo(activo);
        }

        public Carpeta Obtener(Carpeta carpeta, bool cargarDetalle)
        {
            return context.Obtener(carpeta, cargarDetalle);
        }
    }
}
