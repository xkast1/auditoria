﻿using Browne.Core.Modelo.Digitalizacion;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Browne.Intranet.Servicios.Digitalizacion
{
    public class ServicioDocumento : Core.Servicio.Digitalizacion.IServicioDocumento
    {
        private readonly AccesoDatos.Contextos.Digitalizacion.DocumentosDigitalizadosContext context;

        public ServicioDocumento(string conexion)
        {
            this.context = new AccesoDatos.Contextos.Digitalizacion.DocumentosDigitalizadosContext(conexion);
        }

        public IEnumerable<DocumentoDigitalizado> Buscar(Expression<Func<DocumentoDigitalizado, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(DocumentoDigitalizado item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public DocumentoDigitalizado Obtener(int id)
        {
            return context.Obtener(id);
        }

        public IEnumerable<DocumentoDigitalizado> ObtenerDocumentosPorNumeroDespacho(string numero)
        {
            return context.ObtenerTodos(numero);
        }

        public IEnumerable<DocumentoDigitalizado> ObtenerTodo()
        {
            throw new NotImplementedException();
        }
    }
}
