﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Browne.Core.Modelo.Digitalizacion;
using System.Data.SqlClient;

namespace Browne.Intranet.Servicios.Digitalizacion
{
    public class ServicioCertificado : Core.Servicio.Digitalizacion.IServicioCertificado
    {
        private readonly AccesoDatos.Contextos.Digitalizacion.CertificadosContext context;

        public ServicioCertificado(string connectionString)
        {
            context = new AccesoDatos.Contextos.Digitalizacion.CertificadosContext(connectionString);
        }

        public IEnumerable<Certificado> Buscar(Expression<Func<Certificado, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Certificado item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public Certificado Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public Certificado Obtener(Certificado c)
        {
            return context.Obtener(c);
        }

        public IEnumerable<Certificado> ObtenerTodo()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Certificado> ObtenerTodo(bool activo = true)
        {
            return context.ObteneTodos(activo);
        }

        public IEnumerable<Certificado> ObtenerTodo(Certificado filtro)
        {
            return context.ObteneTodos(filtro);
        }
    }
}
