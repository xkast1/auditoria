﻿using Browne.Core.Modelo.Digitalizacion;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Browne.Intranet.Servicios.Digitalizacion
{
    public class ServicioDocumentoDigitalizado : Browne.Core.Servicio.Digitalizacion.IServicioDocumentoDigitalizado
    {
        private readonly AccesoDatos.Contextos.Digitalizacion.DocumentosDigitalizadosContext context;

        public ServicioDocumentoDigitalizado(string connectionString)
        {
            context = new AccesoDatos.Contextos.Digitalizacion.DocumentosDigitalizadosContext(connectionString);
        }

        public IEnumerable<DocumentoDigitalizado> Buscar(Expression<Func<DocumentoDigitalizado, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(DocumentoDigitalizado item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public DocumentoDigitalizado Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DocumentoDigitalizado> ObtenerDocumentosPorNumeroDespacho(string numero)
        {
            return context.ObtenerTodos(numero);
        }

        public IEnumerable<DocumentoDigitalizado> ObtenerTodo()
        {
            throw new NotImplementedException();
        }
    }
}
