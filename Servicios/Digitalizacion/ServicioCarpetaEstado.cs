﻿using Browne.Core.Modelo.Digitalizacion;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Browne.Intranet.Servicios.Digitalizacion
{
    public class ServicioCarpetaEstado : Core.Servicio.Digitalizacion.IServicioCarpetaEstado
    {
        private readonly AccesoDatos.Contextos.Digitalizacion.CarpetasEstadosContext context;

        public ServicioCarpetaEstado(string cadenaConexion)
        {
            context = new AccesoDatos.Contextos.Digitalizacion.CarpetasEstadosContext(cadenaConexion);
        }

        public IEnumerable<CarpetaEstado> Buscar(Expression<Func<CarpetaEstado, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(CarpetaEstado item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public CarpetaEstado Obtener(int id)
        {
            return context.Obtener(id);
        }

        public IEnumerable<CarpetaEstado> ObtenerTodo()
        {
            throw new NotImplementedException();
        }
    }
}
