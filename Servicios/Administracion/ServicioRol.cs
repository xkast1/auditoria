﻿using Browne.Core.Servicio;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Browne.Core.Modelo.Auth;
using Browne.Core.Servicio.Auth;

namespace Browne.Intranet.Servicios.Administracion
{
    public class ServicioRol : IServicioCrud<Rol>, IServicioRol
    {
        private readonly AccesoDatos.Contextos.Auth.RolesContext contexto;

        public ServicioRol(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Auth.RolesContext(cadenaConexion);
        }
        public IEnumerable<Rol> Buscar(Expression<Func<Rol, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Rol item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public Rol Guardar(Rol rol)
        {
            throw new NotImplementedException();
        }

        public Rol Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public Rol ObtenerPorUsuario(Core.Modelo.Auth.Usuario usuario)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Rol> ObtenerTodo()
        {
            return contexto.ObtenerTodos();
        }
    }
}
