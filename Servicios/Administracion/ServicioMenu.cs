﻿namespace Browne.Intranet.Servicios.Administracion
{
    public class ServicioMenu
    {
        private readonly string connectionString;

        public ServicioMenu(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public System.Collections.Generic.IEnumerable<Browne.Core.Modelo.Navegacion.Menu> ObtenerTodos()
        {
            return new AccesoDatos.Contextos.Navegacion.MenusContext(this.connectionString)
                .ObtenerTodos();
        }

        public void Guardar(Browne.Core.Modelo.Navegacion.Menu menu)
        {
            new AccesoDatos.Contextos.Navegacion.MenusContext(this.connectionString)
                .Guardar(menu);
        }
    }
}
