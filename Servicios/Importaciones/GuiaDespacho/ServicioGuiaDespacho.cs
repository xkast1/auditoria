﻿using Browne.Core.Modelo.Importaciones.GuiaDespacho;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Browne.Intranet.Servicios.Importaciones.GuiaDespacho
{
    public class ServicioGuiaDespacho : Core.Servicio.Importaciones.GuiaDespacho.IServicioGuiaDespacho
    {
        private readonly AccesoDatos.Contextos.Importaciones.GuiasDespacho.GuiasDespachoContext context;

        public ServicioGuiaDespacho(string connectionString)
        {
            this.context = new AccesoDatos.Contextos.Importaciones.GuiasDespacho.GuiasDespachoContext(connectionString);
        }

        public IEnumerable<Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho> Buscar(Expression<Func<Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho guia)
        {
            try
            {
                context.Guardar(guia);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho Obtener(string despacho, int? id = null)
        {
            try
            {
                if (despacho != null)
                {
                    return context.Obtener(despacho);
                }
                else
                    return context.Obtener(null, id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho> ObtenerTodo()
        {
            try
            {
                return context.ObtenerTodo();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}