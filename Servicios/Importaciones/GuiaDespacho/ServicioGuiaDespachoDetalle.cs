﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Importaciones.GuiaDespacho;

namespace Browne.Intranet.Servicios.Importaciones.GuiaDespacho
{
    public class ServicioGuiaDespachoDetalle : Core.Servicio.Importaciones.GuiaDespacho.IServicioGuiaDespachoDetalle
    {
        private readonly AccesoDatos.Contextos.Importaciones.GuiasDespacho.GuiasDespachoDetalleContext contexto;

        public ServicioGuiaDespachoDetalle(string connectionString)
        {
            this.contexto = new AccesoDatos.Contextos.Importaciones.GuiasDespacho.GuiasDespachoDetalleContext(connectionString);
        }

        public IEnumerable<GuiaDespachoDetalle> Buscar(Expression<Func<GuiaDespachoDetalle, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(GuiaDespachoDetalle item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public GuiaDespachoDetalle Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<GuiaDespachoDetalle> ObtenerTodo()
        {
            throw new NotImplementedException();
        }
    }
}
