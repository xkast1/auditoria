﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioViaTransporte : IServicioViaTransporte
    {
        private readonly AccesoDatos.Contextos.Transversales.ViasTransportesContext contexto;

        public ServicioViaTransporte(string cadenaConexion)
        {
            contexto = new AccesoDatos.Contextos.Transversales.ViasTransportesContext(cadenaConexion);
        }
        public IEnumerable<ViaTransporte> Buscar(Expression<Func<ViaTransporte, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(ViaTransporte item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(ViaTransporte via)
        {
            try
            {
                contexto.Guardar(via);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ViaTransporte Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ViaTransporte Obtener(ViaTransporte via)
        {
            try
            {
                return contexto.Obtener(via);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<ViaTransporte> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
