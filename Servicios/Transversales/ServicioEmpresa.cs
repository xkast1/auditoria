﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioEmpresa : Core.Servicio.Transversales.IServicioEmpresa
    {
        private readonly AccesoDatos.Contextos.Transversales.EmpresasContext contexto;

        public ServicioEmpresa(string connectionString)
        {
            contexto = new AccesoDatos.Contextos.Transversales.EmpresasContext(connectionString);
        }
        public IEnumerable<Empresa> Buscar(Expression<Func<Empresa, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Empresa item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Empresa empresa)
        {
            try
            {
                contexto.Guardar(empresa);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Empresa Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Empresa> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
