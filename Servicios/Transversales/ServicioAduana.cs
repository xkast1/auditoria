﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AccesoDatos.Contextos.Transversales;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;
using Browne.Intranet.Servicios.Excepciones;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioAduana : Core.Servicio.Transversales.IServicioAduana
    {
        private readonly AccesoDatos.Contextos.Transversales.AduanasContext contexto;

        public ServicioAduana(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.AduanasContext(cadenaConexion);
        }
        public IEnumerable<Aduana> Buscar(Expression<Func<Aduana, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Aduana item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Aduana aduana)
        {
            try
            {
                contexto.Guardar(aduana);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Aduana Obtener(string codigo)
        {
            throw new NotImplementedException();
        }

        public Aduana Obtener(string codigoAduana, int ? id = null)
        {
            try
            {
                return contexto.Obtener(codigoAduana, id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //Busca por cualquiera de los codigos
        public Aduana Obtener(Aduana aduana)
        {
            try
            {
                return contexto.Obtener(aduana);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Aduana> ObtenerTodo(int? viaID = null, int? grupoTrabajoID = null, bool? bloqueada = false)
        {
            try
            {
                return contexto.ObtenerTodos(viaID, grupoTrabajoID, bloqueada);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<ViaTransporte> ObtenerViasTransporte(Aduana aduana)
        {
            throw new NotImplementedException();
        }

        public Aduana Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Aduana> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos(null);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
