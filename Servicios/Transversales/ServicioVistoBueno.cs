﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioVistoBueno : IServicioVistoBueno
    {
        private readonly AccesoDatos.Contextos.Transversales.VistosBuenosContext contexto;
        public ServicioVistoBueno(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.VistosBuenosContext(cadenaConexion);
        }

        public IEnumerable<VistoBueno> Buscar(Expression<Func<VistoBueno, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(VistoBueno item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(VistoBueno visto)
        {
            try
            {
                contexto.Guardar(visto);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public VistoBueno Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public VistoBueno Obtener(VistoBueno visto)
        {
            try
            {
                return contexto.Obtener(visto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<VistoBueno> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerSecuencia();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
