﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioComuna : Core.Servicio.Transversales.IServicioComuna
    {
        private readonly AccesoDatos.Contextos.Transversales.ComunasContext contexto;

        public ServicioComuna(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.ComunasContext(cadenaConexion);
        }
        public IEnumerable<Comuna> Buscar(Expression<Func<Comuna, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Comuna item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Comuna comuna)
        {
            try
            {
                contexto.Guardar(comuna);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Comuna Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public Comuna Obtener(Comuna comuna)
        {
            try
            {
                return contexto.Obtener(comuna);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Comuna> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
