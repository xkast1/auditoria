﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioAlmacenista : Core.Servicio.Transversales.IServicioAlmacenista
    {
        private readonly AccesoDatos.Contextos.Transversales.AlmacenistasContext contexto;
        public ServicioAlmacenista(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.AlmacenistasContext(cadenaConexion);
        }

        public IEnumerable<Almacenista> Buscar(Expression<Func<Almacenista, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Almacenista item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Almacenista almacenista)
        {
            try
            {
                contexto.Guardar(almacenista);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Almacenista Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Almacenista Obtener(Almacenista almacenista)
        {
            try
            {
                return contexto.Obtener(almacenista);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Almacenista> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
