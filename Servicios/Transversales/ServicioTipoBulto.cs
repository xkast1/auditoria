﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioTipoBulto : Core.Servicio.Transversales.IServicioTipoBulto
    {
        private readonly AccesoDatos.Contextos.Transversales.TiposBultosContext contexto;
        public ServicioTipoBulto(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.TiposBultosContext(cadenaConexion);
        }

        public IEnumerable<TipoBulto> Buscar(Expression<Func<TipoBulto, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(TipoBulto item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(TipoBulto bulto)
        {
            try
            {
                contexto.Guardar(bulto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TipoBulto Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public TipoBulto Obtener(TipoBulto bulto)
        {
            try
            {
                return contexto.Obtener(bulto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<TipoBulto> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
