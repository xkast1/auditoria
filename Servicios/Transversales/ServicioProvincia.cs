﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioProvincia : IServicioProvincia
    {
        private readonly AccesoDatos.Contextos.Transversales.ProvinciasContext contexto;
        public ServicioProvincia(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.ProvinciasContext(cadenaConexion);
        }
        public IEnumerable<Provincia> Buscar(Expression<Func<Provincia, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Provincia item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Provincia provincia)
        {
            try
            {
                contexto.Guardar(provincia);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Provincia Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Provincia Obtener(Provincia provincia)
        {
            try
            {
                return contexto.Obtener(provincia);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Provincia> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
