﻿using Browne.Core.Modelo.Transversales;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioFuncionario : Core.Servicio.Transversales.IServicioFuncionario
    {
        private readonly AccesoDatos.Contextos.Transversales.FuncionariosContext context;

        public ServicioFuncionario(string cadenaConexion)
        {
            context = new AccesoDatos.Contextos.Transversales.FuncionariosContext(cadenaConexion);
        }

        public IEnumerable<Funcionario> Buscar(Expression<Func<Funcionario, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Funcionario item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public Funcionario Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public Funcionario ObtenerPorRut(string rut)
        {
            return context.Obtener(rut);
        }

        public IEnumerable<string> ObtenerRutsPorPatron(string patron)
        {
            return context.ObtenerRutsPorPatron(patron);
        }

        public IEnumerable<Funcionario> ObtenerTodo()
        {
            throw new NotImplementedException();
        }
    }
}
