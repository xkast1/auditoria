﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioTipoCarga : Core.Servicio.Transversales.IServicioTipoCarga
    {
        private readonly AccesoDatos.Contextos.Transversales.TiposCargasContext contexto;
        public ServicioTipoCarga(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.TiposCargasContext(cadenaConexion);
        }

        public IEnumerable<Core.Modelo.Transversales.TipoCarga> Buscar(Expression<Func<Core.Modelo.Transversales.TipoCarga, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Core.Modelo.Transversales.TipoCarga item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(TipoCarga carga)
        {
            try
            {
                contexto.Guardar(carga);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TipoCarga Obtener(string codigo, int? id = null)
        {
            try
            {
                return contexto.Obtener(codigo, id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TipoCarga Obtener(TipoCarga carga)
        {
            try
            {
                return contexto.Obtener(carga);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Core.Modelo.Transversales.TipoCarga> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TipoCarga Obtener(int id)
        {
            throw new NotImplementedException();
        }
    }
}