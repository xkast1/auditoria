﻿using Browne.Core.Modelo.Transversales;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Browne.Intranet.Servicios.Transversales.Operacion
{
    public class ServicioOperacion : Core.Servicio.Transversales.Operacion.IServicioOperacion
    {

        private readonly AccesoDatos.Contextos.Transversales.Operaciones.OperacionesContext contexto;

        public ServicioOperacion(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.Operaciones.OperacionesContext(cadenaConexion);
        }

        public IEnumerable<ResultadoAutocompletar> Buscar(string patron)
        {
            return contexto.Buscar(patron);
        }

        public IEnumerable<Core.Modelo.Transversales.Operacion.Operacion> Buscar(Expression<Func<Core.Modelo.Transversales.Operacion.Operacion, bool>> func)
        {
            throw new NotImplementedException();
        }


        public int Crear(Core.Modelo.Transversales.Operacion.Operacion item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public Core.Modelo.Transversales.Operacion.Operacion Obtener(string numero)
        {
            return contexto.Obtener(numero);
        }

        public Core.Modelo.Transversales.Operacion.Operacion Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public Core.Modelo.Transversales.Operacion.Operacion Obtener(string tipo, string estado, int correlativo)
        {
            try
            {
                return contexto.Obtener("I-2222");//correlativo);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Core.Modelo.Transversales.Operacion.Operacion> ObtenerTodo()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Core.Modelo.Transversales.Operacion.Operacion> ObtenerTodo(string tipo, int estado)
        {
            try
            {
                return contexto.ObtenerTodo(tipo, estado);
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
