﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioTipoOperacion : Core.Servicio.Transversales.IServicioTipoOperacion
    {
        private readonly AccesoDatos.Contextos.Transversales.TiposOperacionesContext contexto;
        public ServicioTipoOperacion(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.TiposOperacionesContext(cadenaConexion);
        }

        public IEnumerable<TipoOperacion> Buscar(Expression<Func<TipoOperacion, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(TipoOperacion item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(TipoOperacion operacion)
        {
            try
            {
                contexto.Guardar(operacion);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TipoOperacion Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TipoOperacion Obtener(TipoOperacion operacion)
        {
            try
            {
                return contexto.Obtener(operacion);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<TipoOperacion> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
