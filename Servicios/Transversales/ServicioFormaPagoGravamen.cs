﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioFormaPagoGravamen : IServicioFormaPagoGravamen
    {
        private readonly AccesoDatos.Contextos.Transversales.FormasPagosGravamenesContext contexto;
        public ServicioFormaPagoGravamen(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.FormasPagosGravamenesContext(cadenaConexion);
        }

        public IEnumerable<FormaPagoGravamen> Buscar(Expression<Func<FormaPagoGravamen, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(FormaPagoGravamen item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(FormaPagoGravamen forma)
        {
            try
            {
                contexto.Guardar(forma);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FormaPagoGravamen Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FormaPagoGravamen Obtener(FormaPagoGravamen gravamen)
        {
            try
            {
                return contexto.Obtener(gravamen);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<FormaPagoGravamen> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
