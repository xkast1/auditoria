﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioBanco : Core.Servicio.Transversales.IServicioBanco
    {
        private readonly AccesoDatos.Contextos.Transversales.BancosContext contexto;
        public ServicioBanco(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.BancosContext(cadenaConexion);
        }
        public IEnumerable<Banco> Buscar(Expression<Func<Banco, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Banco item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }
        public void Guardar(Banco banco)
        {
            try
            {
                contexto.Guardar(banco);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Banco Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Banco Obtener(Banco banco)
        {
            try
            {
                return contexto.Obtener(banco);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Banco> ObtenerTodo()
        {
            return contexto.ObtenerTodos();
        }
    }
}
