﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioPais : Core.Servicio.Transversales.IServicioPais
    {
        private readonly AccesoDatos.Contextos.Transversales.PaisesContext contexto;
        public ServicioPais(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.PaisesContext(cadenaConexion);
        }
        public IEnumerable<Pais> Buscar(Expression<Func<Pais, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Pais item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Pais pais)
        {
            try
            {
                contexto.Guardar(pais);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Pais Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Pais Obtener(Pais pais)
        {
            try
            {
                return contexto.Obtener(pais);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Pais> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
