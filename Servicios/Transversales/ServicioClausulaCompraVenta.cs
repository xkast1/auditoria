﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioClausulaCompraVenta : IServicioClausulaCompraVenta
    {
        private readonly AccesoDatos.Contextos.Transversales.ClausulasComprasVentasContext contexto;

        public ServicioClausulaCompraVenta(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.ClausulasComprasVentasContext(cadenaConexion);
        }
        public IEnumerable<ClausulaCompraVenta> Buscar(Expression<Func<ClausulaCompraVenta, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(ClausulaCompraVenta item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(ClausulaCompraVenta clausula)
        {
            try
            {
                contexto.Guardar(clausula);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ClausulaCompraVenta Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ClausulaCompraVenta Obtener(ClausulaCompraVenta clausula)
        {
            try
            {
                return contexto.Obtener(clausula);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<ClausulaCompraVenta> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
