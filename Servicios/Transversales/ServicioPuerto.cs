﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioPuerto : IServicioPuerto
    {
        private readonly AccesoDatos.Contextos.Transversales.PuertosContext contexto;

        public ServicioPuerto(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.PuertosContext(cadenaConexion);
        }

        public IEnumerable<Puerto> Buscar(Expression<Func<Puerto, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Puerto item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Puerto puerto)
        {
            try
            {
                contexto.Guardar(puerto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Puerto Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Puerto Obtener(Puerto puerto)
        {
            try
            {
                return contexto.Obtener(puerto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Puerto> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
