﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioUnidadMedida : Core.Servicio.Transversales.IServicioUnidadMedida
    {
        private readonly AccesoDatos.Contextos.Transversales.UnidadesMedidasContext contexto;
        public ServicioUnidadMedida(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.UnidadesMedidasContext(cadenaConexion);
        }

        public IEnumerable<UnidadMedida> Buscar(Expression<Func<UnidadMedida, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(UnidadMedida item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(UnidadMedida unidad)
        {
            try
            {
                contexto.Guardar(unidad);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public UnidadMedida Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public UnidadMedida Obtener(UnidadMedida unidad)
        {
            try
            {
                return contexto.Obtener(unidad);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<UnidadMedida> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}