﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioFormaPago : Core.Servicio.Transversales.IServicioFormaPago
    {
        private readonly AccesoDatos.Contextos.Transversales.FormasPagosContext contexto;
        public ServicioFormaPago(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.FormasPagosContext(cadenaConexion);
        }

        public IEnumerable<FormaPago> Buscar(Expression<Func<FormaPago, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(FormaPago item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(FormaPago forma)
        {
            try
            {
                contexto.Guardar(forma);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FormaPago Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FormaPago Obtener(FormaPago formaPago)
        {
            try
            {
                return contexto.Obtener(formaPago);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<FormaPago> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
