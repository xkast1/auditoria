﻿using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Consumidor;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Browne.Core.Servicio.Consumidor;

namespace Browne.Intranet.Servicios.Transversales.Consumidor
{
    public class ServicioCliente : Core.Servicio.Consumidor.IServicioCliente
    {
        private readonly AccesoDatos.Contextos.Transversales.Consumidores.ClientesContext context;

        public ServicioCliente(string cadenaConexion)
        {
            context = new AccesoDatos.Contextos.Transversales.Consumidores.ClientesContext(cadenaConexion);
        }

        public IEnumerable<Cliente> Buscar(Expression<Func<Cliente, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Cliente item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public Cliente Obtener(string rut)
        {
            throw new NotImplementedException();
        }

        public Cliente Obtener(int id)
        {
            throw new NotImplementedException();
        }

        public Cliente Obtener(int? id = null, bool? bloqueado = null, int? numeroSucursal = null, string rut = null)
        {
            return context.Obtener(id, bloqueado, numeroSucursal, rut);
        }

        public IEnumerable<Cliente> ObtenerTodo()
        {
            try
            {
                return context.ObtenerTodos();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<ResultadoAutocompletar> Buscar(string patron)
        {
            return context.Buscar(patron);
        }

        //Cliente IServicioCliente.Buscar(string patron)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
