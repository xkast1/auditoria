﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Auth;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Auth;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioSucursal : Core.Servicio.Transversales.IServicioSucursal
    {
        private readonly AccesoDatos.Contextos.Transversales.SucursalesContext contexto;
        public ServicioSucursal(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.SucursalesContext(cadenaConexion);
        }
        public IEnumerable<Sucursal> Buscar(Expression<Func<Sucursal, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Sucursal item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Core.Modelo.Transversales.Sucursal sucursal)
        {
            try
            {
                contexto.Guardar(sucursal);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Sucursal Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Sucursal> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos(null);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<Sucursal> ObtenerTodo(bool ? bloqueado = null)
        {
            try
            {
                return contexto.ObtenerTodos(bloqueado);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
