﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioRegion : IServicioRegion
    {
        private readonly AccesoDatos.Contextos.Transversales.RegionesContext contexto;

        public ServicioRegion(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.RegionesContext(cadenaConexion);
        }
        public IEnumerable<Region> Buscar(Expression<Func<Region, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Region item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Region region)
        {
            try
            {
                contexto.Guardar(region);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Region Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Region Obtener(Region region)
        {
            try
            {
                return contexto.Obtener(region);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Region> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
