﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioTipoContenedor : Core.Servicio.Transversales.IServicioTipoContenedor
    {
        private readonly AccesoDatos.Contextos.Transversales.TiposContenedoresContext contexto;
        public ServicioTipoContenedor(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.TiposContenedoresContext(cadenaConexion);
        }

        public IEnumerable<TipoContenedor> Buscar(Expression<Func<TipoContenedor, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(TipoContenedor item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(TipoContenedor contenedor)
        {
            try
            {
                contexto.Guardar(contenedor);
            }
            catch (Exception)
            {
                throw;
            }         
        }

        public TipoContenedor Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public TipoContenedor Obtener(TipoContenedor contenedor)
        {
            try
            {
                return contexto.Obtener(contenedor);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<TipoContenedor> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
