﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioTipoSello : Core.Servicio.Transversales.IServicioTipoSello
    {
        private readonly AccesoDatos.Contextos.Transversales.TiposSellosContext contexto;
        public ServicioTipoSello(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.TiposSellosContext(cadenaConexion);
        }

        public IEnumerable<TipoSello> Buscar(Expression<Func<TipoSello, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(TipoSello item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(TipoSello sello)
        {
            try
            {
                contexto.Guardar(sello);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TipoSello Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TipoSello Obtener(TipoSello sello)
        {
            try
            {
                return contexto.Obtener(sello);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<TipoSello> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
