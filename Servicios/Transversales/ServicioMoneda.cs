﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioMoneda : IServicioMoneda
    {

        private readonly AccesoDatos.Contextos.Transversales.MonedasContext contexto;

        public ServicioMoneda(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.MonedasContext(cadenaConexion);
        }

        public IEnumerable<Moneda> Buscar(Expression<Func<Moneda, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(Moneda item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(Moneda moneda)
        {
            try
            {
                contexto.Guardar(moneda);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Moneda Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public Moneda Obtener(Moneda moneda)
        {
            try
            {
                return contexto.Obtener(moneda);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Moneda> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
