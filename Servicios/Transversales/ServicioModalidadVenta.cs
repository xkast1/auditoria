﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioModalidadVenta : IServicioModalidadVenta
    {
        private readonly AccesoDatos.Contextos.Transversales.ModalidadesVentasContext contexto;
        public ServicioModalidadVenta(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.ModalidadesVentasContext(cadenaConexion);
        }
        public IEnumerable<ModalidadVenta> Buscar(Expression<Func<ModalidadVenta, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(ModalidadVenta item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(ModalidadVenta modalidad)
        {
            try
            {
                contexto.Guardar(modalidad);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ModalidadVenta Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ModalidadVenta Obtener(ModalidadVenta modalidad)
        {
            try
            {
                return contexto.Obtener(modalidad);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<ModalidadVenta> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
