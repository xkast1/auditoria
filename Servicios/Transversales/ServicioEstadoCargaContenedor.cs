﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Servicio.Transversales;

namespace Browne.Intranet.Servicios.Transversales
{
    public class ServicioEstadoCargaContenedor : Core.Servicio.Transversales.IServicioEstadoCargaContenedor
    {
        private readonly AccesoDatos.Contextos.Transversales.EstadosCargasContenedoresContext contexto;
        public ServicioEstadoCargaContenedor(string cadenaConexion)
        {
            this.contexto = new AccesoDatos.Contextos.Transversales.EstadosCargasContenedoresContext(cadenaConexion);
        }

        public IEnumerable<EstadoCargaContenedor> Buscar(Expression<Func<EstadoCargaContenedor, bool>> func)
        {
            throw new NotImplementedException();
        }

        public int Crear(EstadoCargaContenedor item)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public void EliminarPorLotes(int[] ids)
        {
            throw new NotImplementedException();
        }

        public void Guardar()
        {
            throw new NotImplementedException();
        }

        public void Guardar(EstadoCargaContenedor estado)
        {
            try
            {
                contexto.Guardar(estado);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public EstadoCargaContenedor Obtener(int id)
        {
            try
            {
                return contexto.Obtener(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public EstadoCargaContenedor Obtener(EstadoCargaContenedor estado)
        {
            try
            {
                return contexto.Obtener(estado);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<EstadoCargaContenedor> ObtenerTodo()
        {
            try
            {
                return contexto.ObtenerTodos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}