﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Digitalizacion;

namespace AccesoDatos.Contextos.Digitalizacion
{
    public class DocumentosDigitalizadosContext
    {
        private string connectionString = string.Empty;

        public DocumentosDigitalizadosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DocumentosDigitalizadosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        /// <summary>
        /// Obtiene listado de documentos digitalizados de un despacho determinado
        /// </summary>
        /// <param name="numeroDespacho"></param>
        /// <returns></returns>
        public IEnumerable<DocumentoDigitalizado> ObtenerTodos(string numeroDespacho)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_documentosDigitalizados_seleccionar";
                    command.Parameters.AddWithValue("@NumeroDespacho", numeroDespacho);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                yield return Cargar(dr);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Obtiene un documento digitalizado especifico a partir de su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentoDigitalizado Obtener(int id)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_documentosDigitalizados_seleccionar";
                    command.Parameters.AddWithValue("@Id", id);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            return Cargar(dr);
                        }
                    }
                }

                connection.Close();
            }

            throw new Exception("Documento digitalizado no encontrado");
        }

        private DocumentoDigitalizado Cargar(SqlDataReader dr)
        {
            DocumentoDigitalizado Documento = new DocumentoDigitalizado();
            Documento.Id = dr.GetInt32(0);// dr["ID_DESPACHO_DOCTO"];            
            Documento.TipoDocumento = new Transversales.TiposDocumentosContext().Obtener(dr.GetInt32(6));
            Documento.Cantidad = dr.GetInt32(3);// (int)dr["CANT_DOC"];
            Documento.Orden = dr.GetInt32(4);// (int)dr["ORDEN"];
            Documento.Ruta = dr.GetString(1); //dr["DIR_ARCHIVO"].ToString().Trim();
            Documento.Nombre = dr.GetString(2);// dr["NOM_ARCHIVO"].ToString().Trim();
            Documento.FechaDigitalizacion = dr.GetDateTime(5);// (DateTime)dr["FECHA_DIGITALIZA"];
            Documento.Funcionario = new Transversales.FuncionariosContext().Obtener(dr.GetString(7));

            return Documento;
        }
    }
}
