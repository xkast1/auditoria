﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Digitalizacion;

namespace AccesoDatos.Contextos.Digitalizacion
{
    public class CarpetasEstadosContext
    {
        private string connectionString = string.Empty;

        public CarpetasEstadosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public CarpetasEstadosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["Browne_Digitalizacion"].ConnectionString;
        }
        /// <summary>
        /// Obtiene un estado especifico a partir de su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CarpetaEstado Obtener(int id)
        {
            CarpetaEstado estado = new CarpetaEstado();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_estadosCarpetas_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@estadoCarpeta_ID", SqlDbType.Int).Value = id;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                        estado = Cargar(dr);
                }
                cmd.Connection.Close();
            }
            return estado;
        }
        /// <summary>
        /// Obtiene listado de estados
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CarpetaEstado> ObtenerTodos(bool? activo = null)
        {
            List<CarpetaEstado> estados = new List<CarpetaEstado>();
            using (SqlCommand command = new SqlCommand("prc_estadosCarpetas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (activo.HasValue)
                    command.Parameters.Add("@estadoCarpeta_activo", SqlDbType.Bit).Value = activo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        CarpetaEstado estado = new CarpetaEstado();
                        estado = Cargar(dr);
                        yield return estado;
                    }
                }
                command.Connection.Close();
            }
        }


        /// <summary>
        /// Almacena un estado
        /// </summary>
        /// <param name="CarpetaEstado"></param>
        public void Guardar(CarpetaEstado carpetaEstado)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@estadoCarpeta_ID", SqlDbType.Int).Value = carpetaEstado.Id;
                command.Parameters.Add("@estadoCarpeta_nombre", SqlDbType.VarChar, 10).Value = carpetaEstado.Nombre;
                command.Parameters.Add("@estadoCarpeta_siguienteEstadoAprobadoID", SqlDbType.VarChar, 50).Value = carpetaEstado.SiguienteEstadoAprobado;
                command.Parameters.Add("@estadoCarpeta_siguienteRechazadoID", SqlDbType.VarChar, 255).Value = carpetaEstado.SiguienteEstdoRechazado;
                command.Parameters.Add("@estadoCarpeta_activo", SqlDbType.Bit).Value = carpetaEstado.Activo;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (carpetaEstado.Id > 0)
                        command.CommandText = "prc_estadosCarpetas_actualizar";
                    else
                    {
                        command.CommandText = "prc_estadosCarpetas_insertar";
                        command.Parameters["@estadoCarpeta_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en CarpetasEstadosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }

        }

        private CarpetaEstado Cargar(SqlDataReader dr)
        {
            CarpetaEstado Estado = new CarpetaEstado();
            Estado.Id = (int)dr["estadoCarpeta_ID"];
            Estado.Nombre = dr["estadoCarpeta_nombre"].ToString().Trim();
            Estado.SiguienteEstadoAprobado = (int)dr["estadoCarpeta_siguienteAprobadoID"];
            Estado.SiguienteEstdoRechazado = (int)dr["estadoCarpeta_siguienteRechazadoID"];
            Estado.Activo = (bool)dr["estadoCarpeta_activo"];

            return Estado;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
