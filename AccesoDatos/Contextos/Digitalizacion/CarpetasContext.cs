﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Digitalizacion;
using System.Data.SqlTypes;
using System.Linq;

namespace AccesoDatos.Contextos.Digitalizacion
{
    public class CarpetasContext
    {
        private string connectionString = string.Empty;

        public CarpetasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public CarpetasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["Browne_Digitalizacion"].ConnectionString;
        }

        public Carpeta ObtenerUsandoConsultaDirecta(Carpeta c)
        {
            var id = c.Id;
            var numeroDespacho = c.NumeroDespacho;

            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = @"
                        SELECT [dbo].[carpetas].[carpeta_ID]
                             , [dbo].[carpetas].[aduana_ID]
                             , [dbo].[carpetas].[carpeta_aduanaSolicitante]
                             , [dbo].[carpetas].[agente_ID]
                             , [dbo].[carpetas].[agen_codAgente]
                             , [dbo].[carpetas].[carpeta_fecha]
                             , [dbo].[carpetas].[carpeta_numeroDespacho]
                             , [dbo].[carpetas].[carpeta_observacion]
                             , [dbo].[carpetas].[carpeta_solicitante]
                             , [dbo].[carpetas].[carpeta_numeroSolicitud]
                             , [dbo].[carpetas].[carpeta_fechaFirma]
                             , [dbo].[carpetas].[carpeta_URI]
                             , [dbo].[carpetas].[carpeta_sobre]
                             , [dbo].[carpetas].[carpeta_cantidadDocumentos]
                             , [dbo].[carpetas].[carpeta_tamanio]
                             , [dbo].[carpetas].[carpeta_hashMD5]
                             , [dbo].[carpetas].[rutFuncionario]
                             , [dbo].[carpetas].[creacion_usuario]
                             , [dbo].[carpetas].[creacion_IP]
                             , [dbo].[carpetas].[actualizacion_usuario]
                             , [dbo].[carpetas].[actualizacion_IP]
                             , coalesce([DESPACHO_ANEXOING].[RUTCLIENTE], [Browne_Exportaciones].[dbo].[DESPACHO_ANEXOEXP].[RUTCLIENTE]) AS [Rut_Cliente]
                             , coalesce([DESPACHO_ANEXOING].[NUMSUCURSAL], [Browne_Exportaciones].[dbo].[DESPACHO_ANEXOEXP].[NUMSUCURSAL]) AS [Numero_Sucursal]
     
                             , [dbo].[carpetasArchivos].[carpetaArchivo_ID]
                             , [dbo].[carpetasArchivos].[carpeta_ID]
                             , [dbo].[carpetasArchivos].[documentoDigitalizado_ID]
                             , [dbo].[carpetasArchivos].[carpetaArchivo_nombre]
                             , [dbo].[carpetasArchivos].[carpetaArchivo_version]
                             , [dbo].[carpetasArchivos].[carpetaArchivo_hashMD5]
                             , [dbo].[carpetasArchivos].[carpetaArchivo_fecha]
                             , [dbo].[carpetasArchivos].[carpetaArchivo_tamanio]
                             , [dbo].[carpetasArchivos].[carpetaArchivo_tamanioSinFirmar]
                             , [dbo].[carpetasArchivos].[carpetaArchivo_hashMD5SinFirmar]
                             , [dbo].[carpetasArchivos].[carpetaArchivo_datos]
     
                             , [dbo].[documentosDigitalizados].[documentoDigitalizado_ID]
                             , [dbo].[documentosDigitalizados].[documentoDigitalizado_web]
                             , [dbo].[documentosDigitalizados].[documentoDigitalizado_nombreArchivo]
                             , [dbo].[documentosDigitalizados].[documentoDigitalizado_cantidadDocumentos]
                             , [dbo].[documentosDigitalizados].[documentoDigitalizado_orden]
                             , [dbo].[documentosDigitalizados].[documentoDigitalizado_fecha]
                             , [dbo].[documentosDigitalizados].[documentoDigitalizado_codigoDocumento]
                             , [dbo].[documentosDigitalizados].[documentoDigitalizado_rutDigitalizador]

                             , [Funcionario].[FUNC_NOMBRES]
                             , [Funcionario].[FUNC_APELLIDO]
     
                             , [Aduana_desarrollo].[dbo].[DOCUMENTOS_BASE].[DOC_NOMBRE]

                        FROM [Aduana_desarrollo].[dbo].[DOCUMENTOS_BASE]

                        INNER JOIN [dbo].[documentosDigitalizados]
                        ON [DOCUMENTOS_BASE].[ID_DOCUMENTO] = [documentosDigitalizados].[documentoDigitalizado_codigoDocumento]

                        INNER JOIN [Aduana_Desarrollo].[dbo].[Funcionario]
                        ON [documentosDigitalizados].[documentoDigitalizado_rutDigitalizador] = [Funcionario].[RUTFUNCION]

                        LEFT JOIN carpetasArchivos
                        ON [documentosDigitalizados].[documentoDigitalizado_ID] = [carpetasArchivos].[documentoDigitalizado_ID]

                        LEFT JOIN [dbo].[carpetas]
                        ON [carpetasArchivos].[carpeta_ID] = [carpetas].[carpeta_ID]

                        LEFT OUTER JOIN [Aduana_desarrollo].[dbo].[DESPACHO_ANEXOING] AS DESPACHO_ANEXOING
                        ON [DESPACHO_ANEXOING].[NUMDESPACHINGRE] = [dbo].[carpetas].[carpeta_numeroDespacho]

                        LEFT OUTER JOIN [Browne_Exportaciones].[dbo].[DESPACHO_ANEXOEXP]
                        ON [Browne_Exportaciones].[dbo].[DESPACHO_ANEXOEXP].[NUMDESPACHEGRE] = [dbo].[carpetas].[carpeta_numeroDespacho]

                        WHERE (@carpeta_ID IS NULL OR [carpetas].[carpeta_ID] = @carpeta_ID)
                          AND (@carpeta_numeroDespacho IS NULL OR [carpeta_numeroDespacho] = @carpeta_numeroDespacho)";
                    command.Parameters.AddWithValue("@carpeta_ID", id == 0 ? SqlInt32.Null : id);
                    command.Parameters.AddWithValue("@carpeta_numeroDespacho", numeroDespacho == null ? SqlString.Null : numeroDespacho);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            var funcionario = new Transversales.FuncionariosContext().Obtener(dr["RUTFuncionario"].ToString());
                            var cliente = new Transversales.Consumidores.ClientesContext().Obtener(null, null, (int)(dr.GetDecimal(22)), dr.GetString(21));
                            var seguimiento = new CarpetasSeguimientosContext().ObtenerActual(id);
                            var seguimientos = new CarpetasSeguimientosContext().ObtenerTodos(id);

                            var carpeta = new Carpeta
                            {
                                Id = int.Parse(dr["carpeta_ID"].ToString()),
                                AduanaSolicitante = new Transversales.AduanasContext().Obtener(new Browne.Core.Modelo.Transversales.Aduana
                                {
                                    Codigo = dr["carpeta_aduanaSolicitante"].ToString()
                                }),
                                Fecha = DateTime.Parse(dr["carpeta_fecha"].ToString()),
                                NumeroDespacho = dr["carpeta_numeroDespacho"].ToString(),
                                Observacion = dr["carpeta_observacion"].ToString(),
                                Solicitante = dr["carpeta_solicitante"].ToString(),
                                NumeroSolicitud = dr["carpeta_numeroSolicitud"].ToString(),
                                FechaFirma = dr.IsDBNull(dr.GetOrdinal("carpeta_fechaFirma")) ? null : (DateTime?)(DateTime.Parse(dr["carpeta_fechaFirma"].ToString())),
                                Uri = dr["carpeta_URI"].ToString(),
                                //Zip = 
                                ZipFirmado = dr["carpeta_sobre"].ToString(),
                                CantidadDocumentos = (int)dr["carpeta_cantidadDocumentos"],
                                Tamanio = dr.IsDBNull(dr.GetOrdinal("carpeta_tamanio")) ? 0 : int.Parse(dr["carpeta_tamanio"].ToString()),
                                HashMD5 = dr["carpeta_HashMD5"].ToString(),
                                FuncionarioSolicitante = funcionario,
                                CreacionUsuario = dr["creacion_usuario"].ToString(),
                                CreacionFecha = dr.GetDateTime(5),
                                CreacionIp = dr["creacion_IP"].ToString(),
                                ActualizacionUsuario = dr["actualizacion_usuario"].ToString(),
                                // TODO: ActualizacionFecha = (DateTime)dr["actualizacion_fecha"],
                                ActualizacionIp = dr["actualizacion_IP"].ToString(),
                                Cliente = cliente,
                                SeguimientoActual = seguimiento,
                                LstArchivos = new List<Archivo>(),
                                LstSeguimiento = seguimientos
                            };

                            do
                            {
                                carpeta.LstArchivos.Add(new Archivo
                                {
                                    Id = (int)dr["carpetaArchivo_ID"],
                                    Documento = new DocumentoDigitalizado
                                    {
                                        Id = int.Parse(dr["documentoDigitalizado_ID"].ToString()),
                                        TipoDocumento = new Browne.Core.Modelo.Transversales.TipoDocumento
                                        {
                                            Nombre = dr["DOC_NOMBRE"].ToString()
                                        },
                                        Cantidad = int.Parse(dr["documentoDigitalizado_cantidadDocumentos"].ToString()),
                                        Orden = int.Parse(dr["documentoDigitalizado_orden"].ToString()),
                                        Ruta = dr["documentoDigitalizado_web"].ToString().Trim(),
                                        Nombre = dr["documentoDigitalizado_nombreArchivo"].ToString().Trim(),
                                        FechaDigitalizacion = DateTime.Parse(dr["documentoDigitalizado_fecha"].ToString()),
                                        Funcionario = new Browne.Core.Modelo.Transversales.Funcionario
                                        {
                                            Rut = dr["documentoDigitalizado_rutDigitalizador"].ToString(),
                                            Nombre = dr["FUNC_NOMBRES"].ToString(),
                                            Apellido = dr["FUNC_APELLIDO"].ToString()
                                        }
                                    },
                                    Nombre = dr["carpetaArchivo_nombre"].ToString(),
                                    Version = int.Parse(dr["carpetaArchivo_version"].ToString()),
                                    HashMD5 = dr["carpetaArchivo_HashMD5"].ToString(),
                                    Fecha = DateTime.Parse(dr["carpetaArchivo_fecha"].ToString()),
                                    Tamanio = decimal.Parse(dr["carpetaArchivo_tamanio"].ToString()),
                                    TamanioSinFirmar = decimal.Parse(dr["carpetaArchivo_tamanioSinFirmar"].ToString()),
                                    HashMD5SinFirmar = dr["carpetaArchivo_HashMD5SinFirmar"].ToString(),
                                    Datos = dr["carpetaArchivo_datos"].ToString(),
                                    ReferenciaArchivoId = dr["documentoDigitalizado_ID"] as int?
                                });

                            } while (dr.Read());

                            return carpeta;
                        }
                    }
                }
            }

            throw new Exception("No se encontró la carpeta buscada.");
        }

        public Carpeta Obtener(Carpeta carpeta, bool cargarDetalle = true)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_carpetas_seleccionar";
                    command.Parameters.AddWithValue("@carpeta_ID", carpeta.Id == 0 ? SqlInt32.Null : carpeta.Id);
                    command.Parameters.AddWithValue("@NumeroDespacho", carpeta.NumeroDespacho == null ? SqlString.Null : carpeta.NumeroDespacho);
                    command.Parameters.AddWithValue("@carpeta_tokenAccesoTemporal", carpeta.TokenAccesoTemporal == null ? SqlString.Null : carpeta.TokenAccesoTemporal);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            return Cargar(dr, cargarDetalle);
                        }
                    }
                }
            }

            throw new Exception("No se encontró la carpeta buscada.");
        }

        /// <summary>
        /// Obtiene listado de carpetas entre una fechas
        /// </summary>
        /// <param name="desde"></param>
        /// <param name="hasta"></param>
        /// <returns></returns>
        public IEnumerable<Carpeta> ObtenerTodas(DateTime? desde, DateTime? hasta)
        {
            List<Carpeta> carpetas = new List<Carpeta>();

            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_carpetas_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@fecha_desde", SqlDbType.Date).Value = desde;
                cmd.Parameters.Add("@fecha_hasta", SqlDbType.Date).Value = hasta;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Carpeta carpeta = Cargar(dr, false);
                        yield return carpeta;
                    }
                }
                cmd.Connection.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numero"></param>
        /// <returns></returns>
        public Carpeta ObtenerFirmadaPorNumeroDeDespacho(string numero)
        {
            var carpeta = new Browne.Core.Modelo.Digitalizacion.Carpeta();

            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_carpetas_seleccionar";
                    command.Parameters.Add("@NumeroDespacho", SqlDbType.VarChar, 10).Value = numero;
                    command.Parameters.Add("@EstaFirmada", SqlDbType.Bit).Value = 1;

                    connection.Open();

                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            carpeta = Cargar(dr);
                        }
                    }
                    command.Connection.Close();
                }
            }

            return carpeta;
        }

        public void Guardar(Carpeta carpeta)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = carpeta.Id;
                        command.Parameters.Add("@aduana_ID", SqlDbType.Int).Value = carpeta.AduanaSolicitante.Id;
                        command.Parameters.Add("@carpeta_aduanaSolicitante", SqlDbType.VarChar, 6).Value = carpeta.AduanaSolicitante.Codigo;
                        command.Parameters.Add("@agente_ID", SqlDbType.Int).Value = SqlInt32.Null;
                        command.Parameters.Add("@agen_codAgente", SqlDbType.VarChar, 6).Value = carpeta.Agente.Codigo ?? SqlString.Null;
                        command.Parameters.Add("@carpeta_fecha", SqlDbType.Date).Value = carpeta.Fecha;
                        command.Parameters.Add("@carpeta_numeroDespacho", SqlDbType.VarChar, 10).Value = carpeta.NumeroDespacho;
                        command.Parameters.Add("@carpeta_observacion", SqlDbType.VarChar, 250).Value = string.IsNullOrWhiteSpace(carpeta.Observacion) ? SqlString.Null : carpeta.Observacion;
                        command.Parameters.Add("@carpeta_solicitante", SqlDbType.VarChar, 50).Value = carpeta.Solicitante;
                        command.Parameters.Add("@carpeta_numeroSolicitud", SqlDbType.VarChar, 15).Value = carpeta.NumeroSolicitud;
                        command.Parameters.Add("@carpeta_fechaFirma", SqlDbType.DateTime).Value = carpeta.FechaFirma ?? SqlDateTime.Null;
                        command.Parameters.Add("@carpeta_URI", SqlDbType.VarChar, 100).Value = string.IsNullOrWhiteSpace(carpeta.Uri) ? SqlString.Null : carpeta.Uri;
                        command.Parameters.Add("@carpeta_sobre", SqlDbType.VarChar, -1).Value = string.IsNullOrWhiteSpace(carpeta.ZipFirmado) ? SqlString.Null : carpeta.ZipFirmado;
                        // TODO: Cantidad de documentos debería ser nula al inicio.
                        //       Modificar acceso a datos y definición de la tabla.
                        //       No asignar valor por defecto; carece de sentido.
                        command.Parameters.Add("@carpeta_cantidadDocumentos", SqlDbType.Int).Value = carpeta.LstArchivos.ToList().Count;
                        command.Parameters.Add("@carpeta_tamanio", SqlDbType.Int).Value = carpeta.Tamanio;
                        command.Parameters.Add("@carpeta_HashMD5", SqlDbType.VarChar, -1).Value = string.IsNullOrWhiteSpace(carpeta.HashMD5) ? SqlString.Null : carpeta.HashMD5;
                        command.Parameters.Add("@RUTFuncionario", SqlDbType.VarChar, 12).Value = carpeta.FuncionarioSolicitante.Rut;
                        command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = carpeta.CreacionUsuario;
                        command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = carpeta.CreacionIp;
                        command.Connection = connection;
                        command.Transaction = transaction;

                        if (carpeta.Id > 0)
                        {
                            command.CommandText = "prc_carpetas_actualizar";
                            command.Parameters.AddWithValue("@carpeta_tokenAccesoTemporal", carpeta.TokenAccesoTemporal);
                            command.Parameters["@usuario"].Value = carpeta.ActualizacionUsuario;
                            command.Parameters["@IP"].Value = carpeta.ActualizacionIp;
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_carpetasArchivos_eliminar", connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = carpeta.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            command.CommandText = "prc_carpetas_insertar";

                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    carpeta.Id = (int)dr["carpeta_ID"];
                            }
                        }

                        using (SqlCommand commandPagina = new SqlCommand("prc_carpetasArchivos_insertar", connection))
                        {
                            commandPagina.CommandType = CommandType.StoredProcedure;
                            commandPagina.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = carpeta.Id;
                            commandPagina.Parameters.Add("@documentoDigitalizado_ID", SqlDbType.Int);
                            commandPagina.Parameters.Add("@carpetaArchivo_nombre", SqlDbType.VarChar, 100);
                            commandPagina.Parameters.Add("@carpetaArchivo_version", SqlDbType.Int);
                            commandPagina.Parameters.Add("@carpetaArchivo_HashMD5", SqlDbType.NVarChar, -1);
                            commandPagina.Parameters.Add("@carpetaArchivo_fecha", SqlDbType.DateTime);
                            commandPagina.Parameters.Add("@carpetaArchivo_tamanio", SqlDbType.Int);
                            commandPagina.Parameters.Add("@carpetaArchivo_tamanioSinFirmar", SqlDbType.Int);
                            commandPagina.Parameters.Add("@carpetaArchivo_HashMD5SinFirmar", SqlDbType.NVarChar, -1);
                            commandPagina.Parameters.Add("@carpetaArchivo_Datos", SqlDbType.NVarChar, -1);
                            commandPagina.Transaction = transaction;

                            foreach (Archivo archivo in carpeta.LstArchivos)
                            {
                                commandPagina.Parameters["@documentoDigitalizado_ID"].Value = archivo.Documento.Id;
                                commandPagina.Parameters["@carpetaArchivo_nombre"].Value = archivo.Nombre;
                                commandPagina.Parameters["@carpetaArchivo_version"].Value = archivo.Version;
                                commandPagina.Parameters["@carpetaArchivo_HashMD5"].Value = archivo.HashMD5;
                                commandPagina.Parameters["@carpetaArchivo_fecha"].Value = archivo.Fecha;
                                commandPagina.Parameters["@carpetaArchivo_tamanio"].Value = archivo.Tamanio;
                                commandPagina.Parameters["@carpetaArchivo_tamanioSinFirmar"].Value = archivo.TamanioSinFirmar;
                                commandPagina.Parameters["@carpetaArchivo_HashMD5SinFirmar"].Value = archivo.HashMD5SinFirmar;
                                commandPagina.Parameters["@carpetaArchivo_Datos"].Value = archivo.Datos;

                                commandPagina.ExecuteNonQuery();
                            }
                        }
                    }
                    using (var cmdSeguimiento = connection.CreateCommand())
                    {
                        cmdSeguimiento.Transaction = transaction;

                        cmdSeguimiento.CommandType = CommandType.StoredProcedure;
                        cmdSeguimiento.Parameters.Add("@seguimientoCarpeta_ID", SqlDbType.Int).Value = SqlInt32.Null;
                        cmdSeguimiento.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = carpeta.Id;
                        cmdSeguimiento.Parameters.Add("@estadoCarpeta_ID", SqlDbType.Int).Value = carpeta.SeguimientoActual.Estado.Id;
                        cmdSeguimiento.Parameters.Add("@seguimientoCarpeta_observacion", SqlDbType.NVarChar, 250).Value = carpeta.SeguimientoActual.Observacion;
                        cmdSeguimiento.Parameters.Add("@creacion_usuario", SqlDbType.NVarChar, 50).Value = carpeta.SeguimientoActual.CreacionUsuario;
                        cmdSeguimiento.Parameters.Add("@creacion_IP", SqlDbType.NVarChar, 50).Value = carpeta.SeguimientoActual.CreacionIp;

                        cmdSeguimiento.CommandText = "prc_seguimientosCarpetas_insertar";

                        carpeta.SeguimientoActual.Id = (int)(cmdSeguimiento.ExecuteScalar());
                    }

                    transaction.Commit();

                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en CarpetasContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }

        private Carpeta Cargar(SqlDataReader dr, bool cargarDetalle = true)
        {
            var id = int.Parse(dr["carpeta_ID"].ToString());

            return new Carpeta
            {
                Id = id,
                AduanaSolicitante = new Transversales.AduanasContext().Obtener(new Browne.Core.Modelo.Transversales.Aduana
                {
                    Codigo = dr["carpeta_aduanaSolicitante"].ToString()
                }),
                Fecha = (DateTime)dr["carpeta_fecha"],
                NumeroDespacho = dr["carpeta_numeroDespacho"].ToString(),
                Observacion = dr["carpeta_observacion"].ToString(),
                Solicitante = dr["carpeta_solicitante"].ToString(),
                NumeroSolicitud = dr["carpeta_numeroSolicitud"].ToString(),
                FechaFirma = dr.IsDBNull(dr.GetOrdinal("carpeta_fechaFirma")) ? null : (DateTime?)(DateTime.Parse(dr["carpeta_fechaFirma"].ToString())),
                Uri = dr["carpeta_URI"].ToString(),
                ZipFirmado = dr["carpeta_sobre"].ToString(),
                CantidadDocumentos = (int)dr["carpeta_cantidadDocumentos"],
                Tamanio = dr.IsDBNull(dr.GetOrdinal("carpeta_tamanio")) ? 0 : int.Parse(dr["carpeta_tamanio"].ToString()),
                HashMD5 = dr["carpeta_HashMD5"].ToString(),
                FuncionarioSolicitante = new Transversales.FuncionariosContext().Obtener(dr["RUTFuncionario"].ToString()),
                CreacionUsuario = dr["creacion_usuario"].ToString(),
                CreacionFecha = DateTime.Parse(dr["carpeta_fecha"].ToString()),
                CreacionIp = dr["creacion_IP"].ToString(),
                ActualizacionUsuario = dr["actualizacion_usuario"].ToString(),
                // TODO: ActualizacionFecha = (DateTime)dr["actualizacion_fecha"],
                ActualizacionIp = dr["actualizacion_IP"].ToString(),
                Cliente = new Transversales.Consumidores.ClientesContext().Obtener(null, null, int.Parse(dr["Numero_Sucursal"].ToString()), dr["Rut_Cliente"].ToString()),
                SeguimientoActual = new CarpetasSeguimientosContext().ObtenerActual(id),
                LstArchivos = cargarDetalle ? new ArchivosContext().ObtenerTodos(id).ToList() : new List<Archivo>(),
                LstSeguimiento = cargarDetalle ? new CarpetasSeguimientosContext().ObtenerTodos(id) : new List<CarpetaSeguimiento>(),
                TokenAccesoTemporal = dr.IsDBNull(dr.GetOrdinal("carpeta_tokenAccesoTemporal")) ? null : dr["carpeta_tokenAccesoTemporal"].ToString()
            };
        }
    }
}
