﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Digitalizacion;
using System.Data.SqlTypes;

namespace AccesoDatos.Contextos.Digitalizacion
{
    public class CertificadosContext
    {
        private string connectionString = string.Empty;

        public CertificadosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public CertificadosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_digitalizacion"].ConnectionString;

        }

        public IEnumerable<Certificado> ObteneTodos(bool activo = true)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_certificados_seleccionar";
                    command.Parameters.AddWithValue("@certificado_activo", activo);
                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                yield return Cargar(dr);
                            }
                        }
                    }
                }
            }
        }

        public IEnumerable<Certificado> ObteneTodos(Certificado c)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_certificados_seleccionar";

                    // TODO Completar los demás parámetros!
                    command.Parameters.AddWithValue("@tipoCertificado_ID", c.TipoCertificado != null ? c.TipoCertificado.Id : SqlInt32.Null);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                yield return Cargar(dr);
                            }
                        }
                    }
                }
            }
        }

        public void Guardar(Certificado certificado)
        {

            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@certificado_ID", SqlDbType.Int).Value = certificado.Id;
                command.Parameters.Add("@tipoCertificado_ID", SqlDbType.Int).Value = certificado.TipoCertificado.Id;
                //command.Parameters.Add("@agente_ID", SqlDbType.Int).Value = certificado.Agente.Id;
                command.Parameters.Add("@codigoAgente", SqlDbType.VarChar, 6).Value = certificado.Agente.Codigo;
                command.Parameters.Add("@certificado_fechaInicioVigencia", SqlDbType.DateTime).Value = certificado.FechaInicioVigencia;
                command.Parameters.Add("@certificado_fechaTerminoVigencia", SqlDbType.DateTime).Value = certificado.FechaTerminoVigencia;
                command.Parameters.Add("@certificado_fechaVencimiento", SqlDbType.DateTime).Value = certificado.FechaVencimiento;
                command.Parameters.Add("@certificado_nombreAlternativo", SqlDbType.VarChar, 50).Value = certificado.NombreAlternativo;
                command.Parameters.Add("@certificado_data", SqlDbType.VarBinary, 5000).Value = certificado.Data;
                command.Parameters.Add("@certificado_activo", SqlDbType.Bit).Value = certificado.Activo;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = certificado.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = certificado.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (certificado.Id > 0)
                    {
                        command.CommandText = "prc_certificados_actualizar";
                        command.Parameters["@usuario"].Value = certificado.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = certificado.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_certificados_insertar";
                        command.Parameters["@certificado_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en CertificadosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        public Certificado Obtener(int id)
        {
            Certificado certificado = new Certificado();
            certificado.Id = id;
            using (SqlCommand command = new SqlCommand("prc_certificados_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@certificado_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        certificado = Cargar(dr);
                }
                command.Connection.Close();
            }
            return certificado;

        }

        public Certificado Obtener(Certificado c)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_certificados_seleccionar";
                    command.Parameters.AddWithValue("@certificado_ID", c.Id);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            return Cargar(dr);
                        }
                    }
                }
            }

            throw new Exception("Certificado no encontrado");
        }

        private Certificado Cargar(SqlDataReader dr)
        {
            return new Certificado
            {
                Id = (int)dr["certificado_ID"],
                Agente = new Transversales.AgentesContext().Obtener(dr["codigoAgente"].ToString()),
                Sujeto = dr["certificado_sujeto"].ToString(),
                FechaInicioVigencia = (DateTime)dr["certificado_fechaInicioVigencia"],
                FechaTerminoVigencia = (DateTime)dr["certificado_fechaTerminoVigencia"],
                FechaVencimiento = (DateTime)dr["certificado_fechaVencimiento"],
                NombreAlternativo = dr["certificado_nombreAlternativo"].ToString(),
                Data = (byte[])dr["certificado_data"],
                Activo = (bool)dr["certificado_activo"],
                CreacionUsuario = dr["creacion_usuario"].ToString(),
                CreacionFecha = (DateTime)dr["creacion_fecha"],
                CreacionIp = dr["creacion_IP"].ToString(),
                ActualizacionUsuario = dr["actualizacion_usuario"].ToString(),
                ActualizacionFecha = (DateTime)dr["actualizacion_fecha"],
                ActualizacionIp = dr["actualizacion_IP"].ToString(),
                TipoCertificado = new TiposCertificadosContext().Obtener((int)dr["tipoCertificado_ID"])
            };
        }
    }
}
