﻿using Browne.Core.Modelo.Digitalizacion;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos.Contextos.Digitalizacion
{
    public class ArchivosContext
    {
        private string connectionString = string.Empty;

        public ArchivosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ArchivosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["Browne_Digitalizacion"].ConnectionString;
        }
        /// <summary>
        /// Obtiene todos los archivos de una carpeta
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Archivo> ObtenerTodosOld(int id)
        {
            List<Archivo> archivos = new List<Archivo>();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_carpetasArchivos_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = id;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Archivo archivo = new Archivo();
                        archivo = Cargar(dr);
                        archivos.Add(archivo);
                    }
                }
                cmd.Connection.Close();
            }
            return archivos;
        }

        public IEnumerable<Archivo> ObtenerTodos(int id, bool flag = false)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_carpetasArchivos_seleccionar";
                    command.Parameters.AddWithValue("@carpeta_ID", id);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows)
                        {
                            while(dr.Read())
                            {
                                yield return Cargar(dr);
                            }
                        }
                    }
                }

                connection.Close();
            }
        }

        /// <summary>
        /// Elimina un archivo
        /// </summary>
        /// <param name="Archivo"></param>
        public void Eliminar(Archivo archivo)
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                SqlCommand cmd = new SqlCommand("prc_carpetasArchivos_eliminar", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@archivo_ID", SqlDbType.Int).Value = archivo.Id;
                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();

                }
                catch (Exception ex)
                {
                    throw new Exception("Error en ArchivosContext.Eliminar " + ex.ToString());
                }
            }

        }

        /// <summary>
        /// Guarda un nuevo archivo de una carpeta
        /// </summary>
        /// <param name="Archivo"></param>
        /// <param name="carpetaId"></param>
        public void Guardar(Archivo archivo, int carpetaId)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@carpetaArchivo_ID", SqlDbType.Int).Value = archivo.Id;
                command.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = carpetaId;
                command.Parameters.Add("@documentoDigitalizado_ID", SqlDbType.Int).Value = archivo.Documento.Id;
                command.Parameters.Add("@carpetaArchivo_nombre", SqlDbType.VarChar, 100).Value = archivo.Nombre;
                command.Parameters.Add("@carpetaArchivo_version", SqlDbType.Int).Value = archivo.Version;
                command.Parameters.Add("@carpetaArchivo_HashMD5", SqlDbType.VarChar).Value = archivo.HashMD5;
                command.Parameters.Add("@carpetaArchivo_fecha", SqlDbType.DateTime).Value = archivo.Fecha;
                command.Parameters.Add("@carpetaArchivo_tamanio", SqlDbType.Int).Value = archivo.Tamanio;
                command.Parameters.Add("@carpetaArchivo_tamanioSinFirmar", SqlDbType.Int).Value = archivo.TamanioSinFirmar;
                command.Parameters.Add("@carpetaArchivo_HashMD5SinFirmar", SqlDbType.VarChar).Value = archivo.HashMD5SinFirmar;
                command.Parameters.Add("@carpetaArchivo_Datos", SqlDbType.VarChar).Value = archivo.Datos;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (archivo.Id > 0)
                        command.CommandText = "prc_carpetasArchivos_actualizar";
                    else
                    {
                        command.CommandText = "prc_carpetasArchivos_insertar";
                        command.Parameters["@carpetaArchivo_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en ArchivosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }

        }

        private Archivo Cargar(SqlDataReader dr)
        {
            var archivo = new Archivo();

            archivo.Id = (int)dr["carpetaArchivo_ID"];
            archivo.Documento = new DocumentosDigitalizadosContext(this.connectionString).Obtener((int)dr["documentoDigitalizado_ID"]);
            archivo.Nombre = dr["carpetaArchivo_nombre"].ToString();
            archivo.Version = (int)dr["carpetaArchivo_version"];
            archivo.HashMD5 = dr["carpetaArchivo_HashMD5"].ToString();
            archivo.Fecha = (DateTime)dr["carpetaArchivo_fecha"];
            archivo.Tamanio = dr.GetDecimal(7);
            archivo.TamanioSinFirmar = dr.GetDecimal(8);
            archivo.HashMD5SinFirmar = dr["carpetaArchivo_HashMD5SinFirmar"].ToString();
            archivo.Datos = dr["carpetaArchivo_datos"].ToString();
            archivo.ReferenciaArchivoId = (int)dr["documentoDigitalizado_ID"] as int?;

            return archivo;
        }
    }
}
