﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Digitalizacion;
using System.Data.SqlTypes;

namespace AccesoDatos.Contextos.Digitalizacion
{
    public class TiposCertificadosContext
    {
        private string connectionString = string.Empty;

        public TiposCertificadosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposCertificadosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_digitalizacion"].ConnectionString;
        }

        public IEnumerable<TipoCertificado> ObteneTodos()
        {
            List<TipoCertificado> tiposCertificados = new List<TipoCertificado>();
            using (SqlCommand command = new SqlCommand("prc_tiposCertificados_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoCertificado tipoCertificado = Cargar(dr);
                        yield return tipoCertificado;
                    }
                }
                command.Connection.Close();
            }
        }
        public TipoCertificado Obtener(int id, string tipo = "CAE")
        {
            TipoCertificado tipoCertificado = new TipoCertificado();
            using (SqlCommand command = new SqlCommand("prc_tiposCerticados_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoCertificado_ID", SqlDbType.Int).Value = id;
                command.Parameters.Add("@tipoCertificado_nombre", SqlDbType.VarChar, 50).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoCertificado = Cargar(dr);
                }
                command.Connection.Close();
            }
            return tipoCertificado;
        }

        public TipoCertificado Obtener(TipoCertificado t)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_tiposCerticados_seleccionar";

                    command.Parameters.AddWithValue("@tipoCertificado_nombre", t.Nombre ?? SqlString.Null);
                    command.Parameters.AddWithValue("@tipoCertificado_ID", t.Id == 0 ? SqlInt32.Null : t.Id);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            return new TipoCertificado
                            {
                                Id = dr.IsDBNull(0) ? 0 : dr.GetInt32(0),
                                Nombre = dr.IsDBNull(1) ? null : dr.GetString(1)
                            };
                        }
                    }
                }
            }

            throw new Exception("Tipo de certificado no encontrado");
        }

        public void Guardar(TipoCertificado tipoCertificado)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoCertificado_ID", SqlDbType.Int).Value = tipoCertificado.Id;
                command.Parameters.Add("@tipoCertificado_nombre", SqlDbType.VarChar, 50).Value = tipoCertificado.Nombre;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (tipoCertificado.Id > 0)
                    {
                        command.CommandText = "prc_tiposCertificados_actualizar";
                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        command.CommandText = "prc_tiposCertificados_insertar";
                        command.Parameters["@tipoCertificado_ID"].Value = null;
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows && dr.Read())
                                tipoCertificado.Id = (int)dr["tipoCertificado_ID"];
                        }
                        command.Connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TiposCertificadosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private TipoCertificado Cargar(SqlDataReader dr)
        {
            TipoCertificado tipoCertificado = new TipoCertificado();
            tipoCertificado.Id = (int)dr["tipoCertificado_ID"];
            tipoCertificado.Nombre = dr["tipoCertificado_nombre"].ToString();
            return tipoCertificado;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
