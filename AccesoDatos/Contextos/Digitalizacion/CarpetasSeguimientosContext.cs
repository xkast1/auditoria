﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Digitalizacion;


namespace AccesoDatos.Contextos.Digitalizacion
{
    public class CarpetasSeguimientosContext
    {
        private string connectionString = string.Empty;

        public CarpetasSeguimientosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public CarpetasSeguimientosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["Browne_Digitalizacion"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado de seguimiento de una carpeta, obtenido a partir de id de la carpeta
        /// </summary>
        /// <param name="carpetaID"></param>
        /// <returns></returns>
        public IEnumerable<CarpetaSeguimiento> ObtenerTodos(int carpetaID)
        {
            List<CarpetaSeguimiento> seguimientos = new List<CarpetaSeguimiento>();
            using (SqlCommand command = new SqlCommand("prc_seguimientosCarpetas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = carpetaID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        CarpetaSeguimiento Seguimiento = Cargar(dr);
                        yield return Seguimiento;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene el seguimiento actual de una carpeta especifica a partir de su Id
        /// </summary>
        /// <param name="carpetaId"></param>
        /// <returns></returns>
        public CarpetaSeguimiento ObtenerActual(int carpetaId)
        {
            CarpetaSeguimiento Seguimiento = new CarpetaSeguimiento();
            using (SqlCommand command = new SqlCommand("prc_seguimientosCarpetas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = carpetaId;
                command.Parameters.Add("@actual", SqlDbType.Bit).Value = true;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        Seguimiento = Cargar(dr);
                    }
                }
                command.Connection.Close();
            }
            return Seguimiento;
        }
        /// <summary>
        /// Almancena un nuevo seguimiento de la carpeta
        /// </summary>
        /// <param name="Seguimiento"></param>
        /// <param name="carpetaId"></param>
        public void Guardar(CarpetaSeguimiento seguimiento, int carpetaId)
        {

            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@seguimientoCarpeta_ID", SqlDbType.Int).Value = seguimiento.Id;
                command.Parameters.Add("@carpeta_ID", SqlDbType.Int).Value = carpetaId;
                command.Parameters.Add("@estadoCarpeta_ID", SqlDbType.Int).Value = seguimiento.Estado.Id;
                command.Parameters.Add("@seguimientoCarpeta_observacion", SqlDbType.NVarChar, 250).Value = seguimiento.Observacion;
                command.Parameters.Add("@creacion_usuario", SqlDbType.NVarChar, 50).Value = seguimiento.CreacionUsuario;
                command.Parameters.Add("@creacion_IP", SqlDbType.NVarChar, 50).Value = seguimiento.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (seguimiento.Id > 0)
                        command.CommandText = "prc_seguimientosCarpetas_actualizar";
                    else
                    {
                        command.CommandText = "prc_seguimientosCarpetas_insertar";
                        command.Parameters["@seguimientoCarpeta_ID"].Value = null;
                    }

                    // Se obtiene el identificador asignado al nuevo seguimiento.
                    seguimiento.Id = (int)(command.ExecuteScalar());
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en CarpetasSeguimientosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        private CarpetaSeguimiento Cargar(SqlDataReader dr)
        {
            CarpetaSeguimiento Seguimiento = new CarpetaSeguimiento();
            Seguimiento.Id = (int)dr["seguimientoCarpeta_ID"];
            Seguimiento.Estado = new CarpetasEstadosContext()
                .Obtener((int)dr["estadoCarpeta_ID"]);
            Seguimiento.Fecha = (DateTime)dr["seguimientoCarpeta_fecha"];
            Seguimiento.Observacion = dr["seguimientoCarpeta_observacion"].ToString().Trim();
            Seguimiento.CreacionUsuario = dr["creacion_usuario"].ToString().Trim();
            Seguimiento.CreacionIp = dr["creacion_IP"].ToString().Trim();

            return Seguimiento;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }

    }
}
