﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Exportaciones.DocumentoUnicoSalida
{
    public class DusContext
    {
        private string connectionString = string.Empty;

        public DusContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public DusContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        /// <summary>
        /// Busqueda de un DUS
        /// </summary>
        /// <param name="buscado"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado)
        {
            if (!buscado.StartsWith("E"))
                buscado = "E-" + buscado;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("select busqueda"), conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@busqueda", SqlDbType.NVarChar, 50).Value = buscado.Trim() + "%";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar Item = new ResultadoAutocompletar();
                        Item.Id = dr["entr_correlativo"].ToString().Trim();
                        Item.Value = dr["entr_numdesping"].ToString().Trim();
                        Item.Label = dr["entr_numdesping"].ToString().Trim() + " | " + String.Format("{0:d}", dr["PARA_DESPARAM"]).ToUpper() + " | " + String.Format("{0:d}", dr["CLIE_RAZONSOCIA"]).ToUpper();
                        yield return Item;
                    }
                }
                cmd.Connection.Close();
            }
        }
        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into ( 
                                   ) values 
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update  set                                    
                                       
                                    where ";
                    break;

                case "delete":
                    strQuery = @"Delete From ";
                    break;

                case "select busqueda":
                    strQuery = @"select 
                                    top 10
                                    DUS_CABECERA.NUMDESPACHEGRE
                                    ,PARAMETROS.PARA_DESPARAM
                                    ,cliente.CLIE_RAZONSOCIA

                                    from DUS_CABECERA
                                    inner join DESPACHO_ANEXOEXP
                                    on DESPACHO_ANEXOEXP.NUMDESPACHEGRE = DUS_CABECERA.NUMDESPACHEGRE

                                    inner join Aduana_desarrollo.dbo.CLIENTE as cliente
                                    on cliente.RUTCLIENTE = DESPACHO_ANEXOEXP.RUTCLIENTE
                                    and cliente.NUMSUCURSAL = DESPACHO_ANEXOEXP.NUMSUCURSAL

                                    inner join  Aduana_desarrollo.dbo.PARAMETROS as parametros
                                    on parametros.TAPA_CODTABLA = 47
                                    and parametros.PARA_CODPARAM = DUS_CABECERA.TIPO_OPERACION

                                    Where DUS_CABECERA.NUMDESPACHEGRE like @busqueda order by DUS_CABECERA.NUMDESPACHEGRE"
                                    ;
                    break;

                default:
                    strQuery = @"select
	                                                                                                
                                    FROM ingreso  ";
                    break;
            }


            return strQuery;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
