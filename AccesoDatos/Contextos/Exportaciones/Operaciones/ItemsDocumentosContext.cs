﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales.Operacion;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class ItemsDocumentosContext
    {
        private string connectionString = string.Empty;

        public ItemsDocumentosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ItemsDocumentosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<Documento> ObtenerTodos(int idItem)
        {

            using (SqlCommand command = new SqlCommand("prc_itemsDocumentos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@item_ID", SqlDbType.Int).Value = idItem;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Documento documento = Cargar(dr);
                        yield return documento;
                    }
                }
                command.Connection.Close();
            }
        }
        public Documento Cargar(SqlDataReader dr) {
            Documento documento = new Documento();
            documento.Id = (int)dr["itemDocumento_ID"];
            documento.Secuencia = (int)dr["itemDocumento_secuencia"];
            documento.Nombre = dr["itemDocumento_nombre"] != DBNull.Value ? dr["itemDocumento_nombre"].ToString() : "";
            documento.Numero = dr["itemDocumento_numero"] != DBNull.Value ? dr["itemDocumento_numero"].ToString() : "";
            documento.CodigoTipo = dr["itemDocumento_CodigoTipo"] != DBNull.Value ? dr["itemDocumento_CodigoTipo"].ToString() : "";
            documento.Emisor = dr["itemDocumento_emisor"] != DBNull.Value ? dr["itemDocumento_emisor"].ToString() : "";
            documento.Fecha = dr["itemDocumento_fecha"] != DBNull.Value ? (DateTime?)dr["itemDocumento_fecha"] : null;
            documento.Comentario = dr["itemDocumento_comentario"] != DBNull.Value ? dr["itemDocumento_comentario"].ToString() : "";

            return documento;
        }
    }
}
