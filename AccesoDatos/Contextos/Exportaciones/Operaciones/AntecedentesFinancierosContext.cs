﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Exportaciones.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class AntecedentesFinancierosContext
    {
        private string connectionString = string.Empty;

        public AntecedentesFinancierosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public AntecedentesFinancierosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<AntecedenteFinanciero> ObtenerTodos(string numeroOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesAntecedentesFinancieros_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        AntecedenteFinanciero antecedente = Cargar(dr);
                        yield return antecedente;
                    }
                }
                command.Connection.Close();
            }
        }
        public AntecedenteFinanciero Obtener( string numeroOperacion)
        {
            AntecedenteFinanciero antecedenteFinanciero = new AntecedenteFinanciero();
            using (SqlCommand command = new SqlCommand("prc_operacionesAntecedentesFinancieros_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;

                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        antecedenteFinanciero = Cargar(dr);
                }
                command.Connection.Close();
            }

            return antecedenteFinanciero;
        }
        public AntecedenteFinanciero Cargar(SqlDataReader dr)
        {
            AntecedenteFinanciero antecedenteFinanciero = new AntecedenteFinanciero();
            antecedenteFinanciero.Id = (int)dr["operacionAntecedenteFinancerio_ID"];
            antecedenteFinanciero.NumeroFactura = dr["operacionAntecedenteFinancerio_numeroFactura"] != DBNull.Value ? dr["operacionAntecedenteFinancerio_numeroFactura"].ToString() : "";
            antecedenteFinanciero.FechaFactura = dr["operacionAntecedenteFinancerio_fechaFactura"] != DBNull.Value ? (DateTime?)dr["operacionAntecedenteFinancerio_fechaFactura"] : null;
            antecedenteFinanciero.TasaCambio = dr["operacionAntecedenteFinancerio_tasaCambio"] != DBNull.Value ? (decimal)dr["operacionAntecedenteFinancerio_tasaCambio"] : 0;
            antecedenteFinanciero.NumeroCuotaPagoDiferido = dr["operacionAntecedenteFinancerio_numeroDeCuotasPagoDiferido"] != DBNull.Value ? dr["operacionAntecedenteFinancerio_NumeroDeCuotasPagoDiferido"].ToString() : "";
            antecedenteFinanciero.FechaPago = dr["operacionAntecedenteFinancerio_fechaPago"] != DBNull.Value ? (DateTime?)dr["operacionAntecedenteFinancerio_fechaPago"] : null;
            antecedenteFinanciero.CodigoFlete = dr["operacionAntecedenteFinancerio_codigoFlete"] != DBNull.Value ? dr["operacionAntecedenteFinancerio_codigoFlete"].ToString() : "";
            antecedenteFinanciero.CodigoSeguro = dr["operacionAntecedenteFinancerio_codigoSeguro"] != DBNull.Value ? dr["operacionAntecedenteFinancerio_codigoSeguro"].ToString() : "";
            /**************************************/
            //revisar si es objeto
            antecedenteFinanciero.CodigoFabricante = dr["operacionAntecedenteFinancerio_codigoFabricante"] != DBNull.Value ? dr["operacionAntecedenteFinancerio_codigoFabricante"].ToString() : "";
            antecedenteFinanciero.NombreFabricante = dr["operacionAntecedenteFinancerio_nombreFabricante"] != DBNull.Value ? dr["operacionAntecedenteFinancerio_nombreFabricante"].ToString() : "";
            antecedenteFinanciero.DireccionFabricante = dr["operacionAntecedenteFinancerio_direccionFabricante"] != DBNull.Value ? dr["operacionAntecedenteFinancerio_direccionFabricante"].ToString() : "";
            /**************************************/
            antecedenteFinanciero.FacturaComercialDefinitiva = dr["operacionAntecedenteFinancerio_facturaComercialDefinitiva"] != DBNull.Value ? (bool?)dr["operacionAntecedenteFinancerio_facturaComercialDefinitiva"] : false;
            antecedenteFinanciero.ValorFob = dr["operacionAntecedenteFinancerio_valorFob"] != DBNull.Value ? (decimal)dr["operacionAntecedenteFinancerio_valorFob"] : 0;
            antecedenteFinanciero.ComisionesExterior = dr["operacionAntecedenteFinancerio_comisionExterior"] != DBNull.Value ? (decimal)dr["operacionAntecedenteFinancerio_comisionExterior"] : 0;
            antecedenteFinanciero.Descuento = dr["operacionAntecedenteFinancerio_descuento"] != DBNull.Value ? (decimal)dr["operacionAntecedenteFinancerio_descuento"] : 0;
            antecedenteFinanciero.ValorDelFlete = dr["operacionAntecedenteFinancerio_valorFlete"] != DBNull.Value ? (decimal)dr["operacionAntecedenteFinancerio_valorFlete"] : 0;
            antecedenteFinanciero.ValorDelSeguro = dr["operacionAntecedenteFinancerio_valorSeguro"] != null ? (decimal)dr["operacionAntecedenteFinancerio_valorSeguro"] : 0;
            antecedenteFinanciero.OtrosGastosDeducible = dr["operacionAntecedenteFinancerio_otrosGastosDeducible"] != DBNull.Value ? (decimal)dr["operacionAntecedenteFinancerio_otrosGastosDeducible"] : 0;
            antecedenteFinanciero.ValorLiquidoRetorno = dr["operacionAntecedenteFinancerio_valorLiquidoRetorno"] != DBNull.Value ? (decimal)dr["operacionAntecedenteFinancerio_valorLiquidoRetorno"] : 0;
            antecedenteFinanciero.ValorCIFUSD = dr["operacionAntecedenteFinancerio_valorCIFUSD"] != DBNull.Value ? (decimal)dr["operacionAntecedenteFinancerio_valorCIFUSD"] : 0;

            if (dr["pais_codigo"] != DBNull.Value)
            {
                Pais paisAdquisicion = new Pais();
                paisAdquisicion.Codigo =dr["pais_codigo"].ToString();
                antecedenteFinanciero.PaisAdquisicion = new AccesoDatos.Contextos.Transversales.PaisesContext().Obtener(paisAdquisicion);
            }
            if (dr["moneda_codigo"] != DBNull.Value)
            {
                Moneda moneda = new Moneda();
                moneda.Codigo = dr["moneda_codigo"].ToString();
                antecedenteFinanciero.Moneda = new AccesoDatos.Contextos.Transversales.MonedasContext().Obtener(moneda);
            }
            if (dr["modalidadVenta_codigo"] != DBNull.Value)
            {
                ModalidadVenta modalidadVenta = new ModalidadVenta();
                modalidadVenta.Codigo = dr["modalidadVenta_codigo"].ToString();
                antecedenteFinanciero.ModalidadVenta = new AccesoDatos.Contextos.Transversales.ModalidadesVentasContext().Obtener(modalidadVenta);
            }
            if (dr["clausulaCompraVenta_codigo"] != DBNull.Value)
            {
                ClausulaCompraVenta clausulaCompraVenta = new ClausulaCompraVenta();
                clausulaCompraVenta.Codigo = dr["clausulaCompraVenta_codigo"].ToString();
                antecedenteFinanciero.ClausulaCompraVenta = new AccesoDatos.Contextos.Transversales.ClausulasComprasVentasContext().Obtener(clausulaCompraVenta);
            }
            if (dr["formaPago_codigo"] != DBNull.Value)
            {
                FormaPago formaPago = new FormaPago();
                formaPago.Codigo = dr["formaPago_codigo"].ToString();
                antecedenteFinanciero.FormaPago = new AccesoDatos.Contextos.Transversales.FormasPagosContext().Obtener(formaPago);
            }
            return antecedenteFinanciero;
        }
    }
}
