﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.ProductoServicio;
using Browne.Core.Modelo.Exportaciones.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class ItemsContext
    {
        private string connectionString = string.Empty;

        public ItemsContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ItemsContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<Item> ObtenerTodos(string numeroOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesItems_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Item item = Cargar(dr);
                        yield return item;
                    }
                }
                command.Connection.Close();
            }
        }

        private Item Cargar(SqlDataReader dr)
        {
            Item item = new Item();
            item.Id = (int)dr["operacionItem_ID"];
            item.Secuencia = dr["operacionItem_secuencia"] != DBNull.Value ? (int?)dr["operacionItem_secuencia"] : null;
            item.Codigo = dr["operacionItem_codigo"] != DBNull.Value ? dr["operacionitem_codigo"].ToString() : "";
            item.CodigoSicex = dr["operacionitem_codigoSicex"] != DBNull.Value ? dr["operacionitem_codigoSicex"].ToString() : "";
            //item.Descripcion = dr["operacionitem_descripcion"] != DBNull.Value ? dr["operacionitem_descripcion"].ToString() : "";
            item.OtraDescripcion = dr["operacionItem_otraDescripcion"] != DBNull.Value ? dr["operacionItem_otraDescripcion"].ToString() : "";
            item.DescripcionAdicional = dr["operacionItem_descripcionAdicional"] != DBNull.Value ? dr["operacionItem_descripcionAdicional"].ToString() : "";
            item.CodigoArancel = dr["operacionitem_codigoArancel"] != DBNull.Value ? dr["operacionitem_codigoArancel"].ToString() : "";
            item.PesoBruto = dr["operacionItem_pesoBruto"] != DBNull.Value ? (decimal)dr["operacionItem_pesoBruto"] : 0;
            item.PesoNeto = dr["operacionItem_pesoNeto"] != DBNull.Value ? (decimal)dr["operacionItem_pesoNeto"] : 0;
            item.CodigoPesoNetoUDM = dr["operacionItem_codigoPesonetoUMD"] != DBNull.Value ? dr["operacionItem_codigoPesonetoUMD"].ToString() : "";
            item.Cantidad = dr["operacionItem_cantidad"] != DBNull.Value ? (decimal)dr["operacionItem_cantidad"] : 0;
            item.Comentario = dr["operacionItem_comentario"] != DBNull.Value ? dr["operacionItem_comentario"].ToString() : "";
            item.Volumen = dr["operacionItem_volumen"] != DBNull.Value ? (decimal)dr["operacionItem_volumen"] : 0;
            item.CodigoVolumenUDM = dr["operacionItem_codigoVolumneUDM"] != DBNull.Value ? dr["operacionItem_codigoVolumneUDM"].ToString() : "";
            item.TotalObservaciones = dr["operacionItem_totalObservaciones"] != DBNull.Value ? (int)dr["operacionItem_totalObservaciones"] : 0;
            item.TotalDocumentos = dr["operacionItem_totalDocumentos"] != DBNull.Value ? (int)dr["operacionItem_totalDocumentos"] : 0;
            item.TotalAtributos = dr["operacionItem_totalAtributos"] != DBNull.Value ? (int)dr["operacionItem_totalAtributos"] : 0;
            item.TotalLotes = dr["operacionItem_totalLotes"] != DBNull.Value ? (int)dr["operacionItem_totalLotes"] : 0;

            item.CodigoArancelIUDM = dr["operacionItem_codigoArancelIUDM"] != DBNull.Value ? dr["operacionItem_codigoArancelIUDM"].ToString() : "";
            item.ValorMinimoGarantizado = dr["operacionItem_valorMinimoGarantizado"] != DBNull.Value ? (decimal)dr["operacionItem_valorMinimoGarantizado"] : 0;
            item.CodigoAcuerdoComercial = dr["operacionItem_codigoAcuerdoComercial"].ToString();
            item.ValorFobPromedio = dr["operacionItem_valorFobPromedio"] != DBNull.Value ? (bool?)dr["operacionItem_valorFobPromedio"] : null;
            item.MontoFOBUnitario = dr["operacionItem_montoFobUnitario"] != DBNull.Value ? (decimal)dr["operacionItem_montoFobUnitario"] : 0;
            item.MontoFOB = dr["operacionItem_montoFob"] != DBNull.Value ? (decimal)dr["operacionItem_montoFob"] : 0;
            item.CodigoContratoColchico = dr["operacionItem_codigoContratoColchico"] != DBNull.Value ? dr["operacionItem_codigoContratoColchico"].ToString() : "";
            item.CuotaContractual = dr["operacionItem_cuotaContractual"] != DBNull.Value ? dr["operacionItem_cuotaContractual"].ToString() : "";
            item.TotalElementos = dr["operacionItem_totalElementos"] != DBNull.Value ? (int)dr["operacionItem_totalElementos"] : 0;
            item.TotalVistosBuenos = dr["operacionItem_totalVistosBuenos"] != DBNull.Value ? (int)dr["operacionItem_totalVistosBuenos"] : 0;

            if (dr["paisOrigen_codigo"] != DBNull.Value)
            {
                Pais pais = new Pais();
                pais.Codigo = dr["paisOrigen_codigo"].ToString();
                item.PaisOrigen = new AccesoDatos.Contextos.Transversales.PaisesContext().Obtener(pais);
            }

            if (dr["moneda_codigo"] != DBNull.Value) { 
                Moneda moneda = new Moneda();
                moneda.Codigo = dr["moneda_codigo"].ToString();
                item.MonedaValorMinimoGarantizado = new Transversales.MonedasContext().Obtener(moneda);
            }

            if (dr["productoServicio_ID"] != DBNull.Value)
            {
                ProductoServicio productoServicio = new ProductoServicio();
                productoServicio.Id = (int)dr["productoServicio_ID"];
                item.ProductoServicio = new Transversales.ProductosServicios.ProductosServiciosContext().Obtener(productoServicio);
                
            }
         
            item.Observaciones = new ItemsObservacionesContext(this.connectionString).ObtenerTodos(item.Id);
            item.Documentos = new ItemsDocumentosContext(this.connectionString).ObtenerTodos(item.Id);
            item.Atributos = new ItemsAtributosContext(this.connectionString).ObtenerTodos(item.Id);
            item.Lotes = new ItemsLotesContext(this.connectionString).ObtenerTodos(item.Id);
            item.Elementos = new ItemsElementosContext(this.connectionString).ObtenerTodos(item.Id);
            
            return item;

        }
    }
}
