﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class BultosContext
    {
        private string connectionString = string.Empty;

        public BultosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public BultosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<Bulto> ObtenerTodos(string numeroOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesBultos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Bulto bulto = Cargar(dr);
                        yield return bulto;
                    }
                }
                command.Connection.Close();
            }
        }
        private Bulto Cargar(SqlDataReader dr)
        {
            Bulto bulto = new Bulto();
            bulto.Id = dr["bulto_ID"] != DBNull.Value ? (int)dr["bulto_ID"] : 0;
            bulto.Secuencia = (int)dr["bulto_secuencia"];
            if (dr["tipoBulto_codigo"] != DBNull.Value)
            {
                TipoBulto tipo = new TipoBulto();
                tipo.Codigo = dr["tipoBulto_codigo"].ToString();
                bulto.Tipo = new AccesoDatos.Contextos.Transversales.TiposBultosContext().Obtener(tipo);
            }

            bulto.Cantidad = dr["bulto_cantidad"] != DBNull.Value ? (int)dr["bulto_cantidad"] : 0;
            bulto.Identificacion = dr["bulto_identificacion"] != DBNull.Value ? dr["bulto_identificacion"].ToString() : "";
            bulto.SubContinente = dr["bulto_subContinente"] != DBNull.Value ? dr["bulto_subContinente"].ToString() : "";

            return bulto;

        }
    }
}
