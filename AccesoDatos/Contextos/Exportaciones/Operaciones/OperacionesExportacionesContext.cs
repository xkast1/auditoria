﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Operacion;
using Browne.Core.Modelo.Exportaciones.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class OperacionesExportacionesContext
    {
        private string connectionString = string.Empty;

        public OperacionesExportacionesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public OperacionesExportacionesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<OperacionExportacion> ObtenerTodos(string numero, string rutCliente, bool? sicex = null, DateTime? desde = null, DateTime? hasta = null)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesExportacion_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar).Value = numero;
                command.Parameters.Add("@cliente_rut", SqlDbType.VarChar).Value = rutCliente;
                if (sicex.HasValue)
                    command.Parameters.Add("@operacion_sicex", SqlDbType.Bit).Value = sicex;
                if (desde.HasValue)
                    command.Parameters.Add("@fecha_desde", SqlDbType.DateTime).Value = desde;
                if (hasta.HasValue)
                    command.Parameters.Add("@fecha_hasta", SqlDbType.DateTime).Value = hasta;

                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        OperacionExportacion operacion = Cargar(dr,false);
                        yield return operacion;
                    }
                }
                command.Connection.Close();
            }
        }
        public OperacionExportacion Obtener(string numero)
        {
            OperacionExportacion Operacion = new OperacionExportacion();

            using (SqlCommand command = new SqlCommand("prc_operacionesExportacion_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numero;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        Operacion = Cargar(dr);
                }
                command.Connection.Close();
            }
            return Operacion;
        }
        private OperacionExportacion Cargar(SqlDataReader dr, bool cargaDetalle = true)
        {
            OperacionExportacion operacion = new OperacionExportacion();
            operacion.Id = (int)dr["operacion_ID"];
            operacion.Correlativo = (int)dr["operacion_correlativo"];
            operacion.RuceID = dr["Ruce_ID"] != DBNull.Value ? (int?)dr["Ruce_ID"] : null;
            operacion.NumeroRUCE = dr["numeroRUCE"] != DBNull.Value ? dr["numeroRUCE"].ToString() : "";
            operacion.NumeroRUCEProvisional = dr["numeroRUCEProvisional"] != DBNull.Value ? dr["numeroRUCEProvisional"].ToString() : "";
            operacion.FechaHoraRUCE = dr["ruce_fecha"] != DBNull.Value ? (DateTime?)dr["ruce_fecha"] : null;
            operacion.Numero = dr["operacion_numero"] != DBNull.Value ? dr["operacion_numero"].ToString() : "";
            operacion.Tipo = dr["operacion_tipo"].ToString();
            operacion.Fecha = dr["operacion_fecha"] != DBNull.Value ? (DateTime?)dr["operacion_fecha"] : null;
            operacion.Referencia = dr["operacion_referencia"] != DBNull.Value ? dr["operacion_referencia"].ToString() : "";
            operacion.Observacion = dr["operacion_observacion"] != DBNull.Value ? dr["operacion_observacion"].ToString() : "";
            operacion.NumeroAceptacion = dr["operacion_numeroAceptacion"] != DBNull.Value ? dr["operacion_numeroAceptacion"].ToString() : "";
            operacion.FechaAceptacion = dr["operacion_fechaAceptacion"] != DBNull.Value ? (DateTime?)dr["operacion_fechaAceptacion"] : null;
            operacion.Parcial = dr["operacion_parcial"].ToString() == "0" ? true : false;
            operacion.NumeroPacialidad = dr["operacion_numeroParcialidad"] != DBNull.Value ? decimal.Parse(dr["operacion_numeroParcialidad"].ToString()) : 0;
            operacion.TotalPacialidad = dr["operacion_totalParcialidad"] != DBNull.Value ? decimal.Parse(dr["operacion_totalParcialidad"].ToString()) : 0;
            operacion.ObservacionesGenerales = dr["operacion_observacionGeneral"].ToString();
            operacion.TotalItems = (int)dr["operacion_totalItems"];
            operacion.TotalContenedores = (int)dr["operacion_totalContenedores"];
            operacion.FechaDesembarque = dr["operacion_fechaDesembarque"] != DBNull.Value ? (DateTime?)dr["operacion_fechaDesembarque"] : null;
            operacion.CodigoDestinacionAduanera = dr["operacion_codigoDestinacionAduanera"] != DBNull.Value ? dr["operacion_codigoDestinacionAduanera"].ToString() : null;
            operacion.MercaderiaNacional = dr["operacion_mercaderiaNacional"] != DBNull.Value ? (bool?)dr["operacion_mercaderiaNacional"] : null;
            operacion.RutAgenciaCompaniaTransporte = dr["operacion_rutAgenciaCompaniaTransporte"].ToString();
            operacion.ExisteCompaniaTransporte = dr["operacion_ExisteCompaniaTransporte"] != DBNull.Value ? (bool?)dr["operacion_ExisteCompaniaTransporte"] : null;
            operacion.MercaderiaConsolidadaZonaPrimaria = dr["operacion_mercaderiaConsolidadaZonaPrimaria"] != DBNull.Value ? (bool?)dr["operacion_mercaderiaConsolidadaZonaPrimaria"] : null;
            operacion.NumeroReservaEmbarque = dr["operacion_numeroReservaEmbarque"] != DBNull.Value ? dr["operacion_numeroReservaEmbarque"].ToString() : "";
            operacion.NumeroReferenciaEnvio = dr["operacion_numeroReferenciaEnvio"] != DBNull.Value ? dr["operacion_numeroReferenciaEnvio"].ToString() : "";
            operacion.NombrePrincipal = dr["operacion_nombrePrincipal"] != DBNull.Value ? dr["operacion_nombrePrincipal"].ToString() : "";
            operacion.NumeroRotacion = dr["operacion_numeroRotacion"] != DBNull.Value ? dr["operacion_numeroRotacion"].ToString() : "";
            operacion.UCR = dr["operacion_UCR"] != DBNull.Value ? dr["operacion_UCR"].ToString() : "";

            operacion.PesoBruto = dr["operacion_pesoBruto"] != DBNull.Value ? decimal.Parse(dr["operacion_pesoBruto"].ToString()) : 0;
            operacion.PesoNeto = dr["operacion_pesoNeto"] != DBNull.Value ? decimal.Parse(dr["operacion_pesoNeto"].ToString()) : 0;
            operacion.CodigoPesoNetoUDM = dr["operacion_codigoPesoNetoUDM"] != DBNull.Value ? dr["operacion_codigoPesoNetoUDM"].ToString() : "";
            operacion.TotalBultos = dr["operacion_totalBultos"] != DBNull.Value ? (int)dr["operacion_totalBultos"] : 0;
            operacion.Comentarios = dr["operacion_comentarios"] != DBNull.Value ? dr["operacion_comentarios"].ToString() : "";
            operacion.Volumen = (decimal)dr["operacion_volumen"];
            operacion.CodigoVolumenUDM = dr["operacion_codigoVolumenUDM"].ToString();
            operacion.TotalDocumentos = (int)dr["operacion_totalDocumentos"];
            operacion.TotalDocumentosTransportes = (int)dr["operacion_totalDocumentosTransporte"];

            operacion.Cliente = new AccesoDatos.Contextos.Transversales.Consumidores.ClientesContext()
                .Obtener(null, null, int.Parse(dr["cliente_numeroSucursal"].ToString()), dr["cliente_rut"].ToString());

            Aduana aduana = new Aduana();
            aduana.Codigo = dr["aduana_codigo"] != DBNull.Value ? dr["aduana_codigo"].ToString() : string.Empty;
            operacion.Aduana = new AccesoDatos.Contextos.Transversales.AduanasContext().Obtener(aduana);

            Agente agente = new Agente();
            operacion.Agente = new AccesoDatos.Contextos.Transversales.AgentesContext().Obtener(dr["agente_codigo"].ToString());

            Nave nave = new Nave();
            nave.Id = dr["nave_codigo"] != DBNull.Value ? (int)dr["nave_codigo"] : 0;
            operacion.Nave = new Transversales.NavesContext().Obtener(nave);

            //actores
            ActoresContext actoresContext = new AccesoDatos.Contextos.Exportaciones.Operaciones.ActoresContext(this.connectionString);
            Actor exportador = new Actor();
            exportador.CodigoTipo = "EXP";
            operacion.Exportador = actoresContext.Obtener(exportador, operacion.Numero);

            Actor exportadorSecundario = new Actor();
            exportadorSecundario.CodigoTipo = "SEXP";
            operacion.ExportadorSecundario = actoresContext.Obtener(exportadorSecundario, operacion.Numero);

            Actor consignatario = new Actor();
            consignatario.CodigoTipo = "CNR";
            operacion.Consignatario = actoresContext.Obtener(consignatario, operacion.Numero);

            operacion.AntecedentesFinancieros = new AccesoDatos.Contextos.Exportaciones.Operaciones.AntecedentesFinancierosContext(this.connectionString).Obtener(operacion.Numero);

            EmisorDocumentoTransporte emisor = new EmisorDocumentoTransporte();
            emisor.ParaCodParam = dr["emisorDocumentoTransporte_paracodparam"] != DBNull.Value ? dr["emisorDocumentoTransporte_paracodparam"].ToString() : "";
            operacion.EmisorDocumentoTransporte = new Transversales.EmisoresDocumentosTransportesContext().Obtener(emisor);

            operacion.TransporteInternacional = new Transversales.TransportesInternacionalesContext().Obtener((dr["transporteInternacional_ID"] != DBNull.Value ? (int)dr["transporteInternacional_ID"] : 0));

            ModalidadVenta modalidadVenta = new ModalidadVenta();
            modalidadVenta.Codigo = dr["modalidadVenta_codigo"] != DBNull.Value ? dr["modalidadVenta_codigo"].ToString() : "";
            operacion.ModalidadVenta = new AccesoDatos.Contextos.Transversales.ModalidadesVentasContext().Obtener(modalidadVenta);

            ClausulaCompraVenta clausulaCompraVenta = new ClausulaCompraVenta();
            clausulaCompraVenta.Codigo = dr["clausulaCompraVenta_codigo"] != DBNull.Value ? dr["clausulaCompraVenta_codigo"].ToString() : "";
            operacion.ClausulaCompraVenta = new AccesoDatos.Contextos.Transversales.ClausulasComprasVentasContext().Obtener(clausulaCompraVenta);

            FormaPago formaPago = new FormaPago();
            formaPago.Codigo = dr["formaPago_codigo"] != DBNull.Value ? dr["formaPago_codigo"].ToString() : "";
            operacion.FormaPago = new AccesoDatos.Contextos.Transversales.FormasPagosContext().Obtener(formaPago);

            Pais paisDestino = new Pais();
            paisDestino.Codigo = dr["pais_destinoCodigo"] != DBNull.Value ? dr["pais_destinoCodigo"].ToString() : "";
            operacion.PaisDestino = new AccesoDatos.Contextos.Transversales.PaisesContext().Obtener(paisDestino);

            Pais paisAdquisicion = new Pais();
            paisAdquisicion.Codigo = dr["pais_adquisicionCodigo"] != DBNull.Value ? dr["pais_adquisicionCodigo"].ToString() : "";
            operacion.PaisAdquisicion = new AccesoDatos.Contextos.Transversales.PaisesContext().Obtener(paisAdquisicion);

            Pais paisOrigen = new Pais();
            paisOrigen.Codigo = dr["pais_origenCodigo"] != DBNull.Value ? dr["pais_origenCodigo"].ToString() : "";
            operacion.PaisOrigen = new AccesoDatos.Contextos.Transversales.PaisesContext().Obtener(paisOrigen);

            Puerto puertoEmbarque = new Puerto();
            puertoEmbarque.Codigo = dr["puerto_embarqueCodigo"] != DBNull.Value ? dr["puerto_embarqueCodigo"].ToString() : "";
            operacion.PuertoEmbarque = new AccesoDatos.Contextos.Transversales.PuertosContext().Obtener(puertoEmbarque);

            Puerto puertoDesembarque = new Puerto();
            puertoDesembarque.Codigo = dr["puerto_desembarqueCodigo"] != DBNull.Value ? dr["puerto_desembarqueCodigo"].ToString() : "";
            operacion.PuertoDesembarque = new AccesoDatos.Contextos.Transversales.PuertosContext().Obtener(puertoDesembarque);

            Moneda moneda = new Moneda();
            moneda.Codigo = dr["moneda_codigo"] != DBNull.Value ? dr["moneda_codigo"].ToString() : "";
            operacion.Moneda = new AccesoDatos.Contextos.Transversales.MonedasContext().Obtener(moneda);

            var tipoOperacion = new TipoOperacion();
            tipoOperacion.Codigo = dr["tipoOperacion_codigo"] != DBNull.Value ? dr["tipoOperacion_codigo"].ToString() : "";
            operacion.TipoOperacion = new Transversales.TiposOperacionesContext().Obtener(tipoOperacion);

            var viaTransporte = new ViaTransporte();
            viaTransporte.Codigo = dr["viaTransporte_codigo"] != DBNull.Value ? dr["viaTransporte_codigo"].ToString() : "";
            operacion.ViaTransporte = new Transversales.ViasTransportesContext().Obtener(viaTransporte);

            var tipoCarga = new TipoCarga();
            tipoCarga.Codigo = dr["tipoCarga_codigo"] != DBNull.Value ? dr["tipoCarga_codigo"].ToString() : "";
            operacion.TipoCarga = new Transversales.TiposCargasContext().Obtener(tipoCarga);

            var region = new Region();
            region.Codigo = dr["region_origenCodigo"] != DBNull.Value ? dr["region_origenCodigo"].ToString() : "";
            operacion.RegionOrigen = new Transversales.RegionesContext().Obtener(region);

            //ToDo: FALTA PROCEDIMIENTO
            //Operacion ? operacionExportacion?

            //operacion.TipoDeclaracion = dr["tipoDeclaracion"] != DBNull.Value ? dr["tipoDeclaracion"].ToString() : "";
            //operacion.NumeroIdentificacion = dr["numeroIdentificacion"] != DBNull.Value ? dr["numeroIdentificacion"].ToString() : "";
            //operacion.RutaAgenciaCompañiaTransporte = dr["rutAgenciaCompaniaTransporte"] != DBNull.Value ? dr["rutAgenciaCompaniaTransporte"].ToString() : "";
            //operacion.ExisteCompañiaTransporte = dr["existeCompaniaTransporte"] != DBNull.Value ? dr["existeCompaniaTransporte"].ToString() : "";


            //listas
            if (cargaDetalle)
            {
                operacion.Bultos = new AccesoDatos.Contextos.Exportaciones.Operaciones.BultosContext(this.connectionString).ObtenerTodos(operacion.Numero);
                operacion.Contenedores = new AccesoDatos.Contextos.Exportaciones.Operaciones.ContenedoresContext(this.connectionString).ObtenerTodos(operacion.Numero);
                operacion.DocumentosTransporte = new AccesoDatos.Contextos.Exportaciones.Operaciones.DocumentosTransportesContext(this.connectionString).ObtenerTodos(operacion.Numero);
                operacion.Items = new AccesoDatos.Contextos.Exportaciones.Operaciones.ItemsContext(this.connectionString).ObtenerTodos(operacion.Numero);
                operacion.Documentos = new AccesoDatos.Contextos.Exportaciones.Operaciones.DocumentosContext(this.connectionString).ObtenerTodos(operacion.Numero);
                operacion.VistosBuenos = new OperacionesVistosBuenosContext(this.connectionString).ObtenerTodos(operacion.Numero);
            }
            operacion.CreacionUsuario = dr["creacion_usuario"].ToString();
            operacion.CreacionFecha = (DateTime)dr["creacion_fecha"];
            operacion.CreacionIp = dr["creacion_IP"].ToString();
            operacion.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            operacion.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            operacion.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return operacion;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
