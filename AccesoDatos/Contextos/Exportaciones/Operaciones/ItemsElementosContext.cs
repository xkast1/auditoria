﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Exportaciones.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class ItemsElementosContext
    {
        private string connectionString = string.Empty;

        public ItemsElementosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ItemsElementosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }

        public IEnumerable<ItemElemento> ObtenerTodos(int idItem)
        {

            using (SqlCommand command = new SqlCommand("prc_itemsElementos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacionItem_ID", SqlDbType.Int).Value = idItem;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ItemElemento itemElemento = Cargar(dr);
                        yield return itemElemento;
                    }
                }
                command.Connection.Close();
            }
        }
        private ItemElemento Cargar(SqlDataReader dr)
        {
            ItemElemento itemElemento = new ItemElemento();
            itemElemento.Id = (int)dr["itemElemento_ID"];
            itemElemento.Numero = dr["itemElemento_numero"] != DBNull.Value ? (int)dr["itemElemento_numero"] : 0;
            itemElemento.CodigoNombre = dr["itemElemento_codigoNombre"] != DBNull.Value ? dr["itemElemento_codigoNombre"].ToString(): "";
            itemElemento.CodigoTipo = dr["itemElemento_codigoTipo"] != DBNull.Value ? dr["itemElemento_codigoTipo"].ToString(): "";
            itemElemento.LEYDUS = dr["itemElemento_LEYDUS"] != DBNull.Value ? (decimal)dr["itemElemento_LEYDUS"]: 0;
            itemElemento.CodigoLEYDUSUDM = dr["itemElemento_codigoLEYDUSUDM"] != DBNull.Value ? dr["itemElemento_codigoLEYDUSUDM"].ToString(): "";

            return itemElemento;
        }
    }
}
