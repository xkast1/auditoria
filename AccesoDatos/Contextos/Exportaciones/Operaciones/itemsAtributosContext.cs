﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class ItemsAtributosContext
    {
        private string connectionString = string.Empty;

        public ItemsAtributosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ItemsAtributosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }

        public IEnumerable<ItemAtributo> ObtenerTodos(int idItem)
        {

            using (SqlCommand command = new SqlCommand("prc_itemsAtributos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacionItem_ID", SqlDbType.Int).Value = idItem;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ItemAtributo itemAtributo = Cargar(dr);
                        yield return itemAtributo;
                    }
                }
                command.Connection.Close();
            }
        }
        private ItemAtributo Cargar(SqlDataReader dr)
        {
            ItemAtributo itemAtributo = new ItemAtributo();
            itemAtributo.Id = (int)dr["itemAtributo_ID"];
            itemAtributo.Secuencia = dr["itemAtributo_secuencia"] != DBNull.Value ? (int?)dr["itemAtributo_secuencia"] : null;
            itemAtributo.Codigo = dr["itemAtributo_codigo"] != DBNull.Value ? dr["itemAtributo_codigo"].ToString() : "";
            itemAtributo.Valor = dr["itemAtributo_valor"] != DBNull.Value ? dr["itemAtributo_valor"].ToString() : "";

            return itemAtributo;
        }
    }
}
