﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class ContenedoresContext
    {
        private string connectionString = string.Empty;

        public ContenedoresContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ContenedoresContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }

        public IEnumerable<Contenedor> ObtenerTodos(string numeroOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesContenedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Contenedor contenedor = Cargar(dr);
                        yield return contenedor;
                    }
                }
                command.Connection.Close();
            }
        }
        private Contenedor Cargar(SqlDataReader dr)
        {
            Contenedor contenedor = new Contenedor();
            contenedor.Id = (int)dr["operacionContenedor_ID"];
            contenedor.Codigo = dr["operacionContenedor_codigo"] != DBNull.Value ? dr["operacionContenedor_codigo"].ToString() : null;
            contenedor.Secuencia = dr["operacionContenedor_secuencia"] != DBNull.Value ? (int?)dr["operacionContenedor_secuencia"] : null;
            contenedor.Sigla = dr["operacionContenedor_sigla"] != DBNull.Value ? dr["operacionContenedor_sigla"].ToString() : null;
            contenedor.CodigoSello = dr["operacionContenedor_codigoSello"] != DBNull.Value ? dr["operacionContenedor_codigoSello"].ToString() : "";
            contenedor.Peso = (decimal)dr["operacionContenedor_peso"];
            contenedor.CodigoPesoUMD = dr["operacionContenedor_codigoPesoUMD"] != DBNull.Value ? dr["operacionContenedor_codigoPesoUMD"].ToString() : null;

            if (dr["estadoCargaContenedor_codigo"] != DBNull.Value)
            {
                EstadoCargaContenedor estadoCarga = new EstadoCargaContenedor();
                estadoCarga.Codigo = dr["estadoCargaContenedor_codigo"].ToString();
                contenedor.EstadoCarga = new AccesoDatos.Contextos.Transversales.EstadosCargasContenedoresContext().Obtener(estadoCarga);
            }
            if (dr["tipoContenedor_ID"] != DBNull.Value)
            {
                TipoContenedor tipo = new TipoContenedor();
                tipo.Codigo =  dr["tipoContenedor_ID"].ToString();
                contenedor.Tipo = new AccesoDatos.Contextos.Transversales.TiposContenedoresContext().Obtener(tipo);
            }
            if (dr["tipoSello_codigo"] != DBNull.Value)
            {
                TipoSello sello = new TipoSello();
                sello.Codigo = dr["tipoSello_codigo"].ToString();
                contenedor.Sello = new Transversales.TiposSellosContext().Obtener(sello);
            }
            return contenedor;


        }
    }
}
