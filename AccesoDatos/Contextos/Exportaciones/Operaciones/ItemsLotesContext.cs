﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class ItemsLotesContext
    {
        private string connectionString = string.Empty;

        public ItemsLotesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ItemsLotesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<ItemLote> ObtenerTodos(int idItem)
        {

            using (SqlCommand command = new SqlCommand("prc_itemsLotes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacionItem_ID", SqlDbType.Int).Value = idItem;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ItemLote itemLote = Cargar(dr);
                        yield return itemLote;
                    }
                }
                command.Connection.Close();
            }
        }
        private ItemLote Cargar(SqlDataReader dr)
        {
            ItemLote itemLote = new ItemLote();
            itemLote.Id = (int)dr["itemLote_ID"];
            itemLote.Lote = dr["itemLote_lote"] != DBNull.Value ? dr["itemLote_lote"].ToString() : "";
            itemLote.Numero = dr["itemLote_numero"] != DBNull.Value ? dr["itemLote_numero"].ToString(): "";

            return itemLote;
        }

    }
}
