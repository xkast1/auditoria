﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Exportaciones.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class DocumentosContext
    {
        private string connectionString = string.Empty;

        public DocumentosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public DocumentosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<Documento> ObtenerTodos(string numeroOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesDocumentos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Documento documento = Cargar(dr);
                        yield return documento;
                    }
                }
                command.Connection.Close();
            }
        }
        private Documento Cargar(SqlDataReader dr)
        {
            Documento documento = new Documento();
            documento.Id = (int)dr["operacionDocumento_ID"];
            documento.Secuencia = dr["operacionDocumento_secuencia"] != DBNull.Value ? (int?)dr["operacionDocumento_secuencia"] : null;
            documento.Nombre = dr["operacionDocumento_nombre"] != DBNull.Value ? dr["operacionDocumento_nombre"].ToString() : null;
            documento.Numero = dr["operacionDocumento_numero"] != DBNull.Value ? dr["operacionDocumento_numero"].ToString() : null;
            documento.CodigoTipo = dr["operacionDocumento_codigoTipo"] != DBNull.Value ? dr["operacionDocumento_codigoTipo"].ToString() : "";
            documento.Emisor = dr["operacionDocumento_emisor"] != DBNull.Value ? dr["operacionDocumento_emisor"].ToString() : null;
            documento.Fecha = dr["operacionDocumento_fecha"] != DBNull.Value ? (DateTime?)dr["operacionDocumento_fecha"] : null;
            documento.Comentario = dr["operacionDocumento_comentario"] != DBNull.Value ? dr["operacionDocumento_comentario"].ToString() : null;
            return documento;
        }
    }
}
