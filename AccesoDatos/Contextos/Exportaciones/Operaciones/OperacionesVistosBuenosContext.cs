﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Exportaciones.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class OperacionesVistosBuenosContext
    {
        private string connectionString = string.Empty;

        public OperacionesVistosBuenosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public OperacionesVistosBuenosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<OperacionVistoBueno> ObtenerTodos(string numeroOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesVistosBuenos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        OperacionVistoBueno itemVistoBueno = Cargar(dr);
                        yield return itemVistoBueno;
                    }
                }
                command.Connection.Close();
            }
        }

        private OperacionVistoBueno Cargar(SqlDataReader dr)
        {
            OperacionVistoBueno itemVistoBueno = new OperacionVistoBueno();
            itemVistoBueno.Id = (int)dr["operacionVistoBueno_ID"];
            itemVistoBueno.Secuencia = (int)dr["operacionVistoBueno_secuencia"];
            itemVistoBueno.CodigoEvaluador = dr["operacionVistoBueno_codigoEvaluador"].ToString();
            itemVistoBueno.Resolucion = dr["operacionVistoBueno_resolucion"].ToString();
            itemVistoBueno.FechaResolucion = (DateTime)dr["operacionVistoBueno_fechaResolucion"];
            itemVistoBueno.GlosaResolucion = dr["operacionVistoBueno_glosaResolucion"].ToString();
            VistoBueno vistoBueno = new VistoBueno();
            vistoBueno.Codigo = dr["vistoBueno_codigo"].ToString();
            itemVistoBueno.Tipo = new AccesoDatos.Contextos.Transversales.VistosBuenosContext().Obtener(vistoBueno);


            return itemVistoBueno;
        }
    }
}
