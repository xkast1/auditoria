﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class ItemsObservacionesContext
    {
        private string connectionString = string.Empty;

        public ItemsObservacionesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ItemsObservacionesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }

        public IEnumerable<ItemObservacion> ObtenerTodos(int idItem)
        {

            using (SqlCommand command = new SqlCommand("prc_itemsObservaciones_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacionItem_ID", SqlDbType.Int).Value = idItem;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ItemObservacion itemObservacion = Cargar(dr);
                        yield return itemObservacion;
                    }
                }
                command.Connection.Close();
            }
        }

        public ItemObservacion Cargar(SqlDataReader dr)
        {
            ItemObservacion itemObservacion = new ItemObservacion();
            itemObservacion.Id = (int)dr["itemObservacion_ID"];
            itemObservacion.Secuencia = (int)dr["itemObservacion_secuencia"];
            itemObservacion.Codigo = dr["itemObservacion_codigo"] != DBNull.Value ? dr["itemObservacion_codigo"].ToString() : "";
            itemObservacion.Valor = dr["itemObservacion_valor"] != DBNull.Value ? dr["itemObservacion_valor"].ToString() : "";
            itemObservacion.Glosa = dr["itemObservacion_glosa"] != DBNull.Value ? dr["itemObservacion_glosa"].ToString() : "";
            return itemObservacion;

        }
    }
}
