﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales.Operacion;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class ActoresContext
    {
        private string connectionString = string.Empty;

        public ActoresContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ActoresContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }
        public IEnumerable<Actor> ObtenerTodos(string numeroOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesActores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Actor actor = Cargar(dr);
                        yield return actor;
                    }
                }
                command.Connection.Close();
            }
        }
        public Actor Obtener(Actor actor, string numeroOperacion) {
            using (SqlCommand command = new SqlCommand("prc_operacionesActores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@actor_codigoTipo", SqlDbType.VarChar, 3).Value = actor.CodigoTipo;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
             
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        actor = Cargar(dr);
                }
                command.Connection.Close();
            }

            return actor;
        }
        public Actor Cargar(SqlDataReader dr)
        {
            Actor actor = new Actor();
            actor.Id = (int)dr["actor_ID"];
            actor.CodigoTipo = dr["actor_codigoTipo"].ToString();
            actor.CodigoTipoDocumento = dr["actor_codigoTipoDocumento"] != DBNull.Value ? dr["actor_codigoTipoDocumento"].ToString() : "";
            actor.Codigo = dr["actor_codigo"] != DBNull.Value ? dr["actor_codigo"].ToString() : "";
            actor.Codigo = dr["actor_codigoVerificador"] != DBNull.Value ? dr["actor_codigoVerificador"].ToString() : "";
            actor.Nombre = dr["actor_nombre"] != DBNull.Value ? dr["actor_nombre"].ToString() : "";
            actor.Apellido = dr["actor_apellido"] != DBNull.Value ? dr["actor_apellido"].ToString() : "";
            actor.Direccion = dr["actor_direccion"] != DBNull.Value ? dr["actor_direccion"].ToString() : "";
            actor.Telefono = dr["actor_telefono"] != DBNull.Value ? dr["actor_telefono"].ToString() : "";
            actor.TelefonoMovil = dr["actor_telefonoMovil"] != DBNull.Value ? dr["actor_telefonoMovil"].ToString() : "";
            actor.IndicadorALaOrden = dr["actor_indicadorALaOrden"] != DBNull.Value ? (bool?)dr["actor_indicadorALaOrden"] : null;
            actor.Secundario = dr["actor_secundario"] != DBNull.Value ? (bool?)dr["actor_secundario"] : null;
            actor.CorreoElectronico = dr["actor_correoElectronico"] != DBNull.Value ? dr["actor_correoElectronico"].ToString() : "";        
            actor.Porcentaje = dr["actor_porcentaje"] != DBNull.Value ? (decimal)dr["actor_porcentaje"] : 0;
            if (dr["pais_codigo"] != DBNull.Value) {
                Pais pais = new Pais();
                pais.Codigo = dr["pais_codigo"].ToString();
                actor.Pais = new AccesoDatos.Contextos.Transversales.PaisesContext().Obtener(pais);
            }
            if (dr["region_codigo"] != DBNull.Value)
            {
                Region region = new Region();
                region.Codigo = dr["region_codigo"].ToString();
                actor.Region = new AccesoDatos.Contextos.Transversales.RegionesContext().Obtener(region);
            }
            if (dr["comuna_codigo"] != DBNull.Value)
            {
                Comuna comuna = new Comuna();
                comuna.Codigo = dr["comuna_codigo"].ToString();
                actor.Comuna = new AccesoDatos.Contextos.Transversales.ComunasContext().Obtener(comuna);
            }

            return actor;
        }
    }
}
