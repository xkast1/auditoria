﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Exportaciones.Operacion;

namespace AccesoDatos.Contextos.Exportaciones.Operaciones
{
    public class DocumentosTransportesContext
    {
        private string connectionString = string.Empty;

        public DocumentosTransportesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public DocumentosTransportesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_exportaciones"].ConnectionString;
        }

        public IEnumerable<DocumentoTransporte> ObtenerTodos(string numeroOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_documentosTransportes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numeroOperacion;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DocumentoTransporte documentoTransporte = Cargar(dr);
                        yield return documentoTransporte;
                    }
                }
                command.Connection.Close();
            }
        }

        private DocumentoTransporte Cargar(SqlDataReader dr) {
            DocumentoTransporte documentoTransporte = new DocumentoTransporte();

            documentoTransporte.Id = dr["documentoTransporte_ID"] != DBNull.Value ? (int)dr["documentoTransporte_ID"] : 0;
            documentoTransporte.Secuencia = (int)dr["documentoTransporte_secuencia"];
            documentoTransporte.Numero = dr["documentoTransporte_numero"].ToString();
            documentoTransporte.Fecha = dr["documentoTransporte_fecha"] != DBNull.Value ? (DateTime?)dr["documentoTransporte_fecha"] : null;
            documentoTransporte.Tipo = dr["documentoTransporte_tipo"].ToString();
            Nave nave = new Nave();
            nave.Id = (int)dr["nave_ID"];
            documentoTransporte.Nave = new AccesoDatos.Contextos.Transversales.NavesContext().Obtener(nave);
            documentoTransporte.NumeroViaje = dr["documentoTransporte_numeroViaje"].ToString();
            documentoTransporte.NumeroManifiesto = dr["documentoTransporte_numeroManifiesto"].ToString();

            return documentoTransporte;
        }
    }
}
