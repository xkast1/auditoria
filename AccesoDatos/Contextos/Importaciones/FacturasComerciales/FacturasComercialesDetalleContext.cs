﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Importaciones.FacturaComercial;
using Browne.Core.Modelo.Transversales.Consumidor;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Importaciones.FacturasComerciales
{
    public class FacturasComercialesDetalleContext
    {
        private string connectionString = string.Empty;

        public FacturasComercialesDetalleContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public FacturasComercialesDetalleContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public IEnumerable<FacturaComercialDetalle> ObtenerTodos(int id)
        {
            using (SqlCommand command = new SqlCommand("prc_facturasComercialesDetalle_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Parameters.Add("@facturaComercial_ID", SqlDbType.Int).Value = id;
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FacturaComercialDetalle facturaComercialDetalle = Cargar(dr);
                        yield return facturaComercialDetalle;
                    }
                }
                command.Connection.Close();
            }
        }
        private FacturaComercialDetalle Cargar(SqlDataReader dr)
        {
            FacturaComercialDetalle facturaComercialDetalle = new FacturaComercialDetalle();
            facturaComercialDetalle.Id = (int)dr["facturaComercialDetalle_ID"];
            facturaComercialDetalle.Correlativo =(int)dr["facturaComercialDetalle_correlativo"];
            facturaComercialDetalle.Codigo = dr["facturaComercialDetalle_codigo"].ToString();
            facturaComercialDetalle.Descripcion = dr["facturaComercialDetalle_descripcion"].ToString();
            facturaComercialDetalle.Cantidad = dr["facturaComercialDetalle_cantidad"] != DBNull.Value ? (decimal)dr["facturaComercialDetalle_cantidad"] : 1;
            facturaComercialDetalle.PrecioUnitario = dr["facturaComercialDetalle_precioUnitario"] != DBNull.Value ?  (decimal)dr["facturaComercialDetalle_precioUnitario"] : 0;

            return facturaComercialDetalle;
        }
    }
}
