﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Importaciones.FacturaComercial;
using Browne.Core.Modelo.Transversales.Consumidor;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Importaciones.FacturasComerciales
{
    public class FacturasComercialesSubDetalleContext
    {
        private string connectionString = string.Empty;

        public FacturasComercialesSubDetalleContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public FacturasComercialesSubDetalleContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        public IEnumerable<FacturaComercialSubDetalle> ObtenerTodos(int id)
        {
            using (SqlCommand command = new SqlCommand("prc_facturasComercialesSubDetalle_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Parameters.Add("@facturaComercialDetalle_ID", SqlDbType.Int).Value = id;
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FacturaComercialSubDetalle facturaComercialDetalle = Cargar(dr);
                        yield return facturaComercialDetalle;
                    }
                }
                command.Connection.Close();
            }
        }
        private FacturaComercialSubDetalle Cargar(SqlDataReader dr)
        {
            FacturaComercialSubDetalle facturaComercialSubDetalle = new FacturaComercialSubDetalle();
            facturaComercialSubDetalle.Id = (int)dr["facturaComercialSubDetalle_ID"];
            facturaComercialSubDetalle.Correlativo = (int)dr["facturaComercialSubDetalle_correlativo"];
            facturaComercialSubDetalle.Cantidad = dr["facturaComercialSubDetalle_cantidad"] != DBNull.Value ? (decimal)dr["facturaComercialSubDetalle_cantidad"] : 1;
            facturaComercialSubDetalle.Marca = dr["facturaComercialSubDetalle_marca"].ToString();
            facturaComercialSubDetalle.Modelo = dr["facturaComercialSubDetalle_modelo"].ToString();
            facturaComercialSubDetalle.Modelo2 = dr["facturaComercialSubDetalle_modelo2"].ToString();
            facturaComercialSubDetalle.Color = dr["facturaComercialSubDetalle_color"].ToString();
            facturaComercialSubDetalle.Ano = dr["facturaComercialSubDetalle_ano"] != DBNull.Value ? (int)dr["facturaComercialSubDetalle_ano"] : 0;
            facturaComercialSubDetalle.NumeroIdenficiacion = dr["facturaComercialSubDetalle_numeroIdentificacion"].ToString();
            facturaComercialSubDetalle.NumeroIdenficiacion2 = dr["facturaComercialSubDetalle_numeroIdentificacion2"].ToString();
            facturaComercialSubDetalle.NumeroIdenficiacion3 = dr["facturaComercialSubDetalle_numeroIdentificacion3"].ToString();
            facturaComercialSubDetalle.NumeroIdenficiacion4 = dr["facturaComercialSubDetalle_numeroIdentificacion4"].ToString();
            facturaComercialSubDetalle.Peso = dr["facturaComercialSubDetalle_peso"] != DBNull.Value ? (decimal)dr["facturaComercialSubDetalle_peso"] : 0;
            facturaComercialSubDetalle.PesoButo = dr["facturaComercialSubDetalle_pesoBruto"] != DBNull.Value ? (decimal)dr["facturaComercialSubDetalle_pesoBruto"] : 0;
            facturaComercialSubDetalle.Volumen = dr["facturaComercialSubDetalle_volumen"] != DBNull.Value ? (decimal)dr["facturaComercialSubDetalle_volumen"] : 0;
            facturaComercialSubDetalle.Largo = dr["facturaComercialSubDetalle_largo"] != DBNull.Value ? (decimal)dr["facturaComercialSubDetalle_largo"] : 0;
            facturaComercialSubDetalle.TipoInternacion = dr["facturaComercialSubDetalle_TipoInternacion"].ToString();

            return facturaComercialSubDetalle;
        }
    }
}
