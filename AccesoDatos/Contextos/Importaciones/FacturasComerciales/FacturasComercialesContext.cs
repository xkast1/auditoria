﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Importaciones.FacturaComercial;
using Browne.Core.Modelo.Transversales.Consumidor;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Importaciones.FacturasComerciales
{
    public class FacturasComercialesContext
    {
        private string connectionString = string.Empty;

        public FacturasComercialesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public FacturasComercialesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public IEnumerable<FacturaComercial> ObtenerTodos(DateTime desde, DateTime hasta, string rut)
        {
            using (SqlCommand command = new SqlCommand("prc_facturasComerciales_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Parameters.Add("@facturaComercial_fechaDesde", SqlDbType.Date).Value = desde.Date;
                command.Parameters.Add("@facturaComercial_fechaHasta", SqlDbType.Date).Value = hasta.Date;
                if (!rut.Equals(string.Empty))
                    command.Parameters.Add("@cliente_rut", SqlDbType.VarChar, 11).Value = rut;
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FacturaComercial facturaComercial = Cargar(dr, false);
                        yield return facturaComercial;
                    }
                }
                command.Connection.Close();
            }
        }



        public void Guardar(FacturaComercial facturaComercial)
        {

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@facturaComeracial_ID", SqlDbType.Int).Value = facturaComercial.Id;
                        command.Parameters.Add("@cliente_ID", SqlDbType.Int).Value = facturaComercial.Cliente.Id;
                        command.Parameters.Add("@facturaComercial_numero", SqlDbType.VarChar, 20).Value = facturaComercial.Numero;
                        command.Parameters.Add("@facturaComercial_fecha", SqlDbType.Date).Value = facturaComercial.Fecha;
                        command.Parameters.Add("@moneda_ID", SqlDbType.Int).Value = facturaComercial.Moneda.Id;
                        command.Parameters.Add("@clausulaCompraVenta_ID", SqlDbType.Int).Value = facturaComercial.ClausulaCompraVenta.Id;
                        command.Parameters.Add("@facturaComercial_paridad", SqlDbType.Decimal).Value = facturaComercial.Paridad;
                        command.Parameters.Add("@facturaComercial_total", SqlDbType.Decimal).Value = facturaComercial.Total;
                        command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = facturaComercial.CreacionUsuario;
                        command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = facturaComercial.CreacionIp;
                        command.Connection = connection;
                        command.Transaction = transaction;

                        if (facturaComercial.Id > 0)
                        {
                            command.CommandText = "prc_facturasComerciales_actualizar";
                            command.Parameters["@usuario"].Value = facturaComercial.ActualizacionUsuario;
                            command.Parameters["@IP"].Value = facturaComercial.ActualizacionIp;
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_facturasComercialesDetalle_eliminar", connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@facturaComercial_ID", SqlDbType.Int).Value = facturaComercial.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }

                        }
                        else
                        {
                            command.CommandText = "prc_facturaComercial_insertar";
                            command.Parameters["@facturaComercial_ID"].Value = null;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    facturaComercial.Id = (int)dr["facturaComercial_ID"];
                            }
                        }
                        using (SqlCommand commandDetalle = new SqlCommand("prc_facturasComercialesDetalle_insertar", connection))
                        {
                            commandDetalle.CommandType = CommandType.StoredProcedure;
                            commandDetalle.Parameters.Add("@facturaComercial_ID", SqlDbType.Int).Value = facturaComercial.Id;
                            commandDetalle.Parameters.Add("@facturaComercialDetalle_correlativo", SqlDbType.Int);
                            commandDetalle.Parameters.Add("@facturaComercialDetalle_codigo", SqlDbType.VarChar, 16);
                            commandDetalle.Parameters.Add("@facturaComercialDetalle_descripcion", SqlDbType.VarChar, 250);
                            commandDetalle.Parameters.Add("@facturaComercialDetalle_cantidad", SqlDbType.Decimal);
                            commandDetalle.Parameters.Add("@facturaComercialDetalle_precioUnitario", SqlDbType.Decimal);
                            commandDetalle.Parameters.Add("@unidadMedida_ID", SqlDbType.Int);
                            commandDetalle.Transaction = transaction;

                            foreach (FacturaComercialDetalle facturaComercialDetalle in facturaComercial.Detalle)
                            {
                                commandDetalle.Parameters["@facturaComercialDetalle_correlativo"].Value = facturaComercialDetalle.Correlativo;
                                commandDetalle.Parameters["@facturaComercialDetalle_codigo"].Value = facturaComercialDetalle.Codigo;
                                commandDetalle.Parameters["@facturaComercialDetalle_descripcion"].Value = facturaComercialDetalle.Descripcion;
                                commandDetalle.Parameters["@facturaComercialDetalle_cantidad"].Value = facturaComercialDetalle.Cantidad;
                                commandDetalle.Parameters["@facturaComercialDetalle_precioUnitario"].Value = facturaComercialDetalle.PrecioUnitario;
                                commandDetalle.Parameters["@unidadMedida_ID"].Value = facturaComercialDetalle.UnidadMedida.Id;
                                //commandDetalle.ExecuteNonQuery();
                                using (SqlDataReader dr = commandDetalle.ExecuteReader())
                                {
                                    if (dr.HasRows && dr.Read())
                                        facturaComercialDetalle.Id = (int)dr["facturaComercialDetalle_ID"];
                                }

                                //insertar sub detalle 

                                using (SqlCommand commandSubDetalle = new SqlCommand("prc_facturasComercialesSubDetalle_insertar", connection))
                                {
                                    commandSubDetalle.CommandType = CommandType.StoredProcedure;
                                    commandSubDetalle.Parameters.Add("@facturaComercial_ID", SqlDbType.Int).Value = facturaComercialDetalle.Id;
                                    commandSubDetalle.Parameters.Add("@facturaComercialDetalle_correlativo", SqlDbType.Int);
                                    commandSubDetalle.Parameters.Add("@facturaComercialDetalle_codigo", SqlDbType.VarChar, 16);
                                    commandSubDetalle.Parameters.Add("@facturaComercialDetalle_descripcion", SqlDbType.VarChar, 250);
                                    commandSubDetalle.Parameters.Add("@facturaComercialDetalle_cantidad", SqlDbType.Decimal);
                                    commandSubDetalle.Parameters.Add("@facturaComercialDetalle_precioUnitario", SqlDbType.Decimal);
                                    commandSubDetalle.Parameters.Add("@unidadMedida_ID", SqlDbType.Int);
                                    commandSubDetalle.Transaction = transaction;

                                    foreach (FacturaComercialSubDetalle facturaComercialSubDetalle in facturaComercialDetalle.SubDetalle)
                                    {
                                        commandSubDetalle.Parameters["@facturaComercialDetalle_correlativo"].Value = facturaComercialDetalle.Correlativo;
                                        commandSubDetalle.Parameters["@facturaComercialDetalle_codigo"].Value = facturaComercialDetalle.Codigo;
                                        commandSubDetalle.Parameters["@facturaComercialDetalle_descripcion"].Value = facturaComercialDetalle.Descripcion;
                                        commandSubDetalle.Parameters["@facturaComercialDetalle_cantidad"].Value = facturaComercialDetalle.Cantidad;
                                        commandSubDetalle.Parameters["@facturaComercialDetalle_precioUnitario"].Value = facturaComercialDetalle.PrecioUnitario;
                                        commandSubDetalle.Parameters["@unidadMedida_ID"].Value = facturaComercialDetalle.UnidadMedida.Id;
                                        commandSubDetalle.ExecuteNonQuery();



                                    }
                                }


                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en FacturasComercialesContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }

        }
        public FacturaComercial Obtener(FacturaComercial facturaComercial)
        {

            using (SqlCommand command = new SqlCommand("prc_facturasComerciales_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@facturaComercial_ID", SqlDbType.Int).Value = facturaComercial.Id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        facturaComercial = Cargar(dr);
                }
                command.Connection.Close();
            }
            return facturaComercial;
        }

        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, string rut)
        {

            using (SqlCommand command = new SqlCommand("prc_facturasComerciales_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@rutCliente", SqlDbType.VarChar, 11).Value = rut;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["facturaComercial_ID"].ToString();
                        autocompletado.Value = dr["facturaComercial_numero"].ToString();
                        autocompletado.Label = dr["facturaComercial_numero"].ToString() + " | " + dr["facturaComercial_fecha"].ToString();

                        yield return autocompletado;
                    }
                    command.Connection.Close();
                }
            }
        }

        private FacturaComercial Cargar(SqlDataReader dr, bool cargaDetalle = true)
        {
            FacturaComercial facturaComercial = new FacturaComercial();
            facturaComercial.Id = (int)dr["facturaComercial_ID"];
            facturaComercial.Numero = dr["facturaComercial_numero"].ToString();
            facturaComercial.Fecha = (DateTime)dr["facturaComercial_fecha"];
            facturaComercial.Paridad = dr["facturaComercial_paridad"] != DBNull.Value ? (decimal)dr["facturaComercial_paridad"] : 1;
            facturaComercial.Total = dr["facturaComercial_total"] != DBNull.Value ? (decimal)dr["facturaComercial_total"] : 0;

            Cliente cliente = new Cliente();
            cliente = new AccesoDatos.Contextos.Transversales.Consumidores.ClientesContext()
                .Obtener((int)dr["cliente_ID"], null, (int)dr["cliente_sucursal"], dr["cliente_rut"].ToString());

            Moneda moneda = new Moneda();
            moneda.Id = (int)dr["moneda_ID"];
            moneda.Codigo = dr["moneda_codigo"].ToString();
            moneda = new AccesoDatos.Contextos.Transversales.MonedasContext()
                .Obtener(moneda);

            ClausulaCompraVenta clausulaCompraVenta = new ClausulaCompraVenta();
            clausulaCompraVenta.Id = (int)dr["clausulaCompraVenta_ID"];
            clausulaCompraVenta.Codigo = dr["clausualCompraVenta_codigo"].ToString();

            clausulaCompraVenta = new AccesoDatos.Contextos.Transversales.ClausulasComprasVentasContext()
                .Obtener(clausulaCompraVenta);


            facturaComercial.CreacionUsuario = dr["creacion_usuario"].ToString();
            facturaComercial.CreacionFecha = (DateTime)dr["creacion_fecha"];
            facturaComercial.CreacionIp = dr["creacion_IP"].ToString();
            facturaComercial.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            facturaComercial.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            facturaComercial.ActualizacionIp = dr["actualizacion_IP"].ToString();

            if (cargaDetalle)
                facturaComercial.Detalle = new FacturasComercialesDetalleContext(this.connectionString)
                    .ObtenerTodos(facturaComercial.Id);

            return facturaComercial;
        }
    }
}
