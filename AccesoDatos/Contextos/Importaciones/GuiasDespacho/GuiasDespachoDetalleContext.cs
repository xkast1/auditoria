﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Importaciones.GuiaDespacho;

namespace AccesoDatos.Contextos.Importaciones.GuiasDespacho
{
    public class GuiasDespachoDetalleContext
    {
        private string connectionString = string.Empty;

        public GuiasDespachoDetalleContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public GuiasDespachoDetalleContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public IEnumerable<GuiaDespachoDetalle> ObtenerTodos(int? idGuia = null)
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_guiasDespachoDetalle_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                if (idGuia.HasValue)
                    cmd.Parameters.Add("@GuiaDespacho_ID", SqlDbType.Int).Value = idGuia;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        GuiaDespachoDetalle detalle = Cargar(dr);
                        yield return detalle;
                    }
                }
                cmd.Connection.Close();
            }
        }

        private GuiaDespachoDetalle Cargar(SqlDataReader dr)
        {
            var detalle = new GuiaDespachoDetalle();
            detalle.Id = int.Parse(dr["detalle_id"].ToString());
            detalle.DescripcionBulto = dr["descripcion"].ToString();
            detalle.Cantidad = decimal.Parse(dr["cantidad"].ToString());
            detalle.TipoBulto = dr["tipo_bulto"].ToString();
            detalle.Mercaderia = dr["mercaderia"].ToString();
            //detalle.Total = Decimal.Parse(dr["detalle_total"].ToString());
            return detalle;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
