﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Importaciones.GuiaDespacho;
using System.Data.SqlTypes;

namespace AccesoDatos.Contextos.Importaciones.GuiasDespacho
{
    public class GuiasDespachoContext
    {
        private string connectionString = string.Empty;

        public GuiasDespachoContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public GuiasDespachoContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public IEnumerable<GuiaDespacho> Obtener(DateTime? desde = null, DateTime? hasta = null)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = GetQuery() + @"
                        WHERE (@desde IS NULL OR cast(GUIA_DESPACHO.FECHAGUIA AS date) >= @desde)
                          AND (@hasta IS NULL OR cast(GUIA_DESPACHO.FECHAGUIA AS date) <= @hasta)
                    ";

                    command.Parameters.Add("@desde", SqlDbType.DateTime).Value = desde ?? SqlDateTime.Null;
                    command.Parameters.Add("@hasta", SqlDbType.DateTime).Value = hasta ?? SqlDateTime.Null;

                    connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (dr.Read())
                        {
                            var guia = new GuiaDespacho();

                            try
                            {
                                cargarDesdeDataReader(dr, guia);
                            }
                            catch (Exception e)
                            {
                                throw new Exception(e.Message);
                            }

                            yield return guia;
                        }
                    }
                }
            }
        }

        private void cargarDesdeDataReader(IDataReader dr, Browne.Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho guia)
        {
            try
            {
                guia.Id = dr.GetInt32(0);
                guia.Numero = dr.GetInt32(4);
                guia.Fecha = dr.GetDateTime(5);

                guia.Cliente = new Browne.Core.Modelo.Transversales.Consumidor.Cliente
                {
                    RazonSocial = dr.GetString(8)
                };

                guia.NumeroDespacho = dr.GetString(2);
                guia.Patente = dr.GetString(21);
                guia.KilosBruto = dr.GetDecimal(27);
                guia.KilosTara = 0; // dr.GetDecimal();
                guia.KilosNetos = 0; // dr.GetDecimal();
                guia.HoraRetiro = dr.IsDBNull(34) ? null : dr.GetString(34);
                guia.Observacion = dr.GetString(18);
            }
            catch (InvalidCastException) { }
            catch (IndexOutOfRangeException) { }
            catch (Exception) { throw; }
        }

        public IEnumerable<GuiaDespacho> ObtenerTodo()
        {
            for (int i = 0; i < 10; i++)
            {
                var guia = new GuiaDespacho
                {
                    Numero = (10000 + i),
                    Fecha = DateTime.Now,
                    Cliente = new Browne.Core.Modelo.Transversales.Consumidor.Cliente
                    {
                        RazonSocial = string.Format("Cliente {0}", (1000 + i))
                    },
                    NumeroDespacho = "I-1234567",
                    Patente = "EBMG99",
                    KilosBruto = 155,
                    KilosTara = 160,
                    KilosNetos = 130,
                    HoraRetiro = "09:15"
                };

                yield return guia;
            }

            //using (var connection = new SqlConnection(connectionString))
            //{
            //    using (var command = connection.CreateCommand())
            //    {
            //        command.CommandType = CommandType.StoredProcedure;
            //        command.CommandText = "prc_guia_despacho_seleccionar";

            //        using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
            //        {
            //            if (dr.HasRows)
            //            {
            //                while (dr.NextResult())
            //                {
            //                    var guia = new GuiaDespacho();

            //                    yield return guia;
            //                }
            //            }
            //        }
            //    }
            //}
        }

        //public List<GuiaDespacho> ObtenerTodas(DateTime desde, DateTime hasta, string rutCliente, int numeroSucursal)
        //{

        //    List<GuiaDespacho> guiasDespacho = new List<GuiaDespacho>();
        //    using (SqlConnection conn = new SqlConnection(this.connectionString))
        //    {
        //        conn.Open();
        //        string query = ObtenerQuery("") + @" Where CLIENTE.RUTCLIENTE = @Rut and cliente.NUMSUCURSAL = @Sucursal GUIA_DESPACHO.FECHAGUIA BETWEEN @desde and @hasta";
        //        SqlCommand cmd = new SqlCommand(query, conn);
        //        cmd.Parameters.Add("@Rut", SqlDbType.NVarChar, 11).Value = rutCliente;
        //        cmd.Parameters.Add("@Sucursal", SqlDbType.Int).Value = numeroSucursal;
        //        cmd.Parameters.Add("@desde", SqlDbType.Date).Value = desde;
        //        cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = hasta;
        //        using (SqlDataReader dr = cmd.ExecuteReader())
        //        {
        //            while (dr.Read())
        //            {
        //                GuiaDespacho GuiaDespacho = new GuiaDespacho();
        //                GuiaDespacho.Cargar(dr);
        //                guiasDespacho.Add(GuiaDespacho);
        //            }
        //        }
        //    }

        //    return guiasDespacho;
        //}
        //public List<GuiaDespacho> ObtenerTodas(DateTime desde, DateTime hasta, string rutCliente, int numeroSucursal, string codigoAduana)
        //{
        //    List<GuiaDespacho> guiasDespacho = new List<GuiaDespacho>();
        //    using (SqlConnection conn = new SqlConnection(this.connectionString))
        //    {
        //        conn.Open();
        //        string query = ObtenerQuery("") + @" Where CLIENTE.RUTCLIENTE = @Rut and cliente.NUMSUCURSAL = @Sucursal and ingreso.ENTR_CODADUANA = @aduana and GUIA_DESPACHO.FECHAGUIA BETWEEN @desde and @hasta";
        //        SqlCommand cmd = new SqlCommand(query, conn);
        //        cmd.Parameters.Add("@Rut", SqlDbType.NVarChar, 11).Value = rutCliente;
        //        cmd.Parameters.Add("@Sucursal", SqlDbType.Int).Value = numeroSucursal;
        //        cmd.Parameters.Add("@Aduana", SqlDbType.NVarChar, 6).Value = codigoAduana;
        //        cmd.Parameters.Add("@desde", SqlDbType.Date).Value = desde;
        //        cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = hasta;
        //        using (SqlDataReader dr = cmd.ExecuteReader())
        //        {
        //            while (dr.Read())
        //            {
        //                GuiaDespacho GuiaDespacho = new GuiaDespacho();
        //                GuiaDespacho.Cargar(dr);
        //                guiasDespacho.Add(GuiaDespacho);
        //            }
        //        }
        //    }

        //    return guiasDespacho;
        //}
        //public List<GuiaDespacho> ObtenerTodas(DateTime desde, DateTime hasta, string codigoAduana)
        //{
        //    List<GuiaDespacho> guiasDespacho = new List<GuiaDespacho>();
        //    using (SqlConnection conn = new SqlConnection(this.connectionString))
        //    {
        //        conn.Open();
        //        string query = ObtenerQuery("") + @" Where ingreso.ENTR_CODADUANA = @aduana and GUIA_DESPACHO.FECHAGUIA BETWEEN @desde and @hasta";
        //        SqlCommand cmd = new SqlCommand(query, conn);
        //        cmd.Parameters.Add("@Aduana", SqlDbType.NVarChar, 6).Value = codigoAduana;
        //        cmd.Parameters.Add("@desde", SqlDbType.Date).Value = desde;
        //        cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = hasta;
        //        using (SqlDataReader dr = cmd.ExecuteReader())
        //        {
        //            while (dr.Read())
        //            {
        //                GuiaDespacho GuiaDespacho = new GuiaDespacho();
        //                GuiaDespacho.Cargar(dr);
        //                guiasDespacho.Add(GuiaDespacho);
        //            }
        //        }
        //    }

        //    return guiasDespacho;
        //}
        //public GuiaDespacho Obtener(int id) {
        //    GuiaDespacho GuiaDespacho = new GuiaDespacho();

        //    return GuiaDespacho;
        //}

        private string EnviarSII(int intCorrelativo)
        {
            string filename = null;

            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("insesql_GDespElec", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@correl", SqlDbType.Int).Value = intCorrelativo;
                cmd.Parameters.Add("@fechafact", SqlDbType.Date).Value = DateTime.Now;

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        filename = dr[0].ToString();
                    }
                }
                cmd.Connection.Close();

                return filename;
            }
        }

        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into (
                                   ) values
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update  set

                                    where ";
                    break;

                case "delete":
                    strQuery = @"Delete From ";
                    break;

                default:
                    strQuery = @"select
	                                   PARA_CODPARAM  --0
                                        ,PARA_DESPARAM  --1
                                    FROM parametros where TAPA_CODTABLA = 56 ";
                    break;
            }


            return strQuery;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }

        private string GetQuery()
        {
            string strQuery = string.Empty;

            strQuery = @"SELECT TOP 100
                            INGRESO.ENTR_CORRELATIV -- 0
                            ,GUIA_DESPACHO.CORRELATIVO -- 1
                            ,GUIA_DESPACHO.NUMDESPACHO -- 2
                            ,GUIA_DESPACHO.CODAGENTE -- 3
                            ,GUIA_DESPACHO.NUMGUIA -- 4
                            ,GUIA_DESPACHO.FECHAGUIA -- 5
                            ,INGRESO.ENTR_NUMIDENTIF -- 6
                            ,DESPACHO_ANEXOING.DEAN_REFCLIENTE -- 7
                            ,GUIA_DESPACHO.GUDE_NOMCLIE -- 8
                            ,GUIA_DESPACHO.GUDE_DIRCLIE -- 9
                            ,GUIA_DESPACHO.GUDE_FONOCLI -- 10
                            ,GUIA_DESPACHO.GUDE_VEHICUL -- 11
                            ,GUIA_DESPACHO.GUDE_DIRENTR -- 12
                            ,GUIA_DESPACHO.GUDE_SENOR -- 13
                            ,GUIA_DESPACHO.GUDE_CUADRIL -- 14
                            ,GUIA_DESPACHO.GUDE_GRUA -- 15
                            ,GUIA_DESPACHO.GUDE_HORARIO -- 16
                            ,GUIA_DESPACHO.GUDE_FONO -- 17
                            ,GUIA_DESPACHO.GUDE_OBS -- 18
                            ,GUIA_DESPACHO.GUDE_TRASPOR -- 19
                            ,GUIA_DESPACHO.GUDE_EMPRESA -- 20
                            ,GUIA_DESPACHO.GUDE_PATENTE -- 21
                            ,GUIA_DESPACHO.GUDE_HORA -- 22
                            ,GUIA_DESPACHO.GUDE_OBSCONS -- 23
                            ,GUIA_DESPACHO.GUDE_NOMREC -- 24
                            ,GUIA_DESPACHO.GUDE_FECHAREC -- 25
                            ,GUIA_DESPACHO.GUDE_HORAREC -- 26
                            ,GUIA_DESPACHO.GUDE_TOTKILO -- 27
                            ,GUIA_DESPACHO.GUDE_TOTCIF -- 28
                            ,GUIA_DESPACHO.GUDE_ESTADO -- 29
                            ,GUIA_DESPACHO.GUDE_PARCIAL -- 30
                            ,GUIA_DESPACHO.GUDE_TOTGUIAS -- 31
                            ,GUIA_DESPACHO.GUDE_TIPODOC -- 32
                            ,GUIA_DESPACHO.GUDE_FECHRETIRO -- 33
                            ,GUIA_DESPACHO.GUDE_HORARETIRO -- 34
                            ,GUIA_DESPACHO.GUDE_RESPRETIRO -- 35
                            ,GUIA_DESPACHO.GUDE_RESPGUIA -- 36
                            ,GUIA_DESPACHO.GUDE_MOTIVOANUL -- 37
                            ,GUIA_DESPACHO.GUDE_RUTFUNCION -- 38
                            ,GUIA_DESPACHO.GUDE_PEONETAS -- 39
                            ,GUIA_DESPACHO.GUDE_SELLO_CONEXXION -- 40
                            ,GUIA_DESPACHO.GUDE_SELLO_AGENCIA -- 41
                            ,GUIA_DESPACHO.GUDE_PATENTE_RMP -- 42
                            ,GUIA_DESPACHO.GUDE_FLETE_ESPECIAL -- 43
                            ,GUIA_DESPACHO.GUDE_CHOFER -- 44
                            ,GUIA_DESPACHO.GUDE_RUTCHOFER -- 45
                            ,GUIA_DESPACHO.GUDE_CARGA -- 46
                            ,GUIA_DESPACHO.GUDE_TIPO -- 47
                            ,GUIA_DESPACHO.GUDE_SELLO -- 48
                            ,GUIA_DESPACHO.GUDE_TIPOELECTRO -- 49
                            ,GUIA_DESPACHO.GUDE_MANIFIESTO -- 50
                            ,GUIA_DESPACHO.GUDE_FECHAMANIFIESTO -- 51
                            ,GUIA_DESPACHO.envio_cliente -- 52
                            ,GUIA_DESPACHO.informacion_destino -- 53
                            ,CLIENTE.RUTCLIENTE -- 54
                            ,cliente.NUMSUCURSAL -- 55
                            ,CLIENTE.CLIE_RAZONSOCIA -- 56
                            ,CLIENTE.CLIE_RAZONCORTA -- 57
                            ,CLIENTE.CLIE_DIRCLI -- 58
                            ,CLIENTE.CLIE_FONOCL -- 59
                            ,AGENTES.AGEN_CODAGENTE -- 60
                            ,agentes.AGEN_RUTAGENTE -- 61
                            ,agentes.AGEN_NOMAGENTE -- 62
                            ,'' as transporteNacional_codigo -- 63
                            ,'' as transporteNacional_nombre -- 64
                            ,aduanas.PARA_CODPARAM as aduana_codigo -- 65
                            ,aduanas.PARA_DESPARAM as aduana_nombre -- 66
                            ,'' as almacenista_codigo -- 67
                            ,'' as almacenista_nombre -- 68
                            ,FUNCIONARIO.RUTFUNCION -- 69
                            ,FUNCIONARIO.FUNC_NOMBRES -- 70
                            ,FUNCIONARIO.FUNC_APELLIDO -- 71
                            ,tipoCarga.PARA_CODPARAM tipoCarga_codigo -- 72
                            ,tipoCarga.PARA_DESPARAM tipoCarga_nombre -- 73
                            ,Gude_NombreArchivo -- 74

                             from  GUIA_DESPACHO
                             inner join DESPACHO_ANEXOING
                             on DESPACHO_ANEXOING.NUMDESPACHINGRE = GUIA_DESPACHO.NUMDESPACHO

                             inner join INGRESO
                             on ingreso.ENTR_NUMDESPING = DESPACHO_ANEXOING.NUMDESPACHINGRE
 
                             inner join CLIENTE
                             on cliente.RUTCLIENTE = DESPACHO_ANEXOING.RUTCLIENTE
                             and cliente.NUMSUCURSAL = DESPACHO_ANEXOING.NUMSUCURSAL

                             inner join AGENTES
                             on agentes.AGEN_CODAGENTE = GUIA_DESPACHO.CODAGENTE

                            -- left join PARAMETROS as transporteNacional
                            -- on transporteNacional.tapa_codTabla = 19
                            --falta referencia

                             inner join PARAMETROS as aduanas
                             on aduanas.TAPA_CODTABLA = 11
                             and aduanas.PARA_CODPARAM = ingreso.ENTR_CODADUANA

                             --left join PARAMETROS as almacenista
                             --on almacenista.PARA_CODPARAM = 12
                             --and almacenista.PARA_CODPARAM = 

                             left join FUNCIONARIO
                             on FUNCIONARIO.RUTFUNCION = GUIA_DESPACHO.GUDE_RESPRETIRO

                             left join parametros as tipoCarga
                             on tipoCarga.TAPA_CODTABLA = 53
                             and tipoCarga.PARA_CODPARAM = GUIA_DESPACHO.GUDE_CARGA ";

            return strQuery;
        }

        private string GetQueryInsert()
        {
            string strQuery = string.Empty;



            strQuery = @"insert into GUIA_DESPACHO(
                            NUMDESPACHO
                            ,CODAGENTE
                            ,NUMGUIA
                            ,FECHAGUIA
                            ,GUDE_NOMCLIE
                            ,GUDE_DIRCLIE
                            ,GUDE_FONOCLI
                            ,GUDE_VEHICUL
                            ,GUDE_DIRENTR
                            ,GUDE_SENOR
                            ,GUDE_CUADRIL
                            ,GUDE_GRUA
                            ,GUDE_HORARIO
                            ,GUDE_FONO
                            ,GUDE_OBS
                            ,GUDE_TRASPOR
                            ,GUDE_EMPRESA
                            ,GUDE_PATENTE
                            ,GUDE_HORA
                            ,GUDE_OBSCONS
                            ,GUDE_NOMREC
                            ,GUDE_FECHAREC
                            ,GUDE_HORAREC
                            ,GUDE_TOTKILO
                            ,GUDE_TOTTARA
                            ,GUDE_TOTKILONETO
                            ,GUDE_TOTCIF
                            ,GUDE_ESTADO
                            ,GUDE_PARCIAL
                            ,GUDE_TOTGUIAS
                            ,GUDE_TIPODOC
                            ,GUDE_FECHRETIRO
                            ,GUDE_HORARETIRO
                            ,GUDE_RESPRETIRO
                            ,GUDE_RESPGUIA
                            ,GUDE_MOTIVOANUL
                            ,GUDE_RUTFUNCION
                            ,GUDE_PEONETAS
                            ,GUDE_SELLO_CONEXXION
                            ,GUDE_SELLO_AGENCIA
                            ,GUDE_PATENTE_RMP
                            ,GUDE_FLETE_ESPECIAL
                            ,GUDE_CHOFER
                            ,GUDE_RUTCHOFER
                            ,GUDE_CARGA
                            ,GUDE_TIPO
                            ,GUDE_SELLO
                            ,GUDE_TIPOELECTRO
                            ,GUDE_MANIFIESTO
                            ,GUDE_FECHAMANIFIESTO
                            ,informacion_destino) VALUES(
	                            @NUMDESPACHO
	                            ,@CODAGENTE
	                            ,@NUMGUIA
	                            ,@FECHAGUIA
	                            ,@GUDE_NOMCLIE
	                            ,@GUDE_DIRCLIE
	                            ,@GUDE_FONOCLI
	                            ,@GUDE_VEHICUL
	                            ,@GUDE_DIRENTR
	                            ,@GUDE_SENOR
	                            ,@GUDE_CUADRIL
	                            ,@GUDE_GRUA
	                            ,@GUDE_HORARIO
	                            ,@GUDE_FONO
	                            ,@GUDE_OBS
	                            ,@GUDE_TRASPOR
	                            ,@GUDE_EMPRESA
	                            ,@GUDE_PATENTE
	                            ,@GUDE_HORA
	                            ,@GUDE_OBSCONS
	                            ,@GUDE_NOMREC
	                            ,@GUDE_FECHAREC
	                            ,@GUDE_HORAREC
	                            ,@GUDE_TOTKILO
                                ,@GUDE_TOTTARA
                                ,@GUDE_TOTKILONETO
	                            ,@GUDE_TOTCIF
	                            ,@GUDE_ESTADO
	                            ,@GUDE_PARCIAL
	                            ,@GUDE_TOTGUIAS
	                            ,@GUDE_TIPODOC
	                            ,@GUDE_FECHRETIRO
	                            ,@GUDE_HORARETIRO
	                            ,@GUDE_RESPRETIRO
	                            ,@GUDE_RESPGUIA
	                            ,@GUDE_MOTIVOANUL
	                            ,@GUDE_RUTFUNCION
	                            ,@GUDE_PEONETAS
	                            ,@GUDE_SELLO_CONEXXION
	                            ,@GUDE_SELLO_AGENCIA
	                            ,@GUDE_PATENTE_RMP
	                            ,@GUDE_FLETE_ESPECIAL
	                            ,@GUDE_CHOFER
	                            ,@GUDE_RUTCHOFER
	                            ,@GUDE_CARGA
	                            ,@GUDE_TIPO
	                            ,@GUDE_SELLO
	                            ,@GUDE_TIPOELECTRO
	                            ,@GUDE_MANIFIESTO
	                            ,@GUDE_FECHAMANIFIESTO
	                            ,@informacion_destino
                            )
                            SELECT CAST(scope_identity() AS INT)";

            return strQuery;
        }

        private string GetQueryInsertDetalle()
        {

            string strQuery = @"INSERT INTO DET_GUIA_DESPA(
	                                    CORRELATIVO 		 , 
	                                    DEGD_DESCBUL 		 , 
	                                    DEGD_CANTIDAD 		 , 
	                                    DEGD_TIPOBULTO 		 , 
	                                    DEGD_MERCADERIA 	 )
                                    VALUES (
	                                    @P_CORRELATIVO		 , 
	                                    @P_DEGD_DESCBUL		 , 
	                                    @P_DEGD_CANTIDAD	 , 
	                                    @P_DEGD_TIPOBULTO	 , 
	                                    @P_DEGD_MERCADERIA	 )";

            return strQuery;
        }


        public GuiaDespacho Obtener(string despacho, int? id = null)
        {
            GuiaDespacho guia = new GuiaDespacho();
            using (SqlCommand command = new SqlCommand("prc_guiasdespacho_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@numero_despacho", SqlDbType.VarChar, 50).Value = despacho;
                if (id.HasValue)
                    command.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        guia = Cargar(dr);
                }
                command.Connection.Close();
            }
            return guia;
        }

        public IEnumerable<GuiaDespacho> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_guiasDespacho_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fechaDesde", DateTime.Now.AddMonths(-1));

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        GuiaDespacho guia = Cargar(dr);
                        yield return guia;
                    }
                }
                cmd.Connection.Close();
            }
        }

        public void Guardar(GuiaDespacho guia)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@guiaDespacho_ID", SqlDbType.Int).Value = guia.Id;
                        command.Parameters.Add("@guiaDespacho_Numero", SqlDbType.Int).Value = guia.Numero;
                        command.Parameters.Add("@guiaDespacho_Fecha", SqlDbType.DateTime).Value = guia.Fecha;
                        command.Parameters.Add("@guiaDespacho_rutFuncionario", SqlDbType.VarChar, 20).Value = guia.RutUsuario;
                        //command.Parameters.Add("@guiaDespacho_Referencia", SqlDbType.VarChar).Value = guia.Referencia;

                        command.Parameters.Add("@guiaDespacho_tipoDocumento", SqlDbType.VarChar, 25).Value = guia.TipoDocumento;
                        command.Parameters.Add("@guiaDespacho_numeroDespacho", SqlDbType.VarChar, 25).Value = guia.NumeroDespacho;
                        //command.Parameters.Add("@guiaDespacho_numeroDeclaracion", SqlDbType.VarChar).Value = guia.NumeroDeclaracion;
                        //command.Parameters.Add("@guiaDespacho_fechaDeclaracion", SqlDbType.VarChar).Value = guia.FechaAceptacion;
                        //command.Parameters.Add("@guiaDespacho_documentoTransporte", SqlDbType.VarChar).Value = guia.DocumentoTransporte;
                        //command.Parameters.Add("@guiaDespacho_nombreNave", SqlDbType.VarChar).Value = guia.NombreNave;
                        //command.Parameters.Add("@guiaDespacho_fechaDocumentoTransporte", SqlDbType.VarChar).Value = guia.FechaDocumentoTransporte;
                        command.Parameters.Add("@guiaDespacho_numeroManifiesto", SqlDbType.VarChar).Value = guia.NumeroManifiesto;
                        command.Parameters.Add("@guiaDespacho_fechaManifiesto", SqlDbType.DateTime).Value = guia.FechaManifiesto;
                        //command.Parameters.Add("@guiaDespacho_indicadorTraslado", SqlDbType.VarChar).Value = guia.IndicadorTraslado;
                        command.Parameters.Add("@guiaDespacho_parcialTotal", SqlDbType.VarChar, 100).Value = guia.ParcialTotal;

                        command.Parameters.Add("@guiaDespacho_kilosNeto", SqlDbType.Decimal).Value = guia.KilosNetos;
                        command.Parameters.Add("@guiaDespacho_kilosBruto", SqlDbType.Decimal).Value = guia.KilosBruto;
                        command.Parameters.Add("@guiaDespacho_kilosTara", SqlDbType.Decimal).Value = guia.KilosTara;
                        command.Parameters.Add("@guiaDespacho_totalCif", SqlDbType.Decimal).Value = guia.CIF;

                        //command.Parameters.Add("@guiaDespacho_direccionEntrega", SqlDbType.VarChar).Value = guia.DireccionEntrega;
                        command.Parameters.Add("@guiaDespacho_destino", SqlDbType.VarChar, 250).Value = guia.Destino != null ? guia.Destino : "";
                        command.Parameters.Add("@guiaDespacho_cuadrilla", SqlDbType.VarChar, 250).Value = guia.Cuadrilla;
                        command.Parameters.Add("@guiaDespacho_grua", SqlDbType.VarChar, 250).Value = guia.Grua != null ? guia.Grua : "";
                        command.Parameters.Add("@guiaDespacho_senor", SqlDbType.VarChar, 250).Value = guia.Senor;
                        //command.Parameters.Add("@guiaDespacho_horaRecepcion", SqlDbType.VarChar).Value = guia.HoraRecepcion;
                        //command.Parameters.Add("@guiaDespacho_telefonoRecepcion", SqlDbType.VarChar).Value = guia.TelefonoRecepcion;

                        command.Parameters.Add("@guiaDespacho_chofer", SqlDbType.VarChar, 250).Value = guia.Chofer;
                        command.Parameters.Add("@guiaDespacho_rutChofer", SqlDbType.VarChar, 20).Value = guia.RutChofer;
                        command.Parameters.Add("@guiaDespacho_patente", SqlDbType.VarChar, 20).Value = guia.Patente;
                        //command.Parameters.Add("@guiaDespacho_horaEntrega", SqlDbType.VarChar).Value = guia.HoraEntrega;
                        command.Parameters.Add("@guiaDespacho_observacion", SqlDbType.VarChar, 255).Value = guia.Observacion;
                        command.Parameters.Add("@guiaDespacho_codigoAgente", SqlDbType.VarChar, 20).Value = guia.Agente.Codigo;
                        command.Parameters.Add("@guiaDespacho_motivoAnulacion", SqlDbType.VarChar, 250).Value = guia.MotivoAnulacion != null ? guia.MotivoAnulacion : "";
                        //command.Parameters.Add("@guiaDespacho_codigoTipo", SqlDbType.VarChar).Value = guia.CodigoTipo;
                        //command.Parameters.Add("@guiaDespacho_tipoOperacion", SqlDbType.VarChar).Value = guia.TipoOperacion;
                        command.Parameters.Add("@guiaDespacho_fleteEspecial", SqlDbType.Bit).Value = guia.FleteEspecial;
                        command.Parameters.Add("@guiaDespacho_observacionConsignatario", SqlDbType.VarChar, 250).Value = guia.ObservacionConsignatario != null ? guia.ObservacionConsignatario : "";
                        command.Parameters.Add("@guiaDespacho_telefono", SqlDbType.VarChar, 20).Value = guia.Telefono;
                        command.Parameters.Add("@guiaDespacho_horario", SqlDbType.DateTime).Value = guia.Horario;
                        command.Parameters.Add("@guiaDespacho_recepcionista", SqlDbType.VarChar, 250).Value = guia.Recepcionista != null ? guia.Recepcionista : "";
                        command.Parameters.Add("@guiaDespacho_fechaRecepcion", SqlDbType.DateTime).Value = guia.FechaRecepcion;
                        command.Parameters.Add("@guiaDespacho_responsableRetiro", SqlDbType.VarChar, 250).Value = guia.ResponsableRetiro != null? guia.ResponsableRetiro : "";
                        command.Parameters.Add("@guiaDespacho_fechaRetiro", SqlDbType.DateTime).Value = guia.FechaRetiro;//null
                        command.Parameters.Add("@guiaDespacho_horaRetiro", SqlDbType.VarChar).Value = guia.HoraRetiro != null ? guia.HoraRetiro : "";   //null
                        command.Parameters.Add("@guiaDespacho_vehiculo", SqlDbType.VarChar, 250).Value = guia.Vehiculo;
                        command.Parameters.Add("@guiaDespacho_cantidadPeonetas", SqlDbType.Int).Value = guia.CantidadPeonetas;


                        command.Connection = connection;
                        command.Transaction = transaction;

                        if (guia.Id > 0)
                        {
                            command.CommandText = "prc_guiasDespacho_actualizar";
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_guiasDespachoDetalle_eliminar", connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@GuiaDespachoID", SqlDbType.Int).Value = guia.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            command.CommandText = "prc_guiasDespacho_insertar";
                            command.Parameters["@guiaDespacho_ID"].Value = null;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    guia.Id = (int)dr["correlativo"];
                            }
                        }
                        using (SqlCommand commandDetalle = new SqlCommand("prc_guiasDespachoDetalle_insertar", connection))
                        {
                            commandDetalle.CommandType = CommandType.StoredProcedure;
                            commandDetalle.Parameters.Add("@GuiaDespachoID", SqlDbType.Int).Value = guia.Id;
                            commandDetalle.Parameters.Add("@DetalleGuiaDespacho_ID", SqlDbType.Int);
                            commandDetalle.Transaction = transaction;

                            foreach (GuiaDespachoDetalle detalle in guia.Detalle)
                            {
                                commandDetalle.Parameters.Add("@DescripcionBulto", SqlDbType.VarChar).Value = detalle.DescripcionBulto;
                                commandDetalle.Parameters.Add("@Cantidad", SqlDbType.Int).Value = detalle.Cantidad;
                                commandDetalle.Parameters.Add("@TipoBulto", SqlDbType.Decimal).Value = detalle.TipoBulto;
                                commandDetalle.Parameters.Add("@Mercaderia", SqlDbType.VarChar).Value = detalle.Mercaderia;
                                //commandDetalle.Parameters["@DEGD_TOTAL"].Value = detalle.Total;
                                commandDetalle.ExecuteNonQuery();
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en GuiaDespachoContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }

        private GuiaDespacho Cargar(SqlDataReader dr)
        {
            var guia = new GuiaDespacho();

            guia.Id = (int)dr["ID"];
            guia.Numero = dr.IsDBNull(dr.GetOrdinal("numero_guia")) ? 0 : int.Parse(dr["numero_guia"].ToString());
            guia.Fecha = (DateTime)dr["fecha_guia"];
            guia.Referencia = dr["referencia_cliente"].ToString();
            guia.TipoDocumento = dr["tipo_documento"].ToString();
            guia.NumeroDespacho = dr["numero_despacho"].ToString();
            guia.NumeroDeclaracion = dr["numero_declaracion"].ToString();
            guia.FechaAceptacion = (DateTime)dr["fecha_aceptacion"];
            guia.DocumentoTransporte = dr["documento_transporte"].ToString();
            guia.NombreNave = dr["nombre_nave"].ToString();
            guia.FechaDocumentoTransporte = (DateTime)dr["fecha_documentoTransporte"];
            guia.NumeroManifiesto = dr["numero_manifiesto"].ToString();
            // TODO: determinar qué datos pueden ser nulos,
            //       para evitar consultas y conversiones innecesarias.
            guia.FechaManifiesto = dr["fecha_manifiesto"] as DateTime?;
            //guia.IndicadorTraslado = dr["guiaDespacho_indicadorTralado"].ToString();
            guia.ParcialTotal = dr["parcial_total"].ToString();
            guia.KilosNetos = dr.IsDBNull(dr.GetOrdinal("kilos_neto")) ? 0 : decimal.Parse(dr["kilos_neto"].ToString());
            guia.KilosBruto = dr.IsDBNull(dr.GetOrdinal("kilos_bruto")) ? 0 : decimal.Parse(dr["kilos_bruto"].ToString());
            guia.KilosTara = dr.IsDBNull(dr.GetOrdinal("kilos_tara")) ? 0 : decimal.Parse(dr["kilos_tara"].ToString());
            guia.CIF = (decimal)dr["total_CIF"];
            guia.DireccionEntrega = dr["direccion_entrega"].ToString();
            guia.Destino = dr["destino"].ToString();
            guia.Cuadrilla = dr["cuadrilla"].ToString();
            guia.Grua = dr["grua"].ToString();
            guia.Senor = dr["senor"].ToString();
            guia.HoraRecepcion = dr["hora_recepcion"].ToString();
            //guia.TelefonoRecepcion = dr["guiaDespacho_telefonoRecepcion"].ToString();
            guia.Chofer = dr["chofer"].ToString();
            guia.RutChofer = dr["rut_chofer"].ToString();
            guia.Patente = dr["patente"].ToString();
            guia.Observacion = dr["observacion"].ToString();
            //guia.HoraEntrega = dr["guiaDespacho_horaEntrega"].ToString();
            guia.MotivoAnulacion = dr["motivo_anulacion"].ToString();
            //guia.CodigoTipo = (int)dr["guiaDespacho_codigoTipo"];
            guia.TipoOperacion = dr["tipo_operacion"].ToString();
            guia.FleteEspecial = dr.IsDBNull(dr.GetOrdinal("flete_especial")) ? false : bool.Parse(dr["flete_especial"].ToString());
            guia.ObservacionConsignatario = dr["observacion_consignatario"].ToString();
            guia.Telefono = dr["telefono"].ToString();
            guia.Horario = dr["horario"].ToString();
            //guia.Recepcionista = dr["guiaDespacho_recepcionista"].ToString();
            //guia.FechaRecepcion = (DateTime)dr["guiaDespacho_fechaRecepcionista"];
            guia.ResponsableRetiro = dr["responsable_retiro"].ToString();
            guia.FechaRetiro = dr["fecha_retiro"] as DateTime?;
            guia.HoraRetiro = dr["hora_retiro"].ToString();
            guia.Vehiculo = dr["vehiculo"].ToString();
            guia.CantidadPeonetas = dr.IsDBNull(dr.GetOrdinal("cantidad_peonetas")) ? 0 : int.Parse(dr["cantidad_peonetas"].ToString());

            guia.Cliente = new Transversales.Consumidores.ClientesContext(this.connectionString).Obtener(null, null, int.Parse(dr["cliente_sucursal"].ToString()), dr["cliente_rut"].ToString());
            guia.Aduana = new Transversales.AduanasContext().Obtener(dr["aduana_codigo"].ToString());
            guia.Detalle = new GuiasDespachoDetalleContext(this.connectionString).ObtenerTodos(guia.Id);
            guia.TipoCarga = new Transversales.TiposCargasContext().Obtener(dr["tipoCarga_codigo"].ToString());
            guia.Agente = new Transversales.AgentesContext().Obtener(dr["codigo_agente"].ToString());

            guia.TransporteNacional = new Browne.Core.Modelo.Transversales.TransporteNacional();
            guia.TransporteNacional.Codigo = "666";
            guia.TransporteNacional.Nombre = "falta transporte";

            return guia;
        }
    }
}