﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinAntecedentesFinancierosContext
    {
        private string connectionString = string.Empty;

        public DinAntecedentesFinancierosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinAntecedentesFinancierosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinAntecedentesFinancieros Obtener(int correlativo)
        {
            DinAntecedentesFinancieros dinAntecedentesFinancieros = new DinAntecedentesFinancieros();
            using (SqlCommand command = new SqlCommand("prc_DinAntecedentesFinancieros_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinAntecedentesFinancieros = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinAntecedentesFinancieros;
        }

        private DinAntecedentesFinancieros Cargar(SqlDataReader dr)
        {

            DinAntecedentesFinancieros dinAntecedentesFinancieros = new DinAntecedentesFinancieros();

            dinAntecedentesFinancieros.RegimenImportacion = dr["GreGimp"].ToString();
            dinAntecedentesFinancieros.CodigoRegimenImportacion = Convert.ToInt32(dr["Regimp"]);
            dinAntecedentesFinancieros.CodigoBancoComercial = (dr["Bcocom"].ToString() == SqlString.Null || dr["Bcocom"].ToString() == "") ? 0 : Convert.ToInt32(dr["Bcocom"].ToString());
            dinAntecedentesFinancieros.CodigoOrigenDivisas = Convert.ToInt32(dr["CodOrdiv"]);
            dinAntecedentesFinancieros.FormaPago = Convert.ToInt32(dr["FormPago"]);
            dinAntecedentesFinancieros.NumeroDias = Convert.ToInt32(dr["NumDias"]);
            dinAntecedentesFinancieros.ValorMercanciasExFabrica = (decimal)dr["ValexFab"];
            dinAntecedentesFinancieros.Moneda = Convert.ToInt32(dr["Moneda"]);
            dinAntecedentesFinancieros.GastosFob = (decimal)dr["MonGasFob"];
            dinAntecedentesFinancieros.ClausulaCompra = Convert.ToInt32(dr["ClCompra"]);
            dinAntecedentesFinancieros.FormaPagoGravamenes = Convert.ToInt32(dr["pagoGrav"]);
            dinAntecedentesFinancieros.FechaSolicitudFondos = dr["FecSolicitudFondos"].ToString();
            dinAntecedentesFinancieros.MontoSolicitudFondos = Convert.ToDecimal(dr["MonSolicitudFondos"]);
            dinAntecedentesFinancieros.FechaRecepcionFondos = dr["FecRecepcionFondos"].ToString();
            dinAntecedentesFinancieros.MontoRecepcionFondos = Convert.ToDecimal(dr["MonRecepcionFondos"]);
            dinAntecedentesFinancieros.FechaPago = dr["FecPago"].ToString();
            
            return dinAntecedentesFinancieros;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
