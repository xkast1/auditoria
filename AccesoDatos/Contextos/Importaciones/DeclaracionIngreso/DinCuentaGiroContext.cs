﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinCuentaGiroContext
    {
        private string connectionString = string.Empty;

        public DinCuentaGiroContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinCuentaGiroContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinCuentaGiro Obtener(int correlativo)
        {
            DinCuentaGiro dinCuentaGiro = new DinCuentaGiro();
            using (SqlCommand command = new SqlCommand("prc_DinCuentaGiro_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinCuentaGiro = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinCuentaGiro;
        }

        public IEnumerable<DinCuentaGiro> ObtenerTodos(int? correlativo = null)
        {
            //DinCuentaGiro dinCuentaGiro = new DinCuentaGiro();
            using (SqlCommand command = new SqlCommand("prc_DinCuentaGiro_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                if (correlativo.HasValue)
                    command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DinCuentaGiro dinCuentaGiro = Cargar(dr);
                            yield return dinCuentaGiro;
                        }
                    }
                }
                command.Connection.Close();
            }
        }

        private DinCuentaGiro Cargar(SqlDataReader dr)
        {

            DinCuentaGiro dinCuentaGiro = new DinCuentaGiro();

            dinCuentaGiro.CuentaOtro = Convert.ToInt32(dr["CtaOtro"]);
            dinCuentaGiro.MontoOtro = (decimal)dr["MonOtro"];

            return dinCuentaGiro;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
