﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinTotalesContext
    {
        private string connectionString = string.Empty;

        public DinTotalesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinTotalesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinTotal Obtener(int correlativo)
        {
            DinTotal dinTotal = new DinTotal();
            using (SqlCommand command = new SqlCommand("prc_DinTotales_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinTotal = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinTotal;
        }

        private DinTotal Cargar(SqlDataReader dr)
        {

            DinTotal dinTotal = new DinTotal();

            dinTotal.TotalItems = Convert.ToInt32(dr["TotItems"]);
            dinTotal.Fob = (decimal)dr["Fob"];
            dinTotal.TotalHojas = Convert.ToInt32(dr["TotHojas"]);
            dinTotal.CodigoFlete = dr.IsDBNull(3) ? Convert.ToInt32(dr["CodFle"]) : 0;
            dinTotal.Flete = (decimal)dr["Flete"];
            dinTotal.TotalBultos = Convert.ToInt32(dr["totBultos"]);
            dinTotal.CodigoSeguro = dr.IsDBNull(6) ? Convert.ToInt32(dr["CodSeg"]) : 6;
            dinTotal.Seguro = (decimal)dr["Seguro"];
            dinTotal.TotalPeso = (decimal)dr["TotPeso"];
            dinTotal.Cif = (decimal)dr["Cif"];

            return dinTotal;

    }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
