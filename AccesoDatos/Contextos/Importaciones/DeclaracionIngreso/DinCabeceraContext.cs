﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinCabeceraContext
    {
        private string connectionString = string.Empty;

        public DinCabeceraContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinCabeceraContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }


        public IEnumerable<DinCabecera> ObtenerTodos()
        {
            //Din DeclaracionIngreso = new Din();

            using (SqlCommand command = new SqlCommand("prc_dincabecera_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DinCabecera dinCabecera = Cargar(dr);
                        yield return dinCabecera;
                    }
                    //if (dr.HasRows && dr.Read())

                }
                command.Connection.Close();
            }

        }


        public DinCabecera Obtener(int correlativo)
        {
            DinCabecera dinCabecera = new DinCabecera();
            using (SqlCommand command = new SqlCommand("prc_dincabecera_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinCabecera = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinCabecera;
        }


        private DinCabecera Cargar(SqlDataReader dr)
        {
            DinCabecera dinCabecera = new DinCabecera();

            //Din.LstCabecera = new ()
            
            dinCabecera.Forma = dr.IsDBNull(0) ? 0 : (decimal)dr["Form"];
            dinCabecera.NumeroIdentificacion = dr["NumIdentif"].ToString();
            dinCabecera.FechaVencimiento = dr.IsDBNull(3) ? null : (DateTime?)dr["FecVenci"];
            dinCabecera.Aduana = new Transversales.AduanasContext(ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString).Obtener(null,Convert.ToInt32(dr["Adu"]));
                //(int)dr["Adu"];
            dinCabecera.Agente = dr["Agente"].ToString();
            dinCabecera.TipoDocumento = Convert.ToInt32(dr["TpoDocto"]);//tipoDocumento
            dinCabecera.TipoIngreso = dr["TipoIngr"].ToString(); // no existe en el sistema // OPCIONAL
            dinCabecera.NumeroAutorizacion = dr["NumAut"].ToString();//Autorizacion Banco Central no existe en el sistema // OPCIONAL
            dinCabecera.FechaAutorizacion = dr.IsDBNull(9) ? null : (DateTime?)dr["FecAut"]; //no existe en sistema // OPCIONAL
            dinCabecera.GlosaAutorizacion = dr["Gbcocen"].ToString();// OPCIONAL
            dinCabecera.FechaTransaccion = dr.IsDBNull(11) ? null : (DateTime?)dr["Fectra"];// OPCIONAL
            dinCabecera.FechaAceptacion = dr.IsDBNull(12) ? null : (DateTime?)dr["Fecacep"];
            dinCabecera.Aforo = dr["Aforo"].ToString();// OPCIONAL
            dinCabecera.FechaConfeccionDeclaracion = dr.IsDBNull(14) ? null : (DateTime?)dr["FecConfdi"];//FecConfDi  = fecha transaccion
            dinCabecera.NumeroInscripcion = dr["NumenCrip"].ToString();// OPCIONAL
            dinCabecera.FechaConfeccion = dr.IsDBNull(16) ? null : (DateTime?)dr["FecConFec"];// OPCIONAL
            dinCabecera.NumeroIdentificacionAclaracion = dr["NumIdenAcl"].ToString();// OPCIONAL
            dinCabecera.NumeroResolucion = dr["NumRes"].ToString();// OPCIONAL
            dinCabecera.FechaResolucion = dr.IsDBNull(19) ? null : (DateTime?)dr["FecRes"];// OPCIONAL

            dinCabecera.Identificacion = new DinIdentificacionesContext(this.connectionString).Obtener((int)dr["entr_correlativ"]);
            dinCabecera.RegimenSuspensivo = new DinRegimenSuspensivoContext(this.connectionString).Obtener((int)dr["entr_correlativ"]);
            dinCabecera.OrigenTransAlmacenaje = new DinOrigenAlmacenajeContext(this.connectionString).Obtener((int)dr["entr_correlativ"]);
            dinCabecera.AntecedentesFinancieros = new DinAntecedentesFinancierosContext(this.connectionString).Obtener((int)dr["entr_correlativ"]);
            dinCabecera.Total = new DinTotalesContext(this.connectionString).Obtener((int)dr["entr_correlativ"]);

            dinCabecera.FechaPresentacion = dr.IsDBNull(20) ? null : (DateTime?)dr["FecPresentacion"];// = fecha aceptacion // OPCIONAL
            dinCabecera.FechaPresentacionResolucion = dr.IsDBNull(21) ? null : (DateTime?)dr["FecPresentacionResolucion"]; //no existe en sisteam  // OPCIONAL



            return dinCabecera;



        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
