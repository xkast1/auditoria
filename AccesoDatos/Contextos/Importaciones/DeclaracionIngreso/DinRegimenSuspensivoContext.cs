﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinRegimenSuspensivoContext
    {
        private string connectionString = string.Empty;

        public DinRegimenSuspensivoContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinRegimenSuspensivoContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinRegimenSuspensivo Obtener(int correlativo)
        {
            DinRegimenSuspensivo dinRegimenSuspensivo = new DinRegimenSuspensivo();
            using (SqlCommand command = new SqlCommand("prc_DinRegimenSuspensivo_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinRegimenSuspensivo = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinRegimenSuspensivo;
        }

        private DinRegimenSuspensivo Cargar(SqlDataReader dr)
        {

            DinRegimenSuspensivo dinRegimenSuspensivo = new DinRegimenSuspensivo();

            dinRegimenSuspensivo.DireccionAlmacenamiento = dr["Desdiralm"].ToString();
            dinRegimenSuspensivo.CodigoComuna = (dr["CodComrs"].ToString() == SqlString.Null || dr["CodComrs"].ToString() == "") ? 0 : Convert.ToInt32(dr["CodComrs"].ToString());
            dinRegimenSuspensivo.CodigoAduanaControl = Convert.ToInt32(dr["AducTrol"]);
            dinRegimenSuspensivo.Plazo = Convert.ToInt32(dr["NumPlazo"]);
            dinRegimenSuspensivo.IndicadorParcial = Convert.ToInt32(dr["IndParcial"]);
            dinRegimenSuspensivo.NumeroHojasInsumos = Convert.ToInt32(dr["NumHojins"]); 
            dinRegimenSuspensivo.TotalInsumos = Convert.ToInt32(dr["TotInSum"]);
            dinRegimenSuspensivo.CodigoAlmacen = (dr["CodAlma"].ToString() == "") ? 0 : Convert.ToInt32(dr["CodAlma"].ToString());
            dinRegimenSuspensivo.NumeroRegimen = (dr["NumRs"].ToString() == "") ? 0 : Convert.ToInt64(dr["NumRs"].ToString());

            dinRegimenSuspensivo.FechaResolucion = dr["FecRs"].ToString();

            dinRegimenSuspensivo.CodigoAduanaRegimen = Convert.ToInt32(dr["Aduars"]); 
            dinRegimenSuspensivo.NumeroHojasAnexas = Convert.ToInt32(dr["NumHojane"]); 
            dinRegimenSuspensivo.CantidadSecuencias = Convert.ToInt32(dr["NumSec"]);

            return dinRegimenSuspensivo;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
