﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinOrigenAlmacenajeContext
    {
        private string connectionString = string.Empty;

        public DinOrigenAlmacenajeContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinOrigenAlmacenajeContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinOrigenTransAlmacenaje Obtener(int correlativo)
        {
            DinOrigenTransAlmacenaje dinOrigenTransAlmacenaje = new DinOrigenTransAlmacenaje();
            using (SqlCommand command = new SqlCommand("prc_DinOrigenAlmacenaje_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinOrigenTransAlmacenaje = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinOrigenTransAlmacenaje;
        }

        private DinOrigenTransAlmacenaje Cargar(SqlDataReader dr)
        {

            DinOrigenTransAlmacenaje dinOrigenTransAlmacenaje = new DinOrigenTransAlmacenaje();

            dinOrigenTransAlmacenaje.PaisOrigen = (int)dr["PaOrig"];
            dinOrigenTransAlmacenaje.NombrePaisOrigen = dr["GpaOrig"].ToString();
            dinOrigenTransAlmacenaje.PaisAdquisicion = Convert.ToInt32(dr["PaadQ"]);
            dinOrigenTransAlmacenaje.NombrePaisAdquisicion = dr["GpaaDQ"].ToString();
            dinOrigenTransAlmacenaje.ViaTransporte = Convert.ToInt32(dr["ViaTran"]);
            dinOrigenTransAlmacenaje.NombrePuertoEmbarque = dr["DesPuert"].ToString();
            dinOrigenTransAlmacenaje.Transbordo = dr["Transb"].ToString();
            dinOrigenTransAlmacenaje.CodigoPuertoEmbarque = Convert.ToInt32(dr["PtoEmb"]);
            dinOrigenTransAlmacenaje.NombrePuertoDesembarque = dr["GptoDesem"].ToString();
            dinOrigenTransAlmacenaje.CodigoPuertoDesembarque = Convert.ToInt32(dr["PtoDesem"]);
            dinOrigenTransAlmacenaje.TipoCarga = dr["TpoCarga"].ToString();
            dinOrigenTransAlmacenaje.Almacenista = dr["Desalmac"].ToString();
            dinOrigenTransAlmacenaje.CodigoAlmacen = dr["Almacen"].ToString();
            dinOrigenTransAlmacenaje.FechaRecepcionMercaderia = dr["FecAlmac"].ToString();
            dinOrigenTransAlmacenaje.FechaRetiroMercaderia = dr["Fecretiro"].ToString();
            dinOrigenTransAlmacenaje.CiaTransporte = dr["GnomCiat"].ToString();
            dinOrigenTransAlmacenaje.CodigoPaisCiaTransporte = dr["CodPaisCia"].ToString(); ;
            dinOrigenTransAlmacenaje.RutCiaTransporte = Convert.ToInt32(dr["NumRutCia"]);
            dinOrigenTransAlmacenaje.DvRutCiaTransporte = dr["DigVercia"].ToString();
            dinOrigenTransAlmacenaje.NumeroManifiesto = dr["NumManif"].ToString();
            dinOrigenTransAlmacenaje.NumeroManifiesto1 = dr["NumManif1"].ToString();
            dinOrigenTransAlmacenaje.NumeroManifiesto2 = dr["NumManif2"].ToString();
            dinOrigenTransAlmacenaje.FechaManifiesto = dr["FecManif"].ToString();
            dinOrigenTransAlmacenaje.NumeroConocimientoEmbarque = dr["NumConoc"].ToString();
            dinOrigenTransAlmacenaje.FechaConocimientoEmbarque = dr["FecConoc"].ToString();
            dinOrigenTransAlmacenaje.NombreEmisorDocumento = dr["NomEmisor"].ToString();
            dinOrigenTransAlmacenaje.RutEmisorDocumento = Convert.ToInt32(dr["NumRutEmi"]);
            dinOrigenTransAlmacenaje.DvRutEmisorDocumento = Convert.ToInt32(dr["DigVeremi"]);
            dinOrigenTransAlmacenaje.FechaInternacion = dr["FecInternacion"].ToString();
            dinOrigenTransAlmacenaje.FechaBodega = dr["FecBodega"].ToString();

        return dinOrigenTransAlmacenaje;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
