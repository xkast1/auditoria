﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinItemsContext
    {

        private string connectionString = string.Empty;

        public DinItemsContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinItemsContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }


        public DinItem Obtener(int correlativo)
        {
            DinItem dinItem = new DinItem();
            using (SqlCommand command = new SqlCommand("prc_Dinitems_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows)
                    {

                        //while (dr.Read())
                        //{

                            //DinItem dinItem = Cargar(dr);
                            //yield return dinItem;
                            dinItem = Cargar(dr);

                        //}
                    }
                }
                command.Connection.Close();
            }
            return dinItem;
        }

        public IEnumerable<DinItem> ObtenerTodos(int? correlativo = null)
        {
            //DinItem dinItem = new DinItem();
            using (SqlCommand command = new SqlCommand("prc_Dinitems_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                if (correlativo.HasValue)
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if(dr.HasRows)
                    { 

                    while (dr.Read())
                        {

                            DinItem dinItem = Cargar(dr);
                            yield return dinItem;
                        //dinItem = Cargar(dr);

                        }
                    }
                }
                command.Connection.Close();
            }
            
        }


        private DinItem Cargar(SqlDataReader dr)
        {

            DinItem dinItem = new DinItem();

            dinItem.Correlativo = (int)dr["entr_correlativ"];
            dinItem.NumeroItem = Convert.ToInt32(dr["NumItem"]);
            dinItem.Nombre = dr["DNombre"].ToString();
            dinItem.Marca  = dr["DMarca"].ToString();
            dinItem.Variedad  = dr["DVariedad"].ToString();
            dinItem.Otro1  = dr["Dotro1"].ToString();
            dinItem.Otro2  = dr["Dotro2"].ToString();
            dinItem.Atributo5  = dr["Atr5"].ToString();
            dinItem.Atributo6  = dr["Atr6"].ToString();
            dinItem.CodigoAjuste  = dr["SajuItem"].ToString();
            dinItem.Ajuste  = Convert.ToInt32(dr["Ajuitem"]);
            dinItem.CantidadMercancias  = Convert.ToInt32(dr["CantMerc"]);
            dinItem.Medida  = Convert.ToInt32(dr["Medida"]);
            dinItem.CodigoUnidadMedidaEstimada  = Convert.ToInt32(dr["CodEstum"]);
            dinItem.PrecioUnitario  = Convert.ToInt32(dr["PreUnit"]);
            dinItem.Tratado  = Convert.ToInt32(dr["Arancala"]);
            dinItem.CorrelativoDetalle = Convert.ToInt32(dr["NumCor"]);
            dinItem.AcuerdoComercial  = Convert.ToInt32(dr["NumaCu"]);
            dinItem.Cupo  = (dr["Concupo"].ToString() == SqlString.Null || dr["Concupo"].ToString() == "") ? 0 : Convert.ToInt32(dr["Concupo"].ToString());  
            dinItem.ArancelNacional  = Convert.ToInt32(dr["Arancnac"]);
            dinItem.Cif  = Convert.ToInt32(dr["Cifitem"]);
            dinItem.AdValorem  = Convert.ToInt32(dr["Advalala"]);
            dinItem.CodigoAdValorem  = Convert.ToInt32(dr["Adval"]);
            dinItem.AdValoremAdicional  = Convert.ToInt32(dr["Valad"]);

            dinItem.LstObservacionesItem = new DinObservacionesItemsContext(this.connectionString).ObtenerTodos(dinItem.Correlativo, dinItem.NumeroItem);
            //dinItem.LstObservacionesItem.ToList();
            dinItem.LstCuentasItem = new DinCuentasItemContext(this.connectionString).ObtenerTodos(dinItem.Correlativo, dinItem.NumeroItem);
            dinItem.LstCuentasItem.ToList();
            //dinItem.LstObservacionesItem  = (int)dr["NumItem"];
            //dinItem.LstCuentasItem  = (int)dr["NumItem"];

            dinItem.NumeroOrdenCompra  = dr["NumeroOrdenCompra"].ToString();
            dinItem.NumeroEmbarque = (dr["NumeroEmbarque"].ToString() == SqlString.Null || dr["NumeroEmbarque"].ToString() == "") ? 0 : Convert.ToInt32(dr["NumeroEmbarque"].ToString());
            dinItem.NumeroFactura  = dr["NumeroFactura"].ToString();
            dinItem.IdBultos  = dr["tipoBulto"].ToString();

            return dinItem;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }


    }
}
