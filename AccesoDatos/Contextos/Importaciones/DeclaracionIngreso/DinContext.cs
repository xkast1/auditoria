﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using Browne.Core.Modelo.Transversales;
using System.Linq;

// TODO: Cualificar ruta con Browne.AccesoDatos...
namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinContext
    {
        private string connectionString = string.Empty;

        public DinContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public Din Obtener(string numeroDespacho, Browne.Core.Modelo.Transversales.Operacion.Operacion operacion = null)
        {
            Din Din = new Din();
            using (SqlCommand command = new SqlCommand("prc_dins_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@NumeroDespacho", SqlDbType.VarChar, 20).Value = numeroDespacho;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        Din = Cargar(dr, operacion);
                }
                command.Connection.Close();
            }
            return Din;
        }
        public Din Obtener(int ID)
        {
            Din Din = new Din();
            using (SqlCommand command = new SqlCommand("prc_dins_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@declaracionIngreso_ID", SqlDbType.Int).Value = ID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        Din = Cargar(dr, null);
                }
                command.Connection.Close();
            }
            return Din;
        }

        public void GuardarRespuestaAduana(int Id, string despacho, string numeroAceptacion, DateTime fechaAceptacion,
            string aforo, string tipoIngreso, string rutTecnico, string observacion, string numeroAclaracion)
        {

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                using (SqlCommand command = new SqlCommand("prc_dins_seleccionar"))
                {
                    try
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Nro_Despacho", SqlDbType.VarChar, 10).Value = despacho;
                        command.Parameters.Add("@Nro_Aceptacion", SqlDbType.VarChar, 16).Value = numeroAceptacion;
                        command.Parameters.Add("@Fecha_Aceptacion", SqlDbType.DateTime).Value = fechaAceptacion;
                        command.Parameters.Add("@Aforo", SqlDbType.Char, 2).Value = aforo;
                        command.Parameters.Add("@Tipo_Ingreso", SqlDbType.Char, 1).Value = tipoIngreso;
                        command.Parameters.Add("@RutTecnico", SqlDbType.VarChar, 10).Value = rutTecnico;
                        command.Parameters.Add("@Observacion", SqlDbType.VarChar, 1000).Value = observacion;
                        command.Parameters.Add("@@NUMACLAR", SqlDbType.VarChar, 10).Value = numeroAclaracion;
                        command.Connection.Open();
                        command.ExecuteNonQuery();

                    }
                    catch (Exception e)
                    {

                        throw new Exception(e.Message);
                    }
                    finally
                    {
                        command.Connection.Close();
                    }

                }


            }
        }

        public IEnumerable<Din> ObtenerTodos(bool? aceptada = null, int? declaracionIngreso_ID = null, DateTime? fechaDesde = null, DateTime? fechasHasta = null, Browne.Core.Modelo.Transversales.Operacion.Operacion operacion = null)
        {
            using (var conexion = new SqlConnection(this.connectionString))
            {
                conexion.Open();
                SqlCommand command = new SqlCommand("prc_dins_seleccionar", conexion);
                if (aceptada.HasValue)
                    command.Parameters.Add("@SinAceptar", SqlDbType.Bit).Value = aceptada;
                if (aceptada.HasValue)
                    command.Parameters.Add("@declaracionIngreso_ID", SqlDbType.Int).Value = declaracionIngreso_ID;
                if (aceptada.HasValue)
                    command.Parameters.Add("@fechaDesde", SqlDbType.Bit).Value = fechaDesde;
                if (aceptada.HasValue)
                    command.Parameters.Add("@fechaHasta", SqlDbType.Bit).Value = fechasHasta;
                command.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Din din = Cargar(dr, operacion);
                        yield return din;
                    }
                }
                command.Connection.Close();
            }
        }

        private Din Cargar(SqlDataReader dr, Browne.Core.Modelo.Transversales.Operacion.Operacion operacion)
        {
            Din din = new Din();
            var correlativo = (int)dr["correlativo"];
            din.Correlativo = correlativo;
            din.TipoEnvio = dr["TipoEnvio"].ToString();
            din.Cabecera = new DinCabeceraContext(this.connectionString).Obtener(correlativo);
            din.LstItem = new DinItemsContext(this.connectionString).ObtenerTodos(correlativo);
            din.LstVistosBuenos = new DinVistosBuenosContext(this.connectionString).ObtenerTodos(correlativo);
            din.LstBulto = new DinBultosContext(this.connectionString).ObtenerTodos(correlativo);
            //din.LstBulto.ToList();
            din.CuentaValor = new DinCuentasyValoresContext(this.connectionString).Obtener(correlativo);
            //din.CuentaValor.ToList();

            din.Estado = (dr.IsDBNull(3) ? 0 : int.Parse(dr["estado"].ToString()));
            din.NumeroDespacho = dr["numero_despacho"].ToString();
            din.Operacion = operacion ?? new AccesoDatos.Contextos.Importaciones.Operaciones.OperacionesImportacionesContext(this.connectionString).Obtener(din.NumeroDespacho);
            // Null Coalescence
            return din;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
