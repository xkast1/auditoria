﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinObservacionesItemsContext
    {
        private string connectionString = string.Empty;

        public DinObservacionesItemsContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinObservacionesItemsContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        //public DinCuentaGiro Obtener(int correlativo)
        //{
        //    DinCuentaGiro dinCuentaGiro = new DinCuentaGiro();
        //    using (SqlCommand command = new SqlCommand("prc_DinObservacionesitems_seleccionar"))
        //    {

        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
        //        command.Connection = new SqlConnection(this.connectionString);
        //        command.Connection.Open();
        //        using (SqlDataReader dr = command.ExecuteReader())
        //        {
        //            if (dr.HasRows && dr.Read())
        //                dinCuentaGiro = Cargar(dr);
        //        }
        //        command.Connection.Close();
        //    }
        //    return dinCuentaGiro;
        //}

        public IEnumerable<DinObservacionesItem> ObtenerTodos(int? correlativo = null, int? item = null)
        {

            using (SqlCommand command = new SqlCommand("prc_DinObservacionesitems_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                if (correlativo.HasValue)
                    command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                if (item.HasValue)
                    command.Parameters.Add("@item", SqlDbType.Int).Value = item;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DinObservacionesItem dinObservacionesItem = Cargar(dr);
                            yield return dinObservacionesItem;
                        }
                    }
                }
                command.Connection.Close();
            }
        }

        private DinObservacionesItem Cargar(SqlDataReader dr)
        {

            DinObservacionesItem dinObservacionesItem = new DinObservacionesItem();


            dinObservacionesItem.Correlativo = (int)dr["ENTR_CORRELATIV"];
            dinObservacionesItem.Item = Convert.ToInt32(dr["item"]);
            //dinObservacionesItem.Item = new DinItemsContext(this.connectionString).Obtener((int)dr["ENTR_CORRELATIV"]);
            dinObservacionesItem.Codigo = Convert.ToInt32(dr["CodObs"]);
            dinObservacionesItem.Descripcion = dr["DesObs"].ToString();

            return dinObservacionesItem;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
