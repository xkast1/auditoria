﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinIdentificacionesContext
    {
        private string connectionString = string.Empty;

        public DinIdentificacionesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinIdentificacionesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinIdentificacion Obtener(int correlativo)
        {
            DinIdentificacion DinIdentificacion = new DinIdentificacion();
            using (SqlCommand command = new SqlCommand("prc_DinIdentificacion_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        DinIdentificacion = Cargar(dr);
                }
                command.Connection.Close();
            }
            return DinIdentificacion;
        }

        private DinIdentificacion Cargar(SqlDataReader dr) {

            DinIdentificacion DinIdentificacion = new DinIdentificacion();

            DinIdentificacion.Nombre = dr["Nombre"].ToString();
            DinIdentificacion.Direccion = dr["Direc"].ToString();
            DinIdentificacion.Comuna = new Transversales.ComunasContext(ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString).Obtener(null,Convert.ToInt32(dr["CodComun"]));
            DinIdentificacion.TipoRut = Convert.ToInt32(dr["TipRut"]);
            DinIdentificacion.Rut = Convert.ToInt32(dr["Rut"]);
            DinIdentificacion.DigitoVerificador = dr["DvRut"].ToString();
            DinIdentificacion.NombreRepresentanteLegal = dr["NomRepleg"].ToString(); 
            DinIdentificacion.RutRepresentanteLegal = dr.IsDBNull(7) ? Convert.ToInt32(dr["NumRutRl"]) : 0000000;
            DinIdentificacion.DigitoVerificadorRepresenteLegal = dr["DigVerrl"].ToString(); 
            DinIdentificacion.NombreConsignatario = dr["NomConsig"].ToString(); 
            DinIdentificacion.DireccionConsignatario = dr["DesDirCon"].ToString(); 
            DinIdentificacion.CodigoPais = dr["CodPaisCon"].ToString(); 

            return DinIdentificacion;

    }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
