﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinCuentasyValoresContext
    {
        private string connectionString = string.Empty;

        public DinCuentasyValoresContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinCuentasyValoresContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinCuentaValor Obtener(int correlativo)
        {
            DinCuentaValor dinCuentaValor = new DinCuentaValor();
            using (SqlCommand command = new SqlCommand("prc_DinCuentasyValores_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinCuentaValor = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinCuentaValor;
        }

        private DinCuentaValor Cargar(SqlDataReader dr)
        {

            DinCuentaValor dinCuentaValor = new DinCuentaValor();

            dinCuentaValor.Monto178 = (decimal)dr["Mon178"];
            dinCuentaValor.Monto191 = (decimal)dr["Mon191"];
            dinCuentaValor.Monto699 = (decimal)dr["Mon699"];
            dinCuentaValor.Monto199 = (decimal)dr["Mon199"];

            //public IEnumerable<DinCuentaGiro> LstCuentasGiros { get; set; }
            dinCuentaValor.LstCuentasGiros = new DinCuentaGiroContext(this.connectionString).ObtenerTodos((int)dr["entr_correlativ"]);
            //dinCuentaValor.LstCuentasGiros.ToList();

            dinCuentaValor.Paridad = dr.IsDBNull(5) ? 2 : 0;
            dinCuentaValor.TipoCambio =(decimal)dr["TipoCambio"];

            return dinCuentaValor;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
