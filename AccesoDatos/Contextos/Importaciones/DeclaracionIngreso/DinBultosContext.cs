﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinBultosContext
    {
        private string connectionString = string.Empty;

        public DinBultosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinBultosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinBulto Obtener(int correlativo)
        {
            DinBulto dinBulto = new DinBulto();
            using (SqlCommand command = new SqlCommand("prc_DinBultos_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinBulto = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinBulto;
        }

        public IEnumerable<DinBulto> ObtenerTodos(int? correlativo = null)
        {
            //DinBulto dinBulto = new DinBulto();
            using (SqlCommand command = new SqlCommand("prc_DinBultos_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                if (correlativo.HasValue)
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if(dr.HasRows)
                    { 
                        while (dr.Read())
                        {
                            DinBulto dinBulto = Cargar(dr);
                            yield return dinBulto;
                        }
                    }
                }
                command.Connection.Close();
            }
        }

        private DinBulto Cargar(SqlDataReader dr)
        {

            DinBulto dinBulto = new DinBulto();

            dinBulto.Correlativo = Convert.ToInt32(dr["ENTR_CORRELATIV"]);
            dinBulto.Identificacion = dr["ENTR_IDENTIFI"].ToString();
            dinBulto.Descripcion = dr["DESTIPBUL"].ToString();
            dinBulto.Tipo = Convert.ToInt32(dr["TPOBUL"]);
            dinBulto.Cantidad = Convert.ToInt32(dr["CANTBUL"]);

            return dinBulto;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
