﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinVistosBuenosContext
    {
        private string connectionString = string.Empty;

        public DinVistosBuenosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinVistosBuenosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public DinVistosBuenos Obtener(int correlativo)
        {
            DinVistosBuenos dinVistosBuenos = new DinVistosBuenos();
            using (SqlCommand command = new SqlCommand("prc_DinVistosBuenos_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        dinVistosBuenos = Cargar(dr);
                }
                command.Connection.Close();
            }
            return dinVistosBuenos;
        }


        public IEnumerable<DinVistosBuenos> ObtenerTodos(int? correlativo = null)
        {
            //DinVistosBuenos dinVistosBuenos = new DinVistosBuenos();
            using (SqlCommand command = new SqlCommand("prc_DinVistosBuenos_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    
                    if (dr.HasRows)
                        while (dr.Read())
                        {

                            DinVistosBuenos dinVistosBueno = Cargar(dr);
                            yield return dinVistosBueno;
                            //dinVistosBuenos = Cargar(dr);
                        }
                }
                command.Connection.Close();
            }
            //return dinVistosBuenos;
        }

        private DinVistosBuenos Cargar(SqlDataReader dr)
        {

            DinVistosBuenos dinVistosBuenos = new DinVistosBuenos();
           


            dinVistosBuenos.Correlativo = Convert.ToInt32(dr["entr_correlativ"]);
            dinVistosBuenos.NumeroRegistroReconocimiento = Convert.ToInt32(dr["Nuregr"]);
            dinVistosBuenos.AnoRegistroReconocimiento = Convert.ToInt32(dr["Anoreg"]);
            dinVistosBuenos.CodigoVistoBueno = Convert.ToInt32(dr["CodVisBuen"]);
            dinVistosBuenos.NumeroRegla = Convert.ToInt32(dr["NumRegla"]);
            dinVistosBuenos.NumeroAnoResolucion = Convert.ToInt32(dr["Numanores"]);
            dinVistosBuenos.CodigoUltimoVistoBueno = Convert.ToInt32(dr["CodUltvb"]);
            dinVistosBuenos.FechaResolucion = dr["FecRes"].ToString();

            return dinVistosBuenos;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
