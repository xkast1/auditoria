﻿using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contextos.Importaciones.DeclaracionIngreso
{
    public class DinCuentasItemContext
    {
        private string connectionString = string.Empty;

        public DinCuentasItemContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DinCuentasItemContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        //public DinCuentaGiro Obtener(int correlativo)
        //{
        //    DinCuentaGiro dinCuentaGiro = new DinCuentaGiro();
        //    using (SqlCommand command = new SqlCommand("prc_DinObservacionesitems_seleccionar"))
        //    {

        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
        //        command.Connection = new SqlConnection(this.connectionString);
        //        command.Connection.Open();
        //        using (SqlDataReader dr = command.ExecuteReader())
        //        {
        //            if (dr.HasRows && dr.Read())
        //                dinCuentaGiro = Cargar(dr);
        //        }
        //        command.Connection.Close();
        //    }
        //    return dinCuentaGiro;
        //}

        public IEnumerable<DinCuentasItem> ObtenerTodos(int? correlativo = null, int? item = null)
        {

            using (SqlCommand command = new SqlCommand("prc_DinCuentasitems_seleccionar"))
            {

                command.CommandType = CommandType.StoredProcedure;
                if (correlativo.HasValue)
                    command.Parameters.Add("@entr_correlativ", SqlDbType.Int).Value = correlativo;
                if (item.HasValue)
                    command.Parameters.Add("@item", SqlDbType.Int).Value = item;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DinCuentasItem dinCuentasItem = Cargar(dr);
                            yield return dinCuentasItem;
                        }
                    }
                }
                command.Connection.Close();
            }
        }

        private DinCuentasItem Cargar(SqlDataReader dr)
        {

            DinCuentasItem dinCuentasItem = new DinCuentasItem();


            dinCuentasItem.Correlativo = (int)dr["ENTR_CORRELATIV"];
            dinCuentasItem.Item= Convert.ToInt32(dr["item"]);

            //dinCuentasItem.Item = new DinItemsContext(this.connectionString).Obtener((int)dr["ENTR_CORRELATIV"], (int)dr["Item"]);
            dinCuentasItem.Otro = Convert.ToDecimal(dr["OTRO"]);
            dinCuentasItem.Codigo = Convert.ToInt32(dr["CTA"]);
            dinCuentasItem.SignoValor = dr["SIGVAL"].ToString();
            dinCuentasItem.Valor = Convert.ToInt32(dr["VALOR"]);

            return dinCuentasItem;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
