﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Importaciones.Comunes;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Importaciones.Comunes
{
    public class TiposOperacionesIngresoContext
    {
        private string connectionString = string.Empty;

        public TiposOperacionesIngresoContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposOperacionesIngresoContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public IEnumerable<TipoOperacionIngreso> ObtenerTodos()
        {
            List<TipoOperacionIngreso> tipos = new List<TipoOperacionIngreso>();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " order by PARA_DESPARAM", conn);
                cmd.CommandType = CommandType.Text;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoOperacionIngreso TipoOperacionIngreso = Cargar(dr);
                        yield return TipoOperacionIngreso;
                    }
                }
                cmd.Connection.Close();
            }
        }
        public TipoOperacionIngreso Obtener(string codigo)
        {
            TipoOperacionIngreso TipoOperacionIngreso = new TipoOperacionIngreso();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + "PARA_CODPARAM = @codigo", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@codigo", SqlDbType.NVarChar, 6).Value = codigo;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        TipoOperacionIngreso = Cargar(dr);
                    }
                }
                cmd.Connection.Close();
            }
            return TipoOperacionIngreso;
        }
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " and PARA_DESPARAM like @buscado order by PARA_DESPARAM", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@busqueda", SqlDbType.NVarChar, 50).Value = buscado.Trim() + "%";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar Item = new ResultadoAutocompletar();
                        Item.Id = dr["PARA_CODPARAM"].ToString().Trim();
                        Item.Value = dr["PARA_DESPARAM"].ToString().Trim();
                        Item.Label = dr["PARA_CODPARAM"].ToString().Trim() + " | " + String.Format("{0:d}", dr["PARA_DESPARAM"]).ToUpper();
                        yield return Item;
                    }
                }
                cmd.Connection.Close();
            }
        }
        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into (
                                   ) values
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update  set

                                    where ";
                    break;

                case "delete":
                    strQuery = @"Delete From ";
                    break;

                default:
                    strQuery = @"select
	                                   PARA_CODPARAM  --0
                                        ,PARA_DESPARAM  --1
                                    FROM parametros where TAPA_CODTABLA = 47 ";
                    break;
            }


            return strQuery;
        }
        private TipoOperacionIngreso Cargar(SqlDataReader dr) {
            TipoOperacionIngreso Tipo = new TipoOperacionIngreso();
            Tipo.Codigo = dr.GetInt32(0);
            Tipo.Nombre = dr.GetString(1);
            Tipo.Glosa = dr.GetString(2);
            return Tipo;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }

    }
}
