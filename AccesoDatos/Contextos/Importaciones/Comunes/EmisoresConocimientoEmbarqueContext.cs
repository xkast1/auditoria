﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Importaciones.Comunes;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Importaciones.Comunes
{
    class EmisoresConocimientoEmbarqueContext
    {
        private string connectionString = string.Empty;

        public EmisoresConocimientoEmbarqueContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public EmisoresConocimientoEmbarqueContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public IEnumerable<EmisorConocimientoEmbarque> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " order by PARA_DESPARAM", conn);
                cmd.CommandType = CommandType.Text;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        EmisorConocimientoEmbarque EmisorConocimientoEmbarque = Cargar(dr);
                        yield return EmisorConocimientoEmbarque;
                    }
                }
                cmd.Connection.Close();
            }
        }

        public EmisorConocimientoEmbarque Obtener(string codigo)
        {
            EmisorConocimientoEmbarque EmisorConocimientoEmbarque = new EmisorConocimientoEmbarque();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + "PARA_CODPARAM = @codigo", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@codigo", SqlDbType.NVarChar, 6).Value = codigo;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                        EmisorConocimientoEmbarque = Cargar(dr);

                }
                cmd.Connection.Close();
            }
            return EmisorConocimientoEmbarque;
        }

        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado)
        {
            List<ResultadoAutocompletar> listado = new List<ResultadoAutocompletar>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " and PARA_DESPARAM like @buscado order by PARA_DESPARAM", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@busqueda", SqlDbType.NVarChar, 50).Value = buscado.Trim() + "%";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar Item = new ResultadoAutocompletar();
                        Item.Id = dr["PARA_CODPARAM"].ToString().Trim();
                        Item.Value = dr["PARA_DESPARAM"].ToString().Trim();
                        Item.Label = dr["PARA_CODPARAM"].ToString().Trim() + " | " + String.Format("{0:d}", dr["PARA_DESPARAM"]).ToUpper();
                        yield return Item;
                    }
                }
                cmd.Connection.Close();
            }
        }
        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into (
                                   ) values
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update  set

                                    where ";
                    break;

                case "delete":
                    strQuery = @"Delete From ";
                    break;

                default:
                    strQuery = @"select
	                                   PARA_CODPARAM  --0
                                        ,PARA_DESPARAM  --1
                                        ,para_siglaparam --2
                                    FROM parametros where TAPA_CODTABLA = 203 ";
                    break;
            }


            return strQuery;
        }
        private EmisorConocimientoEmbarque Cargar(SqlDataReader dr) {
            EmisorConocimientoEmbarque Emisor = new EmisorConocimientoEmbarque();
            Emisor.Codigo = dr.GetString(0);// dr["PARA_CODPARAM"].ToString().Trim(); ;
            Emisor.Nombre = dr.GetString(1);// dr["PARA_DESPARAM"].ToString().Trim(); ;
            Emisor.Rut = dr.GetString(2);// dr["PARA_DESPARAM"].ToString().Trim(); ;
            return Emisor;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
