﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Importaciones.Operacion;
using Browne.Core.Modelo.Importaciones.DeclaracionIngreso;
using Browne.Core.Modelo.Transversales.Operacion;

namespace AccesoDatos.Contextos.Importaciones.Operaciones
{
    public class OperacionesImportacionesContext
    {
        private string connectionString = string.Empty;

        public OperacionesImportacionesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public OperacionesImportacionesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesImportacion_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["operacion_ID"].ToString();
                        autocompletado.Value = dr["operacion_numero"].ToString(); ;
                        autocompletado.Label = dr["operacion_numero"].ToString() + " | " + dr["cliente_razonSocial"].ToString() + " | " + dr["operacion_referencia"].ToString();
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }

        public OperacionImportacion Obtener(string numero)
        {
            OperacionImportacion Operacion = new OperacionImportacion();

            using (SqlCommand command = new SqlCommand("prc_operacionesImportacion_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_numero", SqlDbType.VarChar, 10).Value = numero;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        Operacion = Cargar(dr);
                }
                command.Connection.Close();
            }
            return Operacion;
        }

        public IEnumerable<OperacionImportacion> ObtenerTodos(string tipo, int estado)
        {

            using (SqlCommand command = new SqlCommand("prc_operacionesImportacion_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@operacion_estado", SqlDbType.Int).Value = estado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        OperacionImportacion operacion = Cargar(dr);
                        yield return operacion;
                    }
                }
                command.Connection.Close();
            }
        }
 
        private OperacionImportacion Cargar(SqlDataReader dr)
        {
            OperacionImportacion operacion = new OperacionImportacion();
            operacion.Id = (int)dr["operacion_ID"];
            operacion.Correlativo = (int)dr["operacion_correlativo"];
            operacion.Numero = dr["operacion_numero"].ToString();
            operacion.Fecha = (DateTime)dr["operacion_fecha"];
            operacion.Referencia = dr["operacion_referencia"].ToString();
            operacion.Observacion = dr["operacion_observacion"].ToString();
            operacion.NumeroAceptacion = dr["operacion_numeroAceptacion"].ToString();
            operacion.FechaAceptacion = (DateTime)dr["operacion_fechaAceptacion"];
            operacion.Tipo = dr["operacion_tipo"].ToString();
            operacion.NumeroIdentificacion = dr["numeroIdentificacion"].ToString();
            operacion.TipoDeclaracion = dr["tipoDeclaracion"].ToString();
            operacion.Tipo = dr["operacion_tipo"].ToString();

            //operacion.CreacionUsuario = dr["creacion_usuario"].ToString();
            //operacion.CreacionFecha = (DateTime)dr["creacion_fecha"];
            //operacion.CreacionIp = dr["creacion_IP"].ToString();
            //operacion.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            //operacion.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            //operacion.ActualizacionIp = dr["actualizacion_IP"].ToString();

            operacion.Cliente = new AccesoDatos.Contextos.Transversales.Consumidores.ClientesContext(this.connectionString)
                .Obtener(null, false, int.Parse(dr["cliente_numeroSucursal"].ToString()), dr["cliente_rut"].ToString());
            return operacion;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
