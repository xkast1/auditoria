﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class RegionesContext
    {
        private string connectionString = string.Empty;

        public RegionesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public RegionesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de todas las regiones
        /// </summary>
        /// <param name="paisID"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Region> ObtenerTodos(int? paisID = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_regiones_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@region_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (paisID.HasValue)
                    command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = paisID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Region region = Cargar(dr);
                        yield return region;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de region con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_regiones_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["region_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["region_codigoAduanas"].ToString();
                                etiqueta = dr["region_codigoAduanas"].ToString() + " | " + dr["region_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["region_codigoSicex"].ToString();
                                etiqueta = dr["region_codigoSicex"].ToString() + " | " + dr["region_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["region_nombre"].ToString();
                                etiqueta = dr["region_codigo"].ToString() + " | " + dr["region_nombre"].ToString();
                                break;
                            default:
                                valor = dr["region_codigo"].ToString();
                                etiqueta = dr["region_codigo"].ToString() + " | " + dr["region_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una region
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        public Region Obtener(Region region)
        {

            using (SqlCommand command = new SqlCommand("prc_regiones_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (region.Id != 0)
                    command.Parameters.Add("@region_ID", SqlDbType.Int).Value = region.Id;
                if (!String.IsNullOrEmpty(region.Codigo))
                    command.Parameters.Add("@region_codigo", SqlDbType.VarChar, 10).Value = region.Codigo;
                if (!String.IsNullOrEmpty(region.CodigoAduanas))
                    command.Parameters.Add("@region_codigoAduanas", SqlDbType.VarChar, 10).Value = region.CodigoAduanas;
                if (!String.IsNullOrEmpty(region.CodigoSicex))
                    command.Parameters.Add("@region_codigoSicex", SqlDbType.VarChar, 10).Value = region.CodigoSicex;
                if (region.Bloqueado.HasValue)
                    command.Parameters.Add("@region_bloqueado", SqlDbType.Bit).Value = region.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        region = Cargar(dr);
                }
                command.Connection.Close();
            }

            return region;
        }
        /// <summary>
        /// Obtiene una region a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Region Obtener(int id)
        {
            Region region = new Region();
            region.Id = id;
            using (SqlCommand command = new SqlCommand("prc_regiones_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@region_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        region = Cargar(dr);
                }
                command.Connection.Close();
            }
            return region;

        }
        /// <summary>
        /// Inserta o Actualiza una region
        /// </summary>
        /// <param name="region"></param>
        public void Guardar(Region region)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@region_ID", SqlDbType.Int).Value = region.Id;
                command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = region.Pais.Id;
                command.Parameters.Add("@region_codigo", SqlDbType.VarChar, 10).Value = region.Codigo;
                command.Parameters.Add("@region_codigoAduanas", SqlDbType.VarChar, 10).Value = region.CodigoAduanas;
                command.Parameters.Add("@region_codigoSicex", SqlDbType.VarChar, 10).Value = region.CodigoSicex;
                command.Parameters.Add("@region_nombre", SqlDbType.VarChar, 100).Value = region.Nombre;
                command.Parameters.Add("@region_bloqueado", SqlDbType.Bit).Value = region.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = region.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = region.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = region.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = region.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (region.Id > 0)
                    {
                        command.CommandText = "prc_regiones_actualizar";
                        command.Parameters["@usuario"].Value = region.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = region.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_regiones_insertar";
                        command.Parameters["@region_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en RegionContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Region Cargar(SqlDataReader dr)
        {
            Region region = new Region();
            region.Id = (int)dr["region_ID"];
            region.Codigo = dr["region_codigo"].ToString();
            region.CodigoAduanas = dr["region_codigoAduanas"].ToString();
            region.CodigoSicex = dr["region_codigoSicex"].ToString();
            region.Nombre = dr["region_nombre"].ToString();
            region.Bloqueado = (bool)dr["region_bloqueado"];
            region.ParaCodParam = dr["para_codParam"].ToString();
            region.TapaCodTabla = (int)dr["tapa_codTabla"];
            region.CreacionUsuario = dr["creacion_usuario"].ToString();
            region.CreacionFecha = (DateTime)dr["creacion_fecha"];
            region.CreacionIp = dr["creacion_IP"].ToString();
            region.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            region.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            region.ActualizacionIp = dr["actualizacion_IP"].ToString();
            region.Pais = dr["pais_ID"] != DBNull.Value ? new PaisesContext(this.connectionString)
                .Obtener((int)dr["pais_ID"]) : new Pais();
            region.Provincias = new ProvinciasContext(this.connectionString)
                .ObtenerTodos(region.Id, false);
            return region;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}