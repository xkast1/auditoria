﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class EstadosCargasContenedoresContext
    {
        private string connectionString = string.Empty;

        public EstadosCargasContenedoresContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public EstadosCargasContenedoresContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene el lstdo secuencial de todos los estados de carga
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<EstadoCargaContenedor> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_estadosCargaContenedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@estadoCargaContenedor_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        EstadoCargaContenedor estadoCargaContenedor = Cargar(dr);
                        yield return estadoCargaContenedor;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de estados de carga con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_estadosCargaContenedores_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["estadoCargaContenedor_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["estadoCargaContenedor_codigoAduanas"].ToString();
                                etiqueta = dr["estadoCargaContenedor_codigoAduanas"].ToString() + " | " + dr["estadoCargaContenedor_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["estadoCargaContenedor_codigoSicex"].ToString();
                                etiqueta = dr["estadoCargaContenedor_codigoSicex"].ToString() + " | " + dr["estadoCargaContenedor_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["estadoCargaContenedor_nombre"].ToString();
                                etiqueta = dr["estadoCargaContenedor_codigo"].ToString() + " | " + dr["estadoCargaContenedor_nombre"].ToString();
                                break;
                            default:
                                valor = dr["estadoCargaContenedor_codigo"].ToString();
                                etiqueta = dr["estadoCargaContenedor_codigo"].ToString() + " | " + dr["estadoCargaContenedor_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un estado de carga del contenedor
        /// </summary>
        /// <param name="estadoCargaContenedor"></param>
        /// <returns></returns>
        public EstadoCargaContenedor Obtener(EstadoCargaContenedor estadoCargaContenedor)
        {

            using (SqlCommand command = new SqlCommand("prc_estadosCargaContenedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (estadoCargaContenedor.Id != 0)
                    command.Parameters.Add("@estadoCargaContenedor_ID", SqlDbType.Int).Value = estadoCargaContenedor.Id;
                if (!String.IsNullOrEmpty(estadoCargaContenedor.Codigo))
                    command.Parameters.Add("@estadoCargaContenedor_codigo", SqlDbType.VarChar, 10).Value = estadoCargaContenedor.Codigo;
                if (!String.IsNullOrEmpty(estadoCargaContenedor.CodigoAduanas))
                    command.Parameters.Add("@estadoCargaContenedor_codigoAduanas", SqlDbType.VarChar, 10).Value = estadoCargaContenedor.CodigoAduanas;
                if (!String.IsNullOrEmpty(estadoCargaContenedor.CodigoSicex))
                    command.Parameters.Add("@estadoCargaContenedor_codigoSicex", SqlDbType.VarChar, 10).Value = estadoCargaContenedor.CodigoSicex;
                if (estadoCargaContenedor.Bloqueado.HasValue)
                    command.Parameters.Add("@estadoCargaContenedor_bloqueado", SqlDbType.Bit).Value = estadoCargaContenedor.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        estadoCargaContenedor = Cargar(dr);
                }
                command.Connection.Close();
            }

            return estadoCargaContenedor;
        }
        /// <summary>
        /// Obtiene un estado de carga a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EstadoCargaContenedor Obtener(int id)
        {
            EstadoCargaContenedor estadoCargaContenedor = new EstadoCargaContenedor();
            estadoCargaContenedor.Id = id;
            using (SqlCommand command = new SqlCommand("prc_estadosCargaContenedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@estadoCargaContenedor_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        estadoCargaContenedor = Cargar(dr);
                }
                command.Connection.Close();
            }
            return estadoCargaContenedor;

        }
        /// <summary>
        /// Insertar o Actualizar un estado de carga contenedor
        /// </summary>
        /// <param name="estadoCargaContenedor"></param>
        public void Guardar(EstadoCargaContenedor estadoCargaContenedor)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@estadoCargaContenedor_ID", SqlDbType.Int).Value = estadoCargaContenedor.Id;
                command.Parameters.Add("@estadoCargaContenedor_codigo", SqlDbType.VarChar, 10).Value = estadoCargaContenedor.Codigo;
                command.Parameters.Add("@estadoCargaContenedor_codigoAduanas", SqlDbType.VarChar, 10).Value = estadoCargaContenedor.CodigoAduanas;
                command.Parameters.Add("@estadoCargaContenedor_codigoSicex", SqlDbType.VarChar, 10).Value = estadoCargaContenedor.CodigoSicex;
                command.Parameters.Add("@estadoCargaContenedor_nombre", SqlDbType.VarChar, 100).Value = estadoCargaContenedor.Nombre;
                command.Parameters.Add("@estadoCargaContenedor_nombreIngles", SqlDbType.VarChar, 100).Value = estadoCargaContenedor.NombreIngles;
                command.Parameters.Add("@estadoCargaContenedor_bloqueado", SqlDbType.Bit).Value = estadoCargaContenedor.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = estadoCargaContenedor.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = estadoCargaContenedor.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (estadoCargaContenedor.Id > 0)
                    {
                        command.CommandText = "prc_estadosCargaContenedores_actualizar";
                        command.Parameters["@usuario"].Value = estadoCargaContenedor.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = estadoCargaContenedor.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_estadosCargaContenedores_insertar";
                        command.Parameters["@estadoCargaContenedor_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en EstadoCargaContenedorContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private EstadoCargaContenedor Cargar(SqlDataReader dr)
        {
            EstadoCargaContenedor estadosCargaContenedor = new EstadoCargaContenedor();

            estadosCargaContenedor.Id = (int)dr["estadoCargaContenedor_ID"];
            estadosCargaContenedor.Codigo = dr["estadoCargaContenedor_codigo"].ToString();
            estadosCargaContenedor.CodigoAduanas = dr["estadoCargaContenedor_codigoAduanas"].ToString();
            estadosCargaContenedor.CodigoSicex = dr["estadoCargaContenedor_codigoSicex"].ToString();
            estadosCargaContenedor.Nombre = dr["estadoCargaContenedor_nombre"].ToString();
            estadosCargaContenedor.NombreIngles = dr["estadoCargaContenedor_nombreIngles"].ToString();
            estadosCargaContenedor.Bloqueado = (bool)dr["estadoCargaContenedor_bloqueado"];
            estadosCargaContenedor.CreacionUsuario = dr["creacion_usuario"].ToString();
            estadosCargaContenedor.CreacionFecha = (DateTime)dr["creacion_fecha"];
            estadosCargaContenedor.CreacionIp = dr["creacion_IP"].ToString();
            estadosCargaContenedor.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            estadosCargaContenedor.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            estadosCargaContenedor.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return estadosCargaContenedor;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}