﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;


namespace AccesoDatos.Contextos.Transversales
{
    public class MonedasContext
    {
        private string connectionString = string.Empty;

        public MonedasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public MonedasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de todas las monedas
        /// </summary>
        /// <param name="paisId"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Moneda> ObtenerTodos(int? paisId = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_monedas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@moneda_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (paisId.HasValue)
                    command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = paisId;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Moneda moneda = Cargar(dr);
                        yield return moneda;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Busqueda de Monedas con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_monedas_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["moneda_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["moneda_codigoAduanas"].ToString();
                                etiqueta = dr["moneda_codigoAduanas"].ToString() + " | " + dr["moneda_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["moneda_codigoSicex"].ToString();
                                etiqueta = dr["moneda_codigoSicex"].ToString() + " | " + dr["moneda_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["moneda_nombre"].ToString();
                                etiqueta = dr["moneda_codigo"].ToString() + " | " + dr["moneda_nombre"].ToString();
                                break;
                            default:
                                valor = dr["moneda_codigo"].ToString();
                                etiqueta = dr["moneda_codigo"].ToString() + " | " + dr["moneda_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }

        /// <summary>
        /// Obtiene una Moneda
        /// </summary>
        /// <param name="moneda"></param>
        /// <returns></returns>
        public Moneda Obtener(Moneda moneda)
        {
            using (SqlCommand command = new SqlCommand("prc_monedas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (moneda.Id != 0)
                    command.Parameters.Add("@moneda_ID", SqlDbType.Int).Value = moneda.Id;
                if (!String.IsNullOrEmpty(moneda.Codigo))
                    command.Parameters.Add("@moneda_codigo", SqlDbType.VarChar, 10).Value = moneda.Codigo;
                if (!String.IsNullOrEmpty(moneda.CodigoAduanas))
                    command.Parameters.Add("@moneda_codigoAduanas", SqlDbType.VarChar, 10).Value = moneda.CodigoAduanas;
                if (!String.IsNullOrEmpty(moneda.CodigoSicex))
                    command.Parameters.Add("@moneda_codigoSicex", SqlDbType.VarChar, 10).Value = moneda.CodigoSicex;
                if (moneda.Bloqueado.HasValue)
                    command.Parameters.Add("@moneda_bloqueado", SqlDbType.Bit).Value = moneda.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        moneda = Cargar(dr);
                }
                command.Connection.Close();
            }
            return moneda;
        }

        /// <summary>
        /// Obtiene una moneda a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Moneda Obtener(int id)
        {
            Moneda moneda = new Moneda();
            moneda.Id = id;
            using (SqlCommand command = new SqlCommand("prc_monedas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@moneda_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        moneda = Cargar(dr);
                }
                command.Connection.Close();
            }
            return moneda;

        }
        /// <summary>
        /// Inserta o Actualiza una moneda
        /// </summary>
        /// <param name="moneda"></param>
        public void Guardar(Moneda moneda)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@moneda_ID", SqlDbType.Int).Value = moneda.Id;
                command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = moneda.Pais.Id;
                command.Parameters.Add("@moneda_codigo", SqlDbType.VarChar, 10).Value = moneda.Codigo;
                command.Parameters.Add("@moneda_codigoAduanas", SqlDbType.VarChar, 10).Value = moneda.CodigoAduanas;
                command.Parameters.Add("@moneda_codigoSicex", SqlDbType.VarChar, 10).Value = moneda.CodigoSicex;
                command.Parameters.Add("@moneda_nombre", SqlDbType.VarChar, 100).Value = moneda.Nombre;
                command.Parameters.Add("@moneda_nombreIngles", SqlDbType.VarChar, 100).Value = moneda.NombreIngles;
                command.Parameters.Add("@moneda_simbolo", SqlDbType.NVarChar, 10).Value = moneda.Simbolo;
                command.Parameters.Add("@moneda_bloqueado", SqlDbType.Bit).Value = moneda.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = moneda.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = moneda.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = moneda.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = moneda.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (moneda.Id > 0)
                    {
                        command.CommandText = "prc_monedas_actualizar";
                        command.Parameters["@usuario"].Value = moneda.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = moneda.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_monedas_insertar";
                        command.Parameters["@moneda_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en MonedaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Moneda Cargar(SqlDataReader dr)
        {
            Moneda moneda = new Moneda();
            moneda.Id = (int)dr["moneda_ID"];
            moneda.Codigo = dr["moneda_codigo"].ToString();
            moneda.CodigoAduanas = dr["moneda_codigoAduanas"].ToString();
            moneda.CodigoSicex = dr["moneda_codigoSicex"].ToString();
            moneda.Nombre = dr["moneda_nombre"].ToString();
            moneda.NombreIngles = dr["moneda_nombreIngles"].ToString();
            moneda.Simbolo = dr["moneda_simbolo"].ToString();
            moneda.Bloqueado = (bool)dr["moneda_bloqueado"];
            moneda.ParaCodParam = dr["para_codParam"].ToString();
            moneda.TapaCodTabla = (int)dr["tapa_codTabla"];
            moneda.CreacionUsuario = dr["creacion_usuario"].ToString();
            moneda.CreacionFecha = (DateTime)dr["creacion_fecha"];
            moneda.CreacionIp = dr["creacion_IP"].ToString();
            moneda.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            moneda.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            moneda.ActualizacionIp = dr["actualizacion_IP"].ToString();
            moneda.Pais = dr["pais_ID"] != DBNull.Value ? new PaisesContext(this.connectionString)
                .Obtener((int)dr["pais_ID"]) : new Pais();

            return moneda;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
