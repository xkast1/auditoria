﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class TransportesInternacionalesContext
    {
        private string connectionString = string.Empty;

        public TransportesInternacionalesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TransportesInternacionalesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado de los trasnportes internacionales
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TransporteInternacional> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " order by citr_nombrecia", conn);
                cmd.CommandType = CommandType.Text;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TransporteInternacional TransporteInternacional = Cargar(dr);
                        yield return TransporteInternacional;
                    }
                }
                cmd.Connection.Close();
            }
        }
 
        /// <summary>
        /// Obtiene un transporte internacional especifico a partir de su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TransporteInternacional Obtener(int id)
        {
            TransporteInternacional TransporteInternacional = new TransporteInternacional();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + "where correlativo = @codigo", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@codigo", SqlDbType.Int).Value = id;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                         TransporteInternacional = Cargar(dr);
                }
                cmd.Connection.Close();
            }
            return TransporteInternacional;
        }
        /// <summary>
        /// Busqueda de un transportes internacional a partir de su nombre autocompletado
        /// </summary>
        /// <param name="buscado"></param>
        /// <returns></returns>
        public List<ResultadoAutocompletar> Buscar(string buscado)
        {
            List<ResultadoAutocompletar> listado = new List<ResultadoAutocompletar>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                //GoTO Tabla y procedimiento
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " and citr_nombrecia like @buscado order by citr_nombrecia", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@busqueda", SqlDbType.NVarChar, 50).Value = buscado.Trim() + "%";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar Item = new ResultadoAutocompletar();
                        Item.Id = dr["correlativo"].ToString().Trim();
                        Item.Value = dr["citr_rutciatra"].ToString().Trim();
                        Item.Label = dr["citr_codcia"].ToString().Trim() + " | " + String.Format("{0:d}", dr["citr_nombrecia"]).ToUpper();
                        listado.Add(Item);
                    }
                }
                cmd.Connection.Close();
            }
            return listado;
        }
        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into ( 
                                   ) values 
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update  set                                    
                                       
                                    where ";
                    break;

                case "delete":
                    strQuery = @"Delete From ";
                    break;

                default:
                    strQuery = @"select 
                                    correlativo --0
                                    , citr_codcia --1
                                    , citr_rutciatra --2
                                    , citr_nombrecia --3
                                    , citr_codPais --4
                                from CIA_TRANSPORTE ";
                    break;
            }


            return strQuery;
        }
        private TransporteInternacional Cargar(SqlDataReader dr) {
            TransporteInternacional Transporte = new TransporteInternacional();
            Transporte.Id =  (int)dr["correlativo"];
            Transporte.Nombre =  dr["citr_nombrecia"].ToString().Trim();
            Transporte.Rut = dr["citr_rutciatra"].ToString().Trim();
            Transporte.Codigo = int.Parse(dr["citr_codcia"].ToString());
            Transporte.Pais = new PaisesContext()
                .Obtener(new Pais { Codigo = dr.GetString(4) });
            return Transporte;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
