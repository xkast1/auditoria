﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class EmpresasContext
    {
        private string connectionString = string.Empty;

        public EmpresasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public EmpresasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }

        public IEnumerable<Empresa> ObtenerTodos()
        {

            using (SqlCommand command = new SqlCommand("prc_empresas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Empresa empresa = Cargar(dr);
                        yield return empresa;
                    }
                }
                command.Connection.Close();
            }
        }

        public Empresa Obtener(int id)
        {
            var empresa = new Empresa();
            using (SqlCommand command = new SqlCommand("prc_empresas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@empresa_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        empresa = Cargar(dr);
                }
                command.Connection.Close();
            }
            return empresa;
        }
        public void Guardar(Empresa empresa)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@empresa_ID", SqlDbType.Int).Value = empresa.Id;
                command.Parameters.Add("@empresa_rut", SqlDbType.VarChar, 100).Value = empresa.Rut;
                command.Parameters.Add("@empresa_razonSocial", SqlDbType.VarChar, 100).Value = empresa.RazonSocial;
                command.Parameters.Add("@empresa_direccion", SqlDbType.VarChar, 50).Value = empresa.Direccion;
                command.Parameters.Add("@empresa_web", SqlDbType.VarChar, 100).Value = empresa.Web;
                command.Parameters.Add("@empresa_representanteLegal", SqlDbType.VarChar, 20).Value = empresa.RepresentanteLegal;

                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (empresa.Id > 0)
                    {
                        command.CommandText = "prc_empresas_actualizar";
                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        command.CommandText = "prc_empresas_insertar";
                        command.Parameters["@empresa_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en EmpresasContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Empresa Cargar(SqlDataReader dr)
        {
            var empresa = new Empresa();
            empresa.Id = (int)dr["empresa_ID"];
            empresa.Rut = dr["empresa_rut"].ToString();
            empresa.RazonSocial = dr["empresa_razonSocial"].ToString();
            empresa.Direccion = dr["empresa_direccion"].ToString();
            empresa.Web = dr["empresa_web"].ToString();
            empresa.RepresentanteLegal = dr["empresa_representanteLegal"].ToString();
            empresa.Sucursales = new SucursalesContext(this.connectionString).ObtenerTodos((int)(dr["empresa_ID"]));
            return empresa;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }

    }
}
