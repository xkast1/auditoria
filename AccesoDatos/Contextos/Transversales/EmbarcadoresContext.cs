﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class EmbarcadoresContext
    {
        private string connectionString = string.Empty;

        public EmbarcadoresContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public EmbarcadoresContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado de todos los embarcadores
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Embarcador> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " order by PARA_DESPARAM", conn);
                cmd.CommandType = CommandType.Text;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Embarcador Embarcador =Cargar(dr);
                        yield return Embarcador;
                    }
                }
                cmd.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un embacador especifico a partir de su código
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public Embarcador Obtener(string codigo)
        {
            Embarcador Embarcador = new Embarcador();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + "PARA_CODPARAM = @codigo", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@codigo", SqlDbType.NVarChar, 6).Value = codigo;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())                    
                        Embarcador = Cargar(dr);
                    
                }
                cmd.Connection.Close();
            }
            return Embarcador;
        }
        /// <summary>
        /// Busqueda de un embarcador a partir de su nombre autocompletado
        /// </summary>
        /// <param name="buscado"></param>
        /// <returns></returns>
        public List<ResultadoAutocompletar> Buscar(string buscado)
        {
            List<ResultadoAutocompletar> listado = new List<ResultadoAutocompletar>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " and PARA_DESPARAM like @buscado order by PARA_DESPARAM", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@busqueda", SqlDbType.NVarChar, 50).Value = buscado.Trim() + "%";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar Item = new ResultadoAutocompletar();
                        Item.Id = dr["PARA_CODPARAM"].ToString().Trim();
                        Item.Value = dr["PARA_DESPARAM"].ToString().Trim();
                        Item.Label = dr["PARA_CODPARAM"].ToString().Trim() + " | " + String.Format("{0:d}", dr["PARA_DESPARAM"]).ToUpper();
                        listado.Add(Item);
                    }
                }
                cmd.Connection.Close();
            }
            return listado;
        }
        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into ( 
                                   ) values 
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update  set                                    
                                       
                                    where ";
                    break;

                case "delete":
                    strQuery = @"Delete From ";
                    break;

                default:
                    strQuery = @"select
	                                   PARA_CODPARAM  --0
                                        ,PARA_DESPARAM  --1                                                                           
                                    FROM parametros where TAPA_CODTABLA = 203 ";
                    break;
            }


            return strQuery;
        }
        private Embarcador Cargar(SqlDataReader dr) {
            Embarcador Embarcador = new Embarcador();
            Embarcador.Codigo = dr.GetString(0);// dr["PARA_CODPARAM"].ToString().Trim(); ;
            Embarcador.Nombre = dr.GetString(1);// dr["PARA_DESPARAM"].ToString().Trim(); ;
            return Embarcador;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }

    }
}
