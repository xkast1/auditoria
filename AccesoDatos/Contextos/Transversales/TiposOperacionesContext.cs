﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class TiposOperacionesContext
    {
        private string connectionString = string.Empty;

        public TiposOperacionesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposOperacionesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }

        /// <summary>
        /// Obtiene listado secuencial de los tipos de operaciones
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<TipoOperacion> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposOperaciones_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@tipoOperacion_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoOperacion tipoOperacion = Cargar(dr);
                        yield return tipoOperacion;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de Tipos de operacion con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_tiposOperaciones_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["tipoOperacion_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["tipoOperacion_codigoAduanas"].ToString();
                                etiqueta = dr["tipoOperacion_codigoAduanas"].ToString() + " | " + dr["tipoOperacion_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["tipoOperacion_codigoSicex"].ToString();
                                etiqueta = dr["tipoOperacion_codigoSicex"].ToString() + " | " + dr["tipoOperacion_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["tipoOperacion_nombre"].ToString();
                                etiqueta = dr["tipoOperacion_codigo"].ToString() + " | " + dr["tipoOperacion_nombre"].ToString();
                                break;
                            default:
                                valor = dr["tipoOperacion_codigo"].ToString();
                                etiqueta = dr["tipoOperacion_codigo"].ToString() + " | " + dr["tipoOperacion_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un tipo de operacion
        /// </summary>
        /// <param name="tipoOperacion"></param>
        /// <returns></returns>
        public TipoOperacion Obtener(TipoOperacion tipoOperacion)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposOperaciones_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (tipoOperacion.Id != 0)
                    command.Parameters.Add("@tipoOperacion_ID", SqlDbType.Int).Value = tipoOperacion.Id;
                if (!String.IsNullOrEmpty(tipoOperacion.Codigo))
                    command.Parameters.Add("@tipoOperacion_codigo", SqlDbType.VarChar, 10).Value = tipoOperacion.Codigo;
                if (!String.IsNullOrEmpty(tipoOperacion.CodigoAduanas))
                    command.Parameters.Add("@tipoOperacion_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoOperacion.CodigoAduanas;
                if (!String.IsNullOrEmpty(tipoOperacion.CodigoSicex))
                    command.Parameters.Add("@tipoOperacion_codigoSicex", SqlDbType.VarChar, 10).Value = tipoOperacion.CodigoSicex;
                if (tipoOperacion.Bloqueado.HasValue)
                    command.Parameters.Add("@tipoOperacion_bloqueado", SqlDbType.Bit).Value = tipoOperacion.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoOperacion = Cargar(dr);
                }
                command.Connection.Close();
            }

            return tipoOperacion;
        }
        /// <summary>
        /// Obtiene un tipo de operacion a partir de su indentificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TipoOperacion Obtener(int id)
        {
            TipoOperacion tipoOperacion = new TipoOperacion();
            tipoOperacion.Id = id;
            using (SqlCommand command = new SqlCommand("prc_tiposOperaciones_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoOperacion_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoOperacion = Cargar(dr);
                }
                command.Connection.Close();
            }
            return tipoOperacion;

        }
        public void Guardar(TipoOperacion tipoOperacion)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoOperacion_ID", SqlDbType.Int).Value = tipoOperacion.Id;
                command.Parameters.Add("@tipoOperacion_codigo", SqlDbType.VarChar, 10).Value = tipoOperacion.Codigo;
                command.Parameters.Add("@tipoOperacion_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoOperacion.CodigoAduanas;
                command.Parameters.Add("@tipoOperacion_codigoSicex", SqlDbType.VarChar, 10).Value = tipoOperacion.CodigoSicex;
                command.Parameters.Add("@tipoOperacion_prefijo", SqlDbType.VarChar, 5).Value = tipoOperacion.Prefijo;
                command.Parameters.Add("@tipoOperacion_nombre", SqlDbType.VarChar, 100).Value = tipoOperacion.Nombre;
                command.Parameters.Add("@tipoOperacion_glosa", SqlDbType.VarChar, 100).Value = tipoOperacion.Glosa;
                command.Parameters.Add("@tipoOperacion_importacion", SqlDbType.Bit).Value = tipoOperacion.Importacion;
                command.Parameters.Add("@tipoOperacion_exportacion", SqlDbType.Bit).Value = tipoOperacion.Exportacion;
                command.Parameters.Add("@tipoOperacion_servicio", SqlDbType.Bit).Value = tipoOperacion.Servicio;
                command.Parameters.Add("@tipoOperacion_otra", SqlDbType.Bit).Value = tipoOperacion.Otra;
                command.Parameters.Add("@tipoOperacion_bloqueado", SqlDbType.Bit).Value = tipoOperacion.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = tipoOperacion.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = tipoOperacion.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = tipoOperacion.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = tipoOperacion.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (tipoOperacion.Id > 0)
                    {
                        command.CommandText = "prc_tiposOperaciones_actualizar";
                        command.Parameters["@usuario"].Value = tipoOperacion.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = tipoOperacion.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_tiposOperaciones_insertar";
                        command.Parameters["@tipoOperacion_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TipoOperacionContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private TipoOperacion Cargar(SqlDataReader dr)
        {
            TipoOperacion tipoOperacion = new TipoOperacion();
            tipoOperacion.Id = (int)dr["tipoOperacion_ID"];
            tipoOperacion.Codigo = dr["tipoOperacion_codigo"].ToString();
            tipoOperacion.CodigoAduanas = dr["tipoOperacion_codigoAduanas"].ToString();
            tipoOperacion.CodigoSicex = dr["tipoOperacion_codigoSicex"].ToString();
            tipoOperacion.Prefijo = dr["tipoOperacion_prefijo"].ToString();
            tipoOperacion.Nombre = dr["tipoOperacion_nombre"].ToString();
            tipoOperacion.Glosa = dr["tipoOperacion_glosa"].ToString();
            tipoOperacion.Importacion = (bool)dr["tipoOperacion_importacion"];
            tipoOperacion.Exportacion = (bool)dr["tipoOperacion_exportacion"];
            tipoOperacion.Servicio = (bool)dr["tipoOperacion_servicio"];
            tipoOperacion.Otra = (bool)dr["tipoOperacion_otra"];
            tipoOperacion.Bloqueado = (bool)dr["tipoOperacion_bloqueado"];
            tipoOperacion.ParaCodParam = dr["para_codParam"].ToString();
            tipoOperacion.TapaCodTabla = (int)dr["tapa_codTabla"];
            tipoOperacion.CreacionUsuario = dr["creacion_usuario"].ToString();
            tipoOperacion.CreacionFecha = (DateTime)dr["creacion_fecha"];
            tipoOperacion.CreacionIp = dr["creacion_IP"].ToString();
            tipoOperacion.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            tipoOperacion.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            tipoOperacion.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return tipoOperacion;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
