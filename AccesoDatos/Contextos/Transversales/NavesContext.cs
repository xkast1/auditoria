﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class NavesContext
    {
        private string connectionString = string.Empty;

        public NavesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public NavesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        /// <summary>
        /// Obtiene el listado de Naves
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Nave> ObtenerTodos(bool? bloqueado = null)
        {
            //ToDo FALTA PROCEDIMIENTO
            using (SqlCommand command = new SqlCommand("prc_naves_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@nave_bloqueado", SqlDbType.Bit).Value = bloqueado;

                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Nave Nave = Cargar(dr);
                        yield return Nave;
                    }
                }
                command.Connection.Close();
            }
        }
        public Nave Obtener(Nave Nave)
        {
            using (SqlCommand command = new SqlCommand("prc_naves_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (Nave.Id != 0)
                    command.Parameters.Add("@nave_ID", SqlDbType.Int).Value = Nave.Id;
                if (!String.IsNullOrEmpty(Nave.Nombre))
                    command.Parameters.Add("@nave_nombre", SqlDbType.VarChar, 50).Value = Nave.Nombre;
                if (Nave.Bloqueado.HasValue)
                    command.Parameters.Add("@moneda_bloqueado", SqlDbType.Bit).Value = Nave.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        Nave = Cargar(dr);
                }
                command.Connection.Close();
            }
            return Nave;
        }
        /// <summary>
        /// Obtiene una nave especifica a partir de su código
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public Nave Obtener(int id)
        {
            Nave Nave = new Nave();
            Nave.Id = id;
            using (SqlCommand command = new SqlCommand("prc_naves_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Nave_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        Nave = Cargar(dr);
                }
                command.Connection.Close();
            }
            return Nave;

        }
        /// <summary>
        /// Busqueda de una nave a partir de su nombre autocompletado
        /// </summary>
        /// <param name="buscado"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado)
        {

            using (SqlCommand command = new SqlCommand("prc_naves_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["nave_ID"].ToString();

                        autocompletado.Value = dr["nave_nombre"].ToString();
                        autocompletado.Label = dr["nave_nombre"].ToString();

                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        public void Guardar(Nave Nave)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@nave_ID", SqlDbType.Int).Value = Nave.Id;
                command.Parameters.Add("@nave_nombre", SqlDbType.Int).Value = Nave.Nombre;
                command.Parameters.Add("@viaTransporte_ID", SqlDbType.VarChar, 10).Value = Nave.ViaTransporte.Id;
                command.Parameters.Add("@pais_ID", SqlDbType.VarChar, 10).Value = Nave.Bandera.Id;
                command.Parameters.Add("@nave_bloqueado", SqlDbType.Bit).Value = Nave.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = Nave.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = Nave.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (Nave.Id > 0)
                    {
                        command.CommandText = "prc_naves_actualizar";
                        command.Parameters["@usuario"].Value = Nave.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = Nave.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_naves_insertar";
                        command.Parameters["@nave_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en NavesContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Nave Cargar(SqlDataReader dr)
        {
            Nave Nave = new Nave();
            Nave.Id = (int)dr["nave_ID"];
            Nave.Nombre = dr["nave_nombre"].ToString();

            ViaTransporte ViaTransporte = new ViaTransporte();
            ViaTransporte.Codigo = dr["viaTransporte_codigo"].ToString();
            Nave.ViaTransporte = new AccesoDatos.Contextos.Transversales.ViasTransportesContext().Obtener(ViaTransporte);

            Pais Bandera = new Pais();
            Bandera.Codigo = dr["pais_codigo"].ToString();
            Nave.Bandera = new AccesoDatos.Contextos.Transversales.PaisesContext().Obtener(Bandera);

            Nave.Bloqueado = (bool)dr["nave_bloqueado"];

            //Nave.CreacionUsuario = dr["creacion_usuario"].ToString();
            //Nave.CreacionFecha = (DateTime)dr["creacion_fecha"];
            //Nave.CreacionIp = dr["creacion_IP"].ToString();
            //Nave.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            //Nave.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            //Nave.ActualizacionIp = dr["actualizacion_IP"].ToString();


            return Nave;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
