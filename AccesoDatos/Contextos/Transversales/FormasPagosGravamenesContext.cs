﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class FormasPagosGravamenesContext
    {
        private string connectionString = string.Empty;

        public FormasPagosGravamenesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public FormasPagosGravamenesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        
        /// <summary>
        /// Obtiene listado secuencial de todas las formas de pagos y gravamenes
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<FormaPagoGravamen> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_formasPagosGravamenes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@formaPagoGravamen_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FormaPagoGravamen formaPagoGravamen = Cargar(dr);
                        yield return formaPagoGravamen;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de formas de pagos gravamenes con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_formasPagosGravamenes_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["formaPagoGravamen_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["formaPagoGravamen_codigoAduanas"].ToString();
                                etiqueta = dr["formaPagoGravamen_codigoAduanas"].ToString() + " | " + dr["formaPagoGravamen_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["formaPagoGravamen_codigoSicex"].ToString();
                                etiqueta = dr["formaPagoGravamen_codigoSicex"].ToString() + " | " + dr["formaPagoGravamen_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["formaPagoGravamen_nombre"].ToString();
                                etiqueta = dr["formaPagoGravamen_codigo"].ToString() + " | " + dr["formaPagoGravamen_nombre"].ToString();
                                break;
                            default:
                                valor = dr["formaPagoGravamen_codigo"].ToString();
                                etiqueta = dr["formaPagoGravamen_codigo"].ToString() + " | " + dr["formaPagoGravamen_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una forma de pago de gravamen
        /// </summary>
        /// <param name="formaPagoGravamen"></param>
        /// <returns></returns>
        public FormaPagoGravamen Obtener(FormaPagoGravamen formaPagoGravamen)
        {

            using (SqlCommand command = new SqlCommand("prc_formasPagosGravamenes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (formaPagoGravamen.Id != 0)
                    command.Parameters.Add("@formaPagoGravamen_ID", SqlDbType.Int).Value = formaPagoGravamen.Id;
                if (!String.IsNullOrEmpty(formaPagoGravamen.Codigo))
                    command.Parameters.Add("@formaPagoGravamen_codigo", SqlDbType.VarChar, 10).Value = formaPagoGravamen.Codigo;
                if (!String.IsNullOrEmpty(formaPagoGravamen.CodigoAduanas))
                    command.Parameters.Add("@formaPagoGravamen_codigoAduanas", SqlDbType.VarChar, 10).Value = formaPagoGravamen.CodigoAduanas;
                if (!String.IsNullOrEmpty(formaPagoGravamen.CodigoSicex))
                    command.Parameters.Add("@formaPagoGravamen_codigoSicex", SqlDbType.VarChar, 10).Value = formaPagoGravamen.CodigoSicex;
                if (formaPagoGravamen.Bloqueado.HasValue)
                    command.Parameters.Add("@formaPagoGravamen_bloqueado", SqlDbType.Bit).Value = formaPagoGravamen.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        formaPagoGravamen = Cargar(dr);
                }
                command.Connection.Close();
            }

            return formaPagoGravamen;
        }
        /// <summary>
        /// Obtiene una forma de pago gravamenes a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FormaPagoGravamen Obtener(int id)
        {
            FormaPagoGravamen formaPagoGravamen = new FormaPagoGravamen();
            formaPagoGravamen.Id = id;
            using (SqlCommand command = new SqlCommand("prc_formasPagosGravamenes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@formaPagoGravamen_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        formaPagoGravamen = Cargar(dr);
                }
                command.Connection.Close();
            }
            return formaPagoGravamen;

        }
        /// <summary>
        /// Inserta o Actualiza una forma de pago Gravamen
        /// </summary>
        /// <param name="formaPagoGravamen"></param>
        public void Guardar(FormaPagoGravamen formaPagoGravamen)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@formaPagoGravamen_ID", SqlDbType.Int).Value = formaPagoGravamen.Id;
                command.Parameters.Add("@formaPagoGravamen_codigo", SqlDbType.VarChar, 10).Value = formaPagoGravamen.Codigo;
                command.Parameters.Add("@formaPagoGravamen_codigoAduanas", SqlDbType.VarChar, 10).Value = formaPagoGravamen.CodigoAduanas;
                command.Parameters.Add("@formaPagoGravamen_codigoSicex", SqlDbType.VarChar, 10).Value = formaPagoGravamen.CodigoSicex;
                command.Parameters.Add("@formaPagoGravamen_nombre", SqlDbType.VarChar, 100).Value = formaPagoGravamen.Nombre;
                command.Parameters.Add("@formaPagoGravamen_nombreIngles", SqlDbType.VarChar, 100).Value = formaPagoGravamen.NombreIngles;
                command.Parameters.Add("@formaPagoGravamen_sigla", SqlDbType.VarChar, 20).Value = formaPagoGravamen.Sigla;
                command.Parameters.Add("@formaPagoGravamen_bloqueado", SqlDbType.Bit).Value = formaPagoGravamen.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = formaPagoGravamen.ParaCodParam;
                command.Parameters.Add("@para_codTabla", SqlDbType.Int).Value = formaPagoGravamen.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = formaPagoGravamen.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = formaPagoGravamen.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (formaPagoGravamen.Id > 0)
                    {
                        command.CommandText = "prc_formasPagosGravamenes_actualizar";
                        command.Parameters["@usuario"].Value = formaPagoGravamen.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = formaPagoGravamen.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_formasPagosGravamenes_insertar";
                        command.Parameters["@formaPagoGravamen_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en FormaPagoGravamenContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private FormaPagoGravamen Cargar(SqlDataReader dr)
        {
            FormaPagoGravamen formaPagoGravamen = new FormaPagoGravamen();
            formaPagoGravamen.Id = (int)dr["formaPagoGravamen_ID"];
            formaPagoGravamen.Codigo = dr["formaPagoGravamen_codigo"].ToString();
            formaPagoGravamen.CodigoAduanas = dr["formaPagoGravamen_codigoAduanas"].ToString();
            formaPagoGravamen.CodigoSicex = dr["formaPagoGravamen_codigoSicex"].ToString();
            formaPagoGravamen.Nombre = dr["formaPagoGravamen_nombre"].ToString();
            formaPagoGravamen.NombreIngles = dr["formaPagoGravamen_nombreIngles"].ToString();
            formaPagoGravamen.Sigla = dr["formaPagoGravamen_sigla"].ToString();
            formaPagoGravamen.Bloqueado = (bool)dr["formaPagoGravamen_bloqueado"];
            formaPagoGravamen.ParaCodParam = dr["para_codParam"].ToString();
            formaPagoGravamen.TapaCodTabla = (int)dr["para_codTabla"];
            formaPagoGravamen.CreacionUsuario = dr["creacion_usuario"].ToString();
            formaPagoGravamen.CreacionFecha = (DateTime)dr["creacion_fecha"];
            formaPagoGravamen.CreacionIp = dr["creacion_IP"].ToString();
            formaPagoGravamen.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            formaPagoGravamen.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            formaPagoGravamen.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return formaPagoGravamen;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}