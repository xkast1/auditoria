﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class FuncionariosContext
    {
        private string connectionString = string.Empty;

        public FuncionariosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public FuncionariosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public IEnumerable<string> ObtenerRutsPorPatron(string patron)
        {
            // Se prepara el patrón recibido para utilizarlo
            // en una sentencia SQL con comodines.
            patron = string.Format("{0}%", patron);

            var qry = @"SELECT TOP 10 [RUTFUNCION]
                        FROM [dbo].[FUNCIONARIO]
                        WHERE [RUTFUNCION] LIKE @Patron";

            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = qry;
                    command.Parameters.Add("@Patron", SqlDbType.VarChar, 10).Value = patron;
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();

                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                yield return dr.GetString(0);
                            }
                        }
                    }
                    command.Connection.Close();
                }
            }
        }

        /// <summary>
        /// Obtiene la lista de todos los funcionarios
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Funcionario> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                string query = "Select RUTFUNCION, FUNC_NOMBRES, FUNC_APELLIDO,FUNC_CARGO,FUNC_EMAIL From Funcionario ORDER BY FUNC_NOMBRES";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandType = CommandType.Text;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Funcionario funcionario = Cargar(dr);
                        yield return funcionario;
                    }
                }
                cmd.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un funcionario especifico a partir de su código (rut)
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public Funcionario Obtener(string codigo)
        {
            Funcionario funcionario = new Funcionario();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select RUTFUNCION, FUNC_NOMBRES, FUNC_APELLIDO,FUNC_CARGO,FUNC_EMAIL From Funcionario WHERE RUTFUNCION  = @codigo", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@codigo", SqlDbType.NVarChar, 12).Value = codigo;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())                    
                        funcionario = Cargar(dr);
                    
                }
                cmd.Connection.Close();
            }
            return funcionario;

        }
        private Funcionario Cargar(SqlDataReader dr) {
            Funcionario Funcionario = new Funcionario();
            Funcionario.Rut = dr["RUTFUNCION"].ToString().Trim();
            Funcionario.Nombre = dr["FUNC_NOMBRES"].ToString().Trim();
            Funcionario.Apellido = dr["FUNC_APELLIDO"].ToString().Trim();
            Funcionario.Cargo = dr["FUNC_CARGO"].ToString().Trim();
            Funcionario.Email = dr["FUNC_EMAIL"].ToString().Trim();
            return Funcionario;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
