﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class UnidadesMedidasContext
    {
        private string connectionString = string.Empty;

        public UnidadesMedidasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public UnidadesMedidasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de las unidades de medida
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<UnidadMedida> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_unidadesMedidas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@unidadMedida_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UnidadMedida unidadMedida = Cargar(dr);
                        yield return unidadMedida;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de Unidades de Medida con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_unidadesMedidas_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["unidadMedida_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["unidadMedida_codigoAduanas"].ToString();
                                etiqueta = dr["unidadMedida_codigoAduanas"].ToString() + " | " + dr["unidadMedida_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["unidadMedida_codigoSicex"].ToString();
                                etiqueta = dr["unidadMedida_codigoSicex"].ToString() + " | " + dr["unidadMedida_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["unidadMedida_nombre"].ToString();
                                etiqueta = dr["unidadMedida_codigo"].ToString() + " | " + dr["unidadMedida_nombre"].ToString();
                                break;
                            default:
                                valor = dr["unidadMedida_codigo"].ToString();
                                etiqueta = dr["unidadMedida_codigo"].ToString() + " | " + dr["unidadMedida_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una unidad de medida
        /// </summary>
        /// <param name="unidadMedida"></param>
        /// <returns></returns>
        public UnidadMedida Obtener(UnidadMedida unidadMedida)
        {

            using (SqlCommand command = new SqlCommand("prc_unidadesMedidas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (unidadMedida.Id != 0)
                    command.Parameters.Add("@unidadMedida_ID", SqlDbType.Int).Value = unidadMedida.Id;
                if (!String.IsNullOrEmpty(unidadMedida.Codigo))
                    command.Parameters.Add("@unidadMedida_codigo", SqlDbType.VarChar, 10).Value = unidadMedida.Codigo;
                if (!String.IsNullOrEmpty(unidadMedida.CodigoAduanas))
                    command.Parameters.Add("@unidadMedida_codigoAduanas", SqlDbType.VarChar, 10).Value = unidadMedida.CodigoAduanas;
                if (!String.IsNullOrEmpty(unidadMedida.CodigoSicex))
                    command.Parameters.Add("@unidadMedida_codigoSicex", SqlDbType.VarChar, 10).Value = unidadMedida.CodigoSicex;
                if (unidadMedida.Bloqueado.HasValue)
                    command.Parameters.Add("@unidadMedida_bloqueado", SqlDbType.Bit).Value = unidadMedida.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        unidadMedida = Cargar(dr);
                }
                command.Connection.Close();
            }

            return unidadMedida;
        }
        /// <summary>
        /// Obtiene una unidad de medida a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UnidadMedida Obtener(int id)
        {
            UnidadMedida unidadMedida = new UnidadMedida();
            unidadMedida.Id = id;
            using (SqlCommand command = new SqlCommand("prc_unidadesMedidas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@unidadMedida_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        unidadMedida = Cargar(dr);
                }
                command.Connection.Close();
            }
            return unidadMedida;

        }
        /// <summary>
        /// Inserta o Actualiza una unidad de medida
        /// </summary>
        /// <param name="unidadMedida"></param>
        public void Guardar(UnidadMedida unidadMedida)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@unidadMedida_ID", SqlDbType.Int).Value = unidadMedida.Id;
                command.Parameters.Add("@unidadMedida_codigo", SqlDbType.VarChar, 10).Value = unidadMedida.Codigo;
                command.Parameters.Add("@unidadMedida_codigoAduanas", SqlDbType.VarChar, 10).Value = unidadMedida.CodigoAduanas;
                command.Parameters.Add("@unidadMedida_codigoSicex", SqlDbType.VarChar, 10).Value = unidadMedida.CodigoSicex;
                command.Parameters.Add("@unidadMedida_nombre", SqlDbType.VarChar, 100).Value = unidadMedida.Nombre;
                command.Parameters.Add("@unidadMedida_nombreIngles", SqlDbType.VarChar, 100).Value = unidadMedida.NombreIngles;
                command.Parameters.Add("@unidadMedida_descripcion", SqlDbType.VarChar, 255).Value = unidadMedida.Descripcion;
                command.Parameters.Add("@unidadMedida_descripcionIngles", SqlDbType.VarChar, 255).Value = unidadMedida.DescripcionIngles;
                command.Parameters.Add("@unidadMedida_tipo", SqlDbType.Char, 1).Value = unidadMedida.Tipo;
                command.Parameters.Add("@unidadMedida_bloqueado", SqlDbType.Bit).Value = unidadMedida.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = unidadMedida.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = unidadMedida.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (unidadMedida.Id > 0)
                    {
                        command.CommandText = "prc_unidadesMedidas_actualizar";
                        command.Parameters["@usuario"].Value = unidadMedida.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = unidadMedida.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_unidadesMedidas_insertar";
                        command.Parameters["@unidadMedida_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en UnidadMedidaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private UnidadMedida Cargar(SqlDataReader dr)
        {
            UnidadMedida unidadMedida = new UnidadMedida();
            unidadMedida.Id = (int)dr["unidadMedida_ID"];
            unidadMedida.Codigo = dr["unidadMedida_codigo"].ToString();
            unidadMedida.CodigoAduanas = dr["unidadMedida_codigoAduanas"].ToString();
            unidadMedida.CodigoSicex = dr["unidadMedida_codigoSicex"].ToString();
            unidadMedida.Nombre = dr["unidadMedida_nombre"].ToString();
            unidadMedida.NombreIngles = dr["unidadMedida_nombreIngles"].ToString();
            unidadMedida.Descripcion = dr["unidadMedida_descripcion"].ToString();
            unidadMedida.DescripcionIngles = dr["unidadMedida_descripcionIngles"].ToString();
            unidadMedida.Tipo = dr["unidadMedida_tipo"].ToString();
            unidadMedida.Bloqueado = (bool)dr["unidadMedida_bloqueado"];
            unidadMedida.CreacionUsuario = dr["creacion_usuario"].ToString();
            unidadMedida.CreacionFecha = (DateTime)dr["creacion_fecha"];
            unidadMedida.CreacionIp = dr["creacion_IP"].ToString();
            unidadMedida.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            unidadMedida.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            unidadMedida.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return unidadMedida;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}