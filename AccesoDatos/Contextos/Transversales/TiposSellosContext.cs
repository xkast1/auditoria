﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class TiposSellosContext
    {
        private string connectionString = string.Empty;

        public TiposSellosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposSellosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene un listado secuencial de los tipos de sellos
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<TipoSello> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposSellos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@tipoSello_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoSello tipoSello = Cargar(dr);
                        yield return tipoSello;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de Tipos de sellos con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_tiposSellos_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["tipoSello_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["tipoSello_codigoAduanas"].ToString();
                                etiqueta = dr["tipoSello_codigoAduanas"].ToString() + " | " + dr["tipoSello_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["tipoSello_codigoSicex"].ToString();
                                etiqueta = dr["tipoSello_codigoSicex"].ToString() + " | " + dr["tipoSello_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["tipoSello_nombre"].ToString();
                                etiqueta = dr["tipoSello_codigo"].ToString() + " | " + dr["tipoSello_nombre"].ToString();
                                break;
                            default:
                                valor = dr["tipoSello_codigo"].ToString();
                                etiqueta = dr["tipoSello_codigo"].ToString() + " | " + dr["tipoSello_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un tipo de sello
        /// </summary>
        /// <param name="tipoSello"></param>
        /// <returns></returns>
        public TipoSello Obtener(TipoSello tipoSello)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposSellos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (tipoSello.Id != 0)
                    command.Parameters.Add("@tipoSello_ID", SqlDbType.Int).Value = tipoSello.Id;
                if (!String.IsNullOrEmpty(tipoSello.Codigo))
                    command.Parameters.Add("@tipoSello_codigo", SqlDbType.VarChar, 10).Value = tipoSello.Codigo;
                if (!String.IsNullOrEmpty(tipoSello.CodigoAduanas))
                    command.Parameters.Add("@tipoSello_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoSello.CodigoAduanas;
                if (!String.IsNullOrEmpty(tipoSello.CodigoSicex))
                    command.Parameters.Add("@tipoSello_codigoSicex", SqlDbType.VarChar, 10).Value = tipoSello.CodigoSicex;
                if (tipoSello.Bloqueado.HasValue)
                    command.Parameters.Add("@tipoSello_bloqueado", SqlDbType.Bit).Value = tipoSello.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoSello = Cargar(dr);
                }
                command.Connection.Close();
            }

            return tipoSello;
        }
        /// <summary>
        /// Obtiene un tipo de sello a partir de su indentificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TipoSello Obtener(int id)
        {
            TipoSello tipoSello = new TipoSello();
            tipoSello.Id = id;
            using (SqlCommand command = new SqlCommand("prc_tiposSellos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoSello_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoSello = Cargar(dr);
                }
                command.Connection.Close();
            }
            return tipoSello;

        }
        /// <summary>
        /// Inserta o Actualiza un tipo de sello
        /// </summary>
        /// <param name="tipoSello"></param>
        public void Guardar(TipoSello tipoSello)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoSello_ID", SqlDbType.Int).Value = tipoSello.Id;
                command.Parameters.Add("@tipoSello_codigo", SqlDbType.VarChar, 10).Value = tipoSello.Codigo;
                command.Parameters.Add("@tipoSello_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoSello.CodigoAduanas;
                command.Parameters.Add("@tipoSello_codigoSicex", SqlDbType.VarChar, 10).Value = tipoSello.CodigoSicex;
                command.Parameters.Add("@tipoSello_nombre", SqlDbType.VarChar, 100).Value = tipoSello.Nombre;
                command.Parameters.Add("@tipoSello_nombreIngles", SqlDbType.VarChar, 100).Value = tipoSello.NombreIngles;
                command.Parameters.Add("@tipoSello_bloqueado", SqlDbType.Bit).Value = tipoSello.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = tipoSello.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = tipoSello.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (tipoSello.Id > 0)
                    {
                        command.CommandText = "prc_tiposSellos_actualizar";
                        command.Parameters["@usuario"].Value = tipoSello.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = tipoSello.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_tiposSellos_insertar";
                        command.Parameters["@tipoSello_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TipoSelloContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private TipoSello Cargar(SqlDataReader dr)
        {
            TipoSello tipoSello = new TipoSello();
            tipoSello.Id = (int)dr["tipoSello_ID"];
            tipoSello.Codigo = dr["tipoSello_codigo"].ToString();
            tipoSello.CodigoAduanas = dr["tipoSello_codigoAduanas"].ToString();
            tipoSello.CodigoSicex = dr["tipoSello_codigoSicex"].ToString();
            tipoSello.Nombre = dr["tipoSello_nombre"].ToString();
            tipoSello.NombreIngles = dr["tipoSello_nombreIngles"].ToString();
            tipoSello.Bloqueado = (bool)dr["tipoSello_bloqueado"];
            tipoSello.CreacionUsuario = dr["creacion_usuario"].ToString();
            tipoSello.CreacionFecha = (DateTime)dr["creacion_fecha"];
            tipoSello.CreacionIp = dr["creacion_IP"].ToString();
            tipoSello.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            tipoSello.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            tipoSello.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return tipoSello;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}