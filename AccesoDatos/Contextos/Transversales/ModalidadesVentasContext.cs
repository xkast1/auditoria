﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class ModalidadesVentasContext
    {
        private string connectionString = string.Empty;

        public ModalidadesVentasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ModalidadesVentasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado de modalidades de ventas
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<ModalidadVenta> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_modalidadesVentas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@modalidadVenta_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ModalidadVenta modalidadVenta = Cargar(dr);
                        yield return modalidadVenta;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de Modalidades de ventas con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_modalidadesVentas_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["modalidadVenta_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["modalidadVenta_codigoAduanas"].ToString();
                                etiqueta = dr["modalidadVenta_codigoAduanas"].ToString() + " | " + dr["modalidadVenta_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["modalidadVenta_codigoSicex"].ToString();
                                etiqueta = dr["modalidadVenta_codigoSicex"].ToString() + " | " + dr["modalidadVenta_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["modalidadVenta_nombre"].ToString();
                                etiqueta = dr["modalidadVenta_codigo"].ToString() + " | " + dr["modalidadVenta_nombre"].ToString();
                                break;
                            default:
                                valor = dr["modalidadVenta_codigo"].ToString();
                                etiqueta = dr["modalidadVenta_codigo"].ToString() + " | " + dr["modalidadVenta_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una modalidad de venta
        /// </summary>
        /// <param name="modalidadVenta"></param>
        /// <returns></returns>
        public ModalidadVenta Obtener(ModalidadVenta modalidadVenta)
        {

            using (SqlCommand command = new SqlCommand("prc_modalidadesVentas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (modalidadVenta.Id != 0)
                    command.Parameters.Add("@modalidadVenta_ID", SqlDbType.Int).Value = modalidadVenta.Id;
                if (!String.IsNullOrEmpty(modalidadVenta.Codigo))
                    command.Parameters.Add("@modalidadVenta_codigo", SqlDbType.VarChar, 10).Value = modalidadVenta.Codigo;
                if (!String.IsNullOrEmpty(modalidadVenta.CodigoAduanas))
                    command.Parameters.Add("@modalidadVenta_codigoAduanas", SqlDbType.VarChar, 10).Value = modalidadVenta.CodigoAduanas;
                if (!String.IsNullOrEmpty(modalidadVenta.CodigoSicex))
                    command.Parameters.Add("@modalidadVenta_codigoSicex", SqlDbType.VarChar, 10).Value = modalidadVenta.CodigoSicex;
                if (modalidadVenta.Bloqueado.HasValue)
                    command.Parameters.Add("@modalidadVenta_bloqueado", SqlDbType.Bit).Value = modalidadVenta.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        modalidadVenta = Cargar(dr);
                }
                command.Connection.Close();
            }

            return modalidadVenta;
        }
        /// <summary>
        /// Obtiene una modalidad de venta a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ModalidadVenta Obtener(int id)
        {
            ModalidadVenta modalidadVenta = new ModalidadVenta();
            modalidadVenta.Id = id;
            using (SqlCommand command = new SqlCommand("prc_modalidadesVentas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@modalidadVenta_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        modalidadVenta = Cargar(dr);
                }
                command.Connection.Close();
            }
            return modalidadVenta;

        }
        /// <summary>
        /// Inserta o Actualiza una Modalidad de Venta
        /// </summary>
        /// <param name="modalidadVenta"></param>
        public void Guardar(ModalidadVenta modalidadVenta)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@modalidadVenta_ID", SqlDbType.Int).Value = modalidadVenta.Id;
                command.Parameters.Add("@modalidadVenta_codigo", SqlDbType.VarChar, 10).Value = modalidadVenta.Codigo;
                command.Parameters.Add("@modalidadVenta_codigoAduanas", SqlDbType.VarChar, 10).Value = modalidadVenta.CodigoAduanas;
                command.Parameters.Add("@modalidadVenta_codigoSicex", SqlDbType.VarChar, 10).Value = modalidadVenta.CodigoSicex;
                command.Parameters.Add("@modalidadVenta_nombre", SqlDbType.VarChar, 100).Value = modalidadVenta.Nombre;
                command.Parameters.Add("@modalidadVenta_nombreIngles", SqlDbType.VarChar, 100).Value = modalidadVenta.NombreIngles;
                command.Parameters.Add("@modalidadVenta_definicion", SqlDbType.VarChar, -1).Value = modalidadVenta.Definicion;
                command.Parameters.Add("@modalidadVenta_bloqueado", SqlDbType.Bit).Value = modalidadVenta.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = modalidadVenta.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = modalidadVenta.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = modalidadVenta.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = modalidadVenta.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (modalidadVenta.Id > 0)
                    {
                        command.CommandText = "prc_modalidadesVentas_actualizar";
                        command.Parameters["@usuario"].Value = modalidadVenta.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = modalidadVenta.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_modalidadesVentas_insertar";
                        command.Parameters["@modalidadVenta_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en ModalidadVentaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private ModalidadVenta Cargar(SqlDataReader dr)
        {
            ModalidadVenta modalidadVenta = new ModalidadVenta();
            modalidadVenta.Id = (int)dr["modalidadVenta_ID"];
            modalidadVenta.Codigo = dr["modalidadVenta_codigo"].ToString();
            modalidadVenta.CodigoAduanas = dr["modalidadVenta_codigoAduanas"].ToString();
            modalidadVenta.CodigoSicex = dr["modalidadVenta_codigoSicex"].ToString();
            modalidadVenta.Nombre = dr["modalidadVenta_nombre"].ToString();
            modalidadVenta.NombreIngles = dr["modalidadVenta_nombreIngles"].ToString();
            modalidadVenta.Definicion = dr["modalidadVenta_definicion"].ToString();
            modalidadVenta.Bloqueado = (bool)dr["modalidadVenta_bloqueado"];
            modalidadVenta.ParaCodParam = dr["para_codParam"].ToString();
            modalidadVenta.TapaCodTabla = (int)dr["tapa_codTabla"];
            modalidadVenta.CreacionUsuario = dr["creacion_usuario"].ToString();
            modalidadVenta.CreacionFecha = (DateTime)dr["creacion_fecha"];
            modalidadVenta.CreacionIp = dr["creacion_IP"].ToString();
            modalidadVenta.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            modalidadVenta.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            modalidadVenta.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return modalidadVenta;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
