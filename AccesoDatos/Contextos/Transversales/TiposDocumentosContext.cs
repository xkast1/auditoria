﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class TiposDocumentosContext
    {
        private string connectionString = string.Empty;

        public TiposDocumentosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposDocumentosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado de los tipos de documentos
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TipoDocumento> ObteneTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " order by doc_nombre", conn);
                cmd.CommandType = CommandType.Text;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoDocumento TipoDocumento = Cargar(dr);
                        yield return TipoDocumento;
                    }
                }
                cmd.Connection.Close();
            }
        }

        /// <summary>
        /// Obtiene un tipo de documento especifico a partir de su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TipoDocumento Obtener(int id)
        {
            TipoDocumento TipoDocumento = new TipoDocumento();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " where id_documento = @ID ", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                        TipoDocumento = Cargar(dr);
                }
                cmd.Connection.Close();
            }
            return TipoDocumento;
        }

        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into ( ) values 
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update  set
                                   ";
                    break;


                case "delete":
                    strQuery = @"Delete From ";
                    break;

                default:
                    strQuery = @"select id_documento --0
                                    ,doc_nombre --1
                                    ,doc_sigla --2
                                FROM documentos_base ";
                    break;
            }


            return strQuery;
        }
        private TipoDocumento Cargar(SqlDataReader dr) {
            TipoDocumento TipoDocumento = new TipoDocumento();
            TipoDocumento.Id = dr.GetInt32(0);// (int)dr["ID_Documento"];
            TipoDocumento.Nombre = dr.GetString(1);// dr["DOC_Nombre"].ToString().Trim();
            TipoDocumento.Sigla = dr.GetString(2);// dr["Doc_Sigla"].ToString().Trim();
            return TipoDocumento;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
