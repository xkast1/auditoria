﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class TiposContenedoresContext
    {
        private string connectionString = string.Empty;

        public TiposContenedoresContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposContenedoresContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de los tipos de contenedores
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<TipoContenedor> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposContenedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@tipoContenedor_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoContenedor tipoContenedor = Cargar(dr);
                        yield return tipoContenedor;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de Tipos de contenedores con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_tiposContenedores_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["tipoContenedor_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["tipoContenedor_codigoAduanas"].ToString();
                                etiqueta = dr["tipoContenedor_codigoAduanas"].ToString() + " | " + dr["tipoContenedor_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["tipoContenedor_codigoSicex"].ToString();
                                etiqueta = dr["tipoContenedor_codigoSicex"].ToString() + " | " + dr["tipoContenedor_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["tipoContenedor_nombre"].ToString();
                                etiqueta = dr["tipoContenedor_codigo"].ToString() + " | " + dr["tipoContenedor_nombre"].ToString();
                                break;
                            default:
                                valor = dr["tipoContenedor_codigo"].ToString();
                                etiqueta = dr["tipoContenedor_codigo"].ToString() + " | " + dr["tipoContenedor_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un tipo de contenedor
        /// </summary>
        /// <param name="tipoContenedor"></param>
        /// <returns></returns>
        public TipoContenedor Obtener(TipoContenedor tipoContenedor)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposContenedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (tipoContenedor.Id != 0)
                    command.Parameters.Add("@tipoContenedor_ID", SqlDbType.Int).Value = tipoContenedor.Id;
                if (!String.IsNullOrEmpty(tipoContenedor.Codigo))
                    command.Parameters.Add("@tipoContenedor_codigo", SqlDbType.VarChar, 10).Value = tipoContenedor.Codigo;
                if (!String.IsNullOrEmpty(tipoContenedor.CodigoAduanas))
                    command.Parameters.Add("@tipoContenedor_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoContenedor.CodigoAduanas;
                if (!String.IsNullOrEmpty(tipoContenedor.CodigoSicex))
                    command.Parameters.Add("@tipoContenedor_codigoSicex", SqlDbType.VarChar, 10).Value = tipoContenedor.CodigoSicex;
                if (tipoContenedor.Bloqueado.HasValue)
                    command.Parameters.Add("@tipoContenedor_bloqueado", SqlDbType.Bit).Value = tipoContenedor.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoContenedor = Cargar(dr);
                }
                command.Connection.Close();
            }

            return tipoContenedor;
        }
        /// <summary>
        /// Obtiene un tipo de contenedor a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TipoContenedor Obtener(int id)
        {
            TipoContenedor tipoContenedor = new TipoContenedor();
            tipoContenedor.Id = id;
            using (SqlCommand command = new SqlCommand("prc_tiposContenedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoContenedor_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoContenedor = Cargar(dr);
                }
                command.Connection.Close();
            }
            return tipoContenedor;

        }
        /// <summary>
        /// inserta o actualiza un tipo de contenedor
        /// </summary>
        /// <param name="tipoContenedor"></param>
        public void Guardar(TipoContenedor tipoContenedor)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoContenedor_ID", SqlDbType.Int).Value = tipoContenedor.Id;
                command.Parameters.Add("@tipoContenedor_codigo", SqlDbType.VarChar, 10).Value = tipoContenedor.Codigo;
                command.Parameters.Add("@tipoContenedor_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoContenedor.CodigoAduanas;
                command.Parameters.Add("@tipoContenedor_codigoSicex", SqlDbType.VarChar, 10).Value = tipoContenedor.CodigoSicex;
                command.Parameters.Add("@tipoContenedor_descripcion", SqlDbType.VarChar, 100).Value = tipoContenedor.Descripcion;
                command.Parameters.Add("@tipoContenedor_descripcionIngles", SqlDbType.VarChar, 100).Value = tipoContenedor.DescripcionIngles;
                command.Parameters.Add("@tipoContenedor_bloqueado", SqlDbType.Bit).Value = tipoContenedor.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = tipoContenedor.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = tipoContenedor.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (tipoContenedor.Id > 0)
                    {
                        command.CommandText = "prc_tiposContenedores_actualizar";
                        command.Parameters["@usuario"].Value = tipoContenedor.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = tipoContenedor.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_tiposContenedores_insertar";
                        command.Parameters["@tipoContenedor_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TipoContenedorContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private TipoContenedor Cargar(SqlDataReader dr)
        {
            TipoContenedor tipoContenedor = new TipoContenedor();
            tipoContenedor.Id = (int)dr["tipoContenedor_ID"];
            tipoContenedor.Codigo = dr["tipoContenedor_codigo"].ToString();
            tipoContenedor.CodigoAduanas = dr["tipoContenedor_codigoAduanas"].ToString();
            tipoContenedor.CodigoSicex = dr["tipoContenedor_codigoSicex"].ToString();
            tipoContenedor.Descripcion = dr["tipoContenedor_descripcion"].ToString();
            tipoContenedor.DescripcionIngles = dr["tipoContenedor_descripcionIngles"].ToString();
            tipoContenedor.Bloqueado = (bool)dr["tipoContenedor_bloqueado"];
            tipoContenedor.CreacionUsuario = dr["creacion_usuario"].ToString();
            tipoContenedor.CreacionFecha = (DateTime)dr["creacion_fecha"];
            tipoContenedor.CreacionIp = dr["creacion_IP"].ToString();
            tipoContenedor.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            tipoContenedor.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            tipoContenedor.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return tipoContenedor;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}