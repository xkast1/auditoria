﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class AlmacenistasContext
    {
        private string connectionString = string.Empty;

        public AlmacenistasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public AlmacenistasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de todos los almacenistas
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Almacenista> ObtenerTodos(bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_almacenistas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@almacenista_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Almacenista almacenista = Cargar(dr);
                        yield return almacenista;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de almacenistas con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_almacenistas_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["almacenista_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["almacenista_codigoAduanas"].ToString();
                                etiqueta = dr["almacenista_codigoAduanas"].ToString() + " | " + dr["almacenista_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["almacenista_codigoSicex"].ToString();
                                etiqueta = dr["almacenista_codigoSicex"].ToString() + " | " + dr["almacenista_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["almacenista_nombre"].ToString();
                                etiqueta = dr["almacenista_codigo"].ToString() + " | " + dr["almacenista_nombre"].ToString();
                                break;
                            default:
                                valor = dr["almacenista_codigo"].ToString();
                                etiqueta = dr["almacenista_codigo"].ToString() + " | " + dr["almacenista_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un almacenista
        /// </summary>
        /// <param name="almacenista"></param>
        /// <returns></returns>
        public Almacenista Obtener(Almacenista almacenista)
        {

            using (SqlCommand command = new SqlCommand("prc_alamacenistas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (almacenista.Id != 0)
                    command.Parameters.Add("@almacenista_ID", SqlDbType.Int).Value = almacenista.Id;
                if (!String.IsNullOrEmpty(almacenista.Codigo))
                    command.Parameters.Add("@almacenista_codigo", SqlDbType.VarChar, 10).Value = almacenista.Codigo;
                if (!String.IsNullOrEmpty(almacenista.CodigoAduanas))
                    command.Parameters.Add("@almacenista_codigoAduanas", SqlDbType.VarChar, 10).Value = almacenista.CodigoAduanas;
                if (!String.IsNullOrEmpty(almacenista.CodigoSicex))
                    command.Parameters.Add("@almacenista_codigoSicex", SqlDbType.VarChar, 10).Value = almacenista.CodigoSicex;
                if (almacenista.Bloqueado.HasValue)
                    command.Parameters.Add("@almacenista_bloqueado", SqlDbType.Bit).Value = almacenista.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        almacenista = Cargar(dr);
                }
                command.Connection.Close();
            }

            return almacenista;
        }
        /// <summary>
        /// Obtiene un Almacenista a partir de su Identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Almacenista Obtener(int id)
        {
            Almacenista almacenista = new Almacenista();
            almacenista.Id = id;
            using (SqlCommand command = new SqlCommand("prc_almacenistas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@almacenista_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        almacenista = Cargar(dr);
                }
                command.Connection.Close();
            }
            return almacenista;

        }
        /// <summary>
        /// Inserta o Actualiza un almacenista
        /// </summary>
        /// <param name="almacenista"></param>
        public void Guardar(Almacenista almacenista)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@almacenista_ID", SqlDbType.Int).Value = almacenista.Id;
                command.Parameters.Add("@almacenista_codigo", SqlDbType.VarChar, 10).Value = almacenista.Codigo;
                command.Parameters.Add("@almacenista_codigoAduanas", SqlDbType.VarChar, 10).Value = almacenista.CodigoAduanas;
                command.Parameters.Add("@almacenista_codigoSicex", SqlDbType.VarChar, 10).Value = almacenista.CodigoSicex;
                command.Parameters.Add("@almacenista_nombre", SqlDbType.VarChar, 100).Value = almacenista.Nombre;
                command.Parameters.Add("@almacenista_nombre2", SqlDbType.VarChar, 100).Value = almacenista.Nombre2;
                command.Parameters.Add("@almacenista_bloqueado", SqlDbType.Bit).Value = almacenista.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = almacenista.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = almacenista.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = almacenista.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = almacenista.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (almacenista.Id > 0)
                    {
                        command.CommandText = "prc_almacenistas_actualizar";
                        command.Parameters["@usuario"].Value = almacenista.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = almacenista.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_almacenistas_insertar";
                        command.Parameters["@almacenista_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en AlmacenistaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Almacenista Cargar(SqlDataReader dr)
        {
            Almacenista almacenista = new Almacenista();
            almacenista.Id = (int)dr["almacenista_ID"];
            almacenista.Codigo = dr["almacenista_codigo"].ToString();
            almacenista.CodigoAduanas = dr["almacenista_codigoAduanas"].ToString();
            almacenista.CodigoSicex = dr["almacenista_codigoSicex"].ToString();
            almacenista.Nombre = dr["almacenista_nombre"].ToString();
            almacenista.Nombre2 = dr["almacenista_nombre2"].ToString();
            almacenista.Bloqueado = (bool)dr["almacenista_bloqueado"];
            almacenista.ParaCodParam = dr["para_codParam"].ToString();
            almacenista.TapaCodTabla = (int)dr["tapa_codTabla"];
            almacenista.CreacionUsuario = dr["creacion_usuario"].ToString();
            almacenista.CreacionFecha = (DateTime)dr["creacion_fecha"];
            almacenista.CreacionIp = dr["creacion_IP"].ToString();
            almacenista.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            almacenista.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            almacenista.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return almacenista;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
