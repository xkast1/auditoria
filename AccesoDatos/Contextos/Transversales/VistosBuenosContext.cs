﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class VistosBuenosContext
    {
        private string connectionString = string.Empty;

        public VistosBuenosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public VistosBuenosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene el listado secuencial de todos los vistos buenos
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<VistoBueno> ObtenerSecuencia(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_vistosBuenos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@vistoBueno_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        VistoBueno vistoBueno = Cargar(dr);
                        yield return vistoBueno;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de vistos buenos con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_vistosBuenos_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["vistoBueno_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["vistoBueno_codigoAduanas"].ToString();
                                etiqueta = dr["vistoBueno_codigoAduanas"].ToString() + " | " + dr["vistoBueno_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["vistoBueno_codigoSicex"].ToString();
                                etiqueta = dr["vistoBueno_codigoSicex"].ToString() + " | " + dr["vistoBueno_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["vistoBueno_nombre"].ToString();
                                etiqueta = dr["vistoBueno_codigo"].ToString() + " | " + dr["vistoBueno_nombre"].ToString();
                                break;
                            default:
                                valor = dr["vistoBueno_codigo"].ToString();
                                etiqueta = dr["vistoBueno_codigo"].ToString() + " | " + dr["vistoBueno_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un visto bueno
        /// </summary>
        /// <param name="vistoBueno"></param>
        /// <returns></returns>
        public VistoBueno Obtener(VistoBueno vistoBueno)
        {

            using (SqlCommand command = new SqlCommand("prc_vistosBuenos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (vistoBueno.Id != 0)
                    command.Parameters.Add("@vistoBueno_ID", SqlDbType.Int).Value = vistoBueno.Id;
                if (!String.IsNullOrEmpty(vistoBueno.Codigo))
                    command.Parameters.Add("@vistoBueno_codigo", SqlDbType.VarChar, 10).Value = vistoBueno.Codigo;
                if (!String.IsNullOrEmpty(vistoBueno.CodigoAduanas))
                    command.Parameters.Add("@vistoBueno_codigoAduanas", SqlDbType.VarChar, 10).Value = vistoBueno.CodigoAduanas;
                if (!String.IsNullOrEmpty(vistoBueno.CodigoSicex))
                    command.Parameters.Add("@vistoBueno_codigoSicex", SqlDbType.VarChar, 10).Value = vistoBueno.CodigoSicex;
                if (vistoBueno.Bloqueado.HasValue)
                    command.Parameters.Add("@vistoBueno_bloqueado", SqlDbType.Bit).Value = vistoBueno.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        vistoBueno = Cargar(dr);
                }
                command.Connection.Close();
            }

            return vistoBueno;
        }
        /// <summary>
        /// Obtiene un visto bueno a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public VistoBueno Obtener(int id)
        {
            VistoBueno vistoBueno = new VistoBueno();
            vistoBueno.Id = id;
            using (SqlCommand command = new SqlCommand("prc_vistosBuenos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@vistoBueno_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        vistoBueno = Cargar(dr);
                }
                command.Connection.Close();
            }
            return vistoBueno;

        }
        /// <summary>
        /// Inserta o Actualiza un visto bueno
        /// </summary>
        /// <param name="vistoBueno"></param>
        public void Guardar(VistoBueno vistoBueno)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@vistoBueno_ID", SqlDbType.Int).Value = vistoBueno.Id;
                command.Parameters.Add("@vistoBueno_codigo", SqlDbType.VarChar, 10).Value = vistoBueno.Codigo;
                command.Parameters.Add("@vistoBueno_codigoAduanas", SqlDbType.VarChar, 10).Value = vistoBueno.CodigoAduanas;
                command.Parameters.Add("@vistoBueno_codigoSicex", SqlDbType.VarChar, 10).Value = vistoBueno.CodigoSicex;
                command.Parameters.Add("@vistoBueno_nombre", SqlDbType.VarChar, 100).Value = vistoBueno.Nombre;
                command.Parameters.Add("@vistoBueno_nombreIngles", SqlDbType.VarChar, 100).Value = vistoBueno.NombreIngles;
                command.Parameters.Add("@vistoBueno_sigla", SqlDbType.VarChar, 20).Value = vistoBueno.Sigla;
                command.Parameters.Add("@vistoBueno_bloqueado", SqlDbType.Bit).Value = vistoBueno.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = vistoBueno.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = vistoBueno.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = vistoBueno.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = vistoBueno.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (vistoBueno.Id > 0)
                    {
                        command.CommandText = "prc_vistosBuenos_actualizar";
                        command.Parameters["@usuario"].Value = vistoBueno.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = vistoBueno.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_vistosBuenos_insertar";
                        command.Parameters["@vistoBueno_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en VistoBuenoContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private VistoBueno Cargar(SqlDataReader dr)
        {
            VistoBueno vistoBueno = new VistoBueno();
            vistoBueno.Id = (int)dr["vistoBueno_ID"];
            vistoBueno.Codigo = dr["vistoBueno_codigo"].ToString();
            vistoBueno.CodigoAduanas = dr["vistoBueno_codigoAduanas"].ToString();
            vistoBueno.CodigoSicex = dr["vistoBueno_codigoSicex"].ToString();
            vistoBueno.Nombre = dr["vistoBueno_nombre"].ToString();
            vistoBueno.NombreIngles = dr["vistoBueno_nombreIngles"].ToString();
            vistoBueno.Sigla = dr["vistoBueno_sigla"].ToString();
            vistoBueno.Bloqueado = (bool)dr["vistoBueno_bloqueado"];
            vistoBueno.ParaCodParam = dr["para_codParam"].ToString();
            vistoBueno.TapaCodTabla = (int)dr["tapa_codTabla"];
            vistoBueno.CreacionUsuario = dr["creacion_usuario"].ToString();
            vistoBueno.CreacionFecha = (DateTime)dr["creacion_fecha"];
            vistoBueno.CreacionIp = dr["creacion_IP"].ToString();
            vistoBueno.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            vistoBueno.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            vistoBueno.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return vistoBueno;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}