﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class PaisesContext
    {
        private string connectionString = string.Empty;

        public PaisesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public PaisesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de todos los paises
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Pais> ObtenerTodos(int? continenteID = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_paises_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@pais_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (continenteID.HasValue)
                    command.Parameters.Add("@continente_ID", SqlDbType.Int).Value = continenteID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Pais pais = Cargar(dr);
                        yield return pais;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de Monedas con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_paises_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["pais_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["pais_codigoAduanas"].ToString();
                                etiqueta = dr["pais_codigoAduanas"].ToString() + " | " + dr["pais_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["pais_codigoSicex"].ToString();
                                etiqueta = dr["pais_codigoSicex"].ToString() + " | " + dr["pais_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["pais_nombre"].ToString();
                                etiqueta = dr["pais_codigo"].ToString() + " | " + dr["pais_nombre"].ToString();
                                break;
                            default:
                                valor = dr["pais_codigo"].ToString();
                                etiqueta = dr["pais_codigo"].ToString() + " | " + dr["pais_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una pais
        /// </summary>
        /// <param name="pais"></param>
        /// <returns></returns>
        public Pais Obtener(Pais pais)
        {

            using (SqlCommand command = new SqlCommand("prc_paises_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (pais.Id != 0)
                    command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = pais.Id;
                if (!String.IsNullOrEmpty(pais.Codigo))
                    command.Parameters.Add("@pais_codigo", SqlDbType.VarChar, 10).Value = pais.Codigo;
                if (!String.IsNullOrEmpty(pais.CodigoAduanas))
                    command.Parameters.Add("@pais_codigoAduanas", SqlDbType.VarChar, 10).Value = pais.CodigoAduanas;
                if (!String.IsNullOrEmpty(pais.CodigoSicex))
                    command.Parameters.Add("@pais_codigoSicex", SqlDbType.VarChar, 10).Value = pais.CodigoSicex;
                if (pais.Bloqueado.HasValue)
                    command.Parameters.Add("@pais_bloqueado", SqlDbType.Bit).Value = pais.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        pais = Cargar(dr);
                }
                command.Connection.Close();
            }

            return pais;
        }
        /// <summary>
        /// Obtiene un pais a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Pais Obtener(int id)
        {
            Pais pais = new Pais();
            pais.Id = id;
            using (SqlCommand command = new SqlCommand("prc_paises_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        pais = Cargar(dr);
                }
                command.Connection.Close();
            }
            return pais;

        }
        /// <summary>
        /// inserta o Actualiza un Pais
        /// </summary>
        /// <param name="pais"></param>
        public void Guardar(Pais pais)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = pais.Id;
                command.Parameters.Add("@pais_codigo", SqlDbType.VarChar, 10).Value = pais.Codigo;
                command.Parameters.Add("@pais_codigoAduanas", SqlDbType.VarChar, 10).Value = pais.CodigoAduanas;
                command.Parameters.Add("@pais_codigoSicex", SqlDbType.VarChar, 10).Value = pais.CodigoSicex;
                command.Parameters.Add("@pais_nombre", SqlDbType.VarChar, 100).Value = pais.Nombre;
                command.Parameters.Add("@pais_nombreIngles", SqlDbType.VarChar, 100).Value = pais.NombreIngles;
                command.Parameters.Add("@pais_bloqueado", SqlDbType.Bit).Value = pais.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = pais.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = pais.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = pais.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = pais.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (pais.Id > 0)
                    {
                        command.CommandText = "prc_paises_actualizar";
                        command.Parameters["@usuario"].Value = pais.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = pais.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_paises_insertar";
                        command.Parameters["@pais_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en PaisContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Pais Cargar(SqlDataReader dr)
        {
            Pais pais = new Pais();
            pais.Id = (int)dr["pais_ID"];
            pais.Codigo = dr["pais_codigo"].ToString();
            pais.CodigoAduanas = dr["pais_codigoAduanas"].ToString();
            pais.CodigoSicex = dr["pais_codigoSicex"].ToString();
            pais.Nombre = dr["pais_nombre"].ToString();
            pais.NombreIngles = dr["pais_nombreIngles"].ToString();
            pais.Bloqueado = (bool)dr["pais_bloqueado"];
            pais.ParaCodParam = dr["para_codParam"].ToString();
            pais.TapaCodTabla = (int)dr["tapa_codTabla"];
            pais.CreacionUsuario = dr["creacion_usuario"].ToString();
            pais.CreacionFecha = (DateTime)dr["creacion_fecha"];
            pais.CreacionIp = dr["creacion_IP"].ToString();
            pais.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            pais.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            pais.ActualizacionIp = dr["actualizacion_IP"].ToString();
            pais.Puertos = new PuertosContext(this.connectionString)
                .ObtenerTodos(pais.Id, false);
            pais.Monedas = new MonedasContext(this.connectionString)
                .ObtenerTodos(pais.Id, false);
            pais.Regiones = new RegionesContext(this.connectionString)
                .ObtenerTodos(pais.Id);
            return pais;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
