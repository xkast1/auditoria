﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class ProvinciasContext
    {
        private string connectionString = string.Empty;

        public ProvinciasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ProvinciasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de todas las provincias
        /// </summary>
        /// <param name="regionID"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Provincia> ObtenerTodos(int? regionID = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_provincias_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@provincia_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (regionID.HasValue)
                    command.Parameters.Add("@region_ID", SqlDbType.Int).Value = regionID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Provincia provincia = Cargar(dr);
                        yield return provincia;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de provincias con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_provincias_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["provincia_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["provincia_codigoAduanas"].ToString();
                                etiqueta = dr["provincia_codigoAduanas"].ToString() + " | " + dr["provincia_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["provincia_codigoSicex"].ToString();
                                etiqueta = dr["provincia_codigoSicex"].ToString() + " | " + dr["provincia_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["provincia_nombre"].ToString();
                                etiqueta = dr["provincia_codigo"].ToString() + " | " + dr["provincia_nombre"].ToString();
                                break;
                            default:
                                valor = dr["provincia_codigo"].ToString();
                                etiqueta = dr["provincia_codigo"].ToString() + " | " + dr["provincia_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una provincia
        /// </summary>
        /// <param name="provincia"></param>
        /// <returns></returns>
        public Provincia Obtener(Provincia provincia)
        {

            using (SqlCommand command = new SqlCommand("prc_tabla_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (provincia.Id != 0)
                    command.Parameters.Add("@provincia_ID", SqlDbType.Int).Value = provincia.Id;
                if (!String.IsNullOrEmpty(provincia.Codigo))
                    command.Parameters.Add("@provincia_codigo", SqlDbType.VarChar, 10).Value = provincia.Codigo;
                if (!String.IsNullOrEmpty(provincia.CodigoAduanas))
                    command.Parameters.Add("@provincia_codigoAduanas", SqlDbType.VarChar, 10).Value = provincia.CodigoAduanas;
                if (!String.IsNullOrEmpty(provincia.CodigoSicex))
                    command.Parameters.Add("@provincia_codigoSicex", SqlDbType.VarChar, 10).Value = provincia.CodigoSicex;
                if (provincia.Bloqueado.HasValue)
                    command.Parameters.Add("@provincia_bloqueado", SqlDbType.Bit).Value = provincia.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        provincia = Cargar(dr);
                }
                command.Connection.Close();
            }

            return provincia;
        }
        /// <summary>
        /// Obtiene una provincia a partir de su Identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Provincia Obtener(int id)
        {
            Provincia provincia = new Provincia();
            provincia.Id = id;
            using (SqlCommand command = new SqlCommand("prc_provincias_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@provincia_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        provincia = Cargar(dr);
                }
                command.Connection.Close();
            }
            return provincia;

        }
        /// <summary>
        /// Inserta o Actualiza una provincia
        /// </summary>
        /// <param name="provincia"></param>
        public void Guardar(Provincia provincia)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@provincia_ID", SqlDbType.Int).Value = provincia.Id;
                command.Parameters.Add("@region_ID", SqlDbType.Int).Value = provincia.Region.Id;
                command.Parameters.Add("@provincia_codigo", SqlDbType.VarChar, 10).Value = provincia.Codigo;
                command.Parameters.Add("@provincia_codigoAduanas", SqlDbType.VarChar, 10).Value = provincia.CodigoAduanas;
                command.Parameters.Add("@provincia_codigoSicex", SqlDbType.VarChar, 10).Value = provincia.CodigoSicex;
                command.Parameters.Add("@provincia_nombre", SqlDbType.VarChar, 100).Value = provincia.Nombre;
                command.Parameters.Add("@provincia_bloqueado", SqlDbType.Bit).Value = provincia.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = provincia.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = provincia.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = provincia.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = provincia.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (provincia.Id > 0)
                    {
                        command.CommandText = "prc_provincias_actualizar";
                        command.Parameters["@usuario"].Value = provincia.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = provincia.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_provincias_insertar";
                        command.Parameters["@provincia_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en ProvinciaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Provincia Cargar(SqlDataReader dr)
        {
            Provincia provincia = new Provincia();
            provincia.Id = (int)dr["provincia_ID"];
            provincia.Codigo = dr["provincia_codigo"].ToString();
            provincia.CodigoAduanas = dr["provincia_codigoAduanas"].ToString();
            provincia.CodigoSicex = dr["provincia_codigoSicex"].ToString();
            provincia.Nombre = dr["provincia_nombre"].ToString();
            provincia.Bloqueado = (bool)dr["provincia_bloqueado"];
            provincia.ParaCodParam = dr["para_codParam"].ToString();
            provincia.TapaCodTabla = (int)dr["tapa_codTabla"];
            provincia.CreacionUsuario = dr["creacion_usuario"].ToString();
            provincia.CreacionFecha = (DateTime)dr["creacion_fecha"];
            provincia.CreacionIp = dr["creacion_IP"].ToString();
            provincia.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            provincia.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            provincia.ActualizacionIp = dr["actualizacion_IP"].ToString();
            provincia.Comunas = new ComunasContext(this.connectionString)
                .ObtenerTodos(provincia.Id, false);
            provincia.Region = dr["region_ID"] != DBNull.Value ? new RegionesContext(this.connectionString)
                .Obtener((int)dr["region_ID"]) : new Region();
            return provincia;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
