﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class TiposBultosContext
    {
        private string connectionString = string.Empty;

        public TiposBultosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposBultosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado de los tipos de bultos
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<TipoBulto> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposBultos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@tipoBulto_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoBulto tipoBulto = Cargar(dr);
                        yield return tipoBulto;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de Tipos de bultos con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_tiposBultos_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["tipoBulto_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["tipoBulto_codigoAduanas"].ToString();
                                etiqueta = dr["tipoBulto_codigoAduanas"].ToString() + " | " + dr["tipoBulto_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["tipoBulto_codigoSicex"].ToString();
                                etiqueta = dr["tipoBulto_codigoSicex"].ToString() + " | " + dr["tipoBulto_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["tipoBulto_nombre"].ToString();
                                etiqueta = dr["tipoBulto_codigo"].ToString() + " | " + dr["tipoBulto_nombre"].ToString();
                                break;
                            default:
                                valor = dr["tipoBulto_codigo"].ToString();
                                etiqueta = dr["tipoBulto_codigo"].ToString() + " | " + dr["tipoBulto_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un tipo bulto
        /// </summary>
        /// <param name="tipoBulto"></param>
        /// <returns></returns>
        public TipoBulto Obtener(TipoBulto tipoBulto)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposBultos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (tipoBulto.Id != 0)
                    command.Parameters.Add("@tipoBulto_ID", SqlDbType.Int).Value = tipoBulto.Id;
                if (!String.IsNullOrEmpty(tipoBulto.Codigo))
                    command.Parameters.Add("@tipoBulto_codigo", SqlDbType.VarChar, 10).Value = tipoBulto.Codigo;
                if (!String.IsNullOrEmpty(tipoBulto.CodigoAduanas))
                    command.Parameters.Add("@tipoBulto_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoBulto.CodigoAduanas;
                if (!String.IsNullOrEmpty(tipoBulto.CodigoSicex))
                    command.Parameters.Add("@tipoBulto_codigoSicex", SqlDbType.VarChar, 10).Value = tipoBulto.CodigoSicex;
                if (tipoBulto.Bloqueado.HasValue)
                    command.Parameters.Add("@tipoBulto_bloqueado", SqlDbType.Bit).Value = tipoBulto.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoBulto = Cargar(dr);
                }
                command.Connection.Close();
            }

            return tipoBulto;
        }
        /// <summary>
        /// Obtiene un tipo de bulto a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TipoBulto Obtener(int id)
        {
            TipoBulto tipoBulto = new TipoBulto();
            tipoBulto.Id = id;
            using (SqlCommand command = new SqlCommand("prc_tiposBultos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoBulto_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoBulto = Cargar(dr);
                }
                command.Connection.Close();
            }
            return tipoBulto;
        }
        /// <summary>
        /// Inserta o Actualiza un tipo de bulto
        /// </summary>
        /// <param name="tipoBulto"></param>
        public void Guardar(TipoBulto tipoBulto)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoBulto_ID", SqlDbType.Int).Value = tipoBulto.Id;
                command.Parameters.Add("@tipoBulto_codigo", SqlDbType.VarChar, 10).Value = tipoBulto.Codigo;
                command.Parameters.Add("@tipoBulto_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoBulto.CodigoAduanas;
                command.Parameters.Add("@tipoBulto_codigoSicex", SqlDbType.VarChar, 10).Value = tipoBulto.CodigoSicex;
                command.Parameters.Add("@tipoBulto_nombre", SqlDbType.VarChar, 100).Value = tipoBulto.Nombre;
                command.Parameters.Add("@tipoBulto_nombreIngles", SqlDbType.VarChar, 100).Value = tipoBulto.NombreIngles;
                command.Parameters.Add("@tipoBulto_nombre2", SqlDbType.VarChar, 100).Value = tipoBulto.Nombre2;
                command.Parameters.Add("@tipoBulto_nombreIngles2", SqlDbType.VarChar, 100).Value = tipoBulto.NombreIngles2;
                command.Parameters.Add("@tipoBulto_bloqueado", SqlDbType.Bit).Value = tipoBulto.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = tipoBulto.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = tipoBulto.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = tipoBulto.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = tipoBulto.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (tipoBulto.Id > 0)
                    {
                        command.CommandText = "prc_tiposBultos_actualizar";
                        command.Parameters["@usuario"].Value = tipoBulto.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = tipoBulto.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_tiposBultos_insertar";
                        command.Parameters["@tipoBulto_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TipoBultoContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private TipoBulto Cargar(SqlDataReader dr)
        {
            TipoBulto tipoBulto = new TipoBulto();
            tipoBulto.Id = (int)dr["tipoBulto_ID"];
            tipoBulto.Codigo = dr["tipoBulto_codigo"].ToString();
            tipoBulto.CodigoAduanas = dr["tipoBulto_codigoAduanas"].ToString();
            tipoBulto.CodigoSicex = dr["tipoBulto_codigoSicex"].ToString();
            tipoBulto.Nombre = dr["tipoBulto_nombre"].ToString();
            tipoBulto.NombreIngles = dr["tipoBulto_nombreIngles"].ToString();
            tipoBulto.Nombre2 = dr["tipoBulto_nombre2"].ToString();
            tipoBulto.NombreIngles2 = dr["tipoBulto_nombreIngles2"].ToString();
            tipoBulto.Bloqueado = (bool)dr["tipoBulto_bloqueado"];
            tipoBulto.ParaCodParam = dr["para_codParam"].ToString();
            tipoBulto.TapaCodTabla = (int)dr["tapa_codTabla"];
            tipoBulto.CreacionUsuario = dr["creacion_usuario"].ToString();
            tipoBulto.CreacionFecha = (DateTime)dr["creacion_fecha"];
            tipoBulto.CreacionIp = dr["creacion_IP"].ToString();
            tipoBulto.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            tipoBulto.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            tipoBulto.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return tipoBulto;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}