﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class TiposCargasContext
    {
        private string connectionString = string.Empty;

        public TiposCargasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposCargasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene un listado secuencial de los tipos de cargas
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<TipoCarga> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposCargas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@tipoCarga_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoCarga tipoCarga = Cargar(dr);
                        yield return tipoCarga;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de Tipos de cargas con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_tiposCargas_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["tipoCarga_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["tipoCarga_codigoAduanas"].ToString();
                                etiqueta = dr["tipoCarga_codigoAduanas"].ToString() + " | " + dr["tipoCarga_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["tipoCarga_codigoSicex"].ToString();
                                etiqueta = dr["tipoCarga_codigoSicex"].ToString() + " | " + dr["tipoCarga_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["tipoCarga_nombre"].ToString();
                                etiqueta = dr["tipoCarga_codigo"].ToString() + " | " + dr["tipoCarga_nombre"].ToString();
                                break;
                            default:
                                valor = dr["tipoCarga_codigo"].ToString();
                                etiqueta = dr["tipoCarga_codigo"].ToString() + " | " + dr["tipoCarga_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un tipo de carga
        /// </summary>
        /// <param name="tipoCarga"></param>
        /// <returns></returns>
        public TipoCarga Obtener(TipoCarga tipoCarga)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposCargas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (tipoCarga.Id != 0)
                    command.Parameters.Add("@tipoCarga_ID", SqlDbType.Int).Value = tipoCarga.Id;
                if (!String.IsNullOrEmpty(tipoCarga.Codigo))
                    command.Parameters.Add("@tipoCarga_codigo", SqlDbType.VarChar, 10).Value = tipoCarga.Codigo;
                if (!String.IsNullOrEmpty(tipoCarga.CodigoAduanas))
                    command.Parameters.Add("@tipoCarga_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoCarga.CodigoAduanas;
                if (!String.IsNullOrEmpty(tipoCarga.CodigoSicex))
                    command.Parameters.Add("@tipoCarga_codigoSicex", SqlDbType.VarChar, 10).Value = tipoCarga.CodigoSicex;
                if (tipoCarga.Bloqueado.HasValue)
                    command.Parameters.Add("@tipoCarga_bloqueado", SqlDbType.Bit).Value = tipoCarga.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoCarga = Cargar(dr);
                }
                command.Connection.Close();
            }

            return tipoCarga;
        }
        /// <summary>
        /// Obtiene un tipo de carga a partir de un identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TipoCarga Obtener(string codigo, int? id = null)
        {
            TipoCarga tipoCarga = new TipoCarga();
            using (SqlCommand command = new SqlCommand("prc_tiposCargas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (id.HasValue)
                    command.Parameters.Add("@tipoCarga_ID", SqlDbType.Int).Value = id;
                command.Parameters.Add("@tipoCarga_codigo", SqlDbType.VarChar).Value = codigo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoCarga = Cargar(dr);
                }
                command.Connection.Close();
            }
            return tipoCarga;

        }
        /// <summary>
        /// Inserta o Actualiza un tipo de carga
        /// </summary>
        /// <param name="tipoCarga"></param>
        public void Guardar(TipoCarga tipoCarga)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoCarga_ID", SqlDbType.Int).Value = tipoCarga.Id;
                command.Parameters.Add("@tipoCarga_codigo", SqlDbType.VarChar, 10).Value = tipoCarga.Codigo;
                command.Parameters.Add("@tipoCarga_codigoAduanas", SqlDbType.VarChar, 10).Value = tipoCarga.CodigoAduanas;
                command.Parameters.Add("@tipoCarga_codigoSicex", SqlDbType.VarChar, 10).Value = tipoCarga.CodigoSicex;
                command.Parameters.Add("@tipoCarga_nombre", SqlDbType.VarChar, 100).Value = tipoCarga.Nombre;
                command.Parameters.Add("@tipoCarga_nombreIngles", SqlDbType.VarChar, 100).Value = tipoCarga.NombreIngles;
                command.Parameters.Add("@tipoCarga_definicion", SqlDbType.VarChar, 255).Value = tipoCarga.Definicion;
                command.Parameters.Add("@tipoCarga_definicionIngles", SqlDbType.VarChar, 255).Value = tipoCarga.DefinicionIngles;
                command.Parameters.Add("@tipoCarga_bloqueado", SqlDbType.Bit).Value = tipoCarga.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = tipoCarga.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = tipoCarga.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = tipoCarga.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = tipoCarga.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (tipoCarga.Id > 0)
                    {
                        command.CommandText = "prc_tiposCargas_actualizar";
                        command.Parameters["@usuario"].Value = tipoCarga.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = tipoCarga.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_tiposCargas_insertar";
                        command.Parameters["@tipoCarga_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TipoCargaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private TipoCarga Cargar(SqlDataReader dr)
        {
            TipoCarga tipoCarga = new TipoCarga();
            tipoCarga.Id = (int)dr["tipoCarga_ID"];
            tipoCarga.Codigo = dr["tipoCarga_codigo"].ToString();
            tipoCarga.CodigoAduanas = dr["tipoCarga_codigoAduanas"].ToString();
            tipoCarga.CodigoSicex = dr["tipoCarga_codigoSicex"].ToString();
            tipoCarga.Nombre = dr["tipoCarga_nombre"].ToString();
            tipoCarga.NombreIngles = dr["tipoCarga_nombreIngles"].ToString();
            tipoCarga.Definicion = dr["tipoCarga_definicion"].ToString();
            tipoCarga.DefinicionIngles = dr["tipoCarga_definicionIngles"].ToString();
            tipoCarga.Bloqueado = (bool)dr["tipoCarga_bloqueado"];
            tipoCarga.ParaCodParam = dr["para_codParam"].ToString();
            tipoCarga.TapaCodTabla = (int)dr["tapa_codTabla"];
            tipoCarga.CreacionUsuario = dr["creacion_usuario"].ToString();
            tipoCarga.CreacionFecha = (DateTime)dr["creacion_fecha"];
            tipoCarga.CreacionIp = dr["creacion_IP"].ToString();
            tipoCarga.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            tipoCarga.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            tipoCarga.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return tipoCarga;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}