﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales.ProductoServicio;

namespace AccesoDatos.Contextos.Transversales.ProductosServicios
{
    public class ProductosServiciosContext
    {
        private string connectionString = string.Empty;

        public ProductosServiciosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ProductosServiciosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public ProductoServicio Obtener(ProductoServicio productoServcicio)
        {

            using (SqlCommand command = new SqlCommand("prc_productosServicios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@productoServicio_ID", SqlDbType.Int).Value = productoServcicio.Id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        productoServcicio = Cargar(dr);
                }
                command.Connection.Close();
            }
            return productoServcicio;
        }
        public ProductoServicio Cargar(SqlDataReader dr)
        {

            ProductoServicio productoServicio = new ProductoServicio();
            productoServicio.Id = (int)dr["productoServicio_ID"];
            productoServicio.Codigo = dr["productoServicio_codigo"] != DBNull.Value ? dr["productoServicio_codigo"].ToString() : "";
            productoServicio.CodigoServicioPublico = dr["productoServicio_codigo"] != DBNull.Value ? dr["productoServicio_codigoServicioPublico"].ToString() : "";
            productoServicio.ServicioPublico = dr["productoServicio_servicioPublico"] != DBNull.Value ? dr["productoServicio_servicioPublico"].ToString() : "";
            productoServicio.CodigoNAB = dr["productoServicio_codigoNAB"] != DBNull.Value ? dr["productoServicio_codigoNAB"].ToString() : "";
            productoServicio.Nombre = dr["productoServicio_nombre"] != DBNull.Value ? dr["productoServicio_nombre"].ToString() : "";
            productoServicio.NombreEspanol = dr["productoServicio_nombreEspanol"] != DBNull.Value ? dr["productoServicio_nombreEspanol"].ToString() : "";
            productoServicio.Descripcion = dr["productoServicio_descripcion"] != DBNull.Value ? dr["productoServicio_descripcion"].ToString() : "";
            productoServicio.DescripcionEspanol = dr["productoServicio_descripcionEspanol"] != DBNull.Value ? dr["productoServicio_descripcionEspanol"].ToString() : "";
            productoServicio.NombreCientifico = dr["productoServicio_nombreCientifico"] != DBNull.Value ? dr["productoServicio_nombreCientifico"].ToString() : "";
            productoServicio.NombreNegocio = dr["productoServicio_nombreNegocio"] != DBNull.Value ? dr["productoServicio_nombreNegocio"].ToString() : "";
            productoServicio.NombreNegocioEspanol = dr["produtoServicio_nombreNegocioEspanol"] != DBNull.Value ? dr["produtoServicio_nombreNegocioEspanol"].ToString() : "";
            productoServicio.EstadoFisico = dr["productoServicio_estadoFisico"] != DBNull.Value ? dr["productoServicio_estadoFisico"].ToString() : "";
            productoServicio.Estado = dr["productoServicio_estado"] != DBNull.Value ? dr["productoServicio_estado"].ToString() : "";
            productoServicio.Regimen = dr["productoServicio_regimen"] != DBNull.Value ? dr["productoServicio_regimen"].ToString() : "";
            productoServicio.CreacionUsuario = dr["creacion_usuario"] != DBNull.Value ? dr["creacion_usuario"].ToString() : "";
            productoServicio.CreacionFecha = (DateTime)dr["creacion_fecha"];
            productoServicio.CreacionIP = dr["creacion_IP"] != DBNull.Value ? dr["creacion_IP"].ToString() : "";
            productoServicio.ActualizacionUsuario = dr["actualizacion_usuario"] != DBNull.Value ? dr["actualizacion_usuario"].ToString() : "";
            productoServicio.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            productoServicio.ActualizacionIP = dr["actualizacion_IP"] != DBNull.Value ? dr["actualizacion_IP"].ToString() : "";
            productoServicio.Atributos = new ProductosServiciosAtributosContext(this.connectionString).ObtenerTodos(productoServicio.Id);


            return productoServicio;
        }
    }
}
