﻿using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales.ProductoServicio;

namespace AccesoDatos.Contextos.Transversales.ProductosServicios
{
    public class ProductosServiciosAtributosContext
    {
        private string connectionString = string.Empty;

        public ProductosServiciosAtributosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ProductosServiciosAtributosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        public IEnumerable<ProductoServicioAtributo> ObtenerTodos(int id)
        {
            using (SqlCommand command = new SqlCommand("prc_productosServiciosAtributos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Parameters.Add("@productoServicio_ID", SqlDbType.Int).Value = id;
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ProductoServicioAtributo productoServicioAtributo = Cargar(dr);
                        yield return productoServicioAtributo;
                    }
                }
                command.Connection.Close();
            }
        }
        private ProductoServicioAtributo Cargar(SqlDataReader dr) {
            ProductoServicioAtributo productoServicioAtributo = new ProductoServicioAtributo();
            productoServicioAtributo.Id = (int)dr["productoServicioAtributo_ID"];
            productoServicioAtributo.Secuencia = dr["productoServicioAtributo_secuencia"] != DBNull.Value ? (int)dr["productoServicioAtributo_secuencia"] : 1;
            productoServicioAtributo.Nombre = dr["productoServicioAtributo_nombre"] != DBNull.Value ? dr["productoServicioAtributo_nombre"].ToString() : "";
            productoServicioAtributo.NombreEspanol = dr["productoServicioAtributo_nombreEspanol"] != DBNull.Value ? dr["productoServicioAtributo_nombreEspanol"].ToString() : "";
            productoServicioAtributo.Valor = dr["productoServicioAtributo_valor"] != DBNull.Value ? dr["productoServicioAtributo_valor"].ToString() : "";
            productoServicioAtributo.TipoDato = dr["productoServicioAtributo_tipoDato"] != DBNull.Value ? dr["productoServicioAtributo_tipoDato"].ToString() : "";
            productoServicioAtributo.TamanoDato = dr["productoServicioAtributo_"] != DBNull.Value ? (int)dr["productoServicioAtributo_"] : 0;
            productoServicioAtributo.TipoCodigo = dr["productoServicioAtributo_tipoCodigo"] != DBNull.Value ? dr["productoServicioAtributo_tipoCodigo"].ToString() : "";
            productoServicioAtributo.TipoCodigoMaestro = dr["productoServicioAtributo_tipoCodigoMaestro"] != DBNull.Value ? dr["productoServicioAtributo_tipoCodigoMaestro"].ToString() : "";
            productoServicioAtributo.IsFixed = dr["productoServicioAtributo_isFixed"] != DBNull.Value ? dr["productoServicioAtributo_isFixed"].ToString() : "";
            productoServicioAtributo.Mandatorio = dr["productoServicioAtributo_mandatorio"] != DBNull.Value ? dr["productoServicioAtributo_mandatorio"].ToString() : "";
            productoServicioAtributo.Status = dr["productoServicioAtributo_status"] != DBNull.Value ? dr["productoServicioAtributo_status"].ToString() : "";
            productoServicioAtributo.Uso = dr["productoServicioAtributo_uso"] != DBNull.Value ? dr["productoServicioAtributo_uso"].ToString() : "";
            productoServicioAtributo.UniqueCode = dr["productoServicioAtributo_unique"] != DBNull.Value ? dr["productoServicioAtributo_unique"].ToString() : "";
            return productoServicioAtributo;
        }
    }
}
