﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class EmisoresDocumentosTransportesContext
    {
        private string connectionString = string.Empty;

        public EmisoresDocumentosTransportesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public EmisoresDocumentosTransportesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        public EmisorDocumentoTransporte Obtener(EmisorDocumentoTransporte emisorDocumentoTransporte)
        {
            //GoTo Falta procedimiento
            using (SqlCommand command = new SqlCommand("prc_emisoresDocumentosTransporte_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (emisorDocumentoTransporte.Id != 0)
                    command.Parameters.Add("@emisorDocumentoTransporte_ID", SqlDbType.Int).Value = emisorDocumentoTransporte.Id;
                if (!String.IsNullOrEmpty(emisorDocumentoTransporte.Rut))
                    command.Parameters.Add("@emisorDocumentoTransporte_rut", SqlDbType.VarChar, 10).Value = emisorDocumentoTransporte.Rut;
                if (!String.IsNullOrEmpty(emisorDocumentoTransporte.RazonSocial))
                    command.Parameters.Add("@emisorDocumentoTransporte_RazonSocial", SqlDbType.VarChar, 50).Value = emisorDocumentoTransporte.RazonSocial;
                if (!String.IsNullOrEmpty(emisorDocumentoTransporte.ParaCodParam))
                    command.Parameters.Add("@ParaCodParam", SqlDbType.VarChar, 6).Value = emisorDocumentoTransporte.ParaCodParam;
                if (emisorDocumentoTransporte.Bloqueado.HasValue)
                    command.Parameters.Add("@emisorDocumentoTransporte_bloqueado", SqlDbType.Bit).Value = emisorDocumentoTransporte.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        emisorDocumentoTransporte = Cargar(dr);
                }
                command.Connection.Close();
            }

            return emisorDocumentoTransporte;
        }

        private EmisorDocumentoTransporte Cargar(SqlDataReader dr)
        {
            EmisorDocumentoTransporte emisorDocumentoTransporte = new EmisorDocumentoTransporte();

            emisorDocumentoTransporte.Id = (int)dr["emisorDocumentoTransporte_ID"];
            emisorDocumentoTransporte.Rut = dr["emisorDocumentoTransporte_rut"].ToString();
            emisorDocumentoTransporte.RazonSocial = dr["emisorDocumentoTransporte_razonSocial"].ToString();
            emisorDocumentoTransporte.ParaCodParam = dr["para_codParam"].ToString();
            emisorDocumentoTransporte.TapaCodTabla = (int)dr["tapa_codTabla"];
            emisorDocumentoTransporte.Bloqueado = (bool)dr["emisorDocumentoTransporte_bloqueado"];
            emisorDocumentoTransporte.CreacionUsuario = dr["creacion_usuario"].ToString();
            emisorDocumentoTransporte.CreacionFecha = (DateTime)dr["creacion_fecha"];
            emisorDocumentoTransporte.CreacionIp = dr["creacion_IP"].ToString();
            emisorDocumentoTransporte.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            emisorDocumentoTransporte.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            emisorDocumentoTransporte.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return emisorDocumentoTransporte;
        }
    }
}
