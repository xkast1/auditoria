﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Transversales.Consumidor;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales.Consumidores
{
    public class ClientesContext
    {
        private string connectionString = string.Empty;

        public ClientesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ClientesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        /// <summary>
        /// Obtiene un cliente especifico
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="numeroSucursal"></param>
        /// <returns></returns>
        public Cliente Obtener(int? ID = null, bool? bloqueado = null, int? numeroSucursal = null, string rut = null)
        {
            Cliente cliente = new Cliente();
            using (SqlCommand command = new SqlCommand("prc_clientes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (ID.HasValue)
                    command.Parameters.Add("@cliente_ID", SqlDbType.Int).Value = ID;
                if (numeroSucursal.HasValue)
                    command.Parameters.Add("@cliente_NumeroSucursal", SqlDbType.Int).Value = numeroSucursal;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@cliente_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Parameters.Add("@cliente_Rut", SqlDbType.VarChar, 20).Value = rut;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        cliente = Cargar(dr);
                }
                command.Connection.Close();
            }
            return cliente;

        }
        /// <summary>
        /// Obtiene el cliente de un despacho
        /// </summary>
        /// <param name="numeroDespacho"></param>
        /// <returns></returns>
        public Cliente ObtenerPorDespacho(string numeroDespacho)
        {
            Cliente Cliente = new Cliente();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("select cliente_despacho"), conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@NumeroDespacho", SqlDbType.NVarChar, 10).Value = numeroDespacho;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                        Cliente = Cargar(dr);
                }
                cmd.Connection.Close();
            }

            return Cliente;
        }

        public IEnumerable<Cliente> ObtenerTodos(int? grupoTrabajoID = null)
        {
            var procedimiento = grupoTrabajoID.HasValue ? "prc_gruposTrabajo_clientes_seleccionar" : "prc_clientes_seleccionar";
            var conexion = grupoTrabajoID.HasValue ? System.Configuration.ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString : this.connectionString;
            using (var comando = new SqlCommand(procedimiento))
            {
                comando.CommandType = CommandType.StoredProcedure;
                if (grupoTrabajoID.HasValue)
                    comando.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = grupoTrabajoID;
                comando.Connection = new SqlConnection(conexion);
                comando.Connection.Open();
                using (SqlDataReader dr = comando.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Cliente cliente = Cargar(dr);
                        yield return cliente;
                    }
                }
                comando.Connection.Close();
            }
        }

        /// <summary>
        /// Busqueda de un cliente autocompletado
        /// </summary>
        /// <param name="buscado"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado)
        {
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                cnn.Open();
                SqlCommand cmd = new SqlCommand("prc_cliente_buscar", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@cliente_buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                using (SqlDataReader drd = cmd.ExecuteReader())
                {
                    while (drd.Read())
                    {
                        ResultadoAutocompletar cliente = new ResultadoAutocompletar();
                        cliente.Id = drd["RUTCLIENTE"].ToString().Trim();
                        cliente.Id2 = drd["NumSucursal"].ToString().Trim();
                        cliente.Value = drd["clie_razonSocia"].ToString().Trim();
                        cliente.Label = drd["RUTCLIENTE"].ToString().Trim() + " | " + String.Format("{0:d}", drd["clie_razonSocia"]).ToUpper() + " | Sucursal N°" + drd["NUMSUCURSAL"].ToString().Trim();
                        yield return cliente;
                    }
                }
                cmd.Connection.Close();
            }
        }

        public IEnumerable<Cliente> Buscar(Cliente cliente)
        {
            // TODO determinar si esta forma de buscar es mejor que recibiendo
            //      un parámetro fijo para autocompletar por la columna
            //      asociada a ese parámetro.
            throw new NotImplementedException();
        }

        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into cliente(
                                   ) values
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update cliente set

                                    where ";
                    break;

                case "delete":
                    strQuery = @"Delete From cliente";
                    break;

                case "select cliente_despacho":
                    strQuery = @"
                        SELECT [dbo].[CLIENTE].[CLIENTE_ID]      [Cliente_ID]             -- 0
                             , [dbo].[CLIENTE].[RUTCLIENTE]      [Cliente_Rut]            -- 1
                             , [dbo].[CLIENTE].[NUMSUCURSAL]     [Cliente_NumeroSucursal] -- 2
	                         , [dbo].[CLIENTE].[CLIE_NOMBRES]    [Cliente_Nombre]         -- 3
	                         , [dbo].[CLIENTE].[CLIE_APELLIDO]   [Cliente_Apellido]       -- 4
	                         , [dbo].[CLIENTE].[CLIE_ESTBLOQ]    [Cliente_Bloqueado]      -- 5
                             , [dbo].[CLIENTE].[CLIE_RAZONSOCIA] [Cliente_RazonSocial]    -- 6
	                         , [dbo].[CLIENTE].[CLIE_RAZONCORTA] [Cliente_RazonCorta]     -- 7
                             , [dbo].[CLIENTE].[CLIE_DIRCLI]     [Cliente_Direccion]      -- 8

                        FROM [dbo].[DESPACHO_ANEXOING]

                        INNER JOIN [dbo].[CLIENTE]
                           ON [dbo].[CLIENTE].[RUTCLIENTE] = [dbo].[DESPACHO_ANEXOING].[RUTCLIENTE]
                          AND [dbo].[CLIENTE].[NUMSUCURSAL] = [dbo].[DESPACHO_ANEXOING].[NUMSUCURSAL]

                        WHERE [dbo].[DESPACHO_ANEXOING].[NUMDESPACHINGRE] = @NumeroDespacho";
                    break;

                default:
                    strQuery = @"select
	                                   rutcliente  --0
                                        ,numsucursal  --1
                                        ,clie_razonSocia  --2
                                        ,clie_dircli  --3
                                    FROM cliente ";
                    break;
            }


            return strQuery;
        }
        private Cliente Cargar(SqlDataReader dr)
        {
            Cliente cliente = new Cliente();
            cliente.Id = (int)dr["Cliente_ID"];
            cliente.Rut = dr["Cliente_Rut"].ToString();
            cliente.Sucursal = Convert.ToInt32(dr["Cliente_NumeroSucursal"]);
            cliente.Nombre = dr["Cliente_Nombre"].ToString();
            cliente.Apellido = dr["Cliente_Apellido"].ToString();
            cliente.Bloqueado = Convert.ToBoolean(dr["Cliente_Bloqueado"].ToString() == "0" ? false : true);
            cliente.RazonSocial = dr["Cliente_RazonSocial"].ToString();
            cliente.RazonCorta = dr["Cliente_RazonCorta"].ToString();
            cliente.Direccion = dr["Cliente_Direccion"].ToString();
            cliente.Telefono = dr["Cliente_Telefono"] == DBNull.Value ? "" : dr["Cliente_Telefono"].ToString();

            return cliente;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
