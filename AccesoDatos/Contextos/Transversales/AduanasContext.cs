﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class AduanasContext
    {
        private string connectionString = string.Empty;

        public AduanasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public AduanasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene el listado secuencial de aduanas segun la vía de transporte
        /// </summary>
        /// <param name="viaTransporteID"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Aduana> ObtenerTodos(int? viaTransporteID = null, int? grupoTrabajoID = null, bool? bloqueado = null)
        {
            var procediminento = grupoTrabajoID.HasValue ? "prc_gruposTrabajo_aduanas_seleccionar" : "prc_aduanas_seleccionar";
            var conexionBD = grupoTrabajoID.HasValue ? ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString : this.connectionString;
            using (SqlCommand command = new SqlCommand(procediminento))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@aduana_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (viaTransporteID.HasValue)
                    command.Parameters.Add("@viaTransporte_ID", SqlDbType.Int).Value = viaTransporteID;
                if (grupoTrabajoID.HasValue)
                    command.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = grupoTrabajoID;
                command.Connection = new SqlConnection(conexionBD);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Aduana aduana = Cargar(dr);
                        yield return aduana;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// obtiene listado secuencial de aduanas
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Aduana> ObtenerTodos(bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_aduanas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@aduana_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Aduana aduana = Cargar(dr);
                        yield return aduana;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Busqueda de aduanas con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_aduanas_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["aduana_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["aduana_codigoAduanas"].ToString();
                                etiqueta = dr["aduana_codigoAduanas"].ToString() + " | " + dr["aduana_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["aduana_codigoSicex"].ToString();
                                etiqueta = dr["aduana_codigoSicex"].ToString() + " | " + dr["aduana_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["aduana_nombre"].ToString();
                                etiqueta = dr["aduana_codigo"].ToString() + " | " + dr["aduana_nombre"].ToString();
                                break;
                            default:
                                valor = dr["aduana_codigo"].ToString();
                                etiqueta = dr["aduana_codigo"].ToString() + " | " + dr["aduana_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una Aduana
        /// </summary>
        /// <param name="aduana"></param>
        /// <returns></returns>
        public Aduana Obtener(Aduana aduana)
        {

            using (SqlCommand command = new SqlCommand("prc_aduanas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (aduana.Id != 0)
                    command.Parameters.Add("@aduana_ID", SqlDbType.Int).Value = aduana.Id;
                if (!String.IsNullOrEmpty(aduana.Codigo))
                    command.Parameters.Add("@aduana_codigo", SqlDbType.VarChar, 10).Value = aduana.Codigo;
                if (!String.IsNullOrEmpty(aduana.CodigoAduanas))
                    command.Parameters.Add("@aduana_codigoAduanas", SqlDbType.VarChar, 10).Value = aduana.CodigoAduanas;
                if (!String.IsNullOrEmpty(aduana.CodigoSicex))
                    command.Parameters.Add("@aduana_codigoSicex", SqlDbType.VarChar, 10).Value = aduana.CodigoSicex;
                if (aduana.Bloqueado.HasValue)
                    command.Parameters.Add("@aduana_bloqueado", SqlDbType.Bit).Value = aduana.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        aduana = Cargar(dr);
                }
                command.Connection.Close();
            }

            return aduana;
        }
        /// <summary>
        /// Obtiene una aduana a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Aduana Obtener(string codigoAduana, int? id = null)
        {
            Aduana aduana = new Aduana();
            using (SqlCommand command = new SqlCommand("prc_aduanas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (id.HasValue)
                    command.Parameters.Add("@aduana_ID", SqlDbType.Int).Value = id;
                command.Parameters.Add("@aduana_codigo", SqlDbType.VarChar).Value = codigoAduana;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        aduana = Cargar(dr);
                }
                command.Connection.Close();
            }
            return aduana;

        }
        /// <summary>
        /// Inserta o actualiza una nueva aduana
        /// </summary>
        /// <param name="aduana"></param>
        public void Guardar(Aduana aduana)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@aduana_ID", SqlDbType.Int).Value = aduana.Id;
                        command.Parameters.Add("@aduana_codigo", SqlDbType.VarChar, 10).Value = aduana.Codigo;
                        command.Parameters.Add("@aduana_codigoAduanas", SqlDbType.VarChar, 10).Value = aduana.CodigoAduanas;
                        command.Parameters.Add("@aduana_codigoSicex", SqlDbType.VarChar, 10).Value = aduana.CodigoSicex;
                        command.Parameters.Add("@aduana_nombre", SqlDbType.VarChar, 100).Value = aduana.Nombre;
                        command.Parameters.Add("@aduana_nombreIngles", SqlDbType.VarChar, 100).Value = aduana.NombreIngles;
                        command.Parameters.Add("@aduana_direccion", SqlDbType.VarChar, 255).Value = aduana.Direccion;
                        command.Parameters.Add("@aduana_bloqueado", SqlDbType.Bit).Value = aduana.Bloqueado;
                        command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = aduana.ParaCodParam;
                        command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = aduana.TapaCodTabla;
                        command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = aduana.CreacionUsuario;
                        command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = aduana.CreacionIp;
                        command.Connection = connection;
                        command.Transaction = transaction;


                        if (aduana.Id > 0)
                        {
                            command.CommandText = "prc_aduanas_actualizar";
                            command.Parameters["@usuario"].Value = aduana.ActualizacionUsuario;
                            command.Parameters["@IP"].Value = aduana.ActualizacionIp;
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_aduanas_viasTransportes_eliminar", connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@aduana_ID", SqlDbType.Int).Value = aduana.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            command.CommandText = "prc_aduanas_insertar";
                            command.Parameters["@aduana_ID"].Value = null;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    aduana.Id = (int)dr["aduana_ID"];
                            }
                        }
                        using (SqlCommand commandAduanasVias = new SqlCommand("prc_aduanas_viasTransportes_insertar", connection))
                        {
                            commandAduanasVias.CommandType = CommandType.StoredProcedure;
                            commandAduanasVias.Parameters.Add("@aduana_ID", SqlDbType.Int).Value = aduana.Id;
                            commandAduanasVias.Parameters.Add("@viaTransporte_ID", SqlDbType.Int);
                            commandAduanasVias.Transaction = transaction;

                            foreach (ViaTransporte viaTransporte in aduana.ViasTransportes)
                            {
                                commandAduanasVias.Parameters["@viaTransporte_ID"].Value = viaTransporte.Id;
                                commandAduanasVias.ExecuteNonQuery();
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en AduanaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }


        }
        private Aduana Cargar(SqlDataReader dr)
        {
            Aduana aduana = new Aduana();
            aduana.Id = (int)dr["aduana_ID"];
            aduana.Codigo = dr["aduana_codigo"].ToString();
            aduana.CodigoAduanas = dr["aduana_codigoAduanas"].ToString();
            aduana.CodigoSicex = dr["aduana_codigoSicex"].ToString();
            aduana.Nombre = dr["aduana_nombre"].ToString();
            aduana.NombreIngles = dr["aduana_nombreIngles"].ToString();
            aduana.Direccion = dr["aduana_direccion"].ToString();
            aduana.Bloqueado = (bool)dr["aduana_bloqueado"];
            aduana.ParaCodParam = dr["para_codParam"].ToString();
            aduana.TapaCodTabla = (int)dr["tapa_codTabla"];
            aduana.CreacionUsuario = dr["creacion_usuario"].ToString();
            aduana.CreacionFecha = (DateTime)dr["creacion_fecha"];
            aduana.CreacionIp = dr["creacion_IP"].ToString();
            aduana.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            aduana.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            aduana.ActualizacionIp = dr["actualizacion_IP"].ToString();
            aduana.ViasTransportes = new ViasTransportesContext()
                .ObtenerTodos(aduana.Id, false);
            return aduana;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}