﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    class ContinentesContext
    {
        private string connectionString = string.Empty;

        public ContinentesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public ContinentesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }

        /// <summary>
        /// Obtiene listado secuencial de todas las continentes
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Continente> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_continentes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@continente_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Continente continente = Cargar(dr);
                        yield return continente;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de continentes con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_continentes_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["continente_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["continente_codigoAduanas"].ToString();
                                etiqueta = dr["continente_codigoAduanas"].ToString() + " | " + dr["continente_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["continente_codigoSicex"].ToString();
                                etiqueta = dr["continente_codigoSicex"].ToString() + " | " + dr["continente_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["continente_nombre"].ToString();
                                etiqueta = dr["continente_codigo"].ToString() + " | " + dr["continente_nombre"].ToString();
                                break;
                            default:
                                valor = dr["continente_codigo"].ToString();
                                etiqueta = dr["continente_codigo"].ToString() + " | " + dr["continente_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un contienente
        /// </summary>
        /// <param name="continente"></param>
        /// <returns></returns>
        public Continente Obtener(Continente continente)
        {

            using (SqlCommand command = new SqlCommand("prc_continentes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (continente.Id != 0)
                    command.Parameters.Add("@continente_ID", SqlDbType.Int).Value = continente.Id;
                if (!String.IsNullOrEmpty(continente.Codigo))
                    command.Parameters.Add("@continente_codigo", SqlDbType.VarChar, 10).Value = continente.Codigo;
                if (!String.IsNullOrEmpty(continente.CodigoAduanas))
                    command.Parameters.Add("@continente_codigoAduanas", SqlDbType.VarChar, 10).Value = continente.CodigoAduanas;
                if (!String.IsNullOrEmpty(continente.CodigoSicex))
                    command.Parameters.Add("@continente_codigoSicex", SqlDbType.VarChar, 10).Value = continente.CodigoSicex;
                if (continente.Bloqueado.HasValue)
                    command.Parameters.Add("@continente_bloqueado", SqlDbType.Bit).Value = continente.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        continente = Cargar(dr);
                }
                command.Connection.Close();
            }

            return continente;
        }
        /// <summary>
        /// Obtiene una continente a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Continente Obtener(int id)
        {
            Continente continente = new Continente();
            continente.Id = id;
            using (SqlCommand command = new SqlCommand("prc_continentes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@continente_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        continente = Cargar(dr);
                }
                command.Connection.Close();
            }
            return continente;

        }
        /// <summary>
        /// Inserta o Actualiza una continente
        /// </summary>
        /// <param name="continente"></param>
        public void Guardar(Continente continente)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@continente_ID", SqlDbType.Int).Value = continente.Id;
                command.Parameters.Add("@continente_codigo", SqlDbType.VarChar, 10).Value = continente.Codigo;
                command.Parameters.Add("@continente_codigoAduanas", SqlDbType.VarChar, 10).Value = continente.CodigoAduanas;
                command.Parameters.Add("@continente_codigoSicex", SqlDbType.VarChar, 10).Value = continente.CodigoSicex;
                command.Parameters.Add("@continente_nombre", SqlDbType.VarChar, 100).Value = continente.Nombre;
                command.Parameters.Add("@continente_nombreIngles", SqlDbType.VarChar, 100).Value = continente.NombreIngles;
                command.Parameters.Add("@continente_bloqueado", SqlDbType.Bit).Value = continente.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = continente.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = continente.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (continente.Id > 0)
                    {
                        command.CommandText = "prc_continentes_actualizar";
                        command.Parameters["@usuario"].Value = continente.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = continente.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_continentes_insertar";
                        command.Parameters["@continente_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en continenteContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Continente Cargar(SqlDataReader dr)
        {
            Continente continente = new Continente();
            continente.Id = (int)dr["continente_ID"];
            continente.Codigo = dr["continente_codigo"].ToString();
            continente.CodigoAduanas = dr["continente_codigoAduanas"].ToString();
            continente.CodigoSicex = dr["continente_codigoSicex"].ToString();
            continente.Nombre = dr["continente_nombre"].ToString();
            continente.NombreIngles = dr["continente_nombreIngles"].ToString();
            continente.Bloqueado = (bool)dr["continente_bloqueado"];
            continente.CreacionUsuario = dr["creacion_usuario"].ToString();
            continente.CreacionFecha = (DateTime)dr["creacion_fecha"];
            continente.CreacionIp = dr["creacion_IP"].ToString();
            continente.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            continente.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            continente.ActualizacionIp = dr["actualizacion_IP"].ToString();
            continente.Paises = new PaisesContext(this.connectionString)
                .ObtenerTodos(continente.Id, false);
            return continente;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}