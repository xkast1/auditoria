﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;


namespace AccesoDatos.Contextos.Transversales
{
    public class ViasTransportesContext
    {
        private string connectionString = string.Empty;

        public ViasTransportesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ViasTransportesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }

        /// <summary>
        /// Obtiene el listado de las vias de transportes de una aduana
        /// </summary>
        /// <param name="aduanaID"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<ViaTransporte> ObtenerTodos(int aduanaID, bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_aduanas_viasTransportes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@viaTransporte_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Parameters.Add("@aduana_ID", SqlDbType.Int).Value = aduanaID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViaTransporte viaTransporte = new ViaTransporte();
                        viaTransporte = Cargar(dr);
                        yield return viaTransporte;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene listado de vias de transportes asociadas a una clausula de compra venta
        /// </summary>
        /// <param name="clausulaCompraVentaID"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<ViaTransporte> ObtenerTodos2(int clausulaCompraVentaID, bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_clausulasComprasVentas_modalidades_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@viaTransporte_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Parameters.Add("@clausulaCompraVenta_ID", SqlDbType.Int).Value = clausulaCompraVentaID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViaTransporte viaTransporte = new ViaTransporte();
                        viaTransporte = Cargar(dr);
                        yield return viaTransporte;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene el listado secuencial de las vias de transportes
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<ViaTransporte> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_viasTransportes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@viaTransporte_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ViaTransporte viaTransporte = Cargar(dr);
                        yield return viaTransporte;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de vias de transporte con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_viasTransportes_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["viaTransporte_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["viaTransporte_codigoAduanas"].ToString();
                                etiqueta = dr["viaTransporte_codigoAduanas"].ToString() + " | " + dr["viaTransporte_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["viaTransporte_codigoSicex"].ToString();
                                etiqueta = dr["viaTransporte_codigoSicex"].ToString() + " | " + dr["viaTransporte_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["viaTransporte_nombre"].ToString();
                                etiqueta = dr["viaTransporte_codigo"].ToString() + " | " + dr["viaTransporte_nombre"].ToString();
                                break;
                            default:
                                valor = dr["viaTransporte_codigo"].ToString();
                                etiqueta = dr["viaTransporte_codigo"].ToString() + " | " + dr["viaTransporte_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una vía de transporte
        /// </summary>
        /// <param name="viaTransporte"></param>
        /// <returns></returns>
        public ViaTransporte Obtener(ViaTransporte viaTransporte)
        {

            using (SqlCommand command = new SqlCommand("prc_unidadesMedidas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (viaTransporte.Id != 0)
                    command.Parameters.Add("@unidadMedida_ID", SqlDbType.Int).Value = viaTransporte.Id;
                if (!String.IsNullOrEmpty(viaTransporte.Codigo))
                    command.Parameters.Add("@unidadMedida_codigo", SqlDbType.VarChar, 10).Value = viaTransporte.Codigo;
                if (!String.IsNullOrEmpty(viaTransporte.CodigoAduanas))
                    command.Parameters.Add("@unidadMedida_codigoAduanas", SqlDbType.VarChar, 10).Value = viaTransporte.CodigoAduanas;
                if (!String.IsNullOrEmpty(viaTransporte.CodigoSicex))
                    command.Parameters.Add("@unidadMedida_codigoSicex", SqlDbType.VarChar, 10).Value = viaTransporte.CodigoSicex;
                if (viaTransporte.Bloqueado.HasValue)
                    command.Parameters.Add("@unidadMedida_bloqueado", SqlDbType.Bit).Value = viaTransporte.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        viaTransporte = Cargar(dr);
                }
                command.Connection.Close();
            }

            return viaTransporte;
        }
        /// <summary>
        /// Obtiene una via de transporte a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ViaTransporte Obtener(int id)
        {
            ViaTransporte viaTransporte = new ViaTransporte();
            viaTransporte.Id = id;
            using (SqlCommand command = new SqlCommand("prc_viasTransportes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@viaTransporte_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        viaTransporte = Cargar(dr);
                }
                command.Connection.Close();
            }
            return viaTransporte;

        }
        /// <summary>
        /// Inserta o Actualiza una via de transporte
        /// </summary>
        /// <param name="viaTransporte"></param>
        public void Guardar(ViaTransporte viaTransporte)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@viaTransporte_ID", SqlDbType.Int).Value = viaTransporte.Id;
                command.Parameters.Add("@viaTransporte_codigo", SqlDbType.VarChar, 10).Value = viaTransporte.Codigo;
                command.Parameters.Add("@viaTransporte_codigoAduanas", SqlDbType.VarChar, 10).Value = viaTransporte.CodigoAduanas;
                command.Parameters.Add("@viaTransporte_codigoSicex", SqlDbType.VarChar, 10).Value = viaTransporte.CodigoSicex;
                command.Parameters.Add("@viaTransporte_nombre", SqlDbType.VarChar, 100).Value = viaTransporte.Nombre;
                command.Parameters.Add("@viaTransporte_nombreIngles", SqlDbType.VarChar, 100).Value = viaTransporte.NombreIngles;
                command.Parameters.Add("@viaTransporte_bloqueado", SqlDbType.Bit).Value = viaTransporte.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = viaTransporte.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = viaTransporte.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = viaTransporte.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = viaTransporte.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (viaTransporte.Id > 0)
                    {
                        command.CommandText = "prc_viasTransportes_actualizar";
                        command.Parameters["@usuario"].Value = viaTransporte.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = viaTransporte.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_viasTransportes_insertar";
                        command.Parameters["@viaTransporte_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en ViaTransporteContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private ViaTransporte Cargar(SqlDataReader dr)
        {
            ViaTransporte viaTransporte = new ViaTransporte();
            viaTransporte.Id = (int)dr["viaTransporte_ID"];
            viaTransporte.Codigo = dr["viaTransporte_codigo"].ToString();
            viaTransporte.CodigoAduanas = dr["viaTransporte_codigoAduanas"].ToString();
            viaTransporte.CodigoSicex = dr["viaTransporte_codigoSicex"].ToString();
            viaTransporte.Nombre = dr["viaTransporte_nombre"].ToString();
            viaTransporte.NombreIngles = dr["viaTransporte_nombreIngles"].ToString();
            viaTransporte.Bloqueado = (bool)dr["viaTransporte_bloqueado"];
            viaTransporte.ParaCodParam = dr["para_codParam"].ToString();
            viaTransporte.TapaCodTabla = (int)dr["tapa_codTabla"];
            viaTransporte.CreacionUsuario = dr["creacion_usuario"].ToString();
            viaTransporte.CreacionFecha = (DateTime)dr["creacion_fecha"];
            viaTransporte.CreacionIp = dr["creacion_IP"].ToString();
            viaTransporte.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            viaTransporte.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            viaTransporte.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return viaTransporte;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}