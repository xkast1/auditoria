﻿using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Operacion;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos.Contextos.Transversales.Operaciones
{
    public class OperacionesContext
    {
        private string connectionString = string.Empty;

        public OperacionesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public OperacionesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    // Sanitiza cadena y prepara para búsqueda.
                    buscado = buscado.Trim() + "%";
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_operaciones_buscar";
                    command.Parameters.AddWithValue("@buscado", buscado);

                    connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (dr.Read())
                        {
                            yield return new ResultadoAutocompletar
                            {
                                Id = dr["operacion_ID"].ToString(),
                                Value = dr["operacion_numero"].ToString(),
                                Label = dr["operacion_numero"].ToString() + " | " + dr["cliente_razonSocial"].ToString() + " | " + dr["operacion_referencia"].ToString()
                            };
                        }
                    }
                }
            }
        }

        public Operacion Obtener(string numero)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_operaciones_seleccionar";
                    command.Parameters.AddWithValue("@operacion_numero", numero);

                    command.Connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            return Cargar(dr);
                        }
                    }
                }
            }

            throw new Exception("Operación no encontrada");
        }

        public IEnumerable<Browne.Core.Modelo.Transversales.Operacion.Operacion> ObtenerTodo(string tipo, int estado)
        {
            throw new NotImplementedException();
        }

        private Operacion Cargar(SqlDataReader dr, bool test)
        {
            Operacion Operacion = new Operacion();
            Operacion.Id = (int)dr["operacion_ID"];
            Operacion.Correlativo = (int)dr["operacion_correlativo"];
            Operacion.Numero = dr["operacion_numero"].ToString();
            Operacion.Fecha = (DateTime)dr["operacion_fecha"];
            Operacion.Referencia = dr["operacion_referencia"].ToString();
            Operacion.Observacion = dr["operacion_observacion"].ToString();
            Operacion.NumeroAceptacion = dr["operacion_numeroAceptacion"].ToString();
            Operacion.FechaAceptacion = (DateTime)dr["operacion_fechaAceptacion"];
            Operacion.Tipo = dr["operacion_tipo"].ToString();
            Operacion.Cliente = new AccesoDatos.Contextos.Transversales.Consumidores.ClientesContext(this.connectionString)
                .Obtener(null, null, (int)dr["cliente_sucursal"], dr["cliente_rut"].ToString());
            return Operacion;

        }

        private Operacion Cargar(SqlDataReader dr)
        {
            Operacion Operacion = new Operacion();
            Operacion.Id = int.Parse(dr["operacion_ID"].ToString());
            Operacion.Correlativo = int.Parse(dr["operacion_correlativo"].ToString());
            Operacion.Numero = dr["operacion_numero"].ToString();
            Operacion.Fecha = DateTime.Parse(dr["operacion_fecha"].ToString());
            Operacion.Referencia = dr["operacion_referencia"].ToString();
            //Operacion.Observacion = dr["operacion_observacion"].ToString(); // TODO: No existse columna!
            Operacion.NumeroAceptacion = dr["operacion_numeroAceptacion"].ToString();
            Operacion.FechaAceptacion = DateTime.Parse(dr["operacion_fechaAceptacion"].ToString());
            Operacion.Tipo = dr["operacion_tipo"].ToString();
            Operacion.Cliente = new AccesoDatos.Contextos.Transversales.Consumidores.ClientesContext(this.connectionString)
                .Obtener(null, null, int.Parse(dr["cliente_numeroSucursal"].ToString()), dr["cliente_rut"].ToString());
            return Operacion;

        }
    }
}
