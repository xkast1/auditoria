﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class ComunasContext
    {
        private string connectionString = string.Empty;

        public ComunasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public ComunasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
       
        /// <summary>
        /// Obtiene listado secuencial de todas las comunas
        /// </summary>
        /// <param name="provincia_ID"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Comuna> ObtenerTodos(int? provinciaID = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_comunas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@comuna_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (provinciaID.HasValue)
                    command.Parameters.Add("@provincia_ID", SqlDbType.Int).Value = provinciaID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Comuna comuna = Cargar(dr);
                        yield return comuna;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Busqueda de comunas con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_comunas_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["comuna_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["comuna_codigoAduanas"].ToString();
                                etiqueta = dr["comuna_codigoAduanas"].ToString() + " | " + dr["comuna_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["comuna_codigoSicex"].ToString();
                                etiqueta = dr["comuna_codigoSicex"].ToString() + " | " + dr["comuna_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["comuna_nombre"].ToString();
                                etiqueta = dr["comuna_codigo"].ToString() + " | " + dr["comuna_nombre"].ToString();
                                break;
                            default:
                                valor = dr["comuna_codigo"].ToString();
                                etiqueta = dr["comuna_codigo"].ToString() + " | " + dr["comuna_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una comuna
        /// </summary>
        /// <param name="comuna"></param>
        /// <returns></returns>
        public Comuna Obtener(Comuna comuna)
        {

            using (SqlCommand command = new SqlCommand("prc_comunas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (comuna.Id != 0)
                    command.Parameters.Add("@comuna_ID", SqlDbType.Int).Value = comuna.Id;
                if (!String.IsNullOrEmpty(comuna.Codigo))
                    command.Parameters.Add("@comuna_codigo", SqlDbType.VarChar, 10).Value = comuna.Codigo;
                if (!String.IsNullOrEmpty(comuna.CodigoAduanas))
                    command.Parameters.Add("@comuna_codigoAduanas", SqlDbType.VarChar, 10).Value = comuna.CodigoAduanas;
                if (!String.IsNullOrEmpty(comuna.CodigoSicex))
                    command.Parameters.Add("@comuna_codigoSicex", SqlDbType.VarChar, 10).Value = comuna.CodigoSicex;
                if (comuna.Bloqueado.HasValue)
                    command.Parameters.Add("@comuna_bloqueado", SqlDbType.Bit).Value = comuna.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        comuna = Cargar(dr);
                }
                command.Connection.Close();
            }

            return comuna;
        }
        /// <summary>
        /// Obtiene una comuna a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Comuna Obtener(int? id = null, int? codigoAduana = null)
        {
            Comuna comuna = new Comuna();
            //comuna.Id = id;
            using (SqlCommand command = new SqlCommand("prc_comunas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (id.HasValue)
                command.Parameters.Add("@comuna_ID", SqlDbType.Int).Value = id;
                if (codigoAduana.HasValue)
                command.Parameters.Add("@comuna_codigo", SqlDbType.Int).Value = codigoAduana;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        comuna = Cargar(dr);
                }
                command.Connection.Close();
            }
            return comuna;

        }
        /// <summary>
        /// Inserta o Actualiza una comuna
        /// </summary>
        /// <param name="comuna"></param>
        public void Guardar(Comuna comuna)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@comuna_ID", SqlDbType.Int).Value = comuna.Id;
                command.Parameters.Add("@provincia_ID", SqlDbType.Int).Value = comuna.Provincia.Id;
                command.Parameters.Add("@comuna_codigo", SqlDbType.VarChar, 10).Value = comuna.Codigo;
                command.Parameters.Add("@comuna_codigoAduanas", SqlDbType.VarChar, 10).Value = comuna.CodigoAduanas;
                command.Parameters.Add("@comuna_codigoSicex", SqlDbType.VarChar, 10).Value = comuna.CodigoSicex;
                command.Parameters.Add("@comuna_nombre", SqlDbType.VarChar, 100).Value = comuna.Nombre;
                command.Parameters.Add("@comuna_ciudad", SqlDbType.VarChar, 100).Value = comuna.Ciudad;
                command.Parameters.Add("@comuna_bloqueado", SqlDbType.Bit).Value = comuna.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = comuna.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = comuna.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = comuna.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = comuna.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (comuna.Id > 0)
                    {
                        command.CommandText = "prc_comunas_actualizar";
                        command.Parameters["@usuario"].Value = comuna.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = comuna.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_comunas_insertar";
                        command.Parameters["@comuna_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en ComunaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Comuna Cargar(SqlDataReader dr)
        {
            Comuna comuna = new Comuna();
            comuna.Id = (int)dr["comuna_ID"];
            comuna.Codigo = dr["comuna_codigo"].ToString();
            comuna.CodigoAduanas = dr["comuna_codigoAduanas"].ToString();
            comuna.CodigoSicex = dr["comuna_codigoSicex"].ToString();
            comuna.Nombre = dr["comuna_nombre"].ToString();
            comuna.Ciudad = dr["comuna_ciudad"].ToString();
            comuna.Bloqueado = (bool)dr["comuna_bloqueado"];
            comuna.ParaCodParam = dr["para_codParam"].ToString();
            comuna.TapaCodTabla = (int)dr["tapa_codTabla"];
            comuna.CreacionUsuario = dr["creacion_usuario"].ToString();
            comuna.CreacionFecha = (DateTime)dr["creacion_fecha"];
            comuna.CreacionIp = dr["creacion_IP"].ToString();
            comuna.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            comuna.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            comuna.ActualizacionIp = dr["actualizacion_IP"].ToString();
            comuna.Provincia = new ProvinciasContext(this.connectionString)
                .Obtener((int)dr["provincia_ID"]);

            return comuna;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}