﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class PuertosContext
    {
        private string connectionString = string.Empty;

        public PuertosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public PuertosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de todos los puertos
        /// </summary>
        /// <param name="paisID"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Puerto> ObtenerTodos(int? paisID = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_puertos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@puerto_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (paisID.HasValue)
                    command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = paisID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Puerto puerto = Cargar(dr);
                        yield return puerto;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de puertos con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_puertos_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["puerto_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["puerto_codigoAduanas"].ToString();
                                etiqueta = dr["puerto_codigoAduanas"].ToString() + " | " + dr["puerto_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["puerto_codigoSicex"].ToString();
                                etiqueta = dr["puerto_codigoSicex"].ToString() + " | " + dr["puerto_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["puerto_nombre"].ToString();
                                etiqueta = dr["puerto_codigo"].ToString() + " | " + dr["puerto_nombre"].ToString();
                                break;
                            default:
                                valor = dr["puerto_codigo"].ToString();
                                etiqueta = dr["puerto_codigo"].ToString() + " | " + dr["puerto_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un puerto
        /// </summary>
        /// <param name="puerto"></param>
        /// <returns></returns>
        public Puerto Obtener(Puerto puerto)
        {

            using (SqlCommand command = new SqlCommand("prc_puertos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (puerto.Id != 0)
                    command.Parameters.Add("@puerto_ID", SqlDbType.Int).Value = puerto.Id;
                if (!String.IsNullOrEmpty(puerto.Codigo))
                    command.Parameters.Add("@puerto_codigo", SqlDbType.VarChar, 10).Value = puerto.Codigo;
                if (!String.IsNullOrEmpty(puerto.CodigoAduanas))
                    command.Parameters.Add("@puerto_codigoAduanas", SqlDbType.VarChar, 10).Value = puerto.CodigoAduanas;
                if (!String.IsNullOrEmpty(puerto.CodigoSicex))
                    command.Parameters.Add("@puerto_codigoSicex", SqlDbType.VarChar, 10).Value = puerto.CodigoSicex;
                if (puerto.Bloqueado.HasValue)
                    command.Parameters.Add("@puerto_bloqueado", SqlDbType.Bit).Value = puerto.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        puerto = Cargar(dr);
                }
                command.Connection.Close();
            }

            return puerto;
        }
        /// <summary>
        /// Obtiene un puerto a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Puerto Obtener(int id)
        {
            Puerto puerto = new Puerto();
            puerto.Id = id;
            using (SqlCommand command = new SqlCommand("prc_puertos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@puerto_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        puerto = Cargar(dr);
                }
                command.Connection.Close();
            }
            return puerto;

        }
        /// <summary>
        /// Inserta o Actualiza un nuevo puerto
        /// </summary>
        /// <param name="puerto"></param>
        public void Guardar(Puerto puerto)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@puerto_ID", SqlDbType.Int).Value = puerto.Id;
                command.Parameters.Add("@pais_ID", SqlDbType.Int).Value = puerto.Pais.Id;
                command.Parameters.Add("@puerto_codigo", SqlDbType.VarChar, 10).Value = puerto.Codigo;
                command.Parameters.Add("@puerto_codigoAduanas", SqlDbType.VarChar, 10).Value = puerto.CodigoAduanas;
                command.Parameters.Add("@puerto_codigoSicex", SqlDbType.VarChar, 10).Value = puerto.CodigoSicex;
                command.Parameters.Add("@puerto_nombre", SqlDbType.VarChar, 100).Value = puerto.Nombre;
                command.Parameters.Add("@puerto_nombreIngles", SqlDbType.VarChar, 100).Value = puerto.NombreIngles;
                command.Parameters.Add("@puerto_bloqueado", SqlDbType.Bit).Value = puerto.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = puerto.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = puerto.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = puerto.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = puerto.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (puerto.Id > 0)
                    {
                        command.CommandText = "prc_puertos_actualizar";
                        command.Parameters["@usuario"].Value = puerto.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = puerto.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_puertos_insertar";
                        command.Parameters["@puerto_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en PuertoContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Puerto Cargar(SqlDataReader dr)
        {
            Puerto puerto = new Puerto();
            puerto.Id = (int)dr["puerto_ID"];
            //puerto.Pais = dr["pais_ID"];
            puerto.Codigo = dr["puerto_codigo"].ToString();
            puerto.CodigoAduanas = dr["puerto_codigoAduanas"].ToString();
            puerto.CodigoSicex = dr["puerto_codigoSicex"].ToString();
            puerto.Nombre = dr["puerto_nombre"].ToString();
            puerto.NombreIngles = dr["puerto_nombreIngles"].ToString();
            puerto.Bloqueado = (bool)dr["puerto_bloqueado"];
            puerto.ParaCodParam = dr["para_codParam"].ToString();
            puerto.TapaCodTabla = (int)dr["tapa_codTabla"];
            puerto.CreacionUsuario = dr["creacion_usuario"].ToString();
            puerto.CreacionFecha = (DateTime)dr["creacion_fecha"];
            puerto.CreacionIp = dr["creacion_IP"].ToString();
            puerto.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            puerto.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            puerto.ActualizacionIp = dr["actualizacion_IP"].ToString();
            puerto.Pais = dr.IsDBNull(1) ? null : new PaisesContext(this.connectionString).
                Obtener((int)dr["pais_ID"]);

            return puerto;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
