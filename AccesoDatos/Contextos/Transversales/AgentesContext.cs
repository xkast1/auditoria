﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class AgentesContext
    {
        private string connectionString = string.Empty;

        public AgentesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public AgentesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }
        /// <summary>
        /// Obtiene el listado de los agentes de aduana
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Agente> ObtenerTodos()
        {
            List<Agente> agentes = new List<Agente>();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " order by agen_nomagente", conn);
                cmd.CommandType = CommandType.Text;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Agente Agente = Cargar(dr);
                        yield return Agente;
                    }
                }
                cmd.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene un agente de aduana específico a partir de su código
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public Agente Obtener(string codigo)
        {
            Agente Agente = new Agente();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " where agen_codAgente = @codigo", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@codigo", SqlDbType.NVarChar, 6).Value = codigo;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        Agente = Cargar(dr);
                    }
                }
                cmd.Connection.Close();
            }
            return Agente;
        }
        /// <summary>
        /// Busca un agente de aduana a partir de su nombre autocompletado
        /// </summary>
        /// <param name="buscado"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado)
        {
            List<ResultadoAutocompletar> listado = new List<ResultadoAutocompletar>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(ObtenerQuery("") + " and agen_nomagente like @buscado order by agen_nomagente", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@busqueda", SqlDbType.NVarChar, 50).Value = buscado.Trim() + "%";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar Item = new ResultadoAutocompletar();
                        Item.Id = dr["agen_codAgente"].ToString().Trim();
                        Item.Value = dr["agen_codAgente"].ToString().Trim();
                        Item.Label = dr["agen_codAgente"].ToString().Trim() + " | " + String.Format("{0:d}", dr["agen_nomagente"]).ToUpper();
                        yield return Item;
                    }
                }
                cmd.Connection.Close();
            }
        }
        private string ObtenerQuery(string value)
        {
            string strQuery = string.Empty;
            switch (value)
            {
                case "insert":
                    strQuery = @"insert into ( 
                                   ) values 
                                        ()";
                    break;
                case "update":
                    strQuery = @"Update  set                                    
                                       
                                    where ";
                    break;

                case "delete":
                    strQuery = @"Delete From ";
                    break;

                default:
                    strQuery = @"select
	                                   agen_codAgente  --0
                                        ,agen_rutagente  --1  
                                        ,agen_nomagente  --2                                                                         
                                    FROM Agentes  ";
                    break;
            }


            return strQuery;
        }
        private Agente Cargar(SqlDataReader dr) {
            Agente Agente = new Agente();
            Agente.Codigo = dr.GetString(0);
            Agente.Rut = dr.GetString(1);
            Agente.Nombre = dr.GetString(2);
            return Agente;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }

    }
}
