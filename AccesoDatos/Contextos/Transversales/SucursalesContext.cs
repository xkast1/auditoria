﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Auth;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class SucursalesContext
    {
        private string connectionString = string.Empty;

        public SucursalesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public SucursalesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }
        public IEnumerable<Sucursal> ObtenerTodos(bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_sucursales_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@sucursal_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Sucursal sucursal = Cargar(dr);
                        yield return sucursal;
                    }
                }
                command.Connection.Close();
            }
        }

        public IEnumerable<Sucursal> ObtenerTodos(int? empresaID = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_sucursales_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (empresaID.HasValue)
                    command.Parameters.Add("@empresa_ID", SqlDbType.Int).Value = empresaID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Sucursal sucursal = Cargar(dr);
                        yield return sucursal;
                    }
                }
                command.Connection.Close();
            }
        }

        public Sucursal Obtener(int id)
        {
            Sucursal sucursal = new Sucursal();
            using (SqlCommand command = new SqlCommand("prc_sucursales_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sucursal_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        sucursal = Cargar(dr);
                }
                command.Connection.Close();
            }
            return sucursal;
        }
        public void Guardar(Sucursal sucursal)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sucursal_ID", SqlDbType.Int).Value = sucursal.Id;
                command.Parameters.Add("@comuna_ID", SqlDbType.Int).Value = sucursal.Comuna.Id;
                command.Parameters.Add("@aduana_ID", SqlDbType.Int).Value = sucursal.Aduana.Id;
                command.Parameters.Add("@empresa_ID", SqlDbType.Int).Value = sucursal.Empresa.Id;
                command.Parameters.Add("@sucursal_nombre", SqlDbType.VarChar, 50).Value = sucursal.Nombre;
                command.Parameters.Add("@sucursal_direccion", SqlDbType.VarChar, 100).Value = sucursal.Direccion;
                command.Parameters.Add("@sucursal_telefono", SqlDbType.VarChar, 20).Value = sucursal.Telefono;
                command.Parameters.Add("@sucursal_email", SqlDbType.VarChar, 50).Value = sucursal.Mail;
                command.Parameters.Add("@sucursal_responsable", SqlDbType.VarChar, 100).Value = sucursal.Responsable;
                command.Parameters.Add("@sucursal_bloqueado", SqlDbType.Bit).Value = sucursal.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = sucursal.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = sucursal.CreacionIp;

                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (sucursal.Id > 0)
                    {
                        command.CommandText = "prc_sucursales_actualizar";
                        command.Parameters["@usuario"].Value = sucursal.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = sucursal.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_sucursales_insertar";
                        command.Parameters["@sucursal_ID"].Value = null;
                        //command.Parameters["@usuario"].Value = sucursal.CreacionUsuario;
                        //command.Parameters["@IP"].Value = sucursal.CreacionIp;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en SucursalesContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Sucursal Cargar(SqlDataReader dr)
        {
            Sucursal Sucursal = new Sucursal();
            Sucursal.Id = (int)dr["sucursal_ID"];
            Sucursal.Nombre = dr["sucursal_nombre"].ToString();
            Sucursal.Direccion = dr["sucursal_direccion"].ToString();
            Sucursal.Telefono = dr["sucursal_telefono"].ToString();
            Sucursal.Mail = dr["sucursal_email"].ToString();
            Sucursal.Responsable = dr["sucursal_responsable"].ToString();
            Sucursal.Bloqueado = (bool)dr["sucursal_bloqueado"];
            Sucursal.CreacionUsuario = dr["creacion_usuario"].ToString();
            Sucursal.CreacionFecha = (DateTime)dr["creacion_fecha"];
            Sucursal.CreacionIp = dr["creacion_IP"].ToString();
            Sucursal.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            Sucursal.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            Sucursal.ActualizacionIp = dr["actualizacion_IP"].ToString();

            Sucursal.Aduana = new AduanasContext().Obtener(null, (dr["aduana_ID"] != DBNull.Value ? int.Parse(dr["aduana_ID"].ToString()) : 0));
            Sucursal.Comuna = new ComunasContext().Obtener((dr["comuna_ID"] != DBNull.Value ? int.Parse(dr["comuna_ID"].ToString()) : 0), null);
            Sucursal.Empresa = new EmpresasContext().Obtener((dr["empresa_ID"] != DBNull.Value ? (int)(dr["empresa_ID"]) : 0));

            return Sucursal;

        }

        public void Disponse()
        {
            this.connectionString = null;
        }

    }
}
