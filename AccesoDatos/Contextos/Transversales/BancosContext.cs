﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class BancosContext
    {
        private string connectionString = string.Empty;

        public BancosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public BancosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        
        /// <summary>
        /// Obtiene listado secuencial de todos los bancos
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<Banco> ObtenerTodos(bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_bancos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@banco_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Banco banco = Cargar(dr);
                        yield return banco;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Busqueda de bancos con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_bancos_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["banco_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["banco_codigoAduanas"].ToString();
                                etiqueta = dr["banco_codigoAduanas"].ToString() + " | " + dr["banco_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["banco_codigoSicex"].ToString();
                                etiqueta = dr["banco_codigoSicex"].ToString() + " | " + dr["banco_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["banco_nombre"].ToString();
                                etiqueta = dr["banco_codigo"].ToString() + " | " + dr["banco_nombre"].ToString();
                                break;
                            default:
                                valor = dr["banco_codigo"].ToString();
                                etiqueta = dr["banco_codigo"].ToString() + " | " + dr["banco_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                    command.Connection.Close();
                }
            }
        }
        /// <summary>
        /// Obtiene un Banco
        /// </summary>
        /// <param name="banco"></param>
        /// <returns></returns>
        public Banco Obtener(Banco banco)
        {

            using (SqlCommand command = new SqlCommand("prc_bancos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (banco.Id != 0)
                    command.Parameters.Add("@banco_ID", SqlDbType.Int).Value = banco.Id;
                if (!String.IsNullOrEmpty(banco.Codigo))
                    command.Parameters.Add("@banco_codigo", SqlDbType.VarChar, 10).Value = banco.Codigo;
                if (!String.IsNullOrEmpty(banco.CodigoAduanas))
                    command.Parameters.Add("@banco_codigoAduanas", SqlDbType.VarChar, 10).Value = banco.CodigoAduanas;
                if (!String.IsNullOrEmpty(banco.CodigoSicex))
                    command.Parameters.Add("@banco_codigoSicex", SqlDbType.VarChar, 10).Value = banco.CodigoSicex;
                if (banco.Bloqueado.HasValue)
                    command.Parameters.Add("@banco_bloqueado", SqlDbType.Bit).Value = banco.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        banco = Cargar(dr);
                }
                command.Connection.Close();
            }

            return banco;
        }
        /// <summary>
        /// Obtiene un banco a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Banco Obtener(int id)
        {
            Banco banco = new Banco();
            banco.Id = id;
            using (SqlCommand command = new SqlCommand("prc_bancos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@banco_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        banco = Cargar(dr);
                }
                command.Connection.Close();
            }
            return banco;

        }
        /// <summary>
        /// Inserta o Actualiza un banco
        /// </summary>
        /// <param name="banco"></param>
        public void Guardar(Banco banco)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@banco_ID", SqlDbType.Int).Value = banco.Id;
                command.Parameters.Add("@banco_codigo", SqlDbType.VarChar, 10).Value = banco.Codigo;
                command.Parameters.Add("@banco_codigoAduanas", SqlDbType.VarChar, 10).Value = banco.CodigoAduanas;
                command.Parameters.Add("@banco_codigoSicex", SqlDbType.VarChar, 10).Value = banco.CodigoSicex;
                command.Parameters.Add("@banco_nombre", SqlDbType.VarChar, 100).Value = banco.Nombre;
                command.Parameters.Add("@banco_nombreIngles", SqlDbType.VarChar, 100).Value = banco.NombreIngles;
                command.Parameters.Add("@banco_bloqueado", SqlDbType.Bit).Value = banco.Bloqueado;
                command.Parameters.Add("@banco_corporativa", SqlDbType.Bit).Value = banco.Corporativa;
                command.Parameters.Add("@banco_extranjero", SqlDbType.Bit).Value = banco.Extranjero;
                command.Parameters.Add("@banco_estatal", SqlDbType.Bit).Value = banco.Estatal;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = banco.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = banco.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (banco.Id > 0)
                    {
                        command.CommandText = "prc_bancos_actualizar";
                        command.Parameters["@usuario"].Value = banco.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = banco.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_bancos_insertar";
                        command.Parameters["@banco_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en BancoContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private Banco Cargar(SqlDataReader dr)
        {
            Banco banco = new Banco();
            banco.Id = (int)dr["banco_ID"];
            banco.Codigo = dr["banco_codigo"].ToString();
            banco.CodigoAduanas = dr["banco_codigoAduanas"].ToString();
            banco.CodigoSicex = dr["banco_codigoSicex"].ToString();
            banco.Nombre = dr["banco_nombre"].ToString();
            banco.NombreIngles = dr["banco_nombreIngles"].ToString();
            banco.Bloqueado = (bool)dr["banco_bloqueado"];
            banco.Corporativa = (bool)dr["banco_corporativa"];
            banco.Extranjero = (bool)dr["banco_extranjero"];
            banco.Estatal = (bool)dr["banco_estatal"];
            banco.CreacionUsuario = dr["creacion_usuario"].ToString();
            banco.CreacionFecha = (DateTime)dr["creacion_fecha"];
            banco.CreacionIp = dr["creacion_IP"].ToString();
            banco.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            banco.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            banco.ActualizacionIp = dr["actualizacion_IP"].ToString();
            banco.CuentasCorrientes = new Contabilidad.Mantenedores.CuentasCorrientesContext(ConfigurationManager.ConnectionStrings["browne_contables"].ConnectionString)
                .ObtenerTodos(banco.Id, false);

            return banco;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}