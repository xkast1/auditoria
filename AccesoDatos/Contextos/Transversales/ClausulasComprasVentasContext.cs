﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class ClausulasComprasVentasContext
    {
        private string connectionString = string.Empty;

        public ClausulasComprasVentasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ClausulasComprasVentasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        
        /// <summary>
        /// Obtiene listado de clausulas de compra venta asociadas a una vía de transporte
        /// </summary>
        /// <param name="viaTransporteID"></param>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<ClausulaCompraVenta> ObtenerTodos(int viaTransporteID, bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_clausulasComprasVentas_modalidades_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@clausulaCompraVenta_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Parameters.Add("@viaTransporte_ID", SqlDbType.Int).Value = viaTransporteID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ClausulaCompraVenta clausulaCompraVenta = new ClausulaCompraVenta();
                        clausulaCompraVenta = Cargar(dr);
                        yield return clausulaCompraVenta;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene listado secuencial de todas las clausulas de compra venta
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<ClausulaCompraVenta> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_clausulasComprasVentas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@clausulaCompraVenta_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ClausulaCompraVenta clausulaCompraVenta = Cargar(dr);
                        yield return clausulaCompraVenta;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de clausulas de compra venta con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_clausulasComprasVentas_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["clausulaCompraVenta_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["clausulaCompraVenta_codigoAduanas"].ToString();
                                etiqueta = dr["clausulaCompraVenta_codigoAduanas"].ToString() + " | " + dr["clausulaCompraVenta_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["clausulaCompraVenta_codigoSicex"].ToString();
                                etiqueta = dr["clausulaCompraVenta_codigoSicex"].ToString() + " | " + dr["clausulaCompraVenta_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["clausulaCompraVenta_nombre"].ToString();
                                etiqueta = dr["clausulaCompraVenta_codigo"].ToString() + " | " + dr["clausulaCompraVenta_nombre"].ToString();
                                break;
                            default:
                                valor = dr["clausulaCompraVenta_codigo"].ToString();
                                etiqueta = dr["clausulaCompraVenta_codigo"].ToString() + " | " + dr["clausulaCompraVenta_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una Clausula de Compra Venta
        /// </summary>
        /// <param name="clausulaCompraVenta"></param>
        /// <returns></returns>
        public ClausulaCompraVenta Obtener(ClausulaCompraVenta clausulaCompraVenta)
        {

            using (SqlCommand command = new SqlCommand("prc_clausulasComprasVentas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (clausulaCompraVenta.Id != 0)
                    command.Parameters.Add("@clausulaCompraVenta_ID", SqlDbType.Int).Value = clausulaCompraVenta.Id;
                if (!String.IsNullOrEmpty(clausulaCompraVenta.Codigo))
                    command.Parameters.Add("@clausulaCompraVenta_codigo", SqlDbType.VarChar, 10).Value = clausulaCompraVenta.Codigo;
                if (!String.IsNullOrEmpty(clausulaCompraVenta.CodigoAduanas))
                    command.Parameters.Add("@clausulaCompraVenta_codigoAduanas", SqlDbType.VarChar, 10).Value = clausulaCompraVenta.CodigoAduanas;
                if (!String.IsNullOrEmpty(clausulaCompraVenta.CodigoSicex))
                    command.Parameters.Add("@clausulaCompraVenta_codigoSicex", SqlDbType.VarChar, 10).Value = clausulaCompraVenta.CodigoSicex;
                if (clausulaCompraVenta.Bloqueado.HasValue)
                    command.Parameters.Add("@clausulaCompraVenta_bloqueado", SqlDbType.Bit).Value = clausulaCompraVenta.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        clausulaCompraVenta = Cargar(dr);
                }
                command.Connection.Close();
            }

            return clausulaCompraVenta;
        }
        /// <summary>
        /// Obtiene una clausula de compra venta a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ClausulaCompraVenta Obtener(int id)
        {
            ClausulaCompraVenta clausulaCompraVenta = new ClausulaCompraVenta();
            clausulaCompraVenta.Id = id;
            using (SqlCommand command = new SqlCommand("prc_clausulasComprasVentas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@clausulaCompraVenta_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        clausulaCompraVenta = Cargar(dr);
                }
                command.Connection.Close();
            }
            return clausulaCompraVenta;

        }
        /// <summary>
        /// Inserta o Actualiza una clausula de compra venta
        /// </summary>
        /// <param name="clausulaCompraVenta"></param>
        public void Guardar(ClausulaCompraVenta clausulaCompraVenta)
        {

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@clausulaCompraVenta_ID", SqlDbType.Int).Value = clausulaCompraVenta.Id;
                        command.Parameters.Add("@clausulaCompraVenta_codigo", SqlDbType.VarChar, 10).Value = clausulaCompraVenta.Codigo;
                        command.Parameters.Add("@clausulaCompraVenta_codigoAduanas", SqlDbType.VarChar, 10).Value = clausulaCompraVenta.CodigoAduanas;
                        command.Parameters.Add("@clausulaCompraVenta_codigoSicex", SqlDbType.VarChar, 10).Value = clausulaCompraVenta.CodigoSicex;
                        command.Parameters.Add("@clausulaCompraVenta_nombre", SqlDbType.VarChar, 100).Value = clausulaCompraVenta.Nombre;
                        command.Parameters.Add("@clausulaCompraVenta_nombreIngles", SqlDbType.VarChar, 100).Value = clausulaCompraVenta.NombreIngles;
                        command.Parameters.Add("@clausulaCompraVenta_sigla", SqlDbType.VarChar, 10).Value = clausulaCompraVenta.Sigla;
                        command.Parameters.Add("@clausulaCompraVenta_embalajeVerificacion", SqlDbType.Char, 1).Value = clausulaCompraVenta.EmbalajeVerificacion;
                        command.Parameters.Add("@clausulaCompraVenta_carga", SqlDbType.Char, 1).Value = clausulaCompraVenta.Carga;
                        command.Parameters.Add("@clausulaCompraVenta_transporteInteriorOrigen", SqlDbType.Char, 1).Value = clausulaCompraVenta.TransporteInteriorOrigen;
                        command.Parameters.Add("@clausulaCompraVenta_formalidadesAduanarasExpo", SqlDbType.Char, 1).Value = clausulaCompraVenta.FormalidadesAduanerasExpo;
                        command.Parameters.Add("@clausulaCompraVenta_costoManipulacionMercaderiaExpo", SqlDbType.Char, 1).Value = clausulaCompraVenta.CostoManipulacionMercaderiaExpo;
                        command.Parameters.Add("@clausulaCompraVenta_transportePrincipal", SqlDbType.Char, 1).Value = clausulaCompraVenta.TransportePrincipal;
                        command.Parameters.Add("@clausulaCompraVenta_seguroMercaderia", SqlDbType.Char, 1).Value = clausulaCompraVenta.SeguroMercaderia;
                        command.Parameters.Add("@clausulaCompraVenta_costoManipulacionMercaderiaImpo", SqlDbType.Char, 1).Value = clausulaCompraVenta.CostoManipulacionMercaderiaImpo;
                        command.Parameters.Add("@clausulaCompraVenta_formalidadesAduanerasImpo", SqlDbType.Char, 1).Value = clausulaCompraVenta.FormalidadesAduanerasImpo;
                        command.Parameters.Add("@clausulaCompraVenta_transporteInteriorDestino", SqlDbType.Char, 1).Value = clausulaCompraVenta.TransporteInteriorDestino;
                        command.Parameters.Add("@clausulaCompraVenta_entrega", SqlDbType.Char, 1).Value = clausulaCompraVenta.Entrega;
                        command.Parameters.Add("@clausulaCompraVenta_bloqueado", SqlDbType.Bit).Value = clausulaCompraVenta.Bloqueado;
                        command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = clausulaCompraVenta.ParaCodParam;
                        command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = clausulaCompraVenta.TapaCodTabla;
                        command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = clausulaCompraVenta.CreacionUsuario;
                        command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = clausulaCompraVenta.CreacionIp;
                        command.Connection = connection;
                        command.Transaction = transaction;

                        if (clausulaCompraVenta.Id > 0)
                        {
                            command.CommandText = "prc_clausulasComprasVentas_actualizar";
                            command.Parameters["@usuario"].Value = clausulaCompraVenta.ActualizacionUsuario;
                            command.Parameters["@IP"].Value = clausulaCompraVenta.ActualizacionIp;
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_clausulasComprasVentas_modalidades_eliminar", connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@clausulaCompraVenta_ID", SqlDbType.Int).Value = clausulaCompraVenta.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }

                        }
                        else
                        {
                            command.CommandText = "prc_clausulasComprasVentas_insertar";
                            command.Parameters["@clausulaCompraVenta_ID"].Value = null;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    clausulaCompraVenta.Id = (int)dr["clausulaCompraVenta_ID"];
                            }
                        }
                        using (SqlCommand commandPagina = new SqlCommand("prc_clausulasComprasVentas_modalidades_insertar", connection))
                        {
                            commandPagina.CommandType = CommandType.StoredProcedure;
                            commandPagina.Parameters.Add("@clausulaCompraVenta_ID", SqlDbType.Int).Value = clausulaCompraVenta.Id;
                            commandPagina.Parameters.Add("@viaTransporte_ID", SqlDbType.Int);
                            commandPagina.Transaction = transaction;

                            foreach (ViaTransporte viaTransporte in clausulaCompraVenta.ViasTransportes)
                            {
                                commandPagina.Parameters["@viaTransporte_ID"].Value = viaTransporte.Id;
                                commandPagina.ExecuteNonQuery();
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en ClausulaCompraVentaContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }

        }
        private ClausulaCompraVenta Cargar(SqlDataReader dr)
        {
            ClausulaCompraVenta clausulaCompraVenta = new ClausulaCompraVenta();

            clausulaCompraVenta.Id = (int)dr["clausulaCompraVenta_ID"];
            clausulaCompraVenta.Codigo = dr["clausulaCompraVenta_codigo"].ToString();
            clausulaCompraVenta.CodigoAduanas = dr["clausulaCompraVenta_codigoAduanas"].ToString();
            clausulaCompraVenta.CodigoSicex = dr["clausulaCompraVenta_codigoSicex"].ToString();
            clausulaCompraVenta.Nombre = dr["clausulaCompraVenta_nombre"].ToString();
            clausulaCompraVenta.NombreIngles = dr["clausulaCompraVenta_nombreIngles"].ToString();
            clausulaCompraVenta.Sigla = dr["clausulaCompraVenta_sigla"].ToString();
            clausulaCompraVenta.EmbalajeVerificacion = dr["clausulaCompraVenta_embalajeVerificacion"].ToString();
            clausulaCompraVenta.Carga = dr["clausulaCompraVenta_carga"].ToString();
            clausulaCompraVenta.TransporteInteriorOrigen = dr["clausulaCompraVenta_transporteInteriorOrigen"].ToString();
            clausulaCompraVenta.FormalidadesAduanerasExpo = dr["clausulaCompraVenta_formalidadesAduanarasExpo"].ToString();
            clausulaCompraVenta.CostoManipulacionMercaderiaExpo = dr["clausulaCompraVenta_costoManipulacionMercaderiaExpo"].ToString();
            clausulaCompraVenta.TransportePrincipal = dr["clausulaCompraVenta_transportePrincipal"].ToString();
            clausulaCompraVenta.SeguroMercaderia = dr["clausulaCompraVenta_seguroMercaderia"].ToString();
            clausulaCompraVenta.CostoManipulacionMercaderiaImpo = dr["clausulaCompraVenta_costoManipulacionMercaderiaImpo"].ToString();
            clausulaCompraVenta.FormalidadesAduanerasImpo = dr["clausulaCompraVenta_formalidadesAduanerasImpo"].ToString();
            clausulaCompraVenta.TransporteInteriorDestino = dr["clausulaCompraVenta_transporteInteriorDestino"].ToString();
            clausulaCompraVenta.Entrega = dr["clausulaCompraVenta_entrega"].ToString();
            clausulaCompraVenta.Bloqueado = (bool)dr["clausulaCompraVenta_Bloqueado"];
            clausulaCompraVenta.ParaCodParam = dr["para_codParam"].ToString();
            clausulaCompraVenta.TapaCodTabla = (int)dr["tapa_codTabla"];
            clausulaCompraVenta.CreacionUsuario = dr["creacion_usuario"].ToString();
            clausulaCompraVenta.CreacionFecha = (DateTime)dr["creacion_fecha"];
            clausulaCompraVenta.CreacionIp = dr["creacion_IP"].ToString();
            clausulaCompraVenta.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            clausulaCompraVenta.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            clausulaCompraVenta.ActualizacionIp = dr["actualizacion_IP"].ToString();
            clausulaCompraVenta.ViasTransportes = new ViasTransportesContext(this.connectionString)
                .ObtenerTodos2(clausulaCompraVenta.Id, false);
            return clausulaCompraVenta;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
