﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Transversales;

namespace AccesoDatos.Contextos.Transversales
{
    public class FormasPagosContext
    {
        private string connectionString = string.Empty;

        public FormasPagosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public FormasPagosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString;
        }
        /// <summary>
        /// Obtiene listado secuencial de todas las formas de pago
        /// </summary>
        /// <param name="bloqueado"></param>
        /// <returns></returns>
        public IEnumerable<FormaPago> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_formasPagos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@formaPago_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FormaPago formaPago = Cargar(dr);
                        yield return formaPago;
                    }
                }
                command.Connection.Close();
            }

        }
        /// <summary>
        /// Busqueda de formas de pago con autocompletado, por defecto la busqueda se realiza por código
        /// </summary>
        /// <param name="buscado"></param>
        /// <param name="aduana"></param>
        /// <param name="sicex"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public IEnumerable<ResultadoAutocompletar> Buscar(string buscado, bool? aduana = null, bool? sicex = null, bool? nombre = null)
        {

            string tipo = "C";
            string valor = "";
            string etiqueta = "";
            if (aduana.HasValue)
                tipo = "A";
            if (sicex.HasValue)
                tipo = "S";
            if (nombre.HasValue)
                tipo = "N";
            using (SqlCommand command = new SqlCommand("prc_formasPagos_buscar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@buscado", SqlDbType.VarChar, 50).Value = buscado.Trim() + "%";
                command.Parameters.Add("@tipo", SqlDbType.Char, 1).Value = tipo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ResultadoAutocompletar autocompletado = new ResultadoAutocompletar();
                        autocompletado.Id = dr["formaPago_ID"].ToString();
                        switch (tipo)
                        {
                            case "A":
                                valor = dr["formaPago_codigoAduanas"].ToString();
                                etiqueta = dr["formaPago_codigoAduanas"].ToString() + " | " + dr["formaPago_nombre"].ToString();
                                break;
                            case "S":
                                valor = dr["formaPago_codigoSicex"].ToString();
                                etiqueta = dr["formaPago_codigoSicex"].ToString() + " | " + dr["formaPago_nombre"].ToString();
                                break;
                            case "N":
                                valor = dr["formaPago_nombre"].ToString();
                                etiqueta = dr["formaPago_codigo"].ToString() + " | " + dr["formaPago_nombre"].ToString();
                                break;
                            default:
                                valor = dr["formaPago_codigo"].ToString();
                                etiqueta = dr["formaPago_codigo"].ToString() + " | " + dr["formaPago_nombre"].ToString();
                                break;

                        }
                        autocompletado.Value = valor;
                        autocompletado.Label = etiqueta;
                        yield return autocompletado;
                    }
                }
                command.Connection.Close();
            }
        }
        /// <summary>
        /// Obtiene una forma de pago
        /// </summary>
        /// <param name="formaPago"></param>
        /// <returns></returns>
        public FormaPago Obtener(FormaPago formaPago)
        {

            using (SqlCommand command = new SqlCommand("prc_formasPagos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (formaPago.Id != 0)
                    command.Parameters.Add("@formaPago_ID", SqlDbType.Int).Value = formaPago.Id;
                if (!String.IsNullOrEmpty(formaPago.Codigo))
                    command.Parameters.Add("@formaPago_codigo", SqlDbType.VarChar, 10).Value = formaPago.Codigo;
                if (!String.IsNullOrEmpty(formaPago.CodigoAduanas))
                    command.Parameters.Add("@formaPago_codigoAduanas", SqlDbType.VarChar, 10).Value = formaPago.CodigoAduanas;
                if (!String.IsNullOrEmpty(formaPago.CodigoSicex))
                    command.Parameters.Add("@formaPago_codigoSicex", SqlDbType.VarChar, 10).Value = formaPago.CodigoSicex;
                if (formaPago.Bloqueado.HasValue)
                    command.Parameters.Add("@formaPago_bloqueado", SqlDbType.Bit).Value = formaPago.Bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        formaPago = Cargar(dr);
                }
                command.Connection.Close();
            }

            return formaPago;
        }
        /// <summary>
        /// Obtiene una forma de pago a partir de su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FormaPago Obtener(int id)
        {
            FormaPago formaPago = new FormaPago();
            formaPago.Id = id;
            using (SqlCommand command = new SqlCommand("prc_formasPagos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@formaPago_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        formaPago = Cargar(dr);
                }
                command.Connection.Close();
            }
            return formaPago;

        }

        /// <summary>
        /// Inserta o Actualiza una forma de pago
        /// </summary>
        /// <param name="formaPago"></param>
        public void Guardar(FormaPago formaPago)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@formaPago_ID", SqlDbType.Int).Value = formaPago.Id;
                command.Parameters.Add("@formaPago_codigo", SqlDbType.VarChar, 10).Value = formaPago.Codigo;
                command.Parameters.Add("@formaPago_codigoAduanas", SqlDbType.VarChar, 10).Value = formaPago.CodigoAduanas;
                command.Parameters.Add("@formaPago_codigoSicex", SqlDbType.VarChar, 10).Value = formaPago.CodigoSicex;
                command.Parameters.Add("@formaPago_nombre", SqlDbType.VarChar, 100).Value = formaPago.Nombre;
                command.Parameters.Add("@formaPago_nombreIngles", SqlDbType.VarChar, 100).Value = formaPago.NombreIngles;
                command.Parameters.Add("@formaPago_sigla", SqlDbType.VarChar, 10).Value = formaPago.Sigla;
                command.Parameters.Add("@formaPago_bloqueado", SqlDbType.Bit).Value = formaPago.Bloqueado;
                command.Parameters.Add("@para_codParam", SqlDbType.VarChar).Value = formaPago.ParaCodParam;
                command.Parameters.Add("@tapa_codTabla", SqlDbType.Int).Value = formaPago.TapaCodTabla;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = formaPago.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = formaPago.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (formaPago.Id > 0)
                    {
                        command.CommandText = "prc_formasPagos_actualizar";
                        command.Parameters["@usuario"].Value = formaPago.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = formaPago.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_formasPagos_insertar";
                        command.Parameters["@formaPago_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en FormaPagoContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private FormaPago Cargar(SqlDataReader dr)
        {
            FormaPago formaPago = new FormaPago();
            formaPago.Id = (int)dr["formaPago_ID"];
            formaPago.Codigo = dr["formaPago_codigo"].ToString();
            formaPago.CodigoAduanas = dr["formaPago_codigoAduanas"].ToString();
            formaPago.CodigoSicex = dr["formaPago_codigoSicex"].ToString();
            formaPago.Nombre = dr["formaPago_nombre"].ToString();
            formaPago.NombreIngles = dr["formaPago_nombreIngles"].ToString();
            formaPago.Sigla = dr["formaPago_sigla"].ToString();
            formaPago.Bloqueado = (bool)dr["formaPago_bloqueado"];
            formaPago.ParaCodParam = dr["para_codParam"].ToString();
            formaPago.TapaCodTabla = (int)dr["tapa_codTabla"];
            formaPago.CreacionUsuario = dr["creacion_usuario"].ToString();
            formaPago.CreacionFecha = (DateTime)dr["creacion_fecha"];
            formaPago.CreacionIp = dr["creacion_IP"].ToString();
            formaPago.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            formaPago.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            formaPago.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return formaPago;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}