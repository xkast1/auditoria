﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Navegacion;

namespace AccesoDatos.Contextos.Navegacion
{
    public class PlataformasContext
    {
        private string connectionString = string.Empty;

        public PlataformasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public PlataformasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;

        }

        public IEnumerable<Plataforma> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_plataformas_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Plataforma Plataforma = Cargar(dr);
                        yield return Plataforma;
                    }
                }
                cmd.Connection.Close();
            }
        }

        public Plataforma Obtener(string codigo)
        {
            Plataforma plataforma = new Plataforma();
            plataforma.Codigo = codigo;
            using (SqlCommand command = new SqlCommand("prc_plataformas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@plataforma_codigo", SqlDbType.VarChar, 10).Value = codigo;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        plataforma = Cargar(dr);
                }
                command.Connection.Close();
            }
            return plataforma;
        }
        public void Guardar(Plataforma plataforma)
        {
            using (SqlCommand command = new SqlCommand("prc_plataformas_insertar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@plataforma_codigo", SqlDbType.VarChar, 10).Value = plataforma.Codigo;
                command.Parameters.Add("@plataforma_nombre", SqlDbType.VarChar, 50).Value = plataforma.Nombre;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en PlataformasContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        private Plataforma Cargar(SqlDataReader dr)
        {
            Plataforma Plataforma = new Plataforma();
            Plataforma.Codigo = dr["plataforma_codigo"].ToString();
            Plataforma.Nombre = dr["plataforma_nombre"].ToString();

            return Plataforma;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
