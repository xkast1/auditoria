﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Navegacion;

namespace AccesoDatos.Contextos.Navegacion
{
    public class MenusContext
    {
        private string connectionString = string.Empty;

        public MenusContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public MenusContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }
        public IEnumerable<Menu> ObtenerTodos(int rolId)
        {
            using (SqlCommand command = new SqlCommand("prc_menus_seleccionarRol"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rolId;
                command.Connection = new SqlConnection(connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Menu menu = Cargar(dr);
                        menu.Items = new MenusItemsContext(this.connectionString)
                            .ObtenerTodos(menu.Id, rolId);
                        yield return menu;
                    }
                }
                command.Connection.Close();
            }
        }

        public IEnumerable<Menu> ObtenerTodos()
        {
            using (SqlCommand command = new SqlCommand("prc_menus_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Menu menu = Cargar(dr);
                        menu.Items = new MenusItemsContext(this.connectionString)
                             .ObtenerTodos(menu.Id);
                        yield return menu;
                    }
                }
                command.Connection.Close();
            }
        }

        public void Guardar(Menu menu)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@menu_ID", SqlDbType.Int).Value = menu.Id;
                        command.Parameters.Add("@plataforma_codigo", SqlDbType.VarChar, 10).Value = menu.Plataforma.Codigo;
                        command.Parameters.Add("@menu_text", SqlDbType.VarChar, 50).Value = menu.Text;
                        command.Parameters.Add("@menu_posicion", SqlDbType.Int).Value = menu.Posicion;
                        command.Parameters.Add("@menu_ubicacion", SqlDbType.Char, 1).Value = menu.Ubicacion;
                        command.Connection = connection;
                        command.Transaction = transaction;

                        if (menu.Id > 0)
                        {
                            command.CommandText = "prc_menus_actualizar";
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_menusItems_eliminar", connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@menu_ID", SqlDbType.Int).Value = menu.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }

                        }
                        else
                        {
                            command.CommandText = "prc_menus_insertar";
                            command.Parameters["@menu_ID"].Value = null;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    menu.Id = (int)dr["menu_ID"];
                            }
                        }
                        using (SqlCommand commandItem = new SqlCommand("prc_menusItems_insertar", connection))
                        {
                            commandItem.CommandType = CommandType.StoredProcedure;
                            commandItem.Parameters.Add("@menuItem_ID", SqlDbType.Int);
                            commandItem.Parameters.Add("@menu_ID", SqlDbType.Int).Value = menu.Id;
                            commandItem.Parameters.Add("@pagina_ID", SqlDbType.Int);
                            commandItem.Parameters.Add("@menuItem_text", SqlDbType.VarChar, 255);
                            commandItem.Parameters.Add("@menuItem_posicion", SqlDbType.Int);
                            commandItem.Transaction = transaction;
                            foreach (MenuItem item in menu.Items)
                            {
                                commandItem.Parameters["@menuItem_ID"].Value = item.Id;
                                commandItem.Parameters["@pagina_ID"].Value = item.Pagina.Id;
                                commandItem.Parameters["@menuItem_text"].Value = item.Text;
                                commandItem.Parameters["@menuItem_posicion"].Value = item.Posicion;
                                commandItem.Parameters["@menuItem_ID"].Value = null;
                                using (SqlDataReader dr = command.ExecuteReader())
                                {
                                    if (dr.HasRows && dr.Read())
                                        item.Id = (int)dr["menuItem_ID"];
                                }
                                command.Connection.Close();
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en Menus.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }
        public void Eliminar(Menu menu)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand("prc_menusItems_eliminar", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@menu_ID", SqlDbType.Int).Value = menu.Id;
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                    }
                    using (SqlCommand command = new SqlCommand("prc_menus_eliminar", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@menu_ID", SqlDbType.Int).Value = menu.Id;
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();

                        command.Connection.Close();
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en Menus.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }

        }

        public Menu Obtener(int id)
        {
            var menu = new Menu();
            using (SqlCommand command = new SqlCommand("prc_menus_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@menu_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        menu = Cargar(dr);
                }
                command.Connection.Close();
            }
            return menu;
        }

        private Menu Cargar(SqlDataReader dr)
        {
            Menu menu = new Menu();
            menu.Id = (int)dr["menu_ID"];
            menu.Text = dr["menu_text"].ToString();
            menu.Posicion = (int)dr["menu_posicion"];
            menu.Ubicacion = dr["menu_ubicacion"].ToString();
            menu.Plataforma = new PlataformasContext()
                .Obtener(dr["plataforma_codigo"].ToString());
            return menu;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }


    }
}
