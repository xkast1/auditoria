﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Navegacion;

namespace AccesoDatos.Contextos.Navegacion
{
    public class ElementosContext
    {
        private string connectionString = string.Empty;

        public ElementosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public ElementosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;

        }

        public IEnumerable<Elemento> ObtenerTodos()
        {
            using (SqlCommand command = new SqlCommand("prc_elementos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Elemento elemento = Cargar(dr);
                        yield return elemento;
                    }
                }
                command.Connection.Close();
            }
        }
        public IEnumerable<Elemento> ObtenerTodos(int paginaID)
        {
            using (SqlCommand command = new SqlCommand("prc_elementos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@pagina_id", SqlDbType.Int).Value = paginaID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Elemento elemento = Cargar(dr);
                        yield return elemento;
                    }
                }
                command.Connection.Close();
            }
        }
        public IEnumerable<Elemento> ObtenerElementosPagina(int? paginaID = null, int? rolID = null, int? elementoID = null, bool? bloqueado = null, bool? invisible = null)
        {
            using (SqlCommand command = new SqlCommand("prc_elementos_pagina_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (paginaID.HasValue)
                    command.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = paginaID;
                if (rolID.HasValue)
                    command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rolID;
                if (elementoID.HasValue)
                    command.Parameters.Add("@elemento_ID", SqlDbType.Int).Value = elementoID;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@elemento_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (invisible.HasValue)
                    command.Parameters.Add("@elemento_invisible", SqlDbType.Bit).Value = invisible;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Elemento elemento = CargarElementosPagina(dr);
                        yield return elemento;
                    }
                }
                command.Connection.Close();
            }
        }
        
        public Elemento Obtener(int id)
        {
            var elemento = new Elemento();
            elemento.Id = id;
            using (SqlCommand command = new SqlCommand("prc_elementos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@elemento_id", SqlDbType.VarChar, 10).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        elemento = Cargar(dr);
                }
                command.Connection.Close();
            }
            return elemento;
        }

        private Elemento Cargar(SqlDataReader dr)
        {
            Elemento elemento = new Elemento();
            elemento.Id = (int)dr["elemento_ID"];
            elemento.HtmlID = dr["elemento_htmlID"].ToString();
            elemento.Nombre = dr["elemento_nombre"].ToString();
            //elemento.Bloqueado = dr.IsDBNull(3) ? null : (bool?)dr["elemento_bloqueado"];
            //elemento.Invisible = dr.IsDBNull(4) ? null : (bool?)dr["elemento_invisible"];
            return elemento;
        }

        private Elemento CargarElementosPagina(SqlDataReader dr)
        {
            var elemento = new Elemento();
            elemento.Nombre = dr.IsDBNull(0) ? null : dr["elemento_nombre"].ToString();
            elemento.Id = (int)dr["elemento_ID"];
            elemento.HtmlID = dr.GetString(3);
            elemento.Pagina = new PaginasContext().Obtener((int)dr["pagina_ID"]);
            elemento.Pagina.Rol = new Auth.RolesContext().Obtener((int)dr["rol_ID"]);
            elemento.Bloqueado = dr.IsDBNull(4) ? null : (bool?)dr["elemento_bloqueado"];
            elemento.Invisible = dr.IsDBNull(5) ? null : (bool?)dr["elemento_invisible"];
            return elemento;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
