﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Navegacion;

namespace AccesoDatos.Contextos.Navegacion
{
    public class MenusItemsContext
    {
        private string connectionString = string.Empty;

        public MenusItemsContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public MenusItemsContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }

        public IEnumerable<MenuItem> ObtenerTodos(int? menuId = null, int? rolId = null)
        {
            using (SqlCommand command = new SqlCommand("prc_menusItems_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (menuId.HasValue)
                    command.Parameters.Add("@menu_ID", SqlDbType.Int).Value = menuId;
                if (rolId.HasValue)
                    command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rolId;
                command.Connection = new SqlConnection(connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        MenuItem menuItem = Cargar(dr);
                        yield return menuItem;
                    }
                }
                command.Connection.Close();
            }
        }

        public IEnumerable<MenuItem> ObteneTodos()
        {
            List<MenuItem> menusItems = new List<MenuItem>();
            using (SqlCommand command = new SqlCommand("prc_menusItems_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        MenuItem menuItem = Cargar(dr);
                        yield return menuItem;
                    }
                }
                command.Connection.Close();
            }
        }
        public IEnumerable<MenuItem> ObtenerTodos(int menuId, int? rolId = null)
        {
            using (SqlCommand command = new SqlCommand("prc_menusItems_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@menu_ID", SqlDbType.Int).Value = menuId;
                command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rolId;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        MenuItem menuItem = Cargar(dr);
                        yield return menuItem;
                    }
                }
                command.Connection.Close();
            }
        }

        public MenuItem Obtener(int id)
        {
            MenuItem menuItem = new MenuItem();
            using (SqlCommand command = new SqlCommand("prc_menusItems_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@menuItem_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        menuItem = Cargar(dr);
                }
                command.Connection.Close();
            }
            return menuItem;
        }

        public void Guardar(MenuItem item)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@menuItem_ID", SqlDbType.Int).Value = item.Id;
                command.Parameters.Add("@menu_ID", SqlDbType.Int).Value = item.Menu.Id;
                command.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = item.Pagina.Id;
                command.Parameters.Add("@menuItem_text", SqlDbType.VarChar, 255).Value = item.Text;
                command.Parameters.Add("@menuItem_posicion", SqlDbType.Int).Value = item.Posicion;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (item.Id > 0)
                    {
                        command.CommandText = "prc_menusItems_actualizar";
                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        command.CommandText = "prc_menusItems_insertar";
                        command.Parameters["@menuItem_ID"].Value = null;
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows && dr.Read())
                                item.Id = (int)dr["menuItem_ID"];
                        }
                        command.Connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Error en MenusItemsContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        public void Eliminar(MenuItem item)
        {
            using (SqlCommand command = new SqlCommand("prc_menusItems_eliminar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@menuItem_ID", SqlDbType.Int).Value = item.Id;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en MenusItemsContext.Eliminar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private MenuItem Cargar(SqlDataReader dr)
        {
            MenuItem menuItem = new MenuItem();
            menuItem.Id = (int)dr["menuItem_ID"];
            menuItem.Text = dr["menuItem_text"].ToString();
            menuItem.Posicion = (int)dr["menuItem_posicion"];
            menuItem.Pagina = new PaginasContext(this.connectionString)
                .Obtener((int)dr["pagina_ID"]);
            menuItem.Menu = new MenusContext(this.connectionString)
                .Obtener((int)dr["menu_ID"]);
            return menuItem;

        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
