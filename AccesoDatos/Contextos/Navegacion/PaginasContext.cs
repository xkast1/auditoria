﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Navegacion;

namespace AccesoDatos.Contextos.Navegacion
{
    public class PaginasContext
    {
        private string connectionString = string.Empty;

        public PaginasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public PaginasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;

        }

        public IEnumerable<Pagina> ObtenerTodos(bool? bloqueado = null)
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_paginas_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    cmd.Parameters.Add("@pagina_bloqueado", SqlDbType.Bit).Value = bloqueado;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Pagina pagina = Cargar(dr);
                        yield return pagina;
                    }
                }
                cmd.Connection.Close();
            }
        }
        public IEnumerable<Pagina> ObtenerTodos(int rolId, bool? bloqueado = null)
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("prc_paginas_seleccionar", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rolId;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@pagina_bloqueado", SqlDbType.Bit).Value = bloqueado;
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Pagina pagina = Cargar(dr);
                        yield return pagina;
                    }
                }
                command.Connection.Close();
            }
        }
        public IEnumerable<Pagina> ObteneTodos(bool? bloqueado = null)
        {
            List<Pagina> paginas = new List<Pagina>();
            using (SqlCommand command = new SqlCommand("prc_paginas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@pagina_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Pagina pagina = new Pagina();
                        pagina = Cargar(dr);
                        yield return pagina;
                    }
                }
                command.Connection.Close();
            }
        }
        public IEnumerable<Pagina> ObteneTodos(int rolId, bool? bloqueado = null)
        {
            List<Pagina> paginas = new List<Pagina>();
            using (SqlCommand command = new SqlCommand("prc_roles_paginas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rolId;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@pagina_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Pagina pagina = new Pagina();
                        pagina = Cargar(dr);
                        yield return pagina;
                    }
                }
                command.Connection.Close();
            }
        }
        public List<Pagina> ObteneTodos(string plataformaCodigo, bool? bloqueado = null)
        {
            List<Pagina> paginas = new List<Pagina>();
            using (SqlCommand command = new SqlCommand("prc_paginas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@plataforma_codigo", SqlDbType.VarChar, 10).Value = plataformaCodigo;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@pagina_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Pagina pagina = new Pagina();
                        pagina = Cargar(dr);
                        paginas.Add(pagina);
                    }
                }
                command.Connection.Close();
            }
            return paginas;
        }
        public Pagina Obtener(int id)
        {
            Pagina pagina = new Pagina();
            pagina.Id = id;
            using (SqlCommand command = new SqlCommand("prc_paginas_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        pagina = Cargar(dr);
                }
                command.Connection.Close();
            }
            return pagina;

        }

        /// <summary>
        /// Obtiene una página de acuerdo a las propiedades inicializadas.
        /// </summary>
        /// <param name="pagina"></param>
        /// <returns></returns>
        public Pagina Obtener(Pagina pagina)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_paginas_seleccionar";
                    command.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = pagina.Id > 0 ? (int?)(pagina.Id) : null;
                    command.Parameters.Add("@plataforma_codigo", SqlDbType.VarChar, 10).Value = pagina.Plataforma == null ? null : pagina.Plataforma.Codigo;
                    command.Parameters.Add("@pagina_bloqueado", SqlDbType.Bit).Value = pagina.Bloqueado;
                    command.Parameters.Add("@pagina_ruta", SqlDbType.VarChar, 255).Value = pagina.Ruta;
                    command.Parameters.Add("@pagina_area", SqlDbType.VarChar, 255).Value = pagina.Area;

                    connection.Open();

                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            return Cargar(dr);
                        }
                    }
                    command.Connection.Close();
                }
            }

            throw new Exception("No se encontró la página buscada.");
        }

        public void Guardar(Pagina pagina)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = pagina.Id;
                        command.Parameters.Add("@plataforma_codigo", SqlDbType.VarChar, 10).Value = pagina.Plataforma.Codigo;
                        command.Parameters.Add("@pagina_nombre", SqlDbType.VarChar, 50).Value = pagina.Nombre;
                        command.Parameters.Add("@pagina_ruta", SqlDbType.VarChar, 255).Value = pagina.Ruta;
                        command.Parameters.Add("@pagina_area", SqlDbType.VarChar, 255).Value = pagina.Area;
                        command.Parameters.Add("@pagina_controlador", SqlDbType.VarChar, 255).Value = pagina.Controlador;
                        command.Parameters.Add("@pagina_accion", SqlDbType.VarChar, 255).Value = pagina.Accion;
                        command.Parameters.Add("@pagina_bloqueado", SqlDbType.Bit).Value = pagina.Bloqueado;
                        command.Connection = connection;
                        command.Transaction = transaction;

                        if (pagina.Id > 0)
                        {
                            command.CommandText = "prc_paginas_actualizar";
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_elementos_eliminar", command.Connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = pagina.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }
                        }

                        else
                        {
                            command.CommandText = "prc_paginas_insertar";
                            command.Parameters["@pagina_ID"].Value = null;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    pagina.Id = (int)dr["pagina_ID"];
                            }
                        }
                        using (SqlCommand commandElemento = new SqlCommand("prc_elementos_insertar", command.Connection))
                        {
                            commandElemento.CommandType = CommandType.StoredProcedure;
                            commandElemento.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = pagina.Id;
                            commandElemento.Parameters.Add("@elemento_htmlID", SqlDbType.VarChar,50);
                            commandElemento.Parameters.Add("@elemento_nombre", SqlDbType.VarChar, 50);
                            commandElemento.Transaction = transaction;
                            foreach (Elemento elemento in pagina.Elementos)
                            {
                                commandElemento.Parameters["@elemento_htmlID"].Value = elemento.HtmlID;
                                commandElemento.Parameters["@elemento_nombre"].Value = elemento.Nombre;
                                commandElemento.ExecuteNonQuery();
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en PaginasContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }
        private Pagina Cargar(SqlDataReader dr)
        {
            Pagina Pagina = new Pagina();
            Pagina.Id = (int)dr["pagina_ID"];
            Pagina.Nombre = dr["pagina_nombre"].ToString();
            Pagina.Ruta = dr["pagina_ruta"].ToString();
            Pagina.Area = dr.IsDBNull(4) ? null : dr["pagina_area"].ToString();
            Pagina.Controlador = dr.IsDBNull(5) ? null : dr["pagina_controlador"].ToString();
            Pagina.Accion = dr.IsDBNull(6) ? null : dr["pagina_accion"].ToString();
            Pagina.Bloqueado = (bool)dr["pagina_bloqueado"];
            Pagina.Elementos = new ElementosContext(this.connectionString).ObtenerTodos(Pagina.Id);
            Pagina.Plataforma = new PlataformasContext().Obtener(dr["plataforma_codigo"].ToString());

            return Pagina;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
