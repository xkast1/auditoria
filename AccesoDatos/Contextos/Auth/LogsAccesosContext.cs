﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Auth;

namespace AccesoDatos.Contextos.Auth
{
    public class LogAccesoContext
    {
        private string connectionString = string.Empty;

        public LogAccesoContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public LogAccesoContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }

        public int GuardarIngreso(LogsAccesos logAcceso)
        {
            int id = 0;
            using (SqlCommand command = new SqlCommand("prc_logsAccesos_insertar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@logAcceso_ID", SqlDbType.Int);
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = logAcceso.UsuarioId;
                command.Parameters.Add("@usuario_nombre", SqlDbType.NVarChar, 50).Value = logAcceso.UsuarioNombre ?? DBNull.Value.ToString();
                command.Parameters.Add("@logAcceso_IP", SqlDbType.NVarChar, 50).Value = logAcceso.Ip;
                //command.Parameters.Add("@log_entrada", SqlDbType.DateTime).Value = log.Entrada;
                //cmd.Parameters.Add("@log_salida", SqlDbType.DateTime).Value = null; // log.Salida.HasValue ? log.Salida : DBNull.Value;
                command.Parameters.Add("@logAcceso_esquema", SqlDbType.NVarChar, 50).Value = logAcceso.Esquema ?? DBNull.Value.ToString();
                command.Parameters.Add("@logAcceso_mensaje", SqlDbType.Text).Value = logAcceso.Mensaje ?? DBNull.Value.ToString();
                command.Parameters.Add("@logAcceso_respuesta", SqlDbType.Text).Value = logAcceso.Respuesta ?? DBNull.Value.ToString();

                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows && dr.Read())
                            id = (int)dr["logAcceso_ID"];
                    }
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en LogsAccesos.GuardarIngreso " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }

            return id;

        }
        public void ActualizarSalida(int id)
        {
            using (SqlCommand command = new SqlCommand("prc_logsAccesos_actualizarSalida"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@logAcceso_ID", SqlDbType.Int).Value = id;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en LogsAccesos.ActualizarSalida " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        public List<LogsAccesos> Obtener(DateTime desde, DateTime hasta)
        {
            List<LogsAccesos> logsAccesos = new List<LogsAccesos>();
            using (SqlCommand command = new SqlCommand("prc_logsAccesos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@logAcceso_desde", SqlDbType.Int).Value = desde;
                command.Parameters.Add("@logAcceso_hasta", SqlDbType.Int).Value = hasta;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        LogsAccesos Log = Cargar(dr);
                        logsAccesos.Add(Log);
                    }
                }
                command.Connection.Close();
            }
            return logsAccesos;
        }

        public IEnumerable<LogsAccesos> Obtener(DateTime? desde = null, DateTime? hasta = null, int? usuarioId = null)
        {
            using (SqlCommand command = new SqlCommand("prc_logsAccesos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (usuarioId.HasValue)
                    command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuarioId;
                if (desde.HasValue)
                    command.Parameters.Add("@logAcceso_desde", SqlDbType.Int).Value = desde;
                if (hasta.HasValue)
                    command.Parameters.Add("@logAcceso_hasta", SqlDbType.Int).Value = hasta;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var Log = Cargar(dr);
                        yield return Log;
                    }
                }
                command.Connection.Close();
            }
        }

        public IEnumerable<LogsAccesos> ObtenerTodos(int? usuarioID = null, DateTime ? desde = null, DateTime ? hasta = null)
        {
            using (SqlCommand command = new SqlCommand("prc_logsAccesos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (usuarioID.HasValue)
                    command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuarioID;
                if (desde.HasValue)
                    command.Parameters.Add("@desde", SqlDbType.DateTime).Value = desde;
                if (hasta.HasValue)
                    command.Parameters.Add("@hasta", SqlDbType.DateTime).Value = hasta;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var Log = Cargar(dr);
                        yield return Log;
                    }
                }
                command.Connection.Close();
            }
        }

        public LogsAccesos Cargar(SqlDataReader dr)
        {
            LogsAccesos LogsAcceso = new LogsAccesos();

            LogsAcceso.Id = (int)dr["logAcceso_ID"];
            LogsAcceso.UsuarioId = (int)dr["usuario_ID"];
            LogsAcceso.UsuarioNombre = dr["usuario_nombre"].ToString();
            LogsAcceso.Ip = dr["logAcceso_IP"].ToString();
            LogsAcceso.Entrada = (DateTime)dr["logAcceso_entrada"];
            LogsAcceso.Salida = dr.IsDBNull(5) ? null : (DateTime?)dr["logAcceso_salida"];
            LogsAcceso.Esquema = dr["logAcceso_esquema"].ToString();
            LogsAcceso.Mensaje = dr["logAcceso_mensaje"].ToString();
            LogsAcceso.Respuesta = dr["logAcceso_respuesta"].ToString();

            return LogsAcceso;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
