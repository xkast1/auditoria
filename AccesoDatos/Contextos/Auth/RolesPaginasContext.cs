﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Auth;
using Browne.Core.Modelo.Navegacion;

namespace AccesoDatos.Contextos.Auth
{
    public class RolesPaginasContext
    {
        private readonly string connectionString;

        public RolesPaginasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public bool ExisteAutorizacion(Rol rol, Pagina pagina)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_roles_paginas_seleccionar";
                    command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rol.Id;
                    command.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = pagina.Id;
                    connection.Open();

                    using (var dr = command.ExecuteReader())
                    {
                        return dr.HasRows;
                    }
                    connection.Close();
                }
               
            }
        }

        public IEnumerable<Rol> ObtenerRoles(Pagina pagina)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Pagina> ObtenerPaginas(Rol rol)
        {
            throw new NotImplementedException();
        }

        private RolPagina Cargar(SqlDataReader dr)
        {
            return new RolPagina
            {
                Rol = new Rol { Id = dr.GetInt32(0) },
                Pagina = new Pagina { Id = dr.GetInt32(1) }
            };
        }
    }
}
