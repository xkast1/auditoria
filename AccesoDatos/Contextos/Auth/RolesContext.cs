﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Auth;
using AccesoDatos.Contextos.Navegacion;
using Browne.Core.Modelo.Navegacion;

namespace AccesoDatos.Contextos.Auth
{
    public class RolesContext
    {
        private string connectionString = string.Empty;

        public RolesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public RolesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }
        public IEnumerable<Rol> ObtenerTodos(bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_roles_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@rol_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Rol rol = new Rol();
                        rol = Cargar(dr);
                        yield return rol;
                    }
                }
                command.Connection.Close();
            }

        }

        public List<Rol> ObteneTodos(int usuarioId, bool? bloqueado = null)
        {
            List<Rol> roles = new List<Rol>();
            using (SqlCommand command = new SqlCommand("prc_usuarios_roles_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuarioId;
                command.Parameters.Add("@rol_bloqueado", SqlDbType.Bit).Value = bloqueado ?? System.Data.SqlTypes.SqlBoolean.Null;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Rol rol = new Rol();
                        rol = Cargar(dr);
                        roles.Add(rol);
                    }
                }
                command.Connection.Close();
            }
            return roles;
        }

        public IEnumerable<Rol> ObtenerTodosPorUsuario(int usuarioId, bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_usuarios_roles_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuarioId;
                    command.Parameters.Add("@rol_bloqueado", SqlDbType.Bit).Value = bloqueado ?? System.Data.SqlTypes.SqlBoolean.Null;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Rol rol = new Rol();
                        rol = Cargar(dr);
                        yield return rol;
                    }
                }
                command.Connection.Close();
            }

        }

        public Rol Obtener(int id)
        {
            Rol rol = new Rol();
            using (SqlCommand command = new SqlCommand("prc_roles_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        rol = Cargar(dr);
                }
                command.Connection.Close();
            }
            return rol;
        }
        public Rol Guardar(Rol rol)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rol.Id;
                        command.Parameters.Add("@rol_nombre", SqlDbType.VarChar, 50).Value = rol.Nombre;
                        command.Parameters.Add("@rol_bloqueado", SqlDbType.Bit).Value = rol.Bloqueado;
                        command.Parameters.Add("@usuario", SqlDbType.VarChar, 50);
                        command.Parameters.Add("@IP", SqlDbType.VarChar, 50);
                        command.Connection = connection;
                        command.Transaction = transaction;

                        if (rol.Id > 0)
                        {
                            command.CommandText = "prc_roles_actualizar";
                            command.Parameters["@usuario"].Value = rol.ActualizacionUsuario;
                            command.Parameters["@IP"].Value = rol.ActualizacionIp;
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminarElemento = new SqlCommand("prc_roles_paginas_elementos_eliminar", connection))
                            {
                                commandEliminarElemento.CommandType = CommandType.StoredProcedure;
                                commandEliminarElemento.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rol.Id;
                                commandEliminarElemento.Transaction = transaction;
                                commandEliminarElemento.ExecuteNonQuery();
                            }

                            using (SqlCommand commandEliminarPagina = new SqlCommand("prc_roles_paginas_eliminar", connection))
                            {
                                commandEliminarPagina.CommandType = CommandType.StoredProcedure;
                                commandEliminarPagina.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rol.Id;
                                commandEliminarPagina.Transaction = transaction;
                                commandEliminarPagina.ExecuteNonQuery();
                            }

                        }
                        else
                        {
                            command.CommandText = "prc_roles_insertar";
                            command.Parameters["@rol_ID"].Value = null;
                            command.Parameters["@pagina_ID"].Value = rol.PaginaInicio;
                            command.Parameters["@usuario"].Value = rol.CreacionUsuario;
                            command.Parameters["@IP"].Value = rol.CreacionIp;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    rol.Id = (int)dr["rol_ID"];
                            }
                        }
                        using (SqlCommand commandPagina = new SqlCommand("prc_roles_paginas_insertar", connection))
                        {
                            commandPagina.CommandType = CommandType.StoredProcedure;
                            commandPagina.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rol.Id;
                            commandPagina.Parameters.Add("@pagina_ID", SqlDbType.Int);
                            commandPagina.Transaction = transaction;

                            foreach (Pagina pagina in rol.Paginas)
                            {
                                commandPagina.Parameters["@pagina_ID"].Value = pagina.Id;
                                commandPagina.ExecuteNonQuery();

                                using (SqlCommand commandElemento = new SqlCommand("prc_roles_paginas_elementos_insertar", connection))
                                {
                                    commandElemento.CommandType = CommandType.StoredProcedure;
                                    commandElemento.Parameters.Add("@rol_ID", SqlDbType.Int).Value = rol.Id;
                                    commandElemento.Parameters.Add("@pagina_ID", SqlDbType.Int).Value = pagina.Id;
                                    commandElemento.Parameters.Add("@elemento_ID", SqlDbType.Int);
                                    commandElemento.Parameters.Add("@elemento_bloqueado", SqlDbType.Bit);
                                    commandElemento.Parameters.Add("@elemento_invisible", SqlDbType.Bit);
                                    commandElemento.Transaction = transaction;
                                    //inserto los elementos
                                    foreach (Elemento elemento in pagina.Elementos)
                                    {
                                        commandElemento.Parameters["@elemento_ID"].Value = elemento.Id;
                                        commandElemento.Parameters["@elemento_bloqueado"].Value = elemento.Bloqueado;
                                        commandElemento.Parameters["@elemento_invisible"].Value = elemento.Invisible;
                                        commandElemento.ExecuteNonQuery();
                                    }

                                }
                                    

                            }
                        }                      
                    }
                    transaction.Commit();
                    return rol;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en RolesContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }
        private Rol Cargar(SqlDataReader dr)
        {
            Rol rol = new Rol();
            rol.Id = (int)dr["rol_ID"];
            rol.Nombre = dr["rol_nombre"].ToString();
            rol.Bloqueado = (bool)dr["rol_bloqueado"];
            rol.CreacionUsuario = dr["creacion_usuario"].ToString();
            rol.CreacionFecha = (DateTime)dr["creacion_fecha"];
            rol.CreacionIp = dr["creacion_IP"].ToString();
            rol.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            rol.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            rol.ActualizacionIp = dr["actualizacion_IP"].ToString();
            rol.PaginaInicio = new PaginasContext(this.connectionString)
                .Obtener((int)dr["pagina_ID"]);
            rol.Paginas = new PaginasContext(this.connectionString)
                .ObteneTodos(rol.Id);
            ElementosContext elementoContext = new ElementosContext(this.connectionString);
            foreach (Pagina pagina in rol.Paginas) {
                pagina.Elementos = elementoContext.ObtenerTodos(pagina.Id);
            }
            return rol;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
