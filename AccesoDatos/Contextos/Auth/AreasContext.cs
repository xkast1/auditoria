﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Browne.Core.Modelo.Auth;

namespace AccesoDatos.Contextos.Auth
{
    public class AreasContext
    {
        private string connectionString = string.Empty;

        public AreasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public AreasContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }
        public IEnumerable<Area> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_areas_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Area Area = Cargar(dr);
                        yield return Area;
                    }
                }
                cmd.Connection.Close();
            }
        }
        public Area Obtener(int id)
        {
            Area Area = new Area();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_areas_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@area_ID", SqlDbType.Int).Value = id;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                        Area = Cargar(dr);
                }
                cmd.Connection.Close();
            }
            return Area;
        }

        public void Guardar(Area Area)
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@area_ID", SqlDbType.Int).Value = Area.Id;
                command.Parameters.Add("@area_nombre", SqlDbType.VarChar, 50).Value = Area.Nombre;
                command.Parameters.Add("@area_responsable", SqlDbType.VarChar, 50).Value = Area.Responsable;
                command.Parameters.Add("@area_correo", SqlDbType.VarChar, 255).Value = Area.Correo;
                command.Connection = conn;

                if (Area.Id > 0)
                    command.CommandText = "prc_areas_actualizar";
                else
                {
                    command.CommandText = "prc_areas_insertar";
                    command.Parameters["@area_ID"].Value = null;
                }
                command.ExecuteNonQuery();
                command.Connection.Close();
            }
        }
        private Area Cargar(SqlDataReader dr)
        {
            Area area = new Area();
            area.Id = (int)dr["area_ID"];
            area.Nombre = dr["area_nombre"].ToString();
            area.Responsable = dr["area_responsable"].ToString();
            area.Correo = dr["area_correo"].ToString();
            return area;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
