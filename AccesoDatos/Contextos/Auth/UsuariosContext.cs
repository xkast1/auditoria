﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Linq;
using Browne.Core.Modelo.Auth;

namespace AccesoDatos.Contextos.Auth
{
    public class UsuariosContext
    {
        private string connectionString = string.Empty;

        public UsuariosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public UsuariosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }

        public Usuario Obtener(string identificador)
        {
            Usuario usuario = new Usuario();
            using (SqlCommand command = new SqlCommand("prc_usuarios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_identificador", SqlDbType.VarChar, 50).Value = identificador;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        usuario = Cargar(dr);
                    else
                        throw new System.Exception("Usuario Inválido, Usuario/Contraseña incorrectos");
                }
                command.Connection.Close();
            }
            return usuario;
        }

        public Usuario Obtener(int id)
        {
            Usuario usuario = new Usuario();
            using (SqlCommand command = new SqlCommand("prc_usuarios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        usuario = Cargar(dr);
                    else
                        throw new System.Exception("Usuario Inválido, Usuario/Contraseña incorrectos");
                }
                command.Connection.Close();
            }
            return usuario;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public Usuario ObtenerPorToken(string token)
        {
            var usuario = new Usuario();

            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_usuarios_seleccionar";
                    command.Parameters.Add("@usuario_tokenCookie", SqlDbType.VarChar, 32).Value = token;
                    command.Connection.Open();

                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.HasRows && dr.Read())
                        {
                            return Cargar(dr);
                        }
                    }
                    command.Connection.Close();
                }
            }

            throw new System.Exception("Usuario Inválido, Usuario/Contraseña incorrectos");
        }

        public IEnumerable<Usuario> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_usuarios_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Usuario usuario = Cargar(dr);
                        yield return usuario;
                    }
                }
                cmd.Connection.Close();
            }
        }


        public IEnumerable<Usuario> ObteneTodos(int? sucursalID = null, int? grupoTrabajoID = null)
        {
            using (SqlCommand command = new SqlCommand(grupoTrabajoID.HasValue ? "prc_gruposTrabajo_usuarios_seleccionar" : "prc_usuarios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (sucursalID.HasValue)
                    command.Parameters.Add("@sucursal_ID", SqlDbType.Int).Value = sucursalID;
                if (grupoTrabajoID.HasValue)
                    command.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = grupoTrabajoID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Usuario usuario = Cargar(dr);
                        yield return usuario;
                    }
                }
                command.Connection.Close();
            }
        }

        public Usuario VerificarAcceso(string identificador, string contrasena, string ip)
        {
            Usuario usuario = new Usuario();
            using (SqlCommand command = new SqlCommand("prc_usuarios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_identificador", SqlDbType.VarChar, 50).Value = identificador;
                command.Parameters.Add("@usuario_contrasenaDigerida", SqlDbType.VarChar, 25).Value = contrasena;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                    {
                        usuario = Cargar(dr);
                        if ((bool)usuario.Bloqueado)
                            throw new Exception("Usuario Bloqueado");
                        else
                        {
                            ActualizarAcceso(usuario.Id);
                            LogsAccesos Log = new LogsAccesos();
                            Log.UsuarioId = usuario.Id;
                            Log.UsuarioNombre = usuario.Nombre;
                            Log.Ip = ip;
                            usuario.LogId = new LogAccesoContext(this.connectionString)
                                .GuardarIngreso(Log);
                        }
                    }
                    else
                        throw new Exception("Usuario Inválido, Usuario/Contraseña incorrectos");
                }
                command.Connection.Close();
            }
            return usuario;
        }



        public void Guardar(Usuario usuario)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuario.Id;
                        command.Parameters.Add("@tipoUsuario_ID", SqlDbType.VarChar, 10).Value = usuario.TipoUsuario.Id;
                        command.Parameters.Add("@sucursal_ID", SqlDbType.Int).Value = usuario.Sucursal.Id;
                        command.Parameters.Add("@area_ID", SqlDbType.Int).Value = usuario.Area.Id;
                        command.Parameters.Add("@usuario_rut", SqlDbType.VarChar, 11).Value = usuario.Rut;
                        command.Parameters.Add("@usuario_nombre", SqlDbType.VarChar, 50).Value = usuario.Nombre;
                        command.Parameters.Add("@usuario_cargo", SqlDbType.VarChar, 50).Value = usuario.Cargo;
                        command.Parameters.Add("@usuario_telefono", SqlDbType.VarChar, 20).Value = usuario.Telefono;
                        command.Parameters.Add("@usuario_celular", SqlDbType.VarChar, 20).Value = usuario.Celular;
                        command.Parameters.Add("@usuario_email", SqlDbType.VarChar, 100).Value = usuario.Email;
                        command.Parameters.Add("@usuario_identificador", SqlDbType.VarChar, 50).Value = usuario.Identificador;
                        command.Parameters.Add("@usuario_contrasenaAnterior", SqlDbType.VarChar, 50).Value = usuario.ContrasenaDigerida;
                        command.Parameters.Add("@usuario_contrasenaDigerida", SqlDbType.VarChar, 25).Value = usuario.Rut;
                        command.Parameters.Add("@usuario_bloqueado", SqlDbType.Bit).Value = usuario.Bloqueado;
                        command.Parameters.Add("@usuario_avatar", SqlDbType.VarChar).Value = usuario.Avatar;
                        command.Parameters.Add("@rutFuncion", SqlDbType.VarChar, 11).Value = usuario.RutFuncion;
                        command.Parameters.Add("@usuario", SqlDbType.VarChar, 50);
                        command.Parameters.Add("@IP", SqlDbType.VarChar, 50);
                        command.Connection = connection;
                        command.Transaction = transaction;

                        if (usuario.Id > 0)
                        {
                            command.CommandText = "prc_usuarios_actualizar";
                            command.Parameters["@usuario"].Value = usuario.ActualizacionUsuario;
                            command.Parameters["@usuario_contrasenaAnterior"].Value = usuario.ContrasenaAnterior;
                            command.Parameters["@IP"].Value = usuario.ActualizacionIp;
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_usuarios_roles_eliminar", command.Connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuario.Id;
                                commandEliminar.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            command.CommandText = "prc_usuarios_insertar";
                            command.Parameters["@usuario_ID"].Value = null;
                            command.Parameters.Add("@usuario_ultimoAcceso", SqlDbType.DateTime).Value = DateTime.Now;
                            command.Parameters["@usuario"].Value = usuario.CreacionUsuario;
                            command.Parameters["@IP"].Value = usuario.CreacionIp;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    usuario.Id = (int)dr["usuario_ID"];
                            }
                        }
                        using (SqlCommand commandPagina = new SqlCommand("prc_usuarios_roles_insertar", command.Connection))
                        {
                            commandPagina.CommandType = CommandType.StoredProcedure;
                            commandPagina.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuario.Id;
                            commandPagina.Parameters.Add("@rol_ID", SqlDbType.Int).Value = usuario.RolActual.Id;
                            commandPagina.Transaction = transaction;
                            commandPagina.ExecuteNonQuery();

                            //foreach (Rol rol in usuario.Roles)
                            //{
                            //    commandPagina.Parameters["@viaTransporte_ID"].Value = rol.Id;
                            //    commandPagina.ExecuteNonQuery();
                            //}
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TiposUsuariosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }
        private void ActualizarAcceso(int id)
        {
            using (SqlCommand command = new SqlCommand("prc_usuarios_actualizarAcceso"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = id;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en UsuarioContext.ActualizarAcceso " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private void Bloquear(Usuario usuario)
        {
            using (SqlCommand command = new SqlCommand("prc_usuarios_bloquear"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuario.Id;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en UsuarioContext.Bloquear " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private void Bloquear(string identificador)
        {
            using (SqlCommand command = new SqlCommand("prc_usuarios_bloquear"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_identificador", SqlDbType.VarChar, 50).Value = identificador;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en UsuarioContext.Bloquear " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private void Bloquear(int id)
        {
            using (SqlCommand command = new SqlCommand("prc_usuarios_bloquear"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = id;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en UsuarioContext.Bloquear " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        public void ActualizarTokenCookie(Usuario usuario)
        {
            using (SqlCommand command = new SqlCommand("prc_usuarios_actualizarToken"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuario.Id;
                command.Parameters.Add("@usuario_tokenCookie", SqlDbType.VarChar, 32).Value = usuario.TokenCookie;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en UsuarioContext.ActualizarTokenCookie " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        public void ActualizarContraseña(Usuario usuario)
        {
            using (SqlCommand command = new SqlCommand("prc_usuarios_actualizarContrasena"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuario.Id;
                command.Parameters.Add("@usuario_contrasenaDigerida", SqlDbType.VarChar, 32).Value = usuario.ContrasenaDigerida;
                //command.Parameters.Add("@contrasenaActual", SqlDbType.VarChar, 32).Value = usuario.ContrasenaAnterior;
                //command.Parameters.Add("@usuario", SqlDbType.VarChar, 32).Value = usuario.Nombre;
                //command.Parameters.Add("@IP", SqlDbType.VarChar, 32).Value = usuario.ActualizacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en UsuarioContext.ActualizarContraseña " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        public void SubirAvatar(Usuario usuario)
        {
            using (SqlCommand command = new SqlCommand("prc_usuarios_subirFotoPerfil"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@usuario_ID", SqlDbType.Int).Value = usuario.Id;
                command.Parameters.Add("@usuario_avatar", SqlDbType.VarChar, 32).Value = usuario.Avatar;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en UsuarioContext.SubirFotoPerfil " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        private Usuario Cargar(SqlDataReader dr)
        {
            Usuario usuario = new Usuario();
            usuario.Id = (int)dr["usuario_ID"];
            usuario.Rut = dr["usuario_rut"].ToString();
            usuario.Nombre = dr["usuario_nombre"].ToString();
            usuario.Cargo = dr["usuario_cargo"].ToString();
            usuario.Telefono = dr["usuario_telefono"].ToString();
            usuario.Celular = dr["usuario_celular"].ToString();
            usuario.Email = dr["usuario_email"].ToString();
            usuario.Identificador = dr["usuario_identificador"].ToString();
            usuario.ContrasenaDigerida = dr["usuario_contrasenaDigerida"].ToString();
            usuario.ContrasenaAnterior = dr["usuario_contrasenaAnterior"].ToString();
            usuario.VencimientoContrasena = dr.IsDBNull(12) ? null : (DateTime?)(dr["usuario_vencimientoContrasena"]);
            usuario.AccesoRemoto = (Boolean)dr["usuario_accesoRemoto"];
            usuario.Bloqueado = (Boolean)dr["usuario_bloqueado"];
            if ((bool)usuario.Bloqueado)
                usuario.Autorizado = false;
            else
                usuario.Autorizado = true;
            usuario.TokenCookie = dr["usuario_tokenCookie"].ToString();
            //usuario.Avatar = dr["usuario_avatar"] != DBNull.Value ? (byte[])dr["usuario_avatar"] : null;
            usuario.UltimoAcceso = dr.IsDBNull(18) ? null : (DateTime?)(dr["usuario_ultimoAcceso"]);
            usuario.RutFuncion = dr["rutFuncion"].ToString();
            usuario.CreacionUsuario = dr["creacion_usuario"].ToString();
            usuario.CreacionFecha = (DateTime)dr["creacion_fecha"];
            usuario.CreacionIp = dr["creacion_IP"].ToString();
            usuario.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            usuario.ActualizacionIp = dr["actualizacion_IP"].ToString();
            usuario.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            usuario.TipoUsuario = new TiposUsuariosContext(this.connectionString)
                .Obtener((int)dr["tipoUsuario_ID"]);
            usuario.Sucursal = new Transversales.SucursalesContext(this.connectionString)
                .Obtener((int)dr["sucursal_ID"]);
            usuario.Area = new AreasContext(this.connectionString)
                .Obtener((int)dr["area_ID"]);
            usuario.Roles = new RolesContext(this.connectionString)
                .ObtenerTodosPorUsuario((int)dr["usuario_ID"]);
            usuario.RolActual = (from aux in usuario.Roles select aux).First();
            return usuario;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}