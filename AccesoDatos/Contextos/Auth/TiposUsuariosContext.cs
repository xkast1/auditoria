﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System;
using Browne.Core.Modelo.Auth;

namespace AccesoDatos.Contextos.Auth
{
    public class TiposUsuariosContext
    {
        private string connectionString = string.Empty;

        public TiposUsuariosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public TiposUsuariosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }

        public IEnumerable<TipoUsuario> obtenerTodos()
        {
            using (SqlCommand command = new SqlCommand("prc_tiposUsuarios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoUsuario tipoUsuario = Cargar(dr);
                        yield return tipoUsuario;
                    }
                }
                command.Connection.Close();
            }
        }

        public TipoUsuario Obtener(int id)
        {
            TipoUsuario tipoUsuario = new TipoUsuario();
            using (SqlCommand command = new SqlCommand("prc_tiposUsuarios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoUsuario_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipoUsuario = Cargar(dr);
                }
                command.Connection.Close();
            }
            return tipoUsuario;
        }
        public void Guardar(TipoUsuario tipoUsuario)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoUsuario_ID", SqlDbType.Int).Value = tipoUsuario.Id;
                command.Parameters.Add("@tipoUsuario_nombre", SqlDbType.NVarChar, 50).Value = tipoUsuario.Nombre;
                command.Parameters.Add("@tipoUsuario_supervisor", SqlDbType.Bit).Value = tipoUsuario.Supervisor;
                command.Parameters.Add("@tipoUsuario_cliente", SqlDbType.Bit).Value = tipoUsuario.Cliente;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (tipoUsuario.Id > 0)
                    {
                        command.CommandText = "prc_tiposUsuarios_actualizar";
                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        command.CommandText = "prc_tiposUsuarios_insertar";
                        command.Parameters["@tipoUsuario_ID"].Value = null;
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows && dr.Read())
                                tipoUsuario.Id = (int)dr["tipoUsuario_ID"];
                        }
                        command.Connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TiposUsuariosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        private TipoUsuario Cargar(SqlDataReader dr)
        {
            TipoUsuario tipoUsuario = new TipoUsuario();
            tipoUsuario.Id = (int)dr["tipoUsuario_ID"];
            tipoUsuario.Nombre = dr["tipoUsuario_nombre"].ToString();
            tipoUsuario.Supervisor = (bool)dr["tipoUsuario_supervisor"];
            tipoUsuario.Cliente = (bool)dr["tipoUsuario_cliente"];
            return tipoUsuario;

        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
