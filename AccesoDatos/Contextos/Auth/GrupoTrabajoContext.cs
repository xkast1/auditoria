﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Auth;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Consumidor;

namespace AccesoDatos.Contextos.Auth
{
    public class GrupoTrabajoContext
    {
        private string connectionString = string.Empty;

        public GrupoTrabajoContext(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public GrupoTrabajoContext()
        {
            this.connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["browne_usuarios"].ConnectionString;
        }
        public IEnumerable<GrupoTrabajo> ObtenerTodos()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_gruposTrabajos_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        GrupoTrabajo grupo = Cargar(dr);
                        yield return grupo;
                    }
                }
                cmd.Connection.Close();
            }
        }
        public GrupoTrabajo Obtener(int id)
        {
            var Grupo = new GrupoTrabajo();
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("prc_gruposTrabajos_seleccionar", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = id;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                        Grupo = Cargar(dr);
                }
                cmd.Connection.Close();
            }
            return Grupo;
        }

        public void Guardar(GrupoTrabajo Grupo)
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                conn.Open();
                SqlTransaction transaction = conn.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = Grupo.Id;
                        command.Parameters.Add("@area_ID", SqlDbType.VarChar, 50).Value = Grupo.Area.Id;
                        command.Parameters.Add("@grupoTrabajo_nombre", SqlDbType.VarChar, 50).Value = Grupo.Nombre;
                        command.Parameters.Add("@grupoTrabajo_correoDistribucion", SqlDbType.VarChar, 255).Value = Grupo.CorreoDistribucion;
                        command.Connection = conn;
                        command.Transaction = transaction;

                        if (Grupo.Id > 0)
                        {
                            command.CommandText = "prc_gruposTrabajos_actualizar";
                            command.Parameters.Add("@usuario", SqlDbType.VarChar, 255).Value = Grupo.ActualizacionUsuario;
                            command.Parameters.Add("@IP", SqlDbType.VarChar, 255).Value = Grupo.ActualizacionIP;
                            command.ExecuteNonQuery();

                            using (SqlCommand commandEliminar = new SqlCommand("prc_gruposTrabajo_aduanas_eliminar", conn))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = Grupo.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }
                            using (SqlCommand commandEliminar = new SqlCommand("prc_gruposTrabajo_clientes_eliminar", conn))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = Grupo.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }
                            using (SqlCommand commandEliminar = new SqlCommand("prc_gruposTrabajo_usuarios_eliminar", conn))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = Grupo.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            command.CommandText = "prc_gruposTrabajos_insertar";
                            command.Parameters["@grupoTrabajo_ID"].Value = null;
                            command.Parameters["@grupoTrabajo_correoDistribucion"].Value = Grupo.CorreoDistribucion;
                            command.Parameters.Add("@usuario", SqlDbType.VarChar, 255).Value = Grupo.CreacionUsuario;
                            command.Parameters.Add("@IP", SqlDbType.VarChar, 255).Value = Grupo.CreacionIP;
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    Grupo.Id = (int)dr["grupoTrabajo_ID"];
                            }
                        }
                        using (SqlCommand commandGruposAduana = new SqlCommand("prc_gruposTrabajo_aduanas_insertar", conn))
                        {
                            commandGruposAduana.CommandType = CommandType.StoredProcedure;
                            commandGruposAduana.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = Grupo.Id;
                            commandGruposAduana.Parameters.Add("@aduana_ID", SqlDbType.Int);
                            commandGruposAduana.Transaction = transaction;

                            foreach (Aduana aduana in Grupo.Aduanas)
                            {
                                commandGruposAduana.Parameters["@aduana_ID"].Value = aduana.Id;
                                commandGruposAduana.ExecuteNonQuery();
                            }
                        }
                        using (SqlCommand commandGruposAduana = new SqlCommand("prc_gruposTrabajo_clientes_insertar", conn))
                        {
                            commandGruposAduana.CommandType = CommandType.StoredProcedure;
                            commandGruposAduana.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = Grupo.Id;
                            commandGruposAduana.Parameters.Add("@cliente_ID", SqlDbType.Int);
                            commandGruposAduana.Parameters.Add("@cliente_rut", SqlDbType.VarChar, 25);
                            commandGruposAduana.Parameters.Add("@cliente_sucursal", SqlDbType.Int);
                            commandGruposAduana.Transaction = transaction;

                            foreach (Cliente cliente in Grupo.Clientes)
                            {
                                commandGruposAduana.Parameters["@cliente_ID"].Value = cliente.Id;
                                commandGruposAduana.Parameters["@cliente_rut"].Value = cliente.Rut;
                                commandGruposAduana.Parameters["@cliente_sucursal"].Value = cliente.Sucursal;
                                commandGruposAduana.ExecuteNonQuery();
                            }
                        }
                        using (SqlCommand commandGruposAduana = new SqlCommand("prc_gruposTrabajo_usuarios_insertar", conn))
                        {
                            commandGruposAduana.CommandType = CommandType.StoredProcedure;
                            commandGruposAduana.Parameters.Add("@grupoTrabajo_ID", SqlDbType.Int).Value = Grupo.Id;
                            commandGruposAduana.Parameters.Add("@usuario_ID", SqlDbType.Int);
                            commandGruposAduana.Transaction = transaction;

                            foreach (Usuario usuario in Grupo.Usuarios)
                            {
                                commandGruposAduana.Parameters["@usuario_ID"].Value = usuario.Id;
                                commandGruposAduana.ExecuteNonQuery();
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en GruposTrabajosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    conn.Close();
                }
            }
        }
        private GrupoTrabajo Cargar(SqlDataReader dr)
        {
            var Grupo = new GrupoTrabajo();
            Grupo.Id = (int)dr["grupoTrabajo_ID"];
            Grupo.Nombre = dr["grupoTrabajo_nombre"].ToString();
            Grupo.CorreoDistribucion = dr["grupoTrabajo_correoDistribucion"].ToString();
            Grupo.CreacionUsuario = dr["creacion_usuario"].ToString();
            Grupo.CreacionFecha = (DateTime)dr["creacion_fecha"];
            Grupo.CreacionIP = dr["creacion_IP"].ToString();
            Grupo.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            Grupo.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            Grupo.ActualizacionIP = dr["actualizacion_IP"].ToString();
            Grupo.Area = new AreasContext(this.connectionString).Obtener((int)dr["area_ID"]);
            Grupo.Aduanas = new Transversales.AduanasContext(this.connectionString).ObtenerTodos(null, (int)dr["grupoTrabajo_ID"], false);
            Grupo.Usuarios = new UsuariosContext(this.connectionString).ObteneTodos(null, (int)dr["grupoTrabajo_ID"]);
            Grupo.Clientes = new Transversales.Consumidores.ClientesContext(this.connectionString).ObtenerTodos((int)dr["grupoTrabajo_ID"]);
            return Grupo;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}
