﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace AccesoDatos.Contextos.Contabilidad.Mantenedores
{
    public class TiposOrdenesComprasContext
    {
        private string connectionString = string.Empty;

        public TiposOrdenesComprasContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public TiposOrdenesComprasContext()
        {
            this.connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["browne_contables"].ConnectionString;
        }


        public IEnumerable<TipoOrdenCompra> ObtenerTodos(int? ID = null, bool? bloqueado = null)
        {
            using (SqlCommand command = new SqlCommand("prc_tiposOrdenesCompras_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@tipoOrdenCompra_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (ID.HasValue)
                    command.Parameters.Add("@tipoOrdenCompra_ID", SqlDbType.Int).Value = ID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoOrdenCompra tipo = Cargar(dr);
                        yield return tipo;
                    }
                }
                command.Connection.Close();
            }
        }

        public TipoOrdenCompra Obtener(int id)
        {
            TipoOrdenCompra tipo = new TipoOrdenCompra();
            tipo.Id = id;
            using (SqlCommand command = new SqlCommand("prc_tiposOrdenesCompras_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoOrden_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        tipo = Cargar(dr);
                }
                command.Connection.Close();
            }
            return tipo;

        }

        public void Guardar(TipoOrdenCompra tipo)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoOrdenCompra_ID", SqlDbType.Int).Value = tipo.Id;
                command.Parameters.Add("@tipoOrdenCompra_nombre", SqlDbType.VarChar, 255).Value = tipo.Nombre;
                command.Parameters.Add("@tipoOrdenCompra_bloqueado", SqlDbType.Bit).Value = tipo.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = tipo.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = tipo.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (tipo.Id > 0)
                    {
                        command.CommandText = "prc_tiposOrdenesCompras_actualizar";
                        command.Parameters["@usuario"].Value = tipo.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = tipo.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_tiposOrdenesCompras_insertar";
                        command.Parameters["@tipoOrdenCompra_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TiposOrdenesComprasContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        private TipoOrdenCompra Cargar(SqlDataReader dr)
        {
            TipoOrdenCompra tipo = new TipoOrdenCompra();
            tipo.Id = (int)dr["tipoOrdenCompra_ID"];
            tipo.Nombre = dr["tipoOrdenCompra_nombre"].ToString();
            tipo.Bloqueado = (bool)dr["tipoOrdenCompra_bloqueado"];
            tipo.CreacionUsuario = dr["creacion_usuario"].ToString();
            tipo.CreacionFecha = (DateTime)dr["creacion_fecha"];
            tipo.CreacionIp = dr["creacion_IP"].ToString();
            tipo.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            tipo.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            tipo.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return tipo;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}