﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace AccesoDatos.Contextos.Contabilidad.Mantenedores
{
    public class TipoConceptoDesembolsoContext
    {
        private string connectionString = string.Empty;

        public TipoConceptoDesembolsoContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public TipoConceptoDesembolsoContext()
        {
            this.connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["browne_contables"].ConnectionString;
        }


        public IEnumerable<TipoConceptoDesembolso> ObtenerTodos(bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_tiposConceptosDesembolsos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@tipoConceptoDesembolso_bloqueado", SqlDbType.Bit).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        TipoConceptoDesembolso concepto = Cargar(dr);
                        yield return concepto;
                    }
                }
                command.Connection.Close();
            }
        }

        public TipoConceptoDesembolso Obtener(int id)
        {
            TipoConceptoDesembolso concepto = new TipoConceptoDesembolso();
            concepto.Id = id;
            using (SqlCommand command = new SqlCommand("prc_tiposConceptosDesembolsos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoConceptoDesembolso_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        concepto = Cargar(dr);
                }
                command.Connection.Close();
            }
            return concepto;

        }

        public void Guardar(TipoConceptoDesembolso concepto)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@tipoConceptoDesembolso_ID", SqlDbType.Int).Value = concepto.Id;
                command.Parameters.Add("@tipoConceptoDesembolso_nombre", SqlDbType.VarChar, 100).Value = concepto.Nombre;
                command.Parameters.Add("@tipoConceptoDesembolso_bloqueado", SqlDbType.Bit).Value = concepto.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = concepto.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = concepto.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (concepto.Id > 0)
                    {
                        command.CommandText = "prc_tiposConceptosDesembolsos_actualizar";
                        command.Parameters["@usuario"].Value = concepto.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = concepto.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_tiposConceptosDesembolsos_insertar";
                        command.Parameters["@tipoConceptoDesembolso_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en TiposConceptosDesembolsosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        private TipoConceptoDesembolso Cargar(SqlDataReader dr)
        {
            TipoConceptoDesembolso concepto = new TipoConceptoDesembolso();
            concepto.Id = (int)dr["tipoConceptoDesembolso_ID"];
            concepto.Nombre = dr["tipoConceptoDesembolso_nombre"].ToString();
            concepto.Bloqueado = (bool)dr["tipoConceptoDesembolso_bloqueado"];
            concepto.CreacionUsuario = dr["creacion_usuario"].ToString();
            concepto.CreacionFecha = (DateTime)dr["creacion_fecha"];
            concepto.CreacionIp = dr["creacion_IP"].ToString();
            concepto.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            concepto.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            concepto.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return concepto;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}