﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace AccesoDatos.Contextos.Contabilidad.Mantenedores
{
    public class CuentasCorrientesTalonariosContext
    {
        private string connectionString = string.Empty;

        public CuentasCorrientesTalonariosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public CuentasCorrientesTalonariosContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_contables"].ConnectionString;
        }


        public IEnumerable<CuentaCorrienteTalonario> ObtenerTodos(int? cuentaCorriente = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_cuentasCorrientesTalonarios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (cuentaCorriente.HasValue)
                    command.Parameters.Add("@cuentaCorriente_ID", SqlDbType.Int).Value = cuentaCorriente;
                if (cuentaCorriente.HasValue)
                    command.Parameters.Add("@cuentaCorrienteTalonario_bloqueado", SqlDbType.Int).Value = bloqueado;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        CuentaCorrienteTalonario talonario = Cargar(dr);
                        yield return talonario;
                    }
                }
                command.Connection.Close();
            }
        }

        public CuentaCorrienteTalonario Obtener(int id)
        {
            CuentaCorrienteTalonario talonario = new CuentaCorrienteTalonario();
            talonario.Id = id;
            using (SqlCommand command = new SqlCommand("prc_cuentasCorrientesTalonarios_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("cuentaCorrienteTalonario_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        talonario = Cargar(dr);
                }
                command.Connection.Close();
            }
            return talonario;

        }

        public void Guardar(CuentaCorrienteTalonario talonario)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@cuentaCorrienteTalonario_ID", SqlDbType.Int).Value = talonario.Id;
                        command.Parameters.Add("@cuentaCorriente_ID", SqlDbType.Int).Value = talonario.CuentaCorriente.Id;
                        command.Parameters.Add("@cuentaCorrienteTalonario_numero", SqlDbType.VarChar, 255).Value = talonario.Numero;
                        command.Parameters.Add("@cuentaCorrienteTalonario_serie", SqlDbType.VarChar, 255).Value = talonario.Serie;
                        command.Parameters.Add("@cuentaCorrienteTalonario_fecha", SqlDbType.DateTime).Value = talonario.Fecha;
                        command.Parameters.Add("@cuentaCorrienteTalonario_cantidadCheques", SqlDbType.Int).Value = talonario.CantidadCheques;
                        command.Parameters.Add("@cuentaCorrienteTalonario_folioDesde", SqlDbType.Int).Value = talonario.FolioDesde;
                        command.Parameters.Add("@cuentaCorrienteTalonario_folioHasta", SqlDbType.Int).Value = talonario.FolioHasta;
                        command.Parameters.Add("@cuentaCorrienteTalonario_observacion", SqlDbType.VarChar, 255).Value = talonario.Observacion;
                        command.Parameters.Add("@cuentaCorrienteTalonario_bloqueado", SqlDbType.Bit).Value = talonario.Bloqueado;
                        command.Parameters.Add("@cuentaCorrienteTalonario_continuo", SqlDbType.Bit).Value = talonario.Continuo;
                        command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = talonario.CreacionUsuario;
                        command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = talonario.CreacionIp;
                        command.Transaction = transaction;

                        if (talonario.Id > 0)
                        {
                            command.CommandText = "prc_cuentasCorrientesTalonarios_actualizar";
                            command.Parameters["@usuario"].Value = talonario.ActualizacionUsuario;
                            command.Parameters["@IP"].Value = talonario.ActualizacionIp;
                            command.ExecuteNonQuery();

                            using (var commandEliminar = new SqlCommand("prc_cheques_eliminar", command.Connection))
                            {
                                commandEliminar.CommandType = CommandType.StoredProcedure;
                                commandEliminar.Parameters.Add("@cuentaCorrienteTalonario_ID", SqlDbType.Int).Value = talonario.Id;
                                commandEliminar.Transaction = transaction;
                                commandEliminar.ExecuteNonQuery();
                            }

                        }
                        else
                        {
                            command.CommandText = "prc_cuentasCorrientesTalonarios_insertar";
                            command.Parameters["@cuentaCorrienteTalonario_ID"].Value = null;
                            using (var dr = command.ExecuteReader())
                            {
                                if (dr.HasRows && dr.Read())
                                    talonario.Id = (int)dr["cuentaCorrienteTalonario_ID"];
                            }
                        }
                        //Creación de cheques
                        using (SqlCommand commandCheque = new SqlCommand("prc_cheques_insertar", connection))
                        {
                            commandCheque.CommandType = CommandType.StoredProcedure;
                            commandCheque.Parameters.Add("@cheque_ID", SqlDbType.Int);
                            commandCheque.Parameters.Add("@cuentaCorrienteTalonario_ID", SqlDbType.Int);
                            commandCheque.Parameters.Add("@cheque_fecha", SqlDbType.DateTime);
                            commandCheque.Parameters.Add("@cheque_monto", SqlDbType.Decimal);
                            commandCheque.Parameters.Add("@cheque_numero", SqlDbType.VarChar, 20);
                            commandCheque.Parameters.Add("@cheque_anulado", SqlDbType.Bit);
                            commandCheque.Parameters.Add("@usuario", SqlDbType.VarChar, 50);
                            commandCheque.Parameters.Add("@IP", SqlDbType.VarChar, 50);
                            commandCheque.Transaction = transaction;

                            for (var numeroCheque = 1; numeroCheque != talonario.CantidadCheques; numeroCheque++)
                            {
                                commandCheque.Parameters["@cuentaCorrienteTalonario_ID"].Value = talonario.Id;
                                commandCheque.Parameters["@cheque_fecha"].Value = DateTime.Now;
                                commandCheque.Parameters["@cheque_anulado"].Value = false;
                                commandCheque.Parameters["@cheque_numero"].Value = numeroCheque;
                                commandCheque.Parameters["@cheque_monto"].Value = 0;
                                commandCheque.Parameters["@usuario"].Value = talonario.CreacionUsuario;
                                commandCheque.Parameters["@IP"].Value = talonario.CreacionIp;
                                commandCheque.Parameters["@cheque_ID"].Value = null;
                                commandCheque.ExecuteNonQuery();
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en TalonarioChequesContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }

        private CuentaCorrienteTalonario Cargar(SqlDataReader dr)
        {
            CuentaCorrienteTalonario talonario = new CuentaCorrienteTalonario();
            talonario.Id = (int)dr["cuentaCorrienteTalonario_ID"];
            talonario.Numero = dr["cuentaCorrienteTalonario_numero"].ToString();
            talonario.Serie = dr["cuentaCorrienteTalonario_serie"].ToString();
            talonario.CantidadCheques = (int)dr["cuentaCorrienteTalonario_cantidadCheques"];
            talonario.Fecha = (DateTime)dr["cuentaCorrienteTalonario_fecha"];
            talonario.FolioDesde = dr["cuentaCorrienteTalonario_folioDesde"].ToString();
            talonario.FolioHasta = dr["cuentaCorrienteTalonario_folioHasta"].ToString();
            talonario.Observacion = dr["cuentaCorrienteTalonario_observacion"].ToString();
            talonario.Bloqueado = (bool)dr["cuentaCorrienteTalonario_bloqueado"];
            talonario.Continuo = (bool)dr["cuentaCorrienteTalonario_continuo"];
            talonario.CreacionUsuario = dr["creacion_usuario"].ToString();
            talonario.CreacionFecha = (DateTime)dr["crecaion_fecha"];
            talonario.CreacionIp = dr["creacion_IP"].ToString();
            talonario.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            talonario.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            talonario.ActualizacionIp = dr["actualizacion_IP"].ToString();
            talonario.CuentaCorriente = new CuentasCorrientesContext(this.connectionString).Obtener((int)dr["cuentaCorriente_ID"]);

            return talonario;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}