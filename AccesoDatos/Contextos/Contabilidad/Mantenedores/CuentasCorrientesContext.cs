﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.Contextos.Auth;
using AccesoDatos.Contextos.Transversales;
using Browne.Core.Modelo.Contabilidad;

namespace AccesoDatos.Contextos.Contabilidad.Mantenedores
{
    public class CuentasCorrientesContext
    {
        private string connectionString = string.Empty;

        public CuentasCorrientesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public CuentasCorrientesContext()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["browne_contables"].ConnectionString;
        }


        public IEnumerable<CuentaCorriente> ObtenerTodos(int? bancoId = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_cuentasCorrientes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@cuentaCorriente_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (bancoId.HasValue)
                    command.Parameters.Add("@banco_ID", SqlDbType.Int).Value = bancoId;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        CuentaCorriente cuenta = Cargar(dr);
                        yield return cuenta;
                    }
                }
                command.Connection.Close();
            }
        }

        public CuentaCorriente Obtener(int id)
        {
            CuentaCorriente cuenta = new CuentaCorriente();
            cuenta.Id = id;
            using (SqlCommand command = new SqlCommand("prc_cuentasCorrientes_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@cuentaCorriente_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        cuenta = Cargar(dr);
                }
                command.Connection.Close();
            }
            return cuenta;

        }

        public void Guardar(CuentaCorriente cuenta)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@cuentaCorriente_ID", SqlDbType.Int).Value = cuenta.Id;
                command.Parameters.Add("@banco_ID", SqlDbType.Int).Value = cuenta.Banco.Id;
                command.Parameters.Add("@sucursal_ID", SqlDbType.Int).Value = cuenta.Sucursal.Id;
                command.Parameters.Add("@cuentaCorriente_numero", SqlDbType.VarChar, 20).Value = cuenta.Numero;
                command.Parameters.Add("@cuentaCorriente_numeroLineaCredito", SqlDbType.VarChar, 20).Value = cuenta.NumeroLineaCredito;
                command.Parameters.Add("@cuentaCorriente_fechaApertura", SqlDbType.DateTime).Value = cuenta.FechaApertura;
                command.Parameters.Add("@cuentaCorriente_montoLineaCredito", SqlDbType.Decimal).Value = cuenta.MontoLineaCredito;
                command.Parameters.Add("@cuentaCorriente_ejecutivo", SqlDbType.VarChar, 20).Value = cuenta.Ejecutivo;
                command.Parameters.Add("@cuentaCorriente_emailEjecutivo", SqlDbType.VarChar, 50).Value = cuenta.EmailEjecutivo;
                command.Parameters.Add("@cuentaCorriente_telefonoEjecutivo", SqlDbType.VarChar, 20).Value = cuenta.TelefonoEjecutivo;
                command.Parameters.Add("@cuentaCorriente_formatoCheque", SqlDbType.VarChar, 100).Value = cuenta.FormatoCheque;
                command.Parameters.Add("@cuentaCorriente_observacion", SqlDbType.VarChar, 255).Value = cuenta.Observacion;
                command.Parameters.Add("@cuentaCorriente_bloquedo", SqlDbType.Bit).Value = cuenta.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = cuenta.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = cuenta.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (cuenta.Id > 0)
                    {
                        command.CommandText = "prc_cuentasCorrientes_actualizar";
                        command.Parameters["@usuario"].Value = cuenta.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = cuenta.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_cuentasCorrientes_insertar";
                        command.Parameters["@cuentaCorriente_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en CuentaCorrienteContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }


        private CuentaCorriente Cargar(SqlDataReader dr)
        {
            CuentaCorriente cuenta = new CuentaCorriente();
            cuenta.Id = (int)dr["cuentaCorriente_ID"];
            cuenta.Numero = dr["cuentaCorriente_numero"].ToString();
            cuenta.NumeroLineaCredito = dr["cuentaCorriente_numeroLineaCredito"].ToString();
            cuenta.FechaApertura = (DateTime)dr["cuentaCorriente_fechaApertura"];
            cuenta.MontoLineaCredito = (decimal)dr["cuentaCorriente_montoLineaCredito"];
            cuenta.Ejecutivo = dr["cuentaCorriente_ejecutivo"].ToString();
            cuenta.EmailEjecutivo = dr["cuentaCorriente_emailEjecutivo"].ToString();
            cuenta.TelefonoEjecutivo = dr["cuentaCorriente_telefonoEjecutivo"].ToString();
            cuenta.FormatoCheque = dr["cuentaCorriente_formatoCheque"].ToString();
            cuenta.Observacion = dr["cuentaCorriente_observacion"].ToString();
            cuenta.Bloqueado = (bool)dr["cuentaCorriente_bloquedo"];
            cuenta.CreacionUsuario = dr["creacion_usuario"].ToString();
            cuenta.CreacionFecha = (DateTime)dr["creacion_fecha"];
            cuenta.CreacionIp = dr["creacion_IP"].ToString();
            cuenta.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            cuenta.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            cuenta.ActualizacionIp = dr["actualizacion_IP"].ToString();
            cuenta.Banco = new BancosContext().Obtener((int)dr["banco_ID"]);
            cuenta.Sucursal = new SucursalesContext().Obtener((int)dr["sucursal_ID"]);

            return cuenta;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}