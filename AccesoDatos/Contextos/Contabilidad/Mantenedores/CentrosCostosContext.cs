﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace AccesoDatos.Contextos.Contabilidad.Mantenedores
{
    public class CentrosCostosContext
    {
        private string connectionString = string.Empty;

        public CentrosCostosContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public CentrosCostosContext()
        {
            this.connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["browne_contables"].ConnectionString;
        }


        public IEnumerable<CentroCosto> ObtenerTodos(int? ID = null, bool? bloqueado = null, int? padreID = null, int? nivel = null)
        {
            using (SqlCommand command = new SqlCommand("prc_centrosCostos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@centroCosto_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (ID.HasValue)
                    command.Parameters.Add("@centroCosto_ID", SqlDbType.Int).Value = ID;
                if (padreID.HasValue)
                    command.Parameters.Add("@centroCosto_padreID", SqlDbType.Int).Value = padreID;
                if (nivel.HasValue)
                    command.Parameters.Add("@centroCosto_nivel", SqlDbType.Int).Value = nivel;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        CentroCosto centro = Cargar(dr);
                        yield return centro;
                    }
                }
                command.Connection.Close();
            }
        }

        public CentroCosto Obtener(int? id = null, int? padreID = null, int? nivel = null)
        {
            CentroCosto centro = new CentroCosto();
            using (SqlCommand command = new SqlCommand("prc_centrosCostos_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (id.HasValue)
                    command.Parameters.Add("@centroCosto_ID", SqlDbType.Int).Value = id;
                if (padreID.HasValue)
                    command.Parameters.Add("@centroCosto_padreID", SqlDbType.Int).Value = padreID;
                if (nivel.HasValue)
                    command.Parameters.Add("@centroCosto_nivel", SqlDbType.Int).Value = nivel;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        centro = Cargar(dr);
                }
                command.Connection.Close();
            }
            return centro;

        }

        public void Guardar(IEnumerable<CentroCosto> centrosCostos)
        {
            //Se divide el código principal para los diferentes niveles que serán guardado en la base de datos.
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                var centrosRecientes = new CentroCosto();
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@centroCosto_ID", SqlDbType.Int);
                        command.Parameters.Add("@centroCosto_codigo", SqlDbType.Int);
                        command.Parameters.Add("@centroCosto_nombre", SqlDbType.VarChar, 255);
                        command.Parameters.Add("@centroCosto_bloqueado", SqlDbType.Bit);
                        command.Parameters.Add("@centroCosto_padreID", SqlDbType.Int);
                        command.Parameters.Add("@centroCosto_nivel", SqlDbType.Int);
                        command.Parameters.Add("@usuario", SqlDbType.VarChar, 50);
                        command.Parameters.Add("@IP", SqlDbType.VarChar, 50);
                        command.Connection = connection;
                        command.Transaction = transaction;
                        var nivel = 1;
                        foreach (var centro in centrosCostos)
                        {
                            command.CommandText = "prc_centrosCostos_insertar";
                            command.Parameters["@centroCosto_ID"].Value = centro.Id;
                            command.Parameters["@centroCosto_codigo"].Value = centro.Codigo;
                            command.Parameters["@centroCosto_nombre"].Value = centro.Nombre;
                            command.Parameters["@centroCosto_bloqueado"].Value = centro.Bloqueado;
                            command.Parameters["@centroCosto_padreID"].Value = centro.PadreID.Id;
                            command.Parameters["@centroCosto_nivel"].Value = nivel;
                            command.Parameters["@usuario"].Value = centro.CreacionUsuario;
                            command.Parameters["@IP"].Value = centro.CreacionIp;
                            if (nivel > 1)
                            {
                                centrosRecientes = ObtenerTodos().LastOrDefault();
                                command.Parameters["@centroCosto_padreID"].Value = centrosRecientes.Id;
                            }
                            else
                            {
                                command.Parameters["@centroCosto_padreID"].Value = centro.PadreID.Id;
                            }
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                            nivel++;
                        }


                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Error en CentrosCostosContext.Guardar " + ex.ToString());
                }
                finally
                {
                    transaction.Dispose();
                    connection.Close();
                }
            }
        }

        private CentroCosto Cargar(SqlDataReader dr)
        {
            CentroCosto centro = new CentroCosto();
            centro.Id = (int)dr["centroCosto_ID"];
            centro.Codigo = (int)dr["centroCosto_codigo"];
            centro.Nombre = dr["centroCosto_nombre"].ToString();
            centro.Bloqueado = (bool)dr["centroCosto_bloqueado"];
            centro.Nivel = (int)dr["centroCosto_nivel"];
            centro.PadreID = Obtener((int)dr["centroCosto_padreID"]);
            centro.CreacionUsuario = dr["creacion_usuario"].ToString();
            centro.CreacionFecha = (DateTime)dr["creacion_fecha"];
            centro.CreacionIp = dr["creacion_IP"].ToString();
            centro.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            centro.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            centro.ActualizacionIp = dr["actualizacion_IP"].ToString();

            return centro;
        }

        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}