﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace AccesoDatos.Contextos.Contabilidad.Mantenedores
{
    public class ProveedoresContext
    {
        private string connectionString = string.Empty;

        public ProveedoresContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public ProveedoresContext()
        {
            this.connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["browne_contables"].ConnectionString;
        }


        public IEnumerable<Proveedor> ObtenerTodos(int? comunaID = null, bool? bloqueado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_proveedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (bloqueado.HasValue)
                    command.Parameters.Add("@proveedor_bloqueado", SqlDbType.Bit).Value = bloqueado;
                if (comunaID.HasValue)
                    command.Parameters.Add("@comuna_ID", SqlDbType.Int).Value = comunaID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Proveedor proveedor = Cargar(dr);
                        yield return proveedor;
                    }
                }
                command.Connection.Close();
            }
        }

        public Proveedor Obtener(int id)
        {
            Proveedor proveedor = new Proveedor();
            proveedor.Id = id;
            using (SqlCommand command = new SqlCommand("prc_proveedores_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@proveedor_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        proveedor = Cargar(dr);
                }
                command.Connection.Close();
            }
            return proveedor;

        }

        public void Guardar(Proveedor proveedor)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@proveedor_ID", SqlDbType.Int).Value = proveedor.Id;
                command.Parameters.Add("@comuna_ID", SqlDbType.Int).Value = proveedor.Comuna.Id;
                command.Parameters.Add("@proveedor_rut", SqlDbType.VarChar, 100).Value = proveedor.Rut;
                command.Parameters.Add("@proveedor_razonSocial", SqlDbType.VarChar, 100).Value = proveedor.RazonSocial;
                command.Parameters.Add("@proveedor_nombre", SqlDbType.VarChar, 100).Value = proveedor.Nombre;
                command.Parameters.Add("@proveedor_Direccion", SqlDbType.VarChar, 100).Value = proveedor.Direccion;
                command.Parameters.Add("@proveedor_Telefono", SqlDbType.VarChar, 20).Value = proveedor.Telefono;
                command.Parameters.Add("@proveedor_contacto", SqlDbType.VarChar, 100).Value = proveedor.Contacto;
                command.Parameters.Add("@proveedor_email", SqlDbType.VarChar, 100).Value = proveedor.Email;
                command.Parameters.Add("@proveedor_bloqueado", SqlDbType.Bit).Value = proveedor.Bloqueado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = proveedor.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = proveedor.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (proveedor.Id > 0)
                    {
                        command.CommandText = "prc_proveedores_actualizar";
                        command.Parameters["@usuario"].Value = proveedor.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = proveedor.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_proveedores_insertar";
                        command.Parameters["@proveedor_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en ProveedoresContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        private Proveedor Cargar(SqlDataReader dr)
        {
            Proveedor proveedor = new Proveedor();
            proveedor.Id = (int)dr["proveedor_ID"];
            proveedor.Rut = dr["proveedor_rut"].ToString();
            proveedor.Nombre = dr["proveedor_nombre"].ToString();
            proveedor.RazonSocial = dr["proveedor_razonSocial"].ToString();
            proveedor.Direccion = dr["proveedor_direccion"].ToString();
            proveedor.Contacto = dr["proveedor_contacto"].ToString();
            proveedor.Bloqueado = (bool)dr["proveedor_bloqueado"];
            proveedor.Telefono = dr["proveedor_telefono"].ToString();
            proveedor.Email = dr["proveedor_email"].ToString();
            proveedor.CreacionUsuario = dr["creacion_usuario"].ToString();
            proveedor.CreacionFecha = (DateTime)dr["creacion_fecha"];
            proveedor.CreacionIp = dr["creacion_IP"].ToString();
            proveedor.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            proveedor.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            proveedor.ActualizacionIp = dr["actualizacion_IP"].ToString();
            proveedor.Comuna = new Transversales.ComunasContext().Obtener((int)dr["comuna_ID"]);

            return proveedor;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}