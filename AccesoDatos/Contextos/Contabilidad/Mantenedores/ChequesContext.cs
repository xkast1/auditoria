﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Contabilidad;

namespace AccesoDatos.Contextos.Contabilidad.Mantenedores
{
    public class ChequesContext
    {
        private string connectionString = string.Empty;

        public ChequesContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public ChequesContext()
        {
            this.connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["browne_contables"].ConnectionString;
        }


        public IEnumerable<Cheque> ObtenerTodos(int? talonarioID = null, bool? anulado = null)
        {

            using (SqlCommand command = new SqlCommand("prc_cheques_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                if (anulado.HasValue)
                    command.Parameters.Add("@cheque_anulado", SqlDbType.Bit).Value = anulado;
                if (talonarioID.HasValue)
                    command.Parameters.Add("@cuentaCorrienteTalotanio_ID", SqlDbType.Int).Value = talonarioID;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Cheque cheque = Cargar(dr);
                        yield return cheque;
                    }
                }
                command.Connection.Close();
            }
        }

        public Cheque Obtener(int id)
        {
            Cheque cheque = new Cheque();
            cheque.Id = id;
            using (SqlCommand command = new SqlCommand("prc_cheques_seleccionar"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@cheque_ID", SqlDbType.Int).Value = id;
                command.Connection = new SqlConnection(this.connectionString);
                command.Connection.Open();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows && dr.Read())
                        cheque = Cargar(dr);
                }
                command.Connection.Close();
            }
            return cheque;

        }

        public void Guardar(Cheque cheque)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@cheque_ID", SqlDbType.Int).Value = cheque.Id;
                command.Parameters.Add("@cuentaCorrienteTalonario_ID", SqlDbType.Int).Value = cheque.CuentaCorrienteTalonario.Id;
                command.Parameters.Add("@cheque_numero", SqlDbType.VarChar, 20).Value = cheque.Numero;
                command.Parameters.Add("@cheque_fecha", SqlDbType.DateTime).Value = cheque.Fecha;
                command.Parameters.Add("@cheque_monto", SqlDbType.Decimal).Value = cheque.Monto;
                command.Parameters.Add("@cheque_ordende", SqlDbType.VarChar, 20).Value = cheque.Ordende;
                command.Parameters.Add("@cheque_fechaCobro", SqlDbType.DateTime).Value = cheque.FechaCobro;
                command.Parameters.Add("@cheque_anulado", SqlDbType.Bit).Value = cheque.Anulado;
                command.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = cheque.CreacionUsuario;
                command.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = cheque.CreacionIp;
                try
                {
                    command.Connection = new SqlConnection(this.connectionString);
                    command.Connection.Open();
                    if (cheque.Id > 0)
                    {
                        command.CommandText = "prc_cheques_actualizar";
                        command.Parameters["@usuario"].Value = cheque.ActualizacionUsuario;
                        command.Parameters["@IP"].Value = cheque.ActualizacionIp;
                    }
                    else
                    {
                        command.CommandText = "prc_cheques_insertar";
                        command.Parameters["@cheque_ID"].Value = null;
                    }
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en ChequesContext.Guardar " + ex.ToString());
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }


        private Cheque Cargar(SqlDataReader dr)
        {
            Cheque cheque = new Cheque();
            cheque.Id = (int)dr["cheque_ID"];
            cheque.Numero = (int)dr["cheque_numero"];
            cheque.Fecha = (DateTime)dr["cheque_fecha"];
            cheque.Monto = (decimal)dr["cheque_monto"];
            cheque.Ordende = dr["cheque_ordende"].ToString();
            cheque.Anulado = (bool)dr["cheque_anulado"];
            cheque.FechaCobro = (DateTime)dr["cheque_fechaCobro"];
            cheque.CreacionUsuario = dr["creacion_usuario"].ToString();
            cheque.CreacionFecha = (DateTime)dr["creacion_fecha"];
            cheque.CreacionIp = dr["creacion_IP"].ToString();
            cheque.ActualizacionUsuario = dr["actualizacion_usuario"].ToString();
            cheque.ActualizacionFecha = (DateTime)dr["actualizacion_fecha"];
            cheque.ActualizacionIp = dr["actualizacion_IP"].ToString();
            cheque.CuentaCorrienteTalonario = new CuentasCorrientesTalonariosContext().Obtener((int)dr["cuentaCorrienteTalonario_ID"]);

            return cheque;
        }
        public void Disponse()
        {
            this.connectionString = null;
        }
    }
}