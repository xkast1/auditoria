﻿using System;
using Browne.Core.Modelo.Transversales;
namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class AntecedenteFinanciero
    {
        public int Id { get; set; }
        public string NumeroFactura { get; set; }
        public DateTime? FechaFactura { get; set; }   
        public decimal TasaCambio { get; set; }        
        public string NumeroCuotaPagoDiferido { get; set; }
        public DateTime? FechaPago { get; set; }
        public string CodigoFlete { get; set; }
        public string CodigoSeguro { get; set; }
        public string CodigoFabricante { get; set; }
        public string NombreFabricante { get; set; }
        public string DireccionFabricante { get; set; }
        public bool? FacturaComercialDefinitiva { get; set; }
        public decimal ValorFob { get; set; }
        public decimal ComisionesExterior { get; set; }
        public decimal Descuento { get; set; }
        public decimal ValorDelFlete { get; set; }
        public decimal ValorDelSeguro { get; set; }
        public decimal OtrosGastosDeducible { get; set; }
        public decimal ValorLiquidoRetorno { get; set; }
        public decimal ValorCIFUSD { get; set; }

        public Pais PaisAdquisicion { get; set; }
        public Moneda Moneda { get; set; }
        public ModalidadVenta ModalidadVenta { get; set; }
        public ClausulaCompraVenta ClausulaCompraVenta { get; set; }
        public FormaPago FormaPago { get; set; }

    }
}
