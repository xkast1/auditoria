﻿using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.ProductoServicio;

namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class Item : Modelo.Transversales.Operacion.Item
    {
        public string CodigoArancelIUDM { get; set; }       
        public decimal ValorMinimoGarantizado { get; set; }        
        public string CodigoAcuerdoComercial { get; set; }
        public bool? ValorFobPromedio { get; set; }
        public decimal MontoFOBUnitario { get; set; }
        public decimal MontoFOB { get; set; }
        public string CodigoContratoColchico { get; set; }
        public string CuotaContractual { get; set; }
        public int TotalElementos { get; set; }
        public int TotalVistosBuenos { get; set; }
        public ProductoServicio ProductoServicio { get; set; }

        public Pais PaisOrigen { get; set; }
        public Moneda MonedaValorMinimoGarantizado { get; set; }
        public System.Collections.Generic.IEnumerable<ItemElemento> Elementos { get; set; }      
        

    }
}
