﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class ItemElemento
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public string CodigoNombre { get; set; }
        public string CodigoTipo { get; set; }
        public decimal LEYDUS { get; set; }
        public string CodigoLEYDUSUDM { get; set; }
    }
}
