﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class ItemObservacion
    {
        public int Id { get; set; }
        public int Secuencia { get; set; }
        public string Codigo { get; set; }
        public string Valor { get; set; }
        public string Glosa { get; set; }
    }
}
