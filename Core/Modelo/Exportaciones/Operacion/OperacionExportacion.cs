﻿using System;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Operacion;

namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class OperacionExportacion : Modelo.Transversales.Operacion.Operacion
    {
        
        public string CodigoDestinacionAduanera { get; set; }
        public bool? MercaderiaNacional { get; set; }
        public string RutAgenciaCompaniaTransporte { get; set; }
        public bool? ExisteCompaniaTransporte { get; set; }
        public bool? MercaderiaConsolidadaZonaPrimaria { get; set; }
        public string NumeroReservaEmbarque { get; set; }
        public string NumeroReferenciaEnvio { get; set; }
        public string NombrePrincipal { get; set; }
        public string NumeroRotacion { get; set; }
        public string UCR { get; set; }
        public decimal PesoBruto { get; set; }
        public decimal PesoNeto { get; set; }
        public string CodigoPesoNetoUDM { get; set; }
        public decimal Volumen { get; set; }
        public string CodigoVolumenUDM { get; set; }
        public int TotalDocumentos { get; set; }
        public int TotalDocumentosTransportes { get; set; }
        public int TotalVistosBuenos { get; set; }
        public int TotalBultos { get; set; }
        public string Comentarios { get; set; }

        public Actor Exportador { get; set; }
        public Actor ExportadorSecundario { get; set; }
        public Actor Consignatario { get; set; }

        public AntecedenteFinanciero AntecedentesFinancieros { get; set; }
        public TipoCarga TipoCarga { get; set; }
        public EmisorDocumentoTransporte EmisorDocumentoTransporte { get; set; }
        public Region RegionOrigen { get; set; }
        public TransporteInternacional TransporteInternacional { get; set; }
        public System.Collections.Generic.IEnumerable<DocumentoTransporte> DocumentosTransporte { get; set; }        
        public System.Collections.Generic.IEnumerable<Item> Items { get; set; }
        public System.Collections.Generic.IEnumerable<Documento> Documentos { get; set; }
        public System.Collections.Generic.IEnumerable<OperacionVistoBueno> VistosBuenos { get; set; }

        public OperacionExportacion()
        {
            this.Id = 0;
            this.Tipo = "E";
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
            this.RuceID = 0;

        }
    }
}
