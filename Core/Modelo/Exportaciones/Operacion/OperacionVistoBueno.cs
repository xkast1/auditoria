﻿using System;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class OperacionVistoBueno
    {
        public int Id { get; set; }
        public int Secuencia { get; set; }
        public string CodigoEvaluador { get; set; }        
        public string Resolucion { get; set; }
        public DateTime? FechaResolucion { get; set; }
        public string GlosaResolucion { get; set; }
        public VistoBueno Tipo { get; set; }
    }
}
