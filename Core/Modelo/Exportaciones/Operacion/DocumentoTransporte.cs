﻿using System;
using Browne.Core.Modelo.Transversales;
namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class DocumentoTransporte
    {
        public int Id { get; set; }
        public int Secuencia { get; set; }
        public string Numero { get; set; }
        public DateTime? Fecha { get; set; }
        public string Tipo { get; set; }
        public Nave Nave { get; set; }
        public string NumeroViaje { get; set; }
        public string NumeroManifiesto { get; set; }

    }
}
