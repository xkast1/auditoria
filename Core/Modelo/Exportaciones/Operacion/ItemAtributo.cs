﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class ItemAtributo
    {
        public int Id { get; set; }
        public int? Secuencia { get; set; }
        public int? Codigo { get; set; }
        public string Valor { get; set; }
    }
}
