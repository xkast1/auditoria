﻿using System;

namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class Documento
    {
        public int Id { get; set; }
        public int? Secuencia { get; set; }
        public string Nombre { get; set; }
        public string Numero { get; set; }
        public string CodigoTipo { get; set; }
        //public Transversales.TipoDocumento TipoDocumento { get; set; }
        public string Emisor { get; set; }
        public DateTime? Fecha { get; set; }
        public string Comentario { get; set; }
    }
}
