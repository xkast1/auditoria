﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Exportaciones.Operacion
{
    public class ItemLote
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Lote { get; set; }
    }
}
