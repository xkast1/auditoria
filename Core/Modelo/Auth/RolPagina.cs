﻿using Browne.Core.Modelo.Navegacion;

namespace Browne.Core.Modelo.Auth
{
    public class RolPagina
    {
        public Rol Rol { get; set; }
        public Pagina Pagina { get; set; }
    }
}
