﻿using System;
using System.Collections.Generic;

namespace Browne.Core.Modelo.Auth
{
    public class GrupoTrabajo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string CorreoDistribucion { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIP { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIP { get; set; }
        public Area Area { get; set; }

        public IEnumerable<Usuario> Usuarios { get; set; }
        public IEnumerable<Transversales.Consumidor.Cliente> Clientes { get; set; }
        public IEnumerable<Transversales.Aduana>  Aduanas{ get; set; }

        public GrupoTrabajo()
        {
            this.Id = 0;
            this.Nombre = "";
            this.CorreoDistribucion = "";
            this.CreacionUsuario = "";
            this.CreacionIP = "";
            this.ActualizacionUsuario = "";
            this.ActualizacionIP = "";
        }
    }
}
