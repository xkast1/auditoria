﻿using System;

namespace Browne.Core.Modelo.Auth
{
    public class LogsAccesos
    {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public string UsuarioNombre { get; set; }
        public string Ip { get; set; }
        public DateTime Entrada { get; set; }
        public DateTime? Salida { get; set; }
        public string Esquema { get; set; }
        public string Mensaje { get; set; }
        public string Respuesta { get; set; }

        public LogsAccesos()
        {
            this.Id = 0;
            this.UsuarioNombre = "";
            this.Ip = "";
            this.Entrada = DateTime.Now;
            this.Esquema = "";
            this.Mensaje = "";
            this.Respuesta = "";
        }
    }
}
