﻿using System;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Modelo.Auth
{
    public class Usuario
    {
        public int Id { get; set; }
        public int LogId { get; set; } //Identifica el log para controlar el acceso
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Cargo { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Identificador { get; set; }
        public string ContrasenaDigerida { get; set; }
        public string ContrasenaAnterior { get; set; }
        public DateTime? VencimientoContrasena { get; set; }
        public bool AccesoRemoto { get; set; }
        public bool? Bloqueado { get; set; }
        public string TokenCookie { get; set; }
        public byte[] Avatar { get; set; }
        public bool Autorizado { get; set; }
        public DateTime? UltimoAcceso { get; set; }
        public string RutFuncion { get; set; }
        public string CreacionUsuario { get; set; }
        public string CreacionIp { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string ActualizacionUsuario { get; set; }
        public string ActualizacionIp { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public TipoUsuario TipoUsuario { get; set; }
        public Sucursal Sucursal { get; set; }
        public Area Area { get; set; }
        public Rol RolActual { get; set; }


        public System.Collections.Generic.IEnumerable<Rol> Roles { get; set; }
        public System.Collections.Generic.IEnumerable<GrupoTrabajo> GruposTrabajo { get; set; }

        public Usuario()
        {
            this.Id = 0;
            this.Rut = "";
            this.Nombre = "";
            this.Cargo = "";
            this.Telefono = "";
            this.Celular = "";
            this.Email = "";
            this.Identificador = "";
            this.ContrasenaDigerida = "";
            this.AccesoRemoto = false;
            this.Bloqueado = false;
            this.Avatar = new byte[0];
            this.RutFuncion = "";
            this.CreacionUsuario = "";
            this.CreacionIp = "";
            this.ActualizacionUsuario = "";
            this.ActualizacionIp = "";
        }
    }
}
