﻿namespace Browne.Core.Modelo.Auth
{
    public class Area
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Responsable { get; set; }
        public string Correo { get; set; }

        public Area()
        {
            this.Id = 0;
            this.Nombre = "";
            this.Responsable = "";
            this.Correo = "";
        }
    }
}
