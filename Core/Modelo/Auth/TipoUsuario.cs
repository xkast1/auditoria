﻿namespace Browne.Core.Modelo.Auth
{
    public class TipoUsuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool? Supervisor { get; set; }
        public bool? Cliente { get; set; }

        public TipoUsuario()
        {
            this.Id = 0;
            this.Nombre = "";
            this.Supervisor = false;
            this.Cliente = false;
        }
    }
}
