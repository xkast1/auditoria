﻿using System.Collections.Generic;
using System;
using Browne.Core.Modelo.Navegacion;

namespace Browne.Core.Modelo.Auth
{
    public class Rol
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool? Bloqueado { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
        public Pagina PaginaInicio { get; set; }
        public IEnumerable<Pagina> Paginas { get; set; }

        public Rol()
        {
            this.Id = 0;
            this.Nombre = "";
            this.Bloqueado = false;
            this.CreacionUsuario = "";
            this.CreacionIp = "";
            this.ActualizacionUsuario = "";
            this.ActualizacionIp = "";
        }

    }
}
