﻿namespace Browne.Core.Modelo.Importaciones.FacturaComercial
{
    public class FacturaComercialSubDetalle
    {
        public int Id { get; set; }
        public int Correlativo { get; set; }
        public decimal Cantidad { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Modelo2 { get; set; }
        public string Color { get; set; }
        public int Ano { get; set; }
        public string NumeroIdenficiacion { get; set; }
        public string NumeroIdenficiacion2 { get; set; }
        public string NumeroIdenficiacion3 { get; set; }
        public string NumeroIdenficiacion4 { get; set; }
        public decimal Peso { get; set; }
        public decimal PesoButo { get; set; }
        public decimal Volumen { get; set; }
        public decimal Largo { get; set; }
        public string TipoInternacion { get; set; }

    }
}
