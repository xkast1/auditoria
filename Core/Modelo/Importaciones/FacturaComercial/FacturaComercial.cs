﻿using System;
using System.Collections.Generic;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Consumidor;

namespace Browne.Core.Modelo.Importaciones.FacturaComercial
{
    public class FacturaComercial
    {
        public int Id { get; set; }
        public Cliente Cliente { get; set; }
        public string Numero { get; set; }
        public DateTime Fecha { get; set; }
        public Moneda Moneda { get; set; }
        public ClausulaCompraVenta ClausulaCompraVenta { get; set; }
        public decimal Paridad { get; set; }
        public decimal Total { get; set; }
        public IEnumerable<FacturaComercialDetalle> Detalle { get; set; }

        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
    }
}
