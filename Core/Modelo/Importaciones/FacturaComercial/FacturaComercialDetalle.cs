﻿using Browne.Core.Modelo.Transversales;
using System.Collections.Generic;

namespace Browne.Core.Modelo.Importaciones.FacturaComercial
{
    public class FacturaComercialDetalle
    {
        public int Id { get; set; }
        public int Correlativo { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public UnidadMedida UnidadMedida { get; set; }
        public IEnumerable<FacturaComercialSubDetalle> SubDetalle { get; set; }

    }
}
