﻿using System;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Modelo.Importaciones.Operacion
{
    public class OperacionImportacion : Modelo.Transversales.Operacion.Operacion
    {
      
        public string TipoDeclaracion { get; set; }
        public string NumeroIdentificacion { get; set; }


        public OperacionImportacion() {
            this.Id = 0;
            this.Tipo = "I";
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;            
        }
    }
}
