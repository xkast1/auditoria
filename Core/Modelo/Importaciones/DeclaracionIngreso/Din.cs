﻿//using System;
//using Browne.Core.Modelo.Transversales;
//using Browne.Core.Modelo.Importaciones.Comunes;
using System;
using System.Collections.Generic;

namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class Din
    {
        public int Correlativo { get; set; }
        public string TipoEnvio { get; set; }
        public DinCabecera Cabecera { get; set; }
        public IEnumerable<DinItem> LstItem { get; set; }
        public IEnumerable<DinVistosBuenos> LstVistosBuenos { get; set; }
        public IEnumerable<DinBulto> LstBulto { get; set; }
        public DinCuentaValor CuentaValor { get; set; }
        //public IEnumerable<DinInsumo> LstInsumo { get; set; }
        public string NumeroDespacho { get; set; }
        public int Estado { get; set; }
        public Core.Modelo.Transversales.Operacion.Operacion Operacion { get; set; }
    }
}
