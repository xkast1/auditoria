﻿using System;

namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinAntecedentesFinancieros
    {
        public string RegimenImportacion { get; set; }
        public int CodigoRegimenImportacion { get; set; }
        public int CodigoBancoComercial { get; set; }
        public int CodigoOrigenDivisas { get; set; }
        public int FormaPago { get; set; }
        public int NumeroDias { get; set; }
        public decimal ValorMercanciasExFabrica { get; set; }
        public int Moneda { get; set; }
        public decimal GastosFob { get; set; }
        public int ClausulaCompra { get; set; }
        public int FormaPagoGravamenes { get; set; }
        public string FechaSolicitudFondos { get; set; }
        public decimal MontoSolicitudFondos { get; set; }
        public string FechaRecepcionFondos { get; set; }
        public decimal MontoRecepcionFondos { get; set; }
        public string FechaPago { get; set; }
        

        /*

                ANTECEDENTES FINANCIEROS	 	 	 

        68	RÉGIMEN IMPORTACIÓN A	15	 
        69	CÓDIGO RÉGIMEN IMPORTACIÓN N	2	 

        77	CÓDIGO BANCO COMERCIAL N	2	 
        78	CÓDIGO ORIGEN DIVISAS N	1	 

        71	CÓDIGO FORMA PAGO COBERTURA N	2	 

        72	DIAS N	4	 
        79	VALOR MERCANCÍAS EX-FÁBRICA N	12	2

        73	MONEDA A	15	 
        80	GASTOS HASTA FOB N	12	2
        75	CLAÚSULA COMPRA A	15	 
        82	CÓDIGO FORMA PAGO GRAVÁMENES    N	2

        

        70	FORMA PAGO COBERTURA A	15	 
        74	CÓDIGO MONEDA   N	3	 
        76	CÓDIGO CLAÚSULA COMPRA N	1	 
        81	FORMA PAGO GRAVAMENES A	15	 
        
        */
    }
}
