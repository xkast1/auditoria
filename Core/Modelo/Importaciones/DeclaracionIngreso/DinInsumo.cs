﻿namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinInsumo
    {
        public int Numero { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public string CodigoMedida { get; set; }
        public decimal CIF { get; set; }
    }
}
