﻿namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinIdentificacion
    {
        public string Nombre { get; set; } // OPCIONAL
        public string Direccion { get; set; } // OPCIONAL
        public Transversales.Comuna Comuna { get; set; }
        public int TipoRut { get; set; }
        public int Rut { get; set; }
        public string DigitoVerificador { get; set; } // OPCIONAL
        public string NombreRepresentanteLegal { get; set; } // OPCIONAL
        public int RutRepresentanteLegal { get; set; } // OPCIONAL
        public string DigitoVerificadorRepresenteLegal { get; set; } // OPCIONAL
        public string NombreConsignatario { get; set; }
        public string DireccionConsignatario { get; set; } // OPCIONAL
        public string CodigoPais { get; set; } // OPCIONAL
    }
}
