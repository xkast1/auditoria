﻿using System.Collections.Generic;

namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinCuentaValor
    {
        public int entr_correlativo { get; set; }
        public decimal Monto178 { get; set; }
        public decimal Monto191 { get; set; }
        public decimal Monto699 { get; set; }
        public decimal Monto199 { get; set; }
        public IEnumerable<DinCuentaGiro> LstCuentasGiros { get; set; }
        public decimal Paridad { get; set; }
        public decimal TipoCambio { get; set; } 
    }
}
