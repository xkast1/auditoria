﻿using System;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Importaciones.Comunes;
using System.Collections.Generic;

namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinCabecera
    {
        public string NumeroDespacho { get; set; }
        public string TipoDeclaracion { get; set; }
        public decimal Forma { get; set; } // OPCIONAL
        public string NumeroIdentificacion { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public Aduana Aduana { get; set; }
        public string Agente { get; set; }
        public int TipoDocumento { get; set; } //tipoDocumento
        public string TipoIngreso { get; set; }  // no existe en el sistema // OPCIONAL
        public string NumeroAutorizacion { get; set; } //Autorizacion Banco Central no existe en el sistema // OPCIONAL
        public DateTime? FechaAutorizacion { get; set; }  //no existe en sistema // OPCIONAL
        public string GlosaAutorizacion { get; set; } // OPCIONAL
        public DateTime? FechaTransaccion { get; set; } // OPCIONAL
        public DateTime? FechaAceptacion { get; set; }
        public string Aforo { get; set; } // OPCIONAL
        public DateTime? FechaConfeccionDeclaracion { get; set; } //FecConfDi  = fecha transaccion
        public string NumeroInscripcion { get; set; } // OPCIONAL
        public DateTime? FechaConfeccion { get; set; } // OPCIONAL
        public string NumeroIdentificacionAclaracion { get; set; } // OPCIONAL
        public string NumeroResolucion { get; set; } // OPCIONAL
        public DateTime? FechaResolucion { get; set; } // OPCIONAL

        public DinIdentificacion Identificacion { get; set; }
        public DinRegimenSuspensivo RegimenSuspensivo { get; set; }
        public DinOrigenTransAlmacenaje OrigenTransAlmacenaje { get; set; }
        public DinAntecedentesFinancieros AntecedentesFinancieros { get; set; }
        public DinTotal Total { get; set; }

        public DateTime? FechaPresentacion { get; set; } // = fecha aceptacion // OPCIONAL
        public DateTime? FechaPresentacionResolucion { get; set; }  //no existe en sisteam  // OPCIONAL

        //public int Id { get; set; }
        //public string NumeroDespacho { get; set; }
        //public string Referencia { get; set; }
    }
}
