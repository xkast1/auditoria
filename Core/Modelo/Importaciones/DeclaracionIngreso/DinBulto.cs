﻿namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinBulto
    {
        public int Correlativo { get; set; }
        public string Identificacion { get; set; }
        public string Descripcion { get; set; }
        public int Tipo { get; set; }
        public int Cantidad { get; set; }
    }
}
