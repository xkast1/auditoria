﻿using System;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Importaciones.Comunes;

namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinOrigenTransAlmacenaje
    {
        public int PaisOrigen { get; set; } // nombre, codigo
        public string NombrePaisOrigen { get; set; } // nombre, codigo
        public int PaisAdquisicion { get; set; }
        public string NombrePaisAdquisicion { get; set; }
        public int ViaTransporte { get; set; }
        public string NombrePuertoEmbarque { get; set; }
        public string Transbordo { get; set; }
        public int CodigoPuertoEmbarque { get; set; }
        public string NombrePuertoDesembarque { get; set; }
        public int CodigoPuertoDesembarque { get; set; }
        public string TipoCarga { get; set; }
        public string Almacenista { get; set; }
        public string CodigoAlmacen { get; set; }
        public string FechaRecepcionMercaderia { get; set; }
        public string FechaRetiroMercaderia { get; set; }
        public string CiaTransporte { get; set; }
        public string CodigoPaisCiaTransporte { get; set; }
        public int RutCiaTransporte { get; set; }
        public string DvRutCiaTransporte { get; set; }
        public string NumeroManifiesto { get; set; }
        public string NumeroManifiesto1 { get; set; }
        public string NumeroManifiesto2 { get; set; }
        public string FechaManifiesto { get; set; }
        public string NumeroConocimientoEmbarque { get; set; }
        public string FechaConocimientoEmbarque { get; set; }
        public string NombreEmisorDocumento { get; set; }
        public int RutEmisorDocumento { get; set; }
        public int DvRutEmisorDocumento { get; set; }
        public string FechaInternacion { get; set; }
        public string FechaBodega { get; set; }

        /*
            ORIGEN TRANSPORTE Y ALMACENAJES	 	 	 
        
        24	PAIS ORIGEN A	15	 
        25	CÓDIGO PAÍS ORIGEN N	3	 
        
        
        26	PAÍS ADQUISICIÓN    A	15	 
        27	CÓDIGO PAÍS ADQUISICIÓN N	3	 
        
        28	CÓDIGO VÍA TRANSPORTE N	2	 
        
        29	CÓDIGO TRANSBORDO   A	1	 

        30	PUERTO EMBARQUE A	15	 
        31	CÓDIGO PUERTO EMBARQUE N	3	 
        
        32	PUERTO DESEMBARQUE  A	15	 
        33	CÓDIGO PUERTO DESEMBARQUE N	3	 
        
        34	CÓDIGO TIPO DE CARGA    A	1	 
        
        46	ALMACENISTA A	30	 
        47	CÓDIGO ALMACENISTA  A	3	 
        
        48	FECHA RECEPCIÓN MERCANCÍAS N	8	 
        49	FECHA RETIRO DE MERCANCÍAS  N	8	 
        
        --------------
        50	NÚMERO REGISTRO DE RECONOCIMIENTO   N	6	 
        50B AÑO REGISTRO RECONOCIMIENTO N	4	 
        51	CÓDIGO REGLA 1 O Vº Bº N	1	 
        52	NÚMERO REGLA 1 O Vº Bº A	13	 
        52B AÑO REGLA 1 O Vº Bº N	4	 
        53	CÓDIGO ÚLTIMA DECLARACIÓN REGLA 1	N	3
        --------------
        
        ---
        35	CIA.TRANSPORT.ABREVIATURA A	20	 
        36	CÓDIGO PAÍS CIA.TRANSP N	3	 
        37	RUT CIA.TRANSPORTADORA N   8	 
        38	DÍGITO VERIFICADOR RUT CIA. TRANSP.A   1	 
        39	MANIFIESTO (3 DE 15 SEPARADOS POR GUIÓN)    A	50	 
        40	FECHA MANIFIESTO    N	8	 
        41	Nº DOCUMENTO TRANSPORTE A	30	 
        42	FECHA N	8	 
        43	EMISOR DOCUMENTO TRANSPORTE A	30	 
        44	RUT N	8	 
        45	DÍGITO VERIFICADOR RUT A	1	 
        ---
        

        */


    }
}
