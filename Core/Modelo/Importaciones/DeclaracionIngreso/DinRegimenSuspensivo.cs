﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinRegimenSuspensivo
    {

        public string DireccionAlmacenamiento { get; set; }
        public int CodigoComuna { get; set; }
        public int CodigoAduanaControl { get; set; }
        public int Plazo { get; set; }
        public int IndicadorParcial { get; set; }
        public int NumeroHojasInsumos { get; set; }
        public decimal TotalInsumos { get; set; }
        public int CodigoAlmacen { get; set; }
        public long NumeroRegimen { get; set; }
        public string FechaResolucion { get; set; }
        public int CodigoAduanaRegimen { get; set; }
        public int NumeroHojasAnexas { get; set; }
        public int CantidadSecuencias { get; set; }

    }
}
