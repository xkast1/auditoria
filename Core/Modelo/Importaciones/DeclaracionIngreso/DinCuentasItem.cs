﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinCuentasItem
    {
        public int Correlativo { get; set; }
        public int Item { get; set; }
        public decimal Otro { get; set; }
        public int Codigo { get; set; }
        public string SignoValor { get; set; }
        public decimal Valor { get; set; } 
    }
}
