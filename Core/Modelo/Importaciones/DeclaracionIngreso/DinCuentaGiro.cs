﻿namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinCuentaGiro
    {
        public int CuentaOtro { get; set; }
        public decimal MontoOtro { get; set; } 
    }
}
