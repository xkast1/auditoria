﻿namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinVistosBuenos
    {
        public int Correlativo { get; set; }
        public int NumeroRegistroReconocimiento { get; set; }
        public int AnoRegistroReconocimiento { get; set; }
        public int CodigoVistoBueno { get; set; }
        public int NumeroRegla { get; set; }
        public int NumeroAnoResolucion { get; set; }
        public int CodigoUltimoVistoBueno { get; set; }
        public string FechaResolucion { get; set; }
    }
}
