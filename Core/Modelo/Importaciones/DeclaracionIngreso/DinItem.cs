﻿using System.Collections.Generic;
namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinItem
    {
        public int Correlativo { get; set; }
        public int NumeroItem { get; set; }
        public string Nombre { get; set; }
        public string Marca { get; set; }
        public string Variedad { get; set; }
        public string Otro1 { get; set; }
        public string Otro2 { get; set; }
        public string Atributo5 { get; set; }
        public string Atributo6 { get; set; }
        public string CodigoAjuste { get; set; }
        public decimal Ajuste { get; set; }
        public decimal CantidadMercancias { get; set; }
        public int Medida { get; set; }
        public int CodigoUnidadMedidaEstimada { get; set; }
        public decimal PrecioUnitario { get; set; }
        public int Tratado { get; set; }
        public int CorrelativoDetalle { get; set; }
        public int AcuerdoComercial { get; set; }
        public int Cupo { get; set; }
        public int ArancelNacional { get; set; }
        public decimal Cif { get; set; }
        public decimal AdValorem { get; set; }
        public int CodigoAdValorem { get; set; }
        public decimal AdValoremAdicional { get; set; }
        public IEnumerable<DinObservacionesItem> LstObservacionesItem { get; set; }
        public IEnumerable<DinCuentasItem> LstCuentasItem { get; set; }
        public string NumeroOrdenCompra { get; set; }
        public int NumeroEmbarque { get; set; }
        public string NumeroFactura { get; set; }
        public string IdBultos { get; set; }
        
    }
}
