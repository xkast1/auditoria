﻿namespace Browne.Core.Modelo.Importaciones.DeclaracionIngreso
{
    public class DinTotal
    {
        public int TotalItems { get; set; }
        public decimal Fob { get; set; }
        public int TotalHojas { get; set; }
        public int CodigoFlete { get; set; }
        public decimal Flete { get; set; }
        public int TotalBultos { get; set; }
        public int CodigoSeguro { get; set; }
        public decimal Seguro { get; set; }
        public decimal TotalPeso { get; set; }
        public decimal Cif { get; set; } 
    }
}
