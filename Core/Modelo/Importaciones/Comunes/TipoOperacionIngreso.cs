﻿namespace Browne.Core.Modelo.Importaciones.Comunes
{
    public class TipoOperacionIngreso
    {
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public string Glosa { get; set; }
    }
}
