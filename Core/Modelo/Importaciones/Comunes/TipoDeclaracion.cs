﻿namespace Browne.Core.Modelo.Importaciones.Comunes
{
    public class TipoDeclaracion
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
    }
}
