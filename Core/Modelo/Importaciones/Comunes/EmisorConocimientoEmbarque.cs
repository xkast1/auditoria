﻿namespace Browne.Core.Modelo.Importaciones.Comunes
{
    public class EmisorConocimientoEmbarque
    {
        public string Codigo { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
    }
}
