﻿namespace Browne.Core.Modelo.Importaciones.GuiaDespacho
{
    public class GuiaDespachoDetalle
    {
        public int Id { get; set; }
        //public GuiaDespacho GuiaDespacho { get; set; }
        public string DescripcionBulto { get; set; }
        public decimal Cantidad { get; set; }
        public string TipoBulto { get; set; }
        public string Mercaderia { get; set; }
        public decimal Total { get; set; }
    }
}
