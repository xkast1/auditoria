﻿using System;
using System.Collections.Generic;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Consumidor;

namespace Browne.Core.Modelo.Importaciones.GuiaDespacho
{
    public class GuiaDespacho
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public DateTime Fecha { get; set; }
        public Cliente Cliente { get; set; }
        public string RutUsuario { get; set; }
        public string Referencia { get; set; }

        public Aduana Aduana { get; set; }
        public string TipoDocumento { get; set; } //Importacion, redestinacion, ALM. PARTICULAR    
        public string NumeroDespacho { get; set; }
        public string NumeroDeclaracion { get; set; }
        public DateTime FechaAceptacion { get; set; }
        public string DocumentoTransporte { get; set; }
        public string NombreNave { get; set; }
        public DateTime FechaDocumentoTransporte { get; set; }
        public string NumeroManifiesto { get; set; }
        public DateTime? FechaManifiesto { get; set; }
        public string IndicadorTraslado { get; set; }
        public string ParcialTotal { get; set; }

        public IEnumerable<GuiaDespachoDetalle> Detalle { get; set; }

        //datos de la carga --RESUMEN
        public decimal KilosNetos { get; set; }
        public decimal KilosBruto { get; set; }
        public decimal KilosTara { get; set; }
        public decimal CIF { get; set; }

        //Entrega
        public string DireccionEntrega { get; set; }
        public string Destino { get; set; }
        public string Cuadrilla { get; set; }
        public string Grua { get; set; }
        public string Senor { get; set; }
        public string HoraRecepcion { get; set; }
        public string TelefonoRecepcion { get; set; }

        //Retira
        public string Chofer { get; set; }
        public string RutChofer { get; set; }
        public string Patente { get; set; }

        public string Observacion { get; set; }

        //Carga
        public TipoCarga TipoCarga { get; set; }
        public string HoraEntrega { get; set; }

        //Empresa de Trasnporte
        public TransporteNacional TransporteNacional { get; set; }

        //Otros datos no impresos
        public Agente Agente { get; set; }
        public string MotivoAnulacion { get; set; }
        public int CodigoTipo { get; set; }
        public string TipoOperacion { get; set; }
        public bool FleteEspecial { get; set; }



        //datos de Entrega




        public string ObservacionConsignatario { get; set; }
        public string Telefono { get; set; }
        public string Horario { get; set; }

        //datos de la Recepción
        public string Recepcionista { get; set; }
        public DateTime FechaRecepcion { get; set; }


        //datos del Retiro
        public string ResponsableRetiro { get; set; }
        public DateTime? FechaRetiro { get; set; }
        public string HoraRetiro { get; set; }

        public string Vehiculo { get; set; }



        public int CantidadPeonetas { get; set; }
    }
}
