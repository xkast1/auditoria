﻿namespace Browne.Core.Modelo.Transversales.ProductoServicio
{
    public class ProductoServicioAtributo
    {
        public int Id { get; set; }
        public int Secuencia { get; set; }
        public string Nombre { get; set; }
        public string NombreEspanol { get; set; }
        public string Valor { get; set; }
        public string TipoDato { get; set; }
        public int TamanoDato { get; set; }
        public string TipoCodigo { get; set; }
        public string TipoCodigoMaestro { get; set; }
        public string IsFixed { get; set; }
        public string Mandatorio { get; set; }
        public string Status { get; set; }
        public string Uso { get; set; }
        public string UniqueCode { get; set; }

    }
}
