﻿using System;
using System.Collections.Generic;

namespace Browne.Core.Modelo.Transversales.ProductoServicio
{
    public class ProductoServicio
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string CodigoServicioPublico { get; set; }
        public string ServicioPublico { get; set; }
        public string CodigoNAB { get; set; }
        public string Nombre { get; set; }
        public string NombreEspanol { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionEspanol { get; set; }
        public string NombreCientifico { get; set; }
        public string NombreNegocio { get; set; }
        public string NombreNegocioEspanol { get; set; }
        public string EstadoFisico { get; set; }
        public string Estado { get; set; }
        public string Regimen { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIP { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIP { get; set; }

        public IEnumerable<ProductoServicioAtributo> Atributos { get; set; }

    }
}
