﻿namespace Browne.Core.Modelo.Transversales
{
    public class Agente
    {
        public string Codigo { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
    }
}
