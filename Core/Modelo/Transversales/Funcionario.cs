﻿namespace Browne.Core.Modelo.Transversales
{
    public class Funcionario
    {
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cargo { get; set; }
        public string Email { get; set; }

        public Funcionario() { }
        public Funcionario(string strRut, string strNombre)
        {
            this.Rut = strRut;
            this.Nombre = strNombre;
        }
    }
}