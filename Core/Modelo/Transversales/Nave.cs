﻿using System;

namespace Browne.Core.Modelo.Transversales
{
    public class Nave
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public ViaTransporte ViaTransporte { get; set; }
        public Pais Bandera { get; set; }
        public bool? Bloqueado { get; set; }

        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
    }
}