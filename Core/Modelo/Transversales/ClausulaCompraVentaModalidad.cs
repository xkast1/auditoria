﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Transversales
{
    public class ClausulaCompraVentaModalidad
    {
        public ClausulaCompraVenta ClausulaCompraVenta { get; set; }
        public ViaTransporte ViaTransporte{ get; set; }
    }
}