﻿namespace Browne.Core.Modelo.Transversales
{
    public class TransporteInternacional
    {
        public int Id { get; set; }
        public int Codigo { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public Pais Pais { get; set; }
    }
}