﻿using System;

namespace Browne.Core.Modelo.Transversales
{
    public class TipoOperacion
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string CodigoAduanas { get; set; }
        public string CodigoSicex { get; set; }
        public string Prefijo { get; set; }
        public string Nombre { get; set; }
        public string Glosa { get; set; }
        public bool? Importacion { get; set; }
        public bool? Exportacion { get; set; }
        public bool? Servicio { get; set; }
        public bool? Otra { get; set; }
        public bool? Bloqueado { get; set; }
        public string ParaCodParam { get; set; }
        public int? TapaCodTabla { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }

        public TipoOperacion()
        {
            this.Id = 0;
            this.Bloqueado = false;
            this.Importacion = false;
            this.Exportacion = false;
            this.Servicio = false;
            this.Otra = false;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
        }
    }
}
