﻿using System;
using System.Collections.Generic;


namespace Browne.Core.Modelo.Transversales
{
    public class ClausulaCompraVenta
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string CodigoAduanas { get; set; }
        public string CodigoSicex { get; set; }
        public string Nombre { get; set; }
        public string NombreIngles { get; set; }
        public string Sigla { get; set; }
        public string EmbalajeVerificacion { get; set; }
        public string Carga { get; set; }
        public string TransporteInteriorOrigen { get; set; }
        public string FormalidadesAduanerasExpo { get; set; }
        public string CostoManipulacionMercaderiaExpo { get; set; }
        public string TransportePrincipal { get; set; }
        public string SeguroMercaderia { get; set; }
        public string CostoManipulacionMercaderiaImpo { get; set; }
        public string FormalidadesAduanerasImpo { get; set; }
        public string TransporteInteriorDestino { get; set; }
        public string Entrega { get; set; }
        public bool? Bloqueado { get; set; }
        public string ParaCodParam { get; set; }
        public int? TapaCodTabla { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
        public IEnumerable<ViaTransporte> ViasTransportes { get; set; }

        public ClausulaCompraVenta()
        {
            this.Id = 0;
            this.EmbalajeVerificacion = "A";
            this.Carga = "A";
            this.TransporteInteriorOrigen = "A";
            this.FormalidadesAduanerasExpo = "A";
            this.CostoManipulacionMercaderiaExpo = "A";
            this.TransportePrincipal = "A";
            this.SeguroMercaderia = "A";
            this.CostoManipulacionMercaderiaImpo = "A";
            this.FormalidadesAduanerasImpo = "A";
            this.TransporteInteriorDestino = "A";
            this.Entrega = "A";
            this.Bloqueado = false;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
        }

    }
}