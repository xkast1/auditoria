﻿using System;


namespace Browne.Core.Modelo.Transversales
{
    public class Comuna
    {
        public int Id { get; set; }
        public Provincia Provincia { get; set; }
        public string Codigo { get; set; }
        public string CodigoAduanas { get; set; }
        public string CodigoSicex { get; set; }
        public string Nombre { get; set; }
        public string Ciudad { get; set; }
        public bool? Bloqueado { get; set; }
        public string ParaCodParam { get; set; }
        public int? TapaCodTabla { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }

        public Comuna()
        {
            this.Id = 0;
            this.Provincia = new Provincia();
            this.Bloqueado = false;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
        }
    }
}