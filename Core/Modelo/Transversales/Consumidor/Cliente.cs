﻿namespace Browne.Core.Modelo.Transversales.Consumidor
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public int Sucursal { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public bool Bloqueado { get; set; }
        public string RazonSocial { get; set; }
        public string RazonCorta { get; set; }
        public string Giro { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }

        public System.Collections.Generic.IEnumerable<Modelo.Auth.GrupoTrabajo> GruposTrabajo{ get; set; }
    }
}
