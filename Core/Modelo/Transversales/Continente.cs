﻿using System;
using System.Collections.Generic;


namespace Browne.Core.Modelo.Transversales
{
    public class Continente
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string CodigoAduanas { get; set; }
        public string CodigoSicex { get; set; }
        public string Nombre { get; set; }
        public string NombreIngles { get; set; }
        public bool? Bloqueado { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
        public IEnumerable<Pais> Paises { get; set; }

        public Continente()
        {
            this.Id = 0;
            this.Bloqueado = false;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
        }
    }
}
