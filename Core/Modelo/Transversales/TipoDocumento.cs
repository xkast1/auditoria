﻿namespace Browne.Core.Modelo.Transversales
{
    public class TipoDocumento
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Sigla { get; set; }
    }
}