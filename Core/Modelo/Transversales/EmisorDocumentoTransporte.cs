﻿using System;

namespace Browne.Core.Modelo.Transversales
{
    public class EmisorDocumentoTransporte
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string Verificador { get; set; }
        public string RazonSocial { get; set; }
        public string RutRepresentateLegal { get; set; }
        public string NombreRepresentanteLegal { get; set; }
        public bool? Bloqueado { get; set; }
        public string ParaCodParam { get; set; }
        public int? TapaCodTabla { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }

        public EmisorDocumentoTransporte() {
            this.Id = 0;
            this.Rut = "";
            this.RazonSocial = "";
            this.RutRepresentateLegal = "";
            this.NombreRepresentanteLegal = "";

        }
    }
}
