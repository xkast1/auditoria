﻿using System;
using System.Collections.Generic;
using Browne.Core.Modelo.Contabilidad;

namespace Browne.Core.Modelo.Transversales
{
    public class Sucursal
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Mail { get; set; }
        public string Responsable { get; set; }
        public bool? Bloqueado { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
        public Aduana Aduana { get; set; }
        public Comuna Comuna { get; set; }
        public Empresa Empresa { get; set; }
        public IEnumerable<CuentaCorriente> CuentasCorrientes { get; set; }

        public Sucursal()
        {
            this.Id = 0;
            this.Nombre = "";
            this.Direccion = "";
            this.Telefono = "";
            this.Mail = "";
            this.Responsable = "";
            this.Bloqueado = false;
            this.CreacionUsuario = "";
            this.CreacionIp = "";
            this.ActualizacionUsuario = "";
            this.ActualizacionIp = "";
            this.Empresa = new Core.Modelo.Transversales.Empresa();
        }
    }
}
