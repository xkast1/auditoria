﻿namespace Browne.Core.Modelo.Transversales
{
    public class Embarcador
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
    }
}