﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Transversales
{
    public class Empresa
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Web { get; set; }
        public string RepresentanteLegal { get; set; }
        public IEnumerable<Sucursal> Sucursales { get; set; }

        public Empresa()
        {
            this.Id = 0;
            this.Rut = "";
            this.RazonSocial = "";
            this.Direccion = "";
            this.Web = "";
            this.RepresentanteLegal = "";
        }
    }
}