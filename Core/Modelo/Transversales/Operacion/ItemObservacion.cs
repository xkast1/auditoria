﻿namespace Browne.Core.Modelo.Transversales.Operacion
{
    public class ItemObservacion
    {
        public int Id { get; set; }
        public int Secuencia { get; set; }
        public string Codigo { get; set; }
        public string Valor { get; set; }
        public string Glosa { get; set; }
    }
}
