﻿namespace Browne.Core.Modelo.Transversales.Operacion
{
    public class Contenedor
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public int? Secuencia { get; set; }

        public string Sigla { get; set; }
        public string CodigoSello { get; set; }
        public decimal Peso { get; set; }
        public string CodigoPesoUMD { get; set; }

        public EstadoCargaContenedor EstadoCarga { get; set; }
        public TipoContenedor Tipo { get; set; }
        public TipoSello Sello { get; set; }
    }
}
