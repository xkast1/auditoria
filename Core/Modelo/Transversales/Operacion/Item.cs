﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Transversales.Operacion
{
    public class Item
    {
        public int Id { get; set; }
        public int? Secuencia { get; set; }
        public string Codigo { get; set; }
        public string CodigoSicex { get; set; }
        public string Descripcion { get; set; }
        public string OtraDescripcion { get; set; }
        public string DescripcionAdicional { get; set; }
        public string CodigoArancel { get; set; }
        public decimal PesoBruto { get; set; }
        public decimal PesoNeto { get; set; }
        public decimal Volumen { get; set; }
        public string CodigoVolumenUDM { get; set; }
        public string CodigoPesoNetoUDM { get; set; }
        public decimal Cantidad { get; set; }
        public string Comentario { get; set; }
        public int TotalObservaciones { get; set; }
        public int TotalDocumentos { get; set; }
        public int TotalAtributos { get; set; }
        public int TotalLotes { get; set; }

        public System.Collections.Generic.IEnumerable<ItemObservacion> Observaciones { get; set; }
        public System.Collections.Generic.IEnumerable<Documento> Documentos { get; set; }
        public System.Collections.Generic.IEnumerable<ItemAtributo> Atributos { get; set; }
        public System.Collections.Generic.IEnumerable<ItemLote> Lotes { get; set; }

    }
}
