﻿using Browne.Core.Modelo.Transversales.Consumidor;
using Browne.Core.Modelo.Transversales;
using System;

namespace Browne.Core.Modelo.Transversales.Operacion
{
    public class Operacion
    {
        public int Id { get; set; }
        public int Correlativo { get; set; }

        public int? RuceID { get; set; }
        public string NumeroRUCEProvisional { get; set; }
        public string NumeroRUCE { get; set; }
        public DateTime? FechaHoraRUCE { get; set; }

        public string Numero { get; set; }
        public string Tipo { get; set; } // I: importaciones, E: Exportaciones, S: Servicio

        public DateTime? Fecha { get; set; }
        public string Referencia { get; set; }

        public string Observacion { get; set; }
        public string NumeroAceptacion { get; set; }
        public DateTime? FechaAceptacion { get; set; }

        public bool Parcial { get; set; }
        public decimal? NumeroPacialidad { get; set; }
        public decimal? TotalPacialidad { get; set; }
        public string ObservacionesGenerales { get; set; }
        public int TotalItems { get; set; }
        public int TotalContenedores { get; set; }
        public DateTime? FechaDesembarque { get; set; }


        public Cliente Cliente { get; set; }
        public TipoOperacion TipoOperacion { get; set; }
        public Aduana Aduana { get; set; }
        public Agente Agente { get; set; }
        public ModalidadVenta ModalidadVenta { get; set; }
        public ClausulaCompraVenta ClausulaCompraVenta { get; set; }
        public FormaPago FormaPago { get; set; }
        public Pais PaisDestino { get; set; }
        public Pais PaisAdquisicion { get; set; }
        public Pais PaisOrigen { get; set; }
        public Puerto PuertoEmbarque { get; set; }
        public Puerto PuertoDesembarque { get; set; }
        public Nave Nave { get; set; }
        public Moneda Moneda { get; set; }
        public ViaTransporte ViaTransporte { get; set; }

        public System.Collections.Generic.IEnumerable<Bulto> Bultos { get; set; }
        public System.Collections.Generic.IEnumerable<Contenedor> Contenedores { get; set; }


        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }


    }
}
