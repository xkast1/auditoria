﻿
namespace Browne.Core.Modelo.Transversales.Operacion
{

    public class Actor
    {
        public int Id { get; set; }
        public string CodigoTipo { get; set; }
        public string CodigoTipoDocumento { get; set; }
        public string Codigo { get; set; }
        public string CodigoVerificador { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string TelefonoMovil { get; set; }
        public string CorreoElectronico { get; set; }
        public bool? IndicadorALaOrden { get; set; }
        public bool? Secundario { get; set; }
        public decimal Porcentaje { get; set; }
        public Pais Pais { get; set; }
        public Region Region { get; set; }
        public Comuna Comuna { get; set; }

    }
}
