﻿namespace Browne.Core.Modelo.Transversales.Operacion
{
    public class ItemLote
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Lote { get; set; }
    }
}
