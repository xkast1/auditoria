﻿
namespace Browne.Core.Modelo.Transversales.Operacion
{
    public class Bulto
    {
        public int Id { get; set; }
        public int Secuencia { get; set; }
        public TipoBulto Tipo { get; set; }
        public int Cantidad { get; set; }
        public string Identificacion { get; set; }
        public string SubContinente { get; set; }
    }
}
