﻿using System;
using System.Collections.Generic;

namespace Browne.Core.Modelo.Transversales
{
    public class Aduana
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string CodigoAduanas { get; set; }
        public string CodigoSicex { get; set; }
        public string Nombre { get; set; }
        public string NombreIngles { get; set; }
        public string Direccion { get; set; }
        public bool? Bloqueado { get; set; }
        public string ParaCodParam { get; set; }
        public int? TapaCodTabla { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }

        public IEnumerable<ViaTransporte> ViasTransportes { get; set; }
        public IEnumerable<Modelo.Auth.GrupoTrabajo> GruposTrabajo { get; set; }

        public Aduana()
        {
            this.Id = 0;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;

        }
        //public void AgregarViaTransporte(ViaTransporte viaTransporte)
        //{
        //    this.ViasTransportes.Add(viaTransporte);
        //}
    }

}