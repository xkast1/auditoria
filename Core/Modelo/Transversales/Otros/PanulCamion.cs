﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Transversales.Otros
{
    public class PanulCamion
    {
        public string Patente { get; set; }
        public string RutConductor { get; set; }
        public string Conductor { get; set; }
        public string Neto { get; set; }
        public string Bruto { get; set; }
        public string Tara { get; set; }
        public string NroOperacion { get; set; }
        public string RutTransportista { get; set; }
        public string Transportista { get; set; }
        public string NroNave { get; set; }
        public string NombreNave { get; set; }
        public string Declaracion { get; set; }
        public string IDProducto { get; set; }
        public string NombreProducto { get; set; }
        public string RutCliente { get; set; }
        public string Cliente { get; set; }
        public string Ingreso { get; set; }
        public string Salida { get; set; }
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
    }
}