﻿using System;

namespace Browne.Core.Modelo.Transversales.Otros
{
    public class AcuseRecibo
    {
        public string NumeroOperacion { get; set; }
        public string CodigoInteraccion { get; set; }
        public string Destinatario { get; set; }
        public string Remitente { get; set; }
        public string NumeroProvisional { get; set; } //para sicex es el numero RUCE provisional
        public string Numero { get; set; } // para sicex es el numero de RUCE
        public string Codigo { get; set; } //RCB - ERR - SCA
        public string Glosa { get; set; }
        public DateTime Fecha { get; set; }
    }
}
