﻿using System.Collections.Generic;

namespace Browne.Core.Modelo.Sesion
{
    public class NavbarGrupo
    {
        public string NombreGrupo { get; set; }
        public IEnumerable<string> Enlaces { get; set; }
    }

    public class Navbar
    {
        public IEnumerable<NavbarGrupo> Grupos { get; set; }
    }
}
