﻿namespace Browne.Core.Modelo.Sesion
{
    public class Sesion
    {
        public int IdentificadorUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public int IdentificadorRolUsuario { get; set; }
        public string NombreRolUsuario { get; set; }
        public string NombreCompletoUsuario { get; set; }
        public Navbar Navbar { get; set; }
    }
}
