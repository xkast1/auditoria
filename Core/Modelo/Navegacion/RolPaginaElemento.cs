﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Auth;

namespace Browne.Core.Modelo.Navegacion
{
    public class RolPaginaElemento
    {
        public Rol Rol { get; set; }
        public Pagina Pagina { get; set; }
        public Elemento Elemento { get; set; }
        public string NombreElemento { get; set; }
        public bool? Bloqueado { get; set; }
        public bool? Invisible { get; set; }

        public RolPaginaElemento()
        {
            this.NombreElemento = string.Empty;
            this.Bloqueado = false;
            this.Invisible = false;
        }
    }
}
