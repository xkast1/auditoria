﻿using System.Collections.Generic;

namespace Browne.Core.Modelo.Navegacion
{
    public class Plataforma
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public IEnumerable<Pagina> Paginas { get; set; }

        public Plataforma()
        {
            this.Codigo = "";
            this.Nombre = "";
        }
    }
}
