﻿using System.Collections.Generic;

namespace Browne.Core.Modelo.Navegacion
{
    public class Pagina
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Ruta { get; set; }
        public string Area { get; set; }
        public string Controlador { get; set; }
        public string Accion { get; set; }
        public bool? Bloqueado { get; set; }
        public IEnumerable<Elemento> Elementos { get; set; }
        public Auth.Rol Rol { get; set; }
        public Plataforma Plataforma { get; set; }

        public Pagina()
        {
            this.Id = 0;
            this.Nombre = "";
            this.Ruta = "";
            this.Bloqueado = false;
        }
    }
}
