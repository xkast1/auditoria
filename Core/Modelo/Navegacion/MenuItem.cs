﻿namespace Browne.Core.Modelo.Navegacion
{
    public class MenuItem
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Posicion { get; set; }
        public Pagina Pagina { get; set; }
        public Menu Menu { get; set; }

        public MenuItem()
        {
            this.Id = 0;
            this.Text = "";
            this.Posicion = 1;
        }
    }
}
