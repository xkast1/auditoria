﻿using System.Collections.Generic;

namespace Browne.Core.Modelo.Navegacion
{
    public class Elemento
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string HtmlID { get; set; }
        //corresponde a roles-paginas-elementos
        public bool? Bloqueado { get; set; }
        public bool? Invisible { get; set; }
        public Pagina Pagina { get; set; }

        public Elemento()
        {
            this.Id = 0;
            this.Nombre = "";
            this.HtmlID = string.Empty;
            this.Bloqueado = false;
            this.Invisible = false;
        }
    }
}
