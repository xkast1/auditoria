﻿using System.Collections.Generic;

namespace Browne.Core.Modelo.Navegacion
{
    public class Menu
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Posicion { get; set; }
        public string Ubicacion { get; set; }
        public Plataforma Plataforma { get; set; }
        public IEnumerable<MenuItem> Items { get; set; }

        public Menu()
        {
            this.Id = 0;
            this.Text = "";
            this.Posicion = 1;
            this.Ubicacion = "";
        }
    }
}
