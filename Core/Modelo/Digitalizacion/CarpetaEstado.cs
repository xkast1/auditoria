﻿using System;
namespace Browne.Core.Modelo.Digitalizacion
{
    public class CarpetaEstado
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int SiguienteEstadoAprobado { get; set; }
        public int SiguienteEstdoRechazado { get; set; }
        public bool Activo { get; set; }
        [Obsolete("No se deben definir formatos en el modelo!", true)]
        public string Color { get; set; } // No se debe definir formato en el modelo;
                                          // cambiar por alguna propiedad que exprese
                                          // el estado de la carpeta, y pueda ser tratada
                                          // como una clase CSS para formatear en el tema.
                                          // El nombre del estado en formato "machine-friendly"
                                          // es una buena opción para discriminar el estado
                                          // en el "front-end".
    }
}
