﻿using System;

namespace Browne.Core.Modelo.Digitalizacion
{
    public class Archivo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Version { get; set; }
        public string HashMD5 { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Tamanio { get; set; }
        public decimal TamanioSinFirmar { get; set; }
        public string HashMD5SinFirmar { get; set; }
        public string Datos { get; set; }
        public int? ReferenciaArchivoId { get; set; }
        public DocumentoDigitalizado Documento { get; set; }

        public Archivo()
        {
            this.Id = 0;
            this.Nombre = "";
            this.Version = 1;
            this.HashMD5 = "";
            this.Fecha = DateTime.Now;
            this.Tamanio = 0;
            this.TamanioSinFirmar = 0;
            this.HashMD5SinFirmar = "";
            this.Datos = "";

        }

    }
}
