﻿using System;

namespace Browne.Core.Modelo.Digitalizacion
{
    public class CarpetaSeguimiento
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Observacion { get; set; }
        public string CreacionUsuario { get; set; }
        public string CreacionIp { get; set; }
        public CarpetaEstado Estado { get; set; }

        public CarpetaSeguimiento()
        {
            this.Fecha = DateTime.Now;
            this.Observacion = "";
            this.CreacionUsuario = "";
            this.CreacionIp = "";
        }
        public CarpetaSeguimiento(int id, DateTime fecha, string observacion, string usuario, string ip)
        {
            this.Id = id;
            this.Fecha = fecha;
            this.Observacion = observacion;
            this.CreacionUsuario = usuario;
            this.CreacionIp = ip;
        }
    }
}
