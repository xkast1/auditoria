﻿using System;
using System.Collections.Generic;
using Browne.Core.Modelo.Transversales;
using Browne.Core.Modelo.Transversales.Consumidor;

namespace Browne.Core.Modelo.Digitalizacion
{
    /// <summary>
    /// Enumeración que permite conocer el estado del proceso
    /// en que se encuentra el usuario del sistema al utilizar
    /// el módulo de digitalización.
    /// </summary>
    public enum EstadosCarpeta
    {
        /// <summary>
        /// La carpeta aún no ha sido creada.
        /// No existe un estado anterior, y sólo
        /// puede pasar al estado EstadosCarpeta.Creada.
        /// </summary>
        Inexistente,

        /// <summary>
        /// La carpeta acaba de ser creada;
        /// su estado anterior sólo puede ser EstadosCarpeta.Inexistente.
        /// Una carpeta Creada puede pasar al estado EstadosCarpeta.Aprobada
        /// o EstadosCarpeta.Rechazada.
        /// </summary>
        Creada,

        /// <summary>
        /// La carpeta fue rechazada;
        /// su estado anterior sólo puede ser EstadosCarpeta.Creada.
        /// Una carpeta Rechazada sólo puede pasar al estado
        /// EstadosCarpeta.Aprobada.
        /// </summary>
        Rechazada,

        /// <summary>
        /// La carpeta ha sido aprobada;
        /// su estado anterior puede ser EstadosCarpeta.Creada o
        /// EstadosCarpeta.Rechazada.
        /// Una carpeta Aprobada sólo puede pasar al estado
        /// EstadosCarpeta.Firmada.
        /// </summary>
        Aprobada,

        /// <summary>
        /// La carpeta ha sido firmada;
        /// su estado anterior puede ser EstadosCarpeta.Creada o
        /// EstadosCarpeta.Rechazada.
        /// Una carpeta Firmada no puede cambiar de estado.
        /// </summary>
        Firmada
    }

    public class Carpeta
    {
        public int Id { get; set; }
        public Aduana AduanaSolicitante { get; set; }
        public DateTime Fecha { get; set; }
        public string NumeroDespacho { get; set; }
        public string Observacion { get; set; }
        public string Solicitante { get; set; }
        public string NumeroSolicitud { get; set; }
        public DateTime? FechaFirma { get; set; }
        public string Uri { get; set; }
        public string Zip { get; set; }
        public string ZipFirmado { get; set; }
        public string TokenAccesoTemporal { get; set; }
        public int CantidadDocumentos { get; set; }
        public int Tamanio { get; set; }
        public string HashMD5 { get; set; }
        public Funcionario FuncionarioSolicitante { get; set; }
        public string CreacionUsuario { get; set; }
        public string CreacionIp { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string ActualizacionUsuario { get; set; }
        public string ActualizacionIp { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public Agente Agente { get; set; }

        public Cliente Cliente { get; set; }
        public IEnumerable<CarpetaSeguimiento> LstSeguimiento { get; set; }
        public CarpetaSeguimiento SeguimientoActual { get; set; }

        public List<Archivo> LstArchivos { get; set; }
        //public IEnumerable<Archivo> LstArchivos { get; set; }
        //public IEnumerable<Archivo> Archivos { get; set; }

        public EstadosCarpeta Estado { get; set; }

        //public void AddArchivo(Archivo item)
        //{
        //    this.LstArchivos.Add(item);
        //}

        //public Carpeta()
        //{
        //    this.Id = 0;
        //    this.Fecha = DateTime.Now;
        //    this.NumeroDespacho = "";
        //    this.Observacion = "";
        //    this.Solicitante = "";
        //    this.NumeroSolicitud = "";
        //    this.Uri = "";
        //    this.Sobre = "";
        //    this.CantidadDocumentos = 0;
        //    this.Tamanio = 0;
        //    this.HashMD5 = "";
        //    this.CreacionUsuario = "";
        //    this.CreacionIp = "";
        //    this.ActualizacionUsuario = "";
        //    this.ActualizacionIp = "";
        //    this.LstArchivos = new List<Archivo>();
        //}
    }
}
