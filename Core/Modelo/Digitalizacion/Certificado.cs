﻿using System;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Modelo.Digitalizacion
{
    public class Certificado
    {
        public int Id { get; set; }
        public TipoCertificado TipoCertificado { get; set; }
        public Agente Agente { get; set; }
        public string Sujeto { get; set; }
        public DateTime FechaInicioVigencia { get; set; }
        public DateTime FechaTerminoVigencia { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string NombreAlternativo { get; set; }
        public byte[] Data { get; set; }
        public bool Activo { get; set; }
        public string CreacionUsuario { get; set; }
        public string CreacionIp { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string ActualizacionUsuario { get; set; }
        public string ActualizacionIp { get; set; }
        public DateTime ActualizacionFecha { get; set; }

        public Certificado()
        {
            this.Id = 0;
            this.NombreAlternativo = "";
            this.Activo = true;
            this.CreacionUsuario = "";
            this.CreacionIp = "";
            this.ActualizacionUsuario = "";
            this.ActualizacionIp = "";

        }
    }
}
