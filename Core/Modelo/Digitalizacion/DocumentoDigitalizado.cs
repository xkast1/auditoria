﻿using System;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Modelo.Digitalizacion
{
    public class DocumentoDigitalizado
    {
        public int Id { get; set; }
        public string Ruta { get; set; }
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public int Orden { get; set; }
        public DateTime FechaDigitalizacion { get; set; }
        public TipoDocumento TipoDocumento { get; set; }
        public Funcionario Funcionario { get; set; }

        /// <summary>
        /// Retorna la ruta del documento en formato compatible
        /// con la sintaxis genérica para URIs.
        /// </summary>
        public string RutaSanitizada
        {
            get
            {
                return string.IsNullOrWhiteSpace(Ruta) ? null : Ruta.Trim().Replace('\\', '/');
            }
        }

        public string Uri
        {
            get
            {
                return RutaSanitizada + Nombre;
            }
        }

        public string Url
        {
            get
            {
                return "http://" + Uri;
            }
        }

        public string UrlSegura
        {
            get
            {
                return "https://" + Uri;
            }
        }
    }
}
