﻿namespace Browne.Core.Modelo.Digitalizacion
{
    public class TipoCertificado
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public TipoCertificado()
        {
            this.Id = 0;
            this.Nombre = "";
        }

    }
}
