﻿using System;
using System.Collections.Generic;
using Browne.Core.Modelo.Contabilidad;

namespace Browne.Core.Modelo.Contabilidad
{
    public class CuentaCorrienteTalonario
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Serie { get; set; }
        public DateTime Fecha { get; set; }
        public string FolioDesde { get; set; }
        public string FolioHasta { get; set; }
        public int CantidadCheques { get; set; }
        public string Observacion { get; set; }
        public bool? Bloqueado { get; set; }
        public bool? Continuo { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
        public CuentaCorriente CuentaCorriente { get; set; }
        public IEnumerable<Cheque> Cheques { get; set; }

        public CuentaCorrienteTalonario()
        {
            this.Numero = string.Empty;
            this.Serie = string.Empty;
            this.Fecha = DateTime.Now;
            this.CantidadCheques = 0;
            this.FolioDesde = string.Empty;
            this.FolioHasta = string.Empty;
            this.Observacion = string.Empty;
            this.Continuo = false;
            this.Bloqueado = false;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
        }
    }
}
