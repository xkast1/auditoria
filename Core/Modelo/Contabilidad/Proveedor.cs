﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Modelo.Contabilidad
{
    public class Proveedor
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string RazonSocial { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Contacto { get; set; }
        public string Email { get; set; }
        public bool? Bloqueado { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
        public Comuna Comuna { get; set; }

        public Proveedor()
        {
            this.Rut = string.Empty;
            this.RazonSocial = string.Empty;
            this.Nombre = string.Empty;
            this.Direccion = string.Empty;
            this.Telefono = string.Empty;
            this.Contacto = string.Empty;
            this.Email = string.Empty;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
        }
    }
}
