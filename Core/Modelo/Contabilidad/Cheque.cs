﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Modelo.Contabilidad
{
    public class Cheque
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Monto { get; set; }
        public string Ordende { get; set; }
        public bool? Anulado { get; set; }
        public DateTime FechaCobro { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
        public CuentaCorrienteTalonario CuentaCorrienteTalonario { get; set; }

        public Cheque()
        {
            this.Numero = 0;
            this.Fecha = DateTime.Now;
            this.Monto = 0;
            this.Ordende = string.Empty;
            this.Anulado = false;
            this.FechaCobro = DateTime.Now;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
        }
    }
}
