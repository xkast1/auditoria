﻿using System;
using System.Collections.Generic;
using System.Linq;
using Browne.Core.Modelo.Auth;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Modelo.Contabilidad
{
    public class CuentaCorriente
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string NumeroLineaCredito { get; set; }
        public DateTime FechaApertura { get; set; }
        public decimal MontoLineaCredito { get; set; }
        public string Observacion { get; set; }
        public bool? Bloqueado { get; set; }
        public string Ejecutivo { get; set; }
        public string EmailEjecutivo { get; set; }
        public string TelefonoEjecutivo { get; set; }
        public string FormatoCheque { get; set; }
        public string CreacionUsuario { get; set; }
        public DateTime CreacionFecha { get; set; }
        public string CreacionIp { get; set; }
        public string ActualizacionUsuario { get; set; }
        public DateTime ActualizacionFecha { get; set; }
        public string ActualizacionIp { get; set; }
        public Banco Banco { get; set; }
        public Sucursal Sucursal { get; set; }
        public IEnumerable<CuentaCorrienteTalonario> TalonariosCheques { get; set; }

        public CuentaCorriente()
        {
            this.Id = 0;
            this.Numero = string.Empty;
            this.NumeroLineaCredito = string.Empty;
            this.FechaApertura = DateTime.Now;
            this.MontoLineaCredito = 0;
            this.Observacion = string.Empty;
            this.Bloqueado = false;
            this.EmailEjecutivo = string.Empty;
            this.Ejecutivo = string.Empty;
            this.TelefonoEjecutivo = string.Empty;
            this.FormatoCheque = string.Empty;
            this.CreacionFecha = DateTime.Now;
            this.ActualizacionFecha = DateTime.Now;
        }
    }
}
