﻿namespace Browne.Core.Servicio.Navegacion
{
    public interface IServicioPagina : IServicioCrud<Modelo.Navegacion.Pagina>
    {
        Modelo.Navegacion.Pagina ObtenerPorRuta(string ruta);
        void Guardar(Modelo.Navegacion.Pagina pagina);
    }
}
