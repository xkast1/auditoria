﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Navegacion
{
    public interface IServicioElemento : IServicioCrud<Core.Modelo.Navegacion.Elemento>
    {
        Modelo.Navegacion.Elemento Guardar(Modelo.Navegacion.Elemento elemento);
        IEnumerable<Modelo.Navegacion.Elemento> ObtenerTodo(int paginaID);
        IEnumerable<Core.Modelo.Navegacion.Elemento> ObtenerElementosPagina(int? paginaID = null, int? rolId = null, int? elementoID = null, bool? bloqueado = null, bool? invisible = null);
        new IEnumerable<Modelo.Navegacion.Elemento> ObtenerTodo();
    }
}
