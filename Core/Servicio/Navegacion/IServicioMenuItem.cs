﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Navegacion
{
    public interface IServicioMenuItem : IServicioCrud<Modelo.Navegacion.MenuItem>
    {
        void Guardar(Core.Modelo.Navegacion.MenuItem menuItem);
        IEnumerable<Core.Modelo.Navegacion.MenuItem> ObtenerTodos(int? menuId = null, int? rolId = null);
    }
}
