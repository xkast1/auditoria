﻿namespace Browne.Core.Servicio.Navegacion
{
    public interface IServicioMenu : IServicioCrud<Modelo.Navegacion.Menu>
    {
        Modelo.Navegacion.Menu ObtenerPorRolId(int rolId);

        Modelo.Navegacion.Menu ObtenerPorRol(Modelo.Auth.Rol rol);

        System.Collections.Generic.IEnumerable<Modelo.Navegacion.Menu> ObtenerTodos();

        System.Collections.Generic.IEnumerable<Modelo.Navegacion.Menu> ObtenerTodosPorRol(Modelo.Auth.Rol rol);

        System.Collections.Generic.IEnumerable<Modelo.Navegacion.Menu> ObtenerTodosPorRolId(int rolId);
    }
}
