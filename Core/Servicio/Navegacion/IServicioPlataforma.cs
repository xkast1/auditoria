﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Navegacion
{
    public interface IServicioPlataforma : IServicioCrud<Core.Modelo.Navegacion.Plataforma>
    {
        Modelo.Navegacion.Plataforma Guardar(Core.Modelo.Navegacion.Plataforma plataforma);
        Modelo.Navegacion.Plataforma Obtener(string codigo);
    }
}
