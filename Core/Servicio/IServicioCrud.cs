﻿namespace Browne.Core.Servicio
{
    public interface IServicioCrud<T> where T : new()
    {
        int Crear(T item);

        void Guardar();

        void Eliminar(int id);

        T Obtener(int id);

        // TODO definir este comportamiento para todos los objetos!!!
        //T Obtener(T t);

        System.Collections.Generic.IEnumerable<T> ObtenerTodo();

        System.Collections.Generic.IEnumerable<T> Buscar(System.Linq.Expressions.Expression<System.Func<T, bool>> func);

        void EliminarPorLotes(int[] ids);
    }
}
