﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Exportaciones.Operacion;
using Browne.Core.Modelo.Transversales.Otros;

namespace Browne.Core.Servicio.Exportaciones.Sicex.Ruce
{
    public interface IServicioSicexRuce : IServicioCrud<Modelo.Exportaciones.Operacion.OperacionExportacion>
    {
        IEnumerable<OperacionExportacion> ObtenerTodo(string numero, string rutCliente, bool? sicex = null, DateTime? desde = null, DateTime? hasta = null);
        List<AcuseRecibo> EnviarOperaciones(IEnumerable<OperacionExportacion> operacionesExportacion, string accion);
    }
}
