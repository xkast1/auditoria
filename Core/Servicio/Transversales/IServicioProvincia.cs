﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioProvincia : IServicioCrud<Core.Modelo.Transversales.Provincia>
    {
        void Guardar(Browne.Core.Modelo.Transversales.Provincia provincia);
    }
}
