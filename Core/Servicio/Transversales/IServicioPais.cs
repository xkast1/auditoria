﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioPais : IServicioCrud<Core.Modelo.Transversales.Pais>
    {
        void Guardar(Browne.Core.Modelo.Transversales.Pais pais);
    }
}
