﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioFormaPagoGravamen : IServicioCrud<Core.Modelo.Transversales.FormaPagoGravamen>
    {
        void Guardar(Core.Modelo.Transversales.FormaPagoGravamen forma);
    }
}
