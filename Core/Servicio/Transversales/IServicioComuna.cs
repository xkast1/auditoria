﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioComuna : IServicioCrud<Core.Modelo.Transversales.Comuna>
    {
        void Guardar(Core.Modelo.Transversales.Comuna comuna);
    }
}
