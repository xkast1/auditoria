﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioTipoContenedor : IServicioCrud<Core.Modelo.Transversales.TipoContenedor>
    {
        void Guardar(Browne.Core.Modelo.Transversales.TipoContenedor contenedor);
    }
}
