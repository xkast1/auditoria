﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioMoneda : IServicioCrud<Core.Modelo.Transversales.Moneda>
    {
        void Guardar(Browne.Core.Modelo.Transversales.Moneda moneda);
    }
}
