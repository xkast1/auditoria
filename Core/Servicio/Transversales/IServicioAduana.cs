﻿using System.Collections.Generic;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioAduana : IServicioCrud<Modelo.Transversales.Aduana>
    {
        Modelo.Transversales.Aduana Obtener(string codigoAduana, int? id = null);
        IEnumerable<Aduana> ObtenerTodo(int? viaID = null, int? grupoTrabajoID = null, bool? bloqueada = false);
        IEnumerable<ViaTransporte> ObtenerViasTransporte(Aduana aduana);
        void Guardar(Aduana aduana);
    }
}
