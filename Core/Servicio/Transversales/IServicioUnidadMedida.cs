﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioUnidadMedida : IServicioCrud<Core.Modelo.Transversales.UnidadMedida>
    {
        void Guardar(Browne.Core.Modelo.Transversales.UnidadMedida unidad);
    }
}
