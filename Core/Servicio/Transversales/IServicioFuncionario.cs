﻿namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioFuncionario : IServicioCrud<Modelo.Transversales.Funcionario>
    {
        System.Collections.Generic.IEnumerable<string> ObtenerRutsPorPatron(string patron);

        Modelo.Transversales.Funcionario ObtenerPorRut(string rut);
    }
}
