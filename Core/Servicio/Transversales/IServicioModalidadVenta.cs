﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioModalidadVenta : IServicioCrud<Core.Modelo.Transversales.ModalidadVenta>
    {
        void Guardar(Browne.Core.Modelo.Transversales.ModalidadVenta modalidad);
    }
}
