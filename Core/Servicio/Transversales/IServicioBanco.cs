﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioBanco : IServicioCrud<Core.Modelo.Transversales.Banco>
    {
        void Guardar(Banco banco);
    }
}
