﻿namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioSucursal : IServicioCrud<Modelo.Transversales.Sucursal>
    {
        void Guardar(Core.Modelo.Transversales.Sucursal sucursal);
    }
}
