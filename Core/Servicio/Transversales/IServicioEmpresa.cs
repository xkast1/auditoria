﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioEmpresa : IServicioCrud<Modelo.Transversales.Empresa>
    {
        void Guardar(Modelo.Transversales.Empresa empresa);
    }
}
