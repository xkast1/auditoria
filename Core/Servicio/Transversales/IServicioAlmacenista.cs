﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioAlmacenista : IServicioCrud<Core.Modelo.Transversales.Almacenista>
    {
        void Guardar(Almacenista almacenista);
    }
}
