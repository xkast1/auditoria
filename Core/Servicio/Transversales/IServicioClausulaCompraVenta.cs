﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioClausulaCompraVenta : IServicioCrud<Core.Modelo.Transversales.ClausulaCompraVenta>
    {
        void Guardar(Core.Modelo.Transversales.ClausulaCompraVenta clausula);
    }
}
