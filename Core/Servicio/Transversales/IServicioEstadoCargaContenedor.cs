﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioEstadoCargaContenedor : IServicioCrud<Core.Modelo.Transversales.EstadoCargaContenedor>
    {
        void Guardar(Core.Modelo.Transversales.EstadoCargaContenedor contenedor);
    }
}
