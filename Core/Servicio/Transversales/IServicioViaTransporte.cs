﻿using System.Collections.Generic;
using Browne.Core.Modelo.Transversales;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioViaTransporte : IServicioCrud<ViaTransporte>
    {
        void Guardar(Browne.Core.Modelo.Transversales.ViaTransporte via);
    }
}
