﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioTipoOperacion : IServicioCrud<Core.Modelo.Transversales.TipoOperacion>
    {
        void Guardar(Browne.Core.Modelo.Transversales.TipoOperacion operacion);
    }
}
