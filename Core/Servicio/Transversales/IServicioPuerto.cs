﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioPuerto : IServicioCrud<Core.Modelo.Transversales.Puerto>
    {
        void Guardar(Browne.Core.Modelo.Transversales.Puerto puerto);
    }
}
