﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioVistoBueno : IServicioCrud<Core.Modelo.Transversales.VistoBueno>
    {
        void Guardar(Browne.Core.Modelo.Transversales.VistoBueno visto);
    }
}
