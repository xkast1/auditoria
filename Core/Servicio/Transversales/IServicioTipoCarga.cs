﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioTipoCarga : IServicioCrud<Core.Modelo.Transversales.TipoCarga>
    {
        void Guardar(Browne.Core.Modelo.Transversales.TipoCarga carga);
        Modelo.Transversales.TipoCarga Obtener(string codigo, int? id = null);
    }
}
