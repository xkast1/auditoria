﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioRegion : IServicioCrud<Core.Modelo.Transversales.Region>
    {
        void Guardar(Browne.Core.Modelo.Transversales.Region region);
    }
}
