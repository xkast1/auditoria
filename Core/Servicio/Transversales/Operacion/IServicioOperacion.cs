﻿using Browne.Core.Modelo.Transversales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales.Operacion
{
    public interface IServicioOperacion : IServicioCrud<Modelo.Transversales.Operacion.Operacion>
    {
        IEnumerable<Modelo.Transversales.Operacion.Operacion> ObtenerTodo(string tipo, int estado);

        Modelo.Transversales.Operacion.Operacion Obtener(string tipo, string estado, int correlativo);

        IEnumerable<ResultadoAutocompletar> Buscar(string patron);

        Modelo.Transversales.Operacion.Operacion Obtener(string numero);
    }
}
