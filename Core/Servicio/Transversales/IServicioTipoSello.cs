﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioTipoSello : IServicioCrud<Core.Modelo.Transversales.TipoSello>
    {
        void Guardar(Browne.Core.Modelo.Transversales.TipoSello sello);
    }
}
