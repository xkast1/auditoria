﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Transversales
{
    public interface IServicioFormaPago : IServicioCrud<Browne.Core.Modelo.Transversales.FormaPago>
    {
        void Guardar(Core.Modelo.Transversales.FormaPago forma);
    }
}
