﻿namespace Browne.Core.Servicio.Consumidor
{
    public interface IServicioCliente : IServicioCrud<Modelo.Transversales.Consumidor.Cliente>
    {
        //System.Collections.Generic.IEnumerable<Modelo.Transversales.Consumidor.Cliente> Buscar(string patron);
        System.Collections.Generic.IEnumerable<Modelo.Transversales.ResultadoAutocompletar> Buscar(string patron);

        Modelo.Transversales.Consumidor.Cliente Obtener(string rut);

        Modelo.Transversales.Consumidor.Cliente Obtener(int? id = null, bool? bloqueado = null, int? numeroSucursal = null, string rut = null);
    }
}
