﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Auth
{
    public interface IServicioArea : IServicioCrud<Core.Modelo.Auth.Area>
    {
        Modelo.Auth.Area Guardar(Modelo.Auth.Area area);
    }
}
