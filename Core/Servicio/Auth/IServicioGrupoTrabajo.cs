﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Auth
{
    public interface IServicioGrupoTrabajo : IServicioCrud<Core.Modelo.Auth.GrupoTrabajo>
    {
        Modelo.Auth.GrupoTrabajo Guardar(Modelo.Auth.GrupoTrabajo Grupotrabajo);
    }
}
