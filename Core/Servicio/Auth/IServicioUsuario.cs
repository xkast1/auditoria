﻿namespace Browne.Core.Servicio.Auth
{
    public interface IServicioUsuario : IServicioCrud<Modelo.Auth.Usuario>
    {
        Modelo.Auth.Usuario ObtenerPorToken(string token);
        void Guardar(Modelo.Auth.Usuario usuario);
        Modelo.Auth.Usuario Obtener(string identificador);
        void ActualizarContraseña(Modelo.Auth.Usuario usuario);
        void SubirAvatar(Modelo.Auth.Usuario usuario);
    }
}
