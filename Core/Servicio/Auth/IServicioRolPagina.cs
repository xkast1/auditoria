﻿using Browne.Core.Modelo.Auth;
using Browne.Core.Modelo.Navegacion;

namespace Browne.Core.Servicio.Auth
{
    public interface IServicioRolPagina
    {
        bool ExisteAutorizacion(Rol rol, Pagina pagina);
    }
}
