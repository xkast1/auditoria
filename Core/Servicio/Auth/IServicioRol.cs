﻿namespace Browne.Core.Servicio.Auth
{
    public interface IServicioRol : IServicioCrud<Modelo.Auth.Rol>
    {
        Modelo.Auth.Rol ObtenerPorUsuario(Modelo.Auth.Usuario usuario);
        Modelo.Auth.Rol Guardar(Modelo.Auth.Rol rol);
    }
}
