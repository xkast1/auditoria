﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Auth
{
    public interface IServicioTipoUsuario : IServicioCrud<Core.Modelo.Auth.TipoUsuario>
    {
        Modelo.Auth.TipoUsuario Guardar(Core.Modelo.Auth.TipoUsuario tipoUsuario);
    }
}
