﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Contabilidad
{
    public interface IServicioCentroCosto : IServicioCrud<Modelo.Contabilidad.CentroCosto>
    {
        void Guardar(IEnumerable<Modelo.Contabilidad.CentroCosto> centroCosto);
        IEnumerable<Modelo.Contabilidad.CentroCosto> ObtenerTodo(int? ID = null, bool? bloqueado = null, int? padreID = null, int? nivel = null);
    }
}
