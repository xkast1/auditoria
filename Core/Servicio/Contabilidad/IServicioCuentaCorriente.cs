﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Contabilidad
{
    public interface IServicioCuentaCorriente : IServicioCrud<Modelo.Contabilidad.CuentaCorriente>
    {
        void Guardar(Modelo.Contabilidad.CuentaCorriente CuentaCorriente);
    }
}
