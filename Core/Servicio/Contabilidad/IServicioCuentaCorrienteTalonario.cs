﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Contabilidad
{
    public interface IServicioCuentaCorrienteTalonario : IServicioCrud<Modelo.Contabilidad.CuentaCorrienteTalonario>
    {
        void Guardar(Modelo.Contabilidad.CuentaCorrienteTalonario TalonarioCheques);
    }
}
