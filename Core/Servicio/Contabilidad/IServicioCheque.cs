﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Contabilidad
{
    public interface IServicioCheque : IServicioCrud<Core.Modelo.Contabilidad.Cheque>
    {
        void Guardar(Modelo.Contabilidad.Cheque cheque);
    }
}
