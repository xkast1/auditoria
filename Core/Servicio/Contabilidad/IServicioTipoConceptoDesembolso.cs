﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Contabilidad
{
    public interface IServicioTipoConceptoDesembolso : IServicioCrud<Modelo.Contabilidad.TipoConceptoDesembolso>
    {
        void Guardar(Modelo.Contabilidad.TipoConceptoDesembolso concepto);
    }
}
