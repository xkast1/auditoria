﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Contabilidad
{
    public interface IServicioProveedor : IServicioCrud<Modelo.Contabilidad.Proveedor>
    {
        void Guardar(Modelo.Contabilidad.Proveedor proveedor);
    }
}
