﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Contabilidad
{
    public interface IServicioTipoOrdenCompra : IServicioCrud<Modelo.Contabilidad.TipoOrdenCompra>
    {
        void Guardar(Modelo.Contabilidad.TipoOrdenCompra tipoOrdenCompra);
    }
}
