﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Sesion
{
    public interface IServicioLog : IServicioCrud<Core.Modelo.Auth.LogsAccesos>
    {
        IEnumerable<Modelo.Auth.LogsAccesos> Obtener(DateTime? desde = null, DateTime? hasta = null, int? id = null);
        IEnumerable<Modelo.Auth.LogsAccesos> ObtenerTodo(int? usuarioID = null, DateTime? desde = null, DateTime? hasta = null);
    }
}
