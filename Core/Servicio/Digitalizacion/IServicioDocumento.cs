﻿namespace Browne.Core.Servicio.Digitalizacion
{
    public interface IServicioDocumento : IServicioCrud<Core.Modelo.Digitalizacion.DocumentoDigitalizado>
    {
        System.Collections.Generic.IEnumerable<Modelo.Digitalizacion.DocumentoDigitalizado> ObtenerDocumentosPorNumeroDespacho(string numero);
    }
}
