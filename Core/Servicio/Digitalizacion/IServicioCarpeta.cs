﻿using Browne.Core.Modelo.Digitalizacion;
using System;
using System.Collections.Generic;

namespace Browne.Core.Servicio.Digitalizacion
{
    public interface IServicioCarpeta : IServicioCrud<Carpeta>
    {
        Carpeta Obtener(Carpeta carpeta, bool cargarDetalle);

        IEnumerable<Carpeta> ObtenerTodasPorRangoDeFechas(DateTime desde, DateTime hasta);

        IEnumerable<Carpeta> ObtenerTodas(DateTime? desde = null, DateTime? hasta = null);

        IEnumerable<Carpeta> ObtenerUltimoMes();

        void Generar(Carpeta carpeta);

        void Rechazar(Carpeta carpeta);

        void Aceptar(Carpeta carpeta);

        IEnumerable<Certificado> ObtenerCertificados(bool activo = true);

        void Firmar(Carpeta carpeta, Certificado cert);

        byte[] GenerarZip(Carpeta carpeta);

        byte[] GenerarZipConFirma(Carpeta carpeta);
    }
}
