﻿using Browne.Core.Modelo.Digitalizacion;

namespace Browne.Core.Servicio.Digitalizacion
{
    public interface IServicioCertificado : IServicioCrud<Certificado>
    {
        System.Collections.Generic.IEnumerable<Certificado> ObtenerTodo(bool activo = true);

        Certificado Obtener(Certificado c);
    }
}
