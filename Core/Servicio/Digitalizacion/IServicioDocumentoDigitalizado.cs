﻿namespace Browne.Core.Servicio.Digitalizacion
{
    public interface IServicioDocumentoDigitalizado : IServicioCrud<Modelo.Digitalizacion.DocumentoDigitalizado>
    {
        System.Collections.Generic.IEnumerable<Modelo.Digitalizacion.DocumentoDigitalizado> ObtenerDocumentosPorNumeroDespacho(string numero);
    }
}
