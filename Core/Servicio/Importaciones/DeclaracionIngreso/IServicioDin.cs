﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Importaciones.DeclaracionIngreso
{
    public interface IServicioDin : IServicioCrud<Core.Modelo.Importaciones.DeclaracionIngreso.Din>
    {
        void CargarDin(IEnumerable<Modelo.Transversales.Operacion.Operacion> Operaciones);
        IEnumerable<Modelo.Importaciones.DeclaracionIngreso.Din> ObtenerTodo(bool? aceptada = null, string numeroDespacho = null, int? declaracionIngreso_ID = null, DateTime? fechaDesde = null, DateTime? fechaHasta = null);
    }
}
