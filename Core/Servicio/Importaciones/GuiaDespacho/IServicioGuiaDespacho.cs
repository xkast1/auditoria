﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Browne.Core.Modelo.Importaciones.GuiaDespacho;

namespace Browne.Core.Servicio.Importaciones.GuiaDespacho
{
    public interface IServicioGuiaDespacho : IServicioCrud<Modelo.Importaciones.GuiaDespacho.GuiaDespacho>
    {
        void Guardar(Modelo.Importaciones.GuiaDespacho.GuiaDespacho guia);
        Core.Modelo.Importaciones.GuiaDespacho.GuiaDespacho Obtener(string patente, int? id = null);
    }
}
