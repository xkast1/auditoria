﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browne.Core.Servicio.Importaciones.GuiaDespacho
{
    public interface IServicioGuiaDespachoDetalle : IServicioCrud<Modelo.Importaciones.GuiaDespacho.GuiaDespachoDetalle>
    {
    }
}
