﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Excel_Mvc_Demo1.Startup))]
namespace Excel_Mvc_Demo1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
