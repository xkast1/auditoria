﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Excel_Mvc_Demo1.Models
{
    public class Vehiculo
    {

        public string Rut { get; set; }
        public int NumSucursa { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoceCurrNameCode { get; set; }
        public int CusdecId { get; set; }
        public Decimal NetValue { get; set; }
        public string Zdescription { get; set; }
        public int LineId { get; set; }
        public int PakageQty { get; set; }
        public string NumeroVin { get; set; }
        public string Modelo { get; set; }
        public string NumeroMotor { get; set; }

        public decimal TotalFactura { get; set; }
        public int CantidadDetalle { get; set; }

        public string KeyNumber { get; set; }

        public int ModelYear { get; set; }

        public string ColorNombre { get; set; }
        public int CantidadModelos { get; set; }

        public string Codigosap { get; set; }
        public string SeudonimoSap { get; set; }

        public string Tramite { get; set; }
        public string ChasisNumero { get; set; }
        public string ClausulaCompraVenta { get; set; }

        public static implicit operator List<object>(Vehiculo v)
        {
            throw new NotImplementedException();
        }
    }
}