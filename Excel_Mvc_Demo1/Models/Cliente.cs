﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Excel_Mvc_Demo1.Models
{
    public class Cliente
    {
        
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Seleccione un rut")]
        public string Rut { get; set; }

        public int Sucursal { get; set; }

    }
}