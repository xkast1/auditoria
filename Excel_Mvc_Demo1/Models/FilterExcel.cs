﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Excel_Mvc_Demo1.Models
{
    public static  class FilterExcel
    {
        public static DataTable Headers(this ExcelPackage package, int firstRow)
        {
            ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
          
            var emptyColumns = new List<int>();

            DataTable dt = new DataTable();
            var columns = workSheet.CellValues(firstRow);
            for (int i = 0; i < columns.Count; i++)
            {
                if (!string.IsNullOrWhiteSpace(columns[i])) 
                {
                    dt.Columns.Add(columns[i]);
                }
            }
           
            return dt;

        }

        public static DataTable FullDataExcel(this ExcelPackage package, int firtsRow)
        {

            ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
            var emptyColumns = new List<int>();

            DataTable dt = new DataTable();




            var columns = workSheet.CellValues(firtsRow);

            for (int i = 0; i < columns.Count; i++)
            {
                if (!string.IsNullOrWhiteSpace(columns[i]))
                {
                    dt.Columns.Add(columns[i]);
                }
                else
                {

                    emptyColumns.Add(i + 1);

                }
            }
           


            for (var RowNumber = firtsRow + 1; RowNumber <= workSheet.Dimension.End.Row; RowNumber++)
            {
                var newRow = dt.NewRow();
                var cellValues = workSheet.CellsValuesNotEmpty(RowNumber, emptyColumns);
                for (int i = 0; i < workSheet.Dimension.End.Row; i++)
                {

                }

                for (int i = 0; i < cellValues.Count; i++)
                {
                    if (cellValues[i] == null || cellValues[i] == string.Empty || cellValues[i] == "")
                    {
                        newRow[i] = 1;
                    }
                }

                
                
               

                

                //for (int i = 0; i < cellValues.Count; i++)
                //{
                //    if (workSheet.Cells == null)
                //    {
                //        newRow[i] = 1;
                //    }
                //}

                for (var i = 0; i < cellValues.Count ; i++)
                {
                    
                        newRow[i] = cellValues[i];
                }
                dt.Rows.Add(newRow);
               

            }
            return dt;
        }
       
        public static DataTable StripEmptyRows(this DataTable dt)
        {
            List<int> rowIndexesToBeDeleted = new List<int>();
            int indexCount = 0;
            foreach (var row in dt.Rows)
            {
                var r = (DataRow)row;
                int emptyCount = 0;
                int itemArrayCount = r.ItemArray.Length;
                foreach (var i in r.ItemArray) if (string.IsNullOrWhiteSpace(i.ToString())) emptyCount++;

                if (emptyCount == itemArrayCount) rowIndexesToBeDeleted.Add(indexCount);

                indexCount++;
            }

            int count = 0;
            foreach (var i in rowIndexesToBeDeleted)
            {
                dt.Rows.RemoveAt(i - count);
                count++;
            }

            return dt;
        }

        public static List<string> CellValues(this ExcelWorksheet workSheet, int row)
        {
            int firstColum = workSheet.Dimension.Start.Column;
           // int firstColum2 = workSheet.Cells.Where(cell => !cell.Value.ToString().Equals("")).Last().Start.Column;
            //int lastRow = workSheet.Cells.Where(cell => !cell.Value.ToString().Equals("")).Last().End.Row;
            int lastColumn = workSheet.Dimension.End.Row;
            return workSheet.CellValuesRange(row, firstColum, lastColumn);
        }

        private static List<string> CellValuesRange(this ExcelWorksheet workSheet, int row, int columnFrom, int columnTo)
        {
            return workSheet
                .Cells[row, columnFrom, row, columnTo]
                .Select(rowValue => (string.IsNullOrWhiteSpace(rowValue.Text) ||  string.IsNullOrWhiteSpace(rowValue.Value.ToString()) ? "100" : rowValue.Value.ToString())) 
                .ToList();
        }
        private static List<string> CellsValuesNotEmpty(this ExcelWorksheet workSheet, int row, List<int> emptyColumns)
        {
            var workSheetValues = new List<string>();
            int previusLastRow = workSheet.Dimension.Start.Column;
            int lastColumn = workSheet.Dimension.End.Column;
            emptyColumns.ForEach(emptyColumn =>
            {
                workSheetValues.AddRange(workSheet.CellValuesRange(row, previusLastRow, emptyColumn -1));
                previusLastRow = emptyColumn + 1;
            });
            workSheetValues.AddRange(workSheet.CellValuesRange(row, previusLastRow, lastColumn));
            return workSheetValues;
        }
    }
}