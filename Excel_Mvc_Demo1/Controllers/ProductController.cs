﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Office.Interop.Excel;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using Excel_Mvc_Demo1.ViewModels;
using Excel;
using Excel_Mvc_Demo1.Models;
using System.Text;
using System.Collections;
using System.Data.Entity;
using Excel_Mvc_Demo1.Metodos;
using System.Dynamic;
using OfficeOpenXml;
using Browne.Intranet.Servicios.Transversales;

namespace Excel_Mvc_Demo1.Controllers
{
    public class ProductController : Controller
    {
        static System.Data.DataTable dt = new System.Data.DataTable();
        static List<Vehiculo> vehicuulos = new List<Vehiculo>();
        System.Data.DataSet result = new System.Data.DataSet();
        static System.Data.DataTable result2 = new System.Data.DataTable();
        System.Data.DataTable tableWithOnlySelectedColumns = new System.Data.DataTable();
        //string[] ColumnsName;
        //static string FileName;
        //Stream fileExcel;


        // GET: Product
        public ActionResult Index()
        {
            return View("Index", new FormdataViewModels());
        }

        //[HttpPost]
        //public JsonResult Buscar(string patron)
        //{
        //    try
        //    {
        //        return Json(new
        //        {
        //            Result = "Ok",
        //            Data = Metodos.CargadorExcel.Buscar(patron),
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(new
        //        {
        //            Result = "Fail",
        //            Message = e.Message
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public JsonResult Obtener(Cliente cliente)
        //{
        //    try
        //    {
        //        return Json(new
        //        {
        //            Result = "Ok",
        //            Data = Metodos.CargadorExcel.Obtener(cliente.Rut, cliente.Sucursal),
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(new
        //        {
        //            Result = "Fail",
        //            Message = e.Message
        //        }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        [HttpPost]
       
        public ActionResult Import(HttpPostedFileBase uploadfile, FormCollection c)
        {
            string mesaje = "";
            // ViewBag.message["mensaje"] = "debe seleccionar un archivo";
            //mesaje = ViewBag.message["mensaje"];
           
            string uploadDirectoryPath = Server.MapPath("~/ExcelTemporal");
            if (uploadfile != null)
            {
                TempData["formulario"] = c;
                var numberHead = c["fila"];

                TempData["ruttt"] = c["rut"].ToString();
                TempData["rsocial"] = c["rsocial"];
                TempData["sucursal"] = c["sucursal"];

                if (Request.HttpMethod == "POST")
                {
                    Session["rut_cliente"] = TempData["ruttt"].ToString();
                }

                int firsRow;
                if (!int.TryParse(c["fila"], out firsRow))
                {
                    firsRow = 1;
                }
                TempData["fila"] = firsRow;
                ExcelPackage package = new ExcelPackage(uploadfile.InputStream);
                System.Data.DataTable Dt = FilterExcel.Headers(package, firsRow);

                
                string fileName = Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".xls";
                string filepath = Path.Combine(uploadDirectoryPath, fileName);

                try
                {
                    if (!Directory.Exists(uploadDirectoryPath))
                    {
                        Directory.CreateDirectory(uploadDirectoryPath);
                    }

                    if (uploadfile.ContentLength > 0)
                    {
                        ViewModels.ViewModelListaVehiculos vh = new ViewModels.ViewModelListaVehiculos();
                        uploadfile.SaveAs(filepath);

                        vh.FilePath = filepath;

                        //ExcelDataReader works on binary excel file
                        Stream stream = uploadfile.InputStream;
                        //We need to written the Interface.
                        IExcelDataReader reader = null;
                        if (uploadfile.FileName.EndsWith(".xls"))
                        {
                            //reads the excel file with .xls extension
                            reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        }
                        else if (uploadfile.FileName.EndsWith(".xlsx"))
                        {
                            //reads excel file with .xlsx extension
                            reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        }
                        else
                        {
                            //Shows error if uploaded file is not Excel file
                            ModelState.AddModelError("File", "This file format is not supported");
                            return View();
                        }
                        //treats the first row of excel file as Coluymn Names
                        reader.IsFirstRowAsColumnNames = true;
                        //Adding reader data to DataSet()
                        result = reader.AsDataSet();

                        reader.Close();

                        string num = Convert.ToString(c["sucursal"]);
                        TempData["rsocial"] = c["rsocial"];
                        TempData["FileName"] = fileName;
                        TempData["Product"] = vh;
                        TempData["Result"] = result;
                       
                        TempData["sucursal"] = num;
                        TempData["Dt"] = Dt;

                        // return RedirectToAction("Mostar");
                    }
                }
                catch (Exception ex)
                {

                }
                return new RedirectResult("UploadFile", true);
            }

            return View("Index", new FormdataViewModels());

        }

        //[HttpGet]
        public ActionResult UploadFile()
        {
            var modeloExcel = TempData["result"] as System.Data.DataSet;
            var NewModel = TempData["Dt"] as System.Data.DataTable;
            var result = new System.Data.DataSet();
            //ViewBag.Monedas = CargadorExcel.GetMoney();

            ViewBag.FileName = TempData["FileName"];
            //ServicioMoneda svcMoneda = new ServicioMoneda(ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString);
            //ServicioClausulaCompraVenta svcComprayVenta = new ServicioClausulaCompraVenta(ConfigurationManager.ConnectionStrings["browne_comunes"].ConnectionString);
            var ViewModel = new FormdataViewModels
            {
                // Ds = modeloExcel,
                Dt = NewModel,
                //Currencies = CargadorExcel.GetMoney(),
                //CompraYVenta = svcComprayVenta.ObtenerTodo(),
                //Currencies = svcMoneda.ObtenerTodo(),
                FilePath = ViewBag.FileName
                //Rut = if TempData.Keys.Contains("ruttt") TempData["ruttt"],
                //RazonSocial = TempData["rsocial"].ToString()
            };
            
                ViewModel.Rut = TempData["ruttt"].ToString();
                        
                ViewModel.RazonSocial = TempData["rsocial"].ToString();
                        
                ViewModel.numSucursal = Convert.ToInt32(TempData["sucursal"]);
            
           

            return View("Index", ViewModel);
        }

        [HttpPost]
        public ActionResult Form()
        {
            return View();
        }
        public FileResult Download()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes("~/ ExcelTemporal");
            string fileName = TempData["fileName"].ToString();
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }



        [HttpPost]
        public ActionResult Mostrar(FormCollection m)
        //public ActionResult Mostrar()
        {

            Vehiculo v = new Vehiculo();

            var FileName = m["file_name"].ToString();
            TempData["fileName"] = FileName;
            //var file2 = Directory.GetFiles(Server.MapPath("~/ExcelTemporal"));
            string Foldername;
            Foldername = Server.MapPath("~/ExcelTemporal");


            // variables de entrada index ---------------Cabecera---------------------

            var rut = m["rut_hiden"];//  TempData["ruttt"];
            var numsucursal = Convert.ToInt32(m["sucursal_hiden"]);// Convert.ToInt32(TempData["sucursal"]);
            var totalFacturaInput = Convert.ToDecimal(m["txtTotal"].ToString());
            string totalFacturaSelect = m["total_factura_cabecera"];
            string NumeroFacturaInput = m["input_numero_factura"];
            string NumeroFacturaSelect = m["value_num_factura"];
            // var cantidadInput = Convert.ToInt32(m["txtcantidad"]);
          
            string moneda = m["moneda"];
            string clausulaCompraVenta = m["clausula"];
            string clausulaCompraVentaInput = m["txtclausula"];


            //------------------------------variables index Detalle FActura-----------------------------

            string codigoMercaderia = m["codigo_mercaderia"];
            string codigoMercaderiaInpuit = m["codigo_mercaderia_input"];
            string descripcionDetalle = m["descripcion_factura"];
            string valorUnitario = m["valor_unitario"];
            string cantidadSelect = m["cantidad_detalle_fact"];
            //-----------------------------sub detalle Factura values index------------------------


            var cantidadPackageQty = m["cantidad_sub_detalle"];
            var cantidadInputSubDetalle = Convert.ToInt32(m["txt_cantidad_sub"]);
            string descripcionSubDetalle = m["descripcion_sub_tetalle"];
            string marcaSubDetalle = m["marca_sub_detalle"];
            string keyNumber = m["modelo"];
            string color = m["color_sub_detalle"];
            string fechaAno = m["fecha_sub_detalle"];
            var fechaDetalle = Convert.ToDateTime(m["fechaCabecera"]);
            string numeroVin = m["vin_sub_detalle"];
            string numeroMotorSubDetalle = m["num_motor_sub_det"];
            string numeroChasisSubDetalle = m["num_chasis_sub_detalle"];
            string tipoInternacion = m["internacion_sub_detalle"];




            //recupero el archivo para leerlo como excel 
            var filename = this.HttpContext.Server.MapPath("~/ExcelTemporal/" + FileName);
            byte[] filecel = System.IO.File.ReadAllBytes(filename);

            //
            MemoryStream ms = new MemoryStream(filecel);


            int firtRow = Convert.ToInt32(TempData["fila"]);
            ExcelPackage package = new ExcelPackage(ms);
            System.Data.DataTable Dt = FilterExcel.FullDataExcel(package, firtRow);
            System.Data.DataTable Dt1 = FilterExcel.StripEmptyRows(Dt);

            string[] columnsName = Dt1.Columns.Cast<DataColumn>()
                                            .Select(x => x.ColumnName).ToArray();



            //var a = Dt1.AsEnumerable().Select(x => x.Field<string>(NumeroFacturaSelect)).ToList();
            //if (NumeroFacturaInput == "" || NumeroFacturaInput == null || NumeroFacturaInput == string.Empty)
            //{
            string[] ColumnsName = { NumeroFacturaSelect ,codigoMercaderia, cantidadSelect, descripcionDetalle, valorUnitario, marcaSubDetalle,
                color, fechaAno, numeroVin, numeroMotorSubDetalle, numeroChasisSubDetalle, tipoInternacion, keyNumber};


            string[] ColumnsNameFinal = ColumnsName.Where(x => x != "0").ToArray();
            System.Data.DataTable tableWithOnlySelectedColumns = new DataView(Dt1).ToTable(false, ColumnsNameFinal);
            System.Data.DataTable Dt3 = FilterExcel.StripEmptyRows(tableWithOnlySelectedColumns);
            if (totalFacturaSelect == "0")
            {
                Dt3.Columns.Add("Total_Factura", typeof(System.Decimal));
                foreach (DataRow row in Dt3.Rows)
                {
                    row["Total_Factura"] = totalFacturaInput;

                }
            }

            if (NumeroFacturaSelect == "0")
            {
                Dt3.Columns.Add("Numero_Factura", typeof(System.Decimal));
                foreach (DataRow row in Dt3.Rows)
                {
                    row["Numero_Factura"] = totalFacturaInput;

                }
            }

            //Dt3.Columns.Add("Moneda", typeof(System.String));
            //Dt3.Columns.Add("CompraVenta", typeof(System.String));
            Dt3.Columns.Add("Rut_cli", typeof(System.String));
            Dt3.Columns.Add("sucursal", typeof(System.Int32));
            Dt3.Columns.Add("Fecha_cabecera", typeof(System.DateTime));



            foreach (DataRow row in Dt3.Rows)
            {
                //row["Moneda"] = moneda;
                //row["CompraVenta"] = clausulaCompraVenta;
                row["Rut_cli"] = rut;
                row["sucursal"] = numsucursal;
                row["Fecha_cabecera"] = fechaDetalle;

            }

            System.Data.DataTable DtClone = Dt3.Copy();
            TempData["DtCopy"] = DtClone;


            if (Dt3.Columns.Count > 9 && Dt3.Columns.Count < 12)
            {
                IList<Vehiculo> Items1 = Dt3.AsEnumerable().Select(row =>
              new Vehiculo
              {
                  Rut = row.Field<string>("Rut_cli"),
                  InvoiceNumber = row.Field<string>(NumeroFacturaSelect),
                  NumSucursa = row.Field<int>("sucursal"),
                  InvoiceDate = row.Field<DateTime>("Fecha_cabecera"),
                  InvoceCurrNameCode =  "012" , //row.Field<string>("Moneda"),
                  NetValue = Convert.ToDecimal(row.Field<string>(valorUnitario).Replace(",",".")),
                  Zdescription = row.Field<string>(descripcionDetalle),
                  Codigosap = row.Field<string>(codigoMercaderia),
                  SeudonimoSap = row.Field<string>(descripcionDetalle),
                  CantidadDetalle = Convert.ToInt32(row.Field<string>(cantidadSelect)),
                  ClausulaCompraVenta = "12",// row.Field<string>("CompraVenta"),
                  TotalFactura = row.Field<decimal>("Total_Factura")



              }).ToList();

                var groupedCustomerList1 = Items1.GroupBy(x => x.InvoiceNumber)
                 .Select(g => g.First()).ToList();

                var GroupCantidadDataTable = from b in Dt3.AsEnumerable()
                                             group b by b.Field<string>(codigoMercaderia) 
                                             into g select new
                                                {
                                                package = g.Count()
                                                };

                var groupCantiad = Items1.GroupBy(x => x.Codigosap).Select(group =>
                                        new
                                        {
                                            package = group.Count()
                                        });

                TempData["groupByF1"] = groupedCustomerList1;
                TempData["Dt1"] = Items1;
            }
            else
            {
                IList<Vehiculo> Items = Dt3.AsEnumerable().Select(row =>
                  new Vehiculo
                  {
                      Rut = row.Field<string>("Rut_cli"),
                      InvoiceNumber = row.Field<string>(NumeroFacturaSelect),
                      NumSucursa = row.Field<int>("sucursal"),
                      InvoiceDate = row.Field<DateTime>("Fecha_cabecera"),
                      InvoceCurrNameCode = "12",// row.Field<string>("Moneda"),
                      NetValue = Convert.ToDecimal(row.Field<string>(valorUnitario)),
                      Zdescription = row.Field<string>(descripcionDetalle),
                      PakageQty = Convert.ToInt16(row.Field<string>(cantidadSelect)),
                      NumeroVin = row.Field<string>(numeroVin),
                      NumeroMotor = row.Field<string>(numeroMotorSubDetalle),
                      KeyNumber = row.Field<string>(keyNumber),
                      ModelYear = Convert.ToInt16(row.Field<string>(fechaAno)),
                      ColorNombre = row.Field<string>(color),
                      Codigosap = row.Field<string>(codigoMercaderia),
                      SeudonimoSap = row.Field<string>(descripcionDetalle),
                      Tramite = row.Field<string>(tipoInternacion),
                      ChasisNumero = row.Field<string>(numeroChasisSubDetalle)

                  }).ToList();

                var groupedCustomerList = Items.GroupBy(x => x.InvoiceNumber)
                  .Select(g => g.First()).ToList();

                TempData["groupByF2"] = groupedCustomerList;
                TempData["Dt2"] = Dt3;

            }
            

            return View("PreViewExcel", Dt3);
        }


        public ActionResult Import(FormCollection m)
        {
            return View();
        }


        public ActionResult InseertRegister()
        {
            //var DataTable = TempData["Dt2"] as System.Data.DataTable;
            var DtCopy = TempData["DtCopy"] as System.Data.DataTable;
            var GoupBy1 = TempData["groupByF1"];
            var GoupBy2 = TempData["groupByF2"];

             return View("Index");
        }

    }

    


}