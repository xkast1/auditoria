﻿//using Excel_Mvc_Demo1.Models;

using Browne.Core.Modelo.Transversales;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Excel_Mvc_Demo1.ViewModels
{
    public class FormdataViewModels
    {
        public DataSet Ds { get; set; }
        public DataTable Dt { get; set; }
        public string  FilePath { get; set; }
        public IEnumerable<Moneda> Currencies{ get; set; }
        public string  Rut { get; set; }

        public string  RazonSocial { get; set; }

        public int numSucursal { get; set; }

        public IEnumerable<ClausulaCompraVenta> CompraYVenta { get; set; }
                
    }
}