﻿using Excel_Mvc_Demo1.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Excel_Mvc_Demo1.Metodos
{
    public class CargadorExcel
    {
        public static string conexion()
        {
            return ConfigurationManager.ConnectionStrings["aduana_desarrollo"].ConnectionString;
        }

        public static Cliente Obtener(string rut, int sucursal)
        {
            using (var connection = new SqlConnection(conexion()))
            {
                using (var command = connection.CreateCommand())
                {
                    // Sanitiza cadena y prepara para búsqueda.
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_clientes_selecionar";
                    command.Parameters.AddWithValue("@cliente_Rut", rut);
                    command.Parameters.AddWithValue("@cliente_NumeroSucursal", sucursal);
                    connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        Cliente cliente = new Cliente();
                        cliente.Nombre = dr["Cliente_Nombre"].ToString();
                        cliente.Sucursal = Convert.ToInt32(dr["Cliente_NumeroSucursal"].ToString());
                        cliente.Rut = dr["Cliente_Rut"].ToString();

                        return cliente;
                    }
                }
            }
        }


        public static List<Moneda> GetMoney()
        {
            List<Moneda> Monedas = new List<Moneda>();
            using (var connection = new SqlConnection(conexion()))
            {
                using (var command = connection.CreateCommand())
                {
                    SqlParameter param;
                    // Sanitiza cadena y prepara para búsqueda.
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_PARAMETROS_lista";
                    command.Parameters.AddWithValue("@tapa_codtabla",41);
                    //param = new SqlParameter("@tapa_codtabla", 41);
                    //param.Direction = ParameterDirection.Input;
                    //param.DbType = DbType.Int16;
                    //command.Parameters.Add(param);
                    connection.Open();
                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (dr.Read())
                        {
                            Monedas.Add(new Moneda
                            {
                                Id = dr["PARA_CODPARAM"].ToString(),
                                Nombre = dr["PARA_DESPARAM"].ToString()
                            });
                        }
                    }
                }
                return Monedas;
            }
        }






        public static IEnumerable<Autocompletar> Buscar(string buscado)
        {
            using (var connection = new SqlConnection(conexion()))
            {
                using (var command = connection.CreateCommand())
                {
                    // Sanitiza cadena y prepara para búsqueda.
                    buscado = buscado.Trim() + "%";
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "prc_cliente_buscar";
                    command.Parameters.AddWithValue("@cliente_buscado", buscado);

                    connection.Open();

                    using (var dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (dr.Read())
                        {
                            yield return new Autocompletar
                            {
                                RazonS = dr["CLIE_RAZONSOCIA"].ToString(),
                                Id2 = dr["RUTCLIENTE"].ToString(),
                                NumSucursal = dr["NUMSUCURSAL"].ToString(),
                                Label = dr["RUTCLIENTE"].ToString() + " | " + dr["CLIE_RAZONSOCIA"].ToString() + " | " + dr["NUMSUCURSAL"].ToString()
                            };
                            
                        }
                    }
                }
            }
        }
    }
}