﻿using System.Web;
using System.Web.Mvc;

namespace Excel_Mvc_Demo1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
